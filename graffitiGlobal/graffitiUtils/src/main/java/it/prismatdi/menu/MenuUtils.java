package it.prismatdi.menu;

import it.prismatdi.general.Ruoli;

public enum MenuUtils {
	SINTESI("Sintesi", Ruoli.ALL, "sintesi"),
	MENU_USER("USERS", Ruoli.ALL, "no view"),
	ADD_USER("Aggiungi user", Ruoli.ADMINISTRATOR, "Aggiungi user"),
	MODIFICA_USER("Modifica user", Ruoli.USER, "Modifica user"),
	REMOVE_USER("Rimuovi user", Ruoli.ADMINISTRATOR, "Rimuovi user"),
	MENU_PRIVILEGI("PRIVILEGI", Ruoli.ADMINISTRATOR, "no view"),
	ADD_PRIVILEGIO("Nuovo", Ruoli.ADMINISTRATOR, "Aggiungi privilegio"),
	AGGIUNGI_PRIVILEGIO_A_USER("Add to user", Ruoli.ADMINISTRATOR, "addPrivUser"),
	REMOVE_PRIVILEGIO("Delete", Ruoli.ADMINISTRATOR, "deletePriv"),
	MENU_AGENTI("AGENTI", Ruoli.AGENTE, "no view"),
	CAMPIONARIO("Magazzino camp.", Ruoli.AGENTE, "campionarioAgente"),
	MENU_LOGOUT("LOGOUT", Ruoli.ALL, "no view"),
	LOGOUT("Esci", Ruoli.ALL, "Logout"),
	CAMBIA_PASSWD("Cambia passwd", Ruoli.ALL, "changePasswordView");

	public static String findComand(String descrizione) {
		if (descrizione == null) {
			return SINTESI.getViewName();
		}
		for (MenuUtils util : MenuUtils.values()) {
			if (util.getString()
					.toUpperCase()
					.equals(descrizione.toUpperCase())) {
				return util.getViewName();
			}
		}
		return SINTESI.getViewName();
	}
	private final String	string;
	private Ruoli			ruolo;
	private String			viewName;

	private MenuUtils(String string, Ruoli ruolo, String viewName) {
		this.string = string;
		this.ruolo = ruolo;
		this.viewName = viewName;
	}

	public String getPermission() {
		return ruolo.toString();
	}

	public String getString() {
		return string;
	}

	public String getViewName() {
		return viewName;
	}

}
