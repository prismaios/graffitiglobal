package it.prismatdi.privilegi;

public enum PrivilegioInputValidator {
	DESCRIZIONE("La descrizione deve essere min 1 carattere e max 256 caratteri"),
	MODULO("Il modulo deve essere min 1 carattere e max 256 caratteri");
	private final String string;

	private PrivilegioInputValidator(String string) {
		this.string = string;
	}

	public String getString() {
		return this.string;
	}

}
