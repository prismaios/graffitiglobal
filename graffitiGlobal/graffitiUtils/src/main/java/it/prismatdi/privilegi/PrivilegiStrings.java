package it.prismatdi.privilegi;

public enum PrivilegiStrings {
	ADD_PRIVILEGIO("Aggiungi/Mod"),
	SHOW_PRIVILEGI("Tutti"),
	DESCRIZIONE("Descrizione"),
	MODULO("Default view"),
	COD_RUOLO("Cod.Ruolo");

	private String string;

	private PrivilegiStrings(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
