package it.prismatdi.user;

public enum UserStringUtil {
	USER_TAB("User tab"),
	RUOLO_TAB("Ruolo tab"),
	SHOW_ALL_TAB("Show users"),
	LOGIN_NAME("Nome per login"),
	PASSWORD("Password"),
	NOME("Nome"),
	COGNOME("Cognome"),
	SAVE_BUTTON("Salva"),
	CLEAR_BUTTON("Reset"),
	COD_GRAFFITI("Cod.Graffiti"),
	COMBO_PROMT("Selezionare..."),
	COMBO_CAPTION("Utenti web"),
	RESET_PASSWD("Reset passwd");

	private final String string;

	private UserStringUtil(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}
}
