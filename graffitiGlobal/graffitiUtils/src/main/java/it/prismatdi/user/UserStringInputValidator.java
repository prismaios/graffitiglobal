package it.prismatdi.user;

public enum UserStringInputValidator {
	NOME_LOGIN("Il nickname deve essere min 1 carattere e max 30 caratteri"),
	PASSWORD_LOGIN("La password deve essere min 1 carattere e max 100 caratteri"),
	NOME("Il nome deve essere min 1 carattere e max 50 caratteri"),
	COGNOME("Il cognome deve essere min 1 carattere e max 50 caratteri"),
	COD_GRAFFITI("Deve esistere il codice di Graffiti (max 20 car)");

	private final String string;

	private UserStringInputValidator(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
