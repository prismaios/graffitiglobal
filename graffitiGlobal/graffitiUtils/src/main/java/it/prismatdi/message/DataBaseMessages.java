package it.prismatdi.message;

public enum DataBaseMessages {
	TITOLO_MESSAGGIO("Operazione database"),
	DESCRIZIONE_MESSAGGIO("Operazione riuscita"),
	CANCELLAZIONE_OK("Cancellazione riuscita");

	private String string;

	private DataBaseMessages(String string) {
		this.string = string;
	}

	public String getString() {
		return this.string;
	}

}
