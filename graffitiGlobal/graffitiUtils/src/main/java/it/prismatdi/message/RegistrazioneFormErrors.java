package it.prismatdi.message;

public enum RegistrazioneFormErrors {
	TITOLO("ERRORE"),
	ERRORE_PASSWORD("Le passwords devono essere uguali"),
	ERRORE_LOGIN_NAME("Il nome per il login non puo\' essere vuoto"),
	ERRORE_MAIL("L\'indirizzo mail è obbligatorio"),
	ERRORE_LOGIN_NAME_DUPLICATO("Il nome scelto è già presente - Si prega di cambiarlo");

	private String string;

	private RegistrazioneFormErrors(String string) {
		this.string = string;
	}

	public String getString() {
		return this.string;
	}

}
