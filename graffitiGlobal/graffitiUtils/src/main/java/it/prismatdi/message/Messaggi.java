package it.prismatdi.message;

public enum Messaggi {
	PASSWD_NON_UGUALI("Password NON uguali"),
	PASSWD_ERRATA("La password NON è valida");
	private String string;

	private Messaggi(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
