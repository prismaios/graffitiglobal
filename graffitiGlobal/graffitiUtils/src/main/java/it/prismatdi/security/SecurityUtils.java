package it.prismatdi.security;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import it.prismatdi.general.Ruoli;
import it.prismatdi.menu.MenuUtils;

/**
 * SecurityUtils takes care of all such static operations that have to do with security and querying rights from
 * different beans of the UI.
 *
 */
public class SecurityUtils {

	/**
	 * Gets the user object for the current user.
	 *
	 * @return the user object
	 */
	//	public static User getCurrentUser(UserService userService) {
	//		return userService.findByEmail(SecurityUtils.getUsername());
	//	}

	/**
	 * Gets the user name of the currently signed in user.
	 *
	 * @return the user name of the current user or <code>null</code> if the user has not signed in
	 */
	public static String getUsername() {
		SecurityContext context = SecurityContextHolder.getContext();
		UserDetails userDetails = (UserDetails) context.getAuthentication()
				.getPrincipal();
		return userDetails.getUsername();
	}

	/**
	 * Gets the roles the currently signed-in user belongs to.
	 *
	 * @return a set of all roles the currently signed-in user belongs to.
	 */
	public static Set<String> getUserRoles() {
		SecurityContext context = SecurityContextHolder.getContext();
		Set<String> ruoli = context.getAuthentication()
				.getAuthorities()
				.stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toSet());
		//		System.err.println(">>>>> SecurityUtils-getUserRoles <<<<<< " + ruoli);
		return ruoli;
	}

	public static boolean isCurrentUserInRole(MenuUtils menu) {
		if (menu.getPermission()
				.equals(Ruoli.ALL.toString())) {
			return true;
		}
		else {
			String role = menu.getPermission();
			return getUserRoles().stream()
					.filter(roleName -> roleName.equals(Objects.requireNonNull(role)))
					.findAny()
					.isPresent();
		}
	}

	/**
	 * Check if currently signed-in user is in the role with the given role name.
	 *
	 * @param role
	 *           the role to check for
	 * @return <code>true</code> if user is in the role, <code>false</code> otherwise
	 */
	public static boolean isCurrentUserInRole(String role) {
		return getUserRoles().stream()
				.filter(roleName -> roleName.equals(Objects.requireNonNull(role)))
				.findAny()
				.isPresent();
	}

	private SecurityUtils() {
		// Util methods only
	}
}
