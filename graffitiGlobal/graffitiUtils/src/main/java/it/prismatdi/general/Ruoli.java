package it.prismatdi.general;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum Ruoli {
	ALL(10), USER(20), MANAGER(30), ADMINISTRATOR(100), AGENTE(40);
	public static Ruoli findRuoloDaNome(String nomeRuolo) {
		for (Ruoli ruolo : Ruoli.values()) {
			if (ruolo.toString()
					.equals(nomeRuolo)) {
				return ruolo;
			}
		}
		return null;
	}

	public static List<Ruoli> getRuoliCompresi(Ruoli ruolo) {
		List<Ruoli> result = new ArrayList<>();
		List<Ruoli> ordered = Ruoli.getRuoliOrdinati();
		for (Ruoli ruoli : ordered) {
			if (ruolo.getVal()
					.compareTo(ruoli.getVal()) >= 0) {
				result.add(ruoli);
			}
		}
		return result;
	}

	public static List<Ruoli> getRuoliOrdinati() {
		List<Ruoli> result = Arrays.asList(Ruoli.values())
				.stream()
				.sorted(Comparator.comparing(Ruoli::getVal))
				.collect(Collectors.toList());
		return result;
	}
	private Integer val;

	private Ruoli(Integer val) {
		this.val = val;
	}

	public Integer getVal() {
		return val;
	}
}