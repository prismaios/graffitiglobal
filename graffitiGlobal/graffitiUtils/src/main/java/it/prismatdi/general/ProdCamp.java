package it.prismatdi.general;

public enum ProdCamp {
	PRODUZIONE("P"), CAMPIONARIO("C");
	private String tipo;

	private ProdCamp(String tipo) {
		this.tipo = tipo;
	}

	public String getString() {
		return tipo;
	}

}
