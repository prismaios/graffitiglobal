package it.prismatdi.general;

public enum ItaliaEstero {
	ITALIA("I"), ESTERO("C");
	private String tipo;

	private ItaliaEstero(String tipo) {
		this.tipo = tipo;
	}

	public String getString() {
		return tipo;
	}

}
