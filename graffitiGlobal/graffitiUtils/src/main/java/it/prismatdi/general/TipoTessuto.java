package it.prismatdi.general;

public enum TipoTessuto {
	SCIARPE("S"), TESSUTO("T");
	private String tipo;

	private TipoTessuto(String tipo) {
		this.tipo = tipo;
	}

	public String getString() {
		return tipo;
	}
}
