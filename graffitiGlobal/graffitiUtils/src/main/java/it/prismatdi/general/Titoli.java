package it.prismatdi.general;

public enum Titoli {
	CAMBIO_PASSWD("CAMBIO PASSWORD");
	private String string;

	private Titoli(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
