package it.prismatdi.general;

public enum Bottoni {
	SAVE("Salva");

	private String string;

	private Bottoni(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
