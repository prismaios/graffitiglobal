package it.prismatdi.general;

public enum AliasPerCriteriaQuery {
	codArticolo,
	codCli,
	codArti,
	qta,
	valore,
	codice,
	stagione,
	descrizione,
	causale,
	codColore,
	desColore,
	maggiorazioni,
	prezzo,
	codRappr,
	unitaM,
	prodCamp,
	valuta,
	dataValuta,
	dataMov,
	dataMin,
	dataMax,
	nOrdine,
	codCli1,
	numMov,
	qtaL,
	costo,
	costoProduzione;

}
