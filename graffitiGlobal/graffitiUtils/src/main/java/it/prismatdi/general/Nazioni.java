package it.prismatdi.general;

public enum Nazioni {
	ITALIA("IT"),
	GERMANIA("DE"),
	FRANCIA("FR"),
	PAESI_BASSI("NL") {

		@Override
		public String toString() {
			return "PAESI BASSI";
		}

	},
	BELGIO("BE"),
	LUSSEMBURGO("LU"),
	REGNO_UNITO("GB") {

		@Override
		public String toString() {
			return "REGNO UNITO";
		}

	},
	IRLANDA("IE"),
	DANIMARCA("DK"),
	GRECIA("EL"),
	SPAGNA("ES"),
	PORTOGALLO("PT"),
	AUSTRIA("AT"),
	FINLANDIA("FI"),
	SVEZIA("SE"),
	SAN_MARINO("SM") {

		@Override
		public String toString() {
			return "SAN MARINO";
		}

	},
	ANDORRA("AD"),
	EMIRATI_ARABI_UNITI("AE") {

		@Override
		public String toString() {
			return "EMIRATI ARABI UNITI";
		}

	},
	AFGHANISTAN("AF"),
	ANTIGUA_E_BARBUTA("AG") {

		@Override
		public String toString() {
			return "ANTIGUA E BARBUTA";
		}

	},
	ANGUILLA("AI"),
	ALBANIA("AL"),
	ARMENIA("AM"),
	ANTILLE_OLANDESI("AN") {

		@Override
		public String toString() {
			return "ANTILLE OLANDESI";
		}

	},
	ANGOLA("AO"),
	ANTARCTICA("AQ"),
	ARGENTINA("AR"),
	AMERICAN_SAMOA("AS"),
	AUSTRALIA("AU"),
	ARUBA("AW"),
	AZERBAIJAN("AZ"),
	BOSNIA_HERZEGOVINA("BA") {

		@Override
		public String toString() {
			return "BOSNIA HERZEGOVINA";
		}

	},
	BARBADOS("BB"),
	BANGLADESH("BD"),
	BURKINA_FASO("BF") {

		@Override
		public String toString() {
			return "BURKINA FASO";
		}

	},
	BULGARIA("BG"),
	BAHRAIN("BH"),
	BURUNDI("BI"),
	BENIN("BJ"),
	BERMUDA("BM"),
	BRUNEI_DARUSSALAM("BN") {

		@Override
		public String toString() {
			return "BRUNEI DARUSSALAM";
		}

	},
	BOLIVIA("BO"),
	BRAZIL("BR"),
	BAHAMAS("BS"),
	BHUTAN("BT"),
	BOUVET_ISLAND("BV") {

		@Override
		public String toString() {
			return "BOUVET ISLAND";
		}

	},
	BOTSWANA("BW"),
	BELARUS("BY"),
	BELIZE("BZ"),
	CANADA("CA"),
	ISOLE_COCOS_KEELING("CC") {

		@Override
		public String toString() {
			return "ISOLE COCOS KEELING";
		}

	},
	REPUBBLICA_DEMOCRATICA_DEL_CONGO("CD") {

		@Override
		public String toString() {
			return "REPUBBLICA DEMOCRATICA DEL CONGO";
		}

	},
	REPUBBLICA_CENTROAFRICANA("CF") {

		@Override
		public String toString() {
			return "REPUBBLICA CENTROAFRICANA";
		}

	},
	REPUBBLICA_DEL_CONGO("CG") {

		@Override
		public String toString() {
			return "REPUBBLICA DEL CONGO";
		}

	},
	SWITZERLAND("CH"),
	COSTA_D_AVORIO("CI") {

		@Override
		public String toString() {
			return "COSTA D\'AVORIO";
		}

	},
	COOK_ISLANDS("CK") {

		@Override
		public String toString() {
			return "ISOLE DI COOK";
		}

	},
	CILE("CL"),
	CAMEROON("CM"),
	CHINA("CN"),
	COLOMBIA("CO"),
	COSTA_RICA("CR") {

		@Override
		public String toString() {
			return "COSTA RICA";
		}

	},
	CUBA("CU"),
	CAPO_VERDE("CV") {

		@Override
		public String toString() {
			return "CAPO VERDE";
		}

	},
	CHRISTMAS_ISLAND("CX") {

		@Override
		public String toString() {
			return "CHRISTMAS ISLAND";
		}

	},
	CIPRO("CY"),
	REPUBBLICA_CECA("CZ") {

		@Override
		public String toString() {
			return "REPUBBLICA CECA";
		}

	},
	DJIBOUTI("DJ"),
	DOMINICA("DM"),
	REPUBBLICA_DOMINICANA("DO") {

		@Override
		public String toString() {
			return "REPUBBLICA DOMINICANA";
		}

	},
	ALGERIA("DZ"),
	ECUADOR("EC"),
	ESTONIA("EE"),
	EGITTO("EG"),
	SAHARA_OCCIDENTALE("EH") {

		@Override
		public String toString() {
			return "SAHARA OCCIDENTALE";
		}

	},
	ERITREA("ER"),
	ETHIOPIA("ET"),
	FIJI("FJ"),
	ISOLE_FALKLAND("FK") {

		@Override
		public String toString() {
			return "ISOLE FALKLAND";
		}

	},
	MICRONESIA("FM"),
	ISOLE_FAROE("FO") {

		@Override
		public String toString() {
			return "ISOLE FAROE";
		}

	},
	GABON("GA"),
	GRENADA("GD"),
	GEORGIA("GE"),
	GUIANA_FRANCESE("GF") {

		@Override
		public String toString() {
			return "GUIANA FRANCESE";
		}

	},
	GHANA("GH"),
	GIBILTERRA("GI"),
	GROENLANDIA("GL"),
	GAMBIA("GM"),
	GUINEA("GN"),
	GUADELUPE("GP"),
	GUINEA_EQUATORIALE("GQ") {

		@Override
		public String toString() {
			return "GUINEA EQUATORIALE";
		}

	},
	SUD_GEORGIA_E_ISOLE_SANDWICH("GS") {

		@Override
		public String toString() {
			return "SUD GEORGIA E ISOLE SANDWICH";
		}

	},
	GUATEMALA("GT"),
	GUAM("GU"),
	GUINEA_BISSAU("GW") {

		@Override
		public String toString() {
			return "GUINEA BISSAU";
		}

	},
	GUYANA("GY"),
	HONG_KONG("HK"),
	HEARD_MC_DONALD("HM") {

		@Override
		public String toString() {
			return "HEARD E MC.DONALD";
		}

	},
	HONDURAS("HN"),
	CROAZIA("HR"),
	HAITI("HT"),
	UNGHERIA("HU"),
	INDONESIA("ID"),
	ISRAELE("IL"),
	INDIA("IN"),
	TERRITORIO_BRITANNICO_DELL_OCEANO_INDIANO("IO") {

		@Override
		public String toString() {
			return "TERRITORIO BRITANNICO DELL\'OCEANO INDIANO";
		}

	},
	IRAQ("IQ"),
	IRAN("IR"),
	ISLANDA("IS"),
	JAMAICA("JM"),
	GIORDANIA("JO"),
	GIAPPONE("JP"),
	KENYA("KE"),
	KYRGYZSTAN("KG"),
	CAMBOGIA("KH"),
	KIRIBATI("KI"),
	COMOROS("KM"),
	SAINT_KITTS_E_NEVIS("KN") {

		@Override
		public String toString() {
			return "SAINt KITTS E NEVIS";
		}

	},
	REPUBBLICA_DEMOCRATICA_DI_COREA("KP") {

		@Override
		public String toString() {
			return "REPUBBLICA DEMOCRATICA DI COREA";
		}

	},
	REPUBBLICA_DI_COREA("KR") {

		@Override
		public String toString() {
			return "REPUBBLICA DI COREA";
		}

	},
	KUWAIT("KW"),
	ISOLE_CAYMAN("KY") {

		@Override
		public String toString() {
			return "ISOLE CAYMAN";
		}

	},
	KAZAKHSTAN("KZ"),
	REPUBBLICA_DEMOCRATICA_DEL_LAOS("LA") {

		@Override
		public String toString() {
			return "REPUBBLICA DEMOCRATICA DEL LAOS";
		}

	},
	LIBANO("LB"),
	SAINT_LUCIA("LC") {

		@Override
		public String toString() {
			return "SAINT LUCIA";
		}

	},
	LIECHTENSTEIN("LI"),
	SRI_LANKA("LK") {

		@Override
		public String toString() {
			return "SRI LANKA";
		}

	},
	LIBERIA("LR"),
	LESOTHO("LS"),
	LITUANIA("LT"),
	LATVIA("LV"),
	LIBIA("LY"),
	MAROCCO("MA"),
	MONACO("MC"),
	REPUBBLICA_MOLDOVA("MD") {

		@Override
		public String toString() {
			return "REPUBBLICA MOLDOVA";
		}

	},
	MADAGASCAR("MG"),
	ISOLE_MARSHALL("MH") {

		@Override
		public String toString() {
			return "ISOLE MARSHALL";
		}

	},
	MACEDONIA("MK"),
	MALI("ML"),
	MYANMAR("MM"),
	MONGOLIA("MN"),
	MACAU("MO"),
	MARIANNE_SETTENTRIONALI("MP") {

		@Override
		public String toString() {
			return "MARIANNE SETTENTRIONALI";
		}

	},
	MARTINIQUE("MQ"),
	MAURITANIA("MR"),
	MONTSERRAT("MS"),
	MALTA("MT"),
	MAURITIUS("MU"),
	MALDIVES("MV"),
	MALAWI("MW"),
	MESSICO("MX"),
	MALAYSIA("MY"),
	MOZAMBIQUE("MZ"),
	NAMIBIA("NA"),
	NUOVA_CALEDONIA("NC") {

		@Override
		public String toString() {
			return "NUOVA CALEDONIA";
		}

	},
	NIGER("NE"),
	ISOLA_DI_NORFOLK("NF") {

		@Override
		public String toString() {
			return "ISOLA DI NORFOLK";
		}

	},
	NIGERIA("NG"),
	NICARAGUA("NI"),
	NORVEGIA("NO"),
	NEPAL("NP"),
	NAURU("NR"),
	NIUE("NU"),
	NUOVA_ZELANDA("NZ") {

		@Override
		public String toString() {
			return "NUOVA ZELANDA";
		}

	},
	OMAN("OM"),
	PANAMA("PA"),
	PERU("PE"),
	POLINESIA_FRANCESE("PF") {

		@Override
		public String toString() {
			return "POLINESIA FRANCESE";
		}

	},
	PAPUA_E_GUINEA("PG") {

		@Override
		public String toString() {
			return "PAPUA E GUINEA";
		}

	},
	FILIPPINE("PH"),
	PAKISTAN("PK"),
	POLONIA("PL"),
	ST_PIERRE_E_MIQUELON("PM") {

		@Override
		public String toString() {
			return "ST PIERRE E MIQUELON";
		}

	},
	PITCAIRN("PN"),
	PORTO_RICO("PR") {

		@Override
		public String toString() {
			return "PORTO RICO";
		}

	},
	PALAU("PW"),
	PARAGUAY("PY"),
	QATAR("QA"),
	REUNION("RE"),
	ROMANIA("RO"),
	FEDERAZIONE_RUSSA("RU") {

		@Override
		public String toString() {
			return "FEDERAZIONE RUSSA";
		}

	},
	RWANDA("RW"),
	ARABIA_SAUDITA("SA") {

		@Override
		public String toString() {
			return "ARABIA SAUDITA";
		}

	},
	ISOLE_SOLOMON("SB") {

		@Override
		public String toString() {
			return "ISOLE SOLOMON";
		}

	},
	SEYCHELLES("SC"),
	SUDAN("SD"),
	SINGAPORE("SG"),
	ST_ELENA("SH") {

		@Override
		public String toString() {
			return "ST ELENA";
		}

	},
	SLOVENIA("SI"),
	SVALBARD("SJ"),
	SLOVAKIA("SK"),
	SIERRA_LEONE("SL") {

		@Override
		public String toString() {
			return "SIERRA LEONE";
		}

	},
	SENEGAL("SN"),
	SOMALIA("SO"),
	SURINAME("SR"),
	SAO_TOME("ST") {

		@Override
		public String toString() {
			return "SAO TOME";
		}

	},
	SAN_SALVADOR("SV") {

		@Override
		public String toString() {
			return "SAN SALVADOR";
		}

	},
	SIRIA("SY"),
	SWAZILAND("SZ"),
	TURKS_E_CAICOS("TC") {

		@Override
		public String toString() {
			return "TURKS E CAICOS";
		}

	},
	CHAD("TD"),
	TERRE_AUSTRALI_FRANCESI("TF") {

		@Override
		public String toString() {
			return "TERRE AUSTRALI FRANCESI";
		}

	},
	TOGO("TG"),
	THAILAND("TH"),
	TAJIKISTAN("TJ"),
	TOKELAU("TK"),
	TURKMENISTAN("TM"),
	TUNISIA("TN"),
	TONGA("TO"),
	TIMOR_EST("TP"),
	TURCHIA("TR"),
	TRINIDAD_E_TOBAGO("TT") {

		@Override
		public String toString() {
			return "TRINIDAD E TOBAGO";
		}

	},
	TUVALU("TV"),
	TAIWAN("TW"),
	TANZANIA("TZ"),
	UKRAINA("UA"),
	UGANDA("UG"),
	STATI_UNITI("US"),
	URUGUAY("UY"),
	UZBEKISTAN("UZ"),
	VATICANO("VA"),
	SAINT_VINCENT_E_GRENADINE("VC") {

		@Override
		public String toString() {
			return "SAINT VINCENT E GRENADINE";
		}

	},
	VENEZUELA("VE"),
	ISOLE_VERGINI_BRITANNICHE("VG") {

		@Override
		public String toString() {
			return "ISOLE VERGINI BRITANNICHE";
		}

	},
	ISOLE_VERGINI_USA("VI") {

		@Override
		public String toString() {
			return "ISOLE VERGINI USA";
		}

	},
	VIETNAM("VN"),
	VANUATU("VU"),
	WALLIS_E_FUTUNA("WF") {

		@Override
		public String toString() {
			return "WALLIS E FUTUNA";
		}

	},
	SAMOA("WS"),
	YEMEN("YE"),
	MAYOTTE("YT"),
	YUGOSLAVIA("YU"),
	SUD_AFRICA("ZA") {

		@Override
		public String toString() {
			return "SUD AFRICA";
		}

	},
	ZAMBIA("ZM"),
	ZIMBABWE("ZW");
	public static Nazioni findNazione(String codIso) throws Exception {
		if (codIso == null) {
			return Nazioni.ITALIA;
		}
		for (Nazioni nazi : Nazioni.values()) {
			if (nazi.codIso.equals(codIso)) {
				return nazi;
			}
		}
		throw new Exception("Nazione non esistente");
	}
	public String codIso;

	private Nazioni(String codice) {
		this.codIso = codice;
	}
}
