package it.prismatdi.errors;

public enum BinderError {
	TITOLO("ERRORE"),
	DESCRIZIONE("I campi non sono coerenti con la richiesta"),
	ERR_ANNO_PRECEDENTE("E\' obbligatorio inserite le date di ricerca!");

	private final String string;

	private BinderError(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}

}
