package it.prismatdi.model.as400;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the cardis database table.
 *
 * Prova di modifica su I9 Ancora prova prima non è andata
 */

@Entity
@NamedQuery(name = "Cardis.findAll", query = "SELECT c FROM Cardis c")
public class Cardis implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private CardisPK				id;

	private String					cocaco;

	private String					codesc;

	private String					cofill;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cardis other = (Cardis) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getCocaco() {
		return cocaco;
	}

	public String getCodesc() {
		return codesc;
	}

	public String getCofill() {
		return cofill;
	}

	public CardisPK getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setCocaco(String cocaco) {
		this.cocaco = cocaco;
	}

	public void setCodesc(String codesc) {
		this.codesc = codesc;
	}

	public void setCofill(String cofill) {
		this.cofill = cofill;
	}

	public void setId(CardisPK id) {
		this.id = id;
	}

}