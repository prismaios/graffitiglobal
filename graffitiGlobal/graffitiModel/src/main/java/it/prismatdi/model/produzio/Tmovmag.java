package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * The persistent class for the tmovmag database table.
 *
 */
@Entity
@NamedQuery(name = "Tmovmag.findAll", query = "SELECT t FROM Tmovmag t")
public class Tmovmag implements Serializable {
	private static final long	serialVersionUID	= 1L;
	@Id
	private Integer				mfkey;
	private String					mfabic;
	private String					mfana1;
	private String					mfana2;
	private Integer				mfanes;
	private String					mfbade;
	private String					mfbanc;
	private String					mfcau;
	private String					mfcauft;
	private String					mfcide;
	private String					mfciva;
	private String					mfcive;
	// data bolla
	private String					mfdado;
	private String					mfdamo;
	private String					mfdapa;
	private String					mfdest;
	private String					mfditt;
	private String					mfdivi;
	private double					mfimiv;
	private double					mfimpd;
	private double					mfimpo;
	private double					mfimpr;
	private double					mfimrd;
	private double					mfimvd;
	private String					mfinde;
	private String					mfinve;
	private String					mflape;
	private double					mfmag1;
	private double					mfmag2;
	private double					mfmag3;
	private String					mfmarc;
	private Integer				mfncol;
	private String					mfnote;
	private String					mfnudo;
	private String					mforpa;
	private String					mfpaga;
	private double					mfper2;
	private double					mfper3;
	private double					mfperc;
	private double					mfpesl;
	private double					mfpesn;
	private String					mfport;
	private String					mfprca;
	private String					mfrade;
	private String					mfrap2;
	private String					mfrap3;
	private String					mfrapp;
	private String					mfrave;
	private String					mfrida;
	private String					mfrifa;
	private String					mfsc1;
	private String					mfsc2;
	private double					mfscfa;
	private double					mfscfu;
	private double					mfsco1;
	private double					mfsco2;
	private double					mfsco3;
	private double					mfsptr;
	private String					mfstag;
	private String					mfstat;
	private String					mftan1;
	private String					mftan2;
	private String					mftido;
	private String					mftipft;
	private String					mfvett;
	@OneToMany(mappedBy = "testa", fetch = FetchType.LAZY)
	@OrderBy("mrarti")
	private List<Tmovrig>		righe;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tmovmag other = (Tmovmag) obj;
		if (mfkey == null) {
			if (other.mfkey != null) {
				return false;
			}
		}
		else if (!mfkey.equals(other.mfkey)) {
			return false;
		}
		return true;
	}

	public String getMfabic() {
		return mfabic;
	}

	public String getMfana1() {
		return mfana1;
	}

	public String getMfana2() {
		return mfana2;
	}

	public Integer getMfanes() {
		return mfanes;
	}

	public String getMfbade() {
		return mfbade;
	}

	public String getMfbanc() {
		return mfbanc;
	}

	public String getMfcau() {
		return mfcau;
	}

	public String getMfcauft() {
		return mfcauft;
	}

	public String getMfcide() {
		return mfcide;
	}

	public String getMfciva() {
		return mfciva;
	}

	public String getMfcive() {
		return mfcive;
	}

	public String getMfdado() {
		return mfdado;
	}

	public String getMfdamo() {
		return mfdamo;
	}

	public String getMfdapa() {
		return mfdapa;
	}

	public String getMfdest() {
		return mfdest;
	}

	public String getMfditt() {
		return mfditt;
	}

	public String getMfdivi() {
		return mfdivi;
	}

	public double getMfimiv() {
		return mfimiv;
	}

	public double getMfimpd() {
		return mfimpd;
	}

	public double getMfimpo() {
		return mfimpo;
	}

	public double getMfimpr() {
		return mfimpr;
	}

	public double getMfimrd() {
		return mfimrd;
	}

	public double getMfimvd() {
		return mfimvd;
	}

	public String getMfinde() {
		return mfinde;
	}

	public String getMfinve() {
		return mfinve;
	}

	public Integer getMfkey() {
		return mfkey;
	}

	public String getMflape() {
		return mflape;
	}

	public double getMfmag1() {
		return mfmag1;
	}

	public double getMfmag2() {
		return mfmag2;
	}

	public double getMfmag3() {
		return mfmag3;
	}

	public String getMfmarc() {
		return mfmarc;
	}

	public Integer getMfncol() {
		return mfncol;
	}

	public String getMfnote() {
		return mfnote;
	}

	public String getMfnudo() {
		return mfnudo;
	}

	public String getMforpa() {
		return mforpa;
	}

	public String getMfpaga() {
		return mfpaga;
	}

	public double getMfper2() {
		return mfper2;
	}

	public double getMfper3() {
		return mfper3;
	}

	public double getMfperc() {
		return mfperc;
	}

	public double getMfpesl() {
		return mfpesl;
	}

	public double getMfpesn() {
		return mfpesn;
	}

	public String getMfport() {
		return mfport;
	}

	public String getMfprca() {
		return mfprca;
	}

	public String getMfrade() {
		return mfrade;
	}

	public String getMfrap2() {
		return mfrap2;
	}

	public String getMfrap3() {
		return mfrap3;
	}

	public String getMfrapp() {
		return mfrapp;
	}

	public String getMfrave() {
		return mfrave;
	}

	public String getMfrida() {
		return mfrida;
	}

	public String getMfrifa() {
		return mfrifa;
	}

	public String getMfsc1() {
		return mfsc1;
	}

	public String getMfsc2() {
		return mfsc2;
	}

	public double getMfscfa() {
		return mfscfa;
	}

	public double getMfscfu() {
		return mfscfu;
	}

	public double getMfsco1() {
		return mfsco1;
	}

	public double getMfsco2() {
		return mfsco2;
	}

	public double getMfsco3() {
		return mfsco3;
	}

	public double getMfsptr() {
		return mfsptr;
	}

	public String getMfstag() {
		return mfstag;
	}

	public String getMfstat() {
		return mfstat;
	}

	public String getMftan1() {
		return mftan1;
	}

	public String getMftan2() {
		return mftan2;
	}

	public String getMftido() {
		return mftido;
	}

	public String getMftipft() {
		return mftipft;
	}

	public String getMfvett() {
		return mfvett;
	}

	public List<Tmovrig> getRighe() {
		return righe;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mfkey == null ? 0 : mfkey.hashCode());
		return result;
	}

	public void setMfabic(String mfabic) {
		this.mfabic = mfabic;
	}

	public void setMfana1(String mfana1) {
		this.mfana1 = mfana1;
	}

	public void setMfana2(String mfana2) {
		this.mfana2 = mfana2;
	}

	public void setMfanes(Integer mfanes) {
		this.mfanes = mfanes;
	}

	public void setMfbade(String mfbade) {
		this.mfbade = mfbade;
	}

	public void setMfbanc(String mfbanc) {
		this.mfbanc = mfbanc;
	}

	public void setMfcau(String mfcau) {
		this.mfcau = mfcau;
	}

	public void setMfcauft(String mfcauft) {
		this.mfcauft = mfcauft;
	}

	public void setMfcide(String mfcide) {
		this.mfcide = mfcide;
	}

	public void setMfciva(String mfciva) {
		this.mfciva = mfciva;
	}

	public void setMfcive(String mfcive) {
		this.mfcive = mfcive;
	}

	public void setMfdado(String mfdado) {
		this.mfdado = mfdado;
	}

	public void setMfdamo(String mfdamo) {
		this.mfdamo = mfdamo;
	}

	public void setMfdapa(String mfdapa) {
		this.mfdapa = mfdapa;
	}

	public void setMfdest(String mfdest) {
		this.mfdest = mfdest;
	}

	public void setMfditt(String mfditt) {
		this.mfditt = mfditt;
	}

	public void setMfdivi(String mfdivi) {
		this.mfdivi = mfdivi;
	}

	public void setMfimiv(double mfimiv) {
		this.mfimiv = mfimiv;
	}

	public void setMfimpd(double mfimpd) {
		this.mfimpd = mfimpd;
	}

	public void setMfimpo(double mfimpo) {
		this.mfimpo = mfimpo;
	}

	public void setMfimpr(double mfimpr) {
		this.mfimpr = mfimpr;
	}

	public void setMfimrd(double mfimrd) {
		this.mfimrd = mfimrd;
	}

	public void setMfimvd(double mfimvd) {
		this.mfimvd = mfimvd;
	}

	public void setMfinde(String mfinde) {
		this.mfinde = mfinde;
	}

	public void setMfinve(String mfinve) {
		this.mfinve = mfinve;
	}

	public void setMfkey(Integer mfkey) {
		this.mfkey = mfkey;
	}

	public void setMflape(String mflape) {
		this.mflape = mflape;
	}

	public void setMfmag1(double mfmag1) {
		this.mfmag1 = mfmag1;
	}

	public void setMfmag2(double mfmag2) {
		this.mfmag2 = mfmag2;
	}

	public void setMfmag3(double mfmag3) {
		this.mfmag3 = mfmag3;
	}

	public void setMfmarc(String mfmarc) {
		this.mfmarc = mfmarc;
	}

	public void setMfncol(Integer mfncol) {
		this.mfncol = mfncol;
	}

	public void setMfnote(String mfnote) {
		this.mfnote = mfnote;
	}

	public void setMfnudo(String mfnudo) {
		this.mfnudo = mfnudo;
	}

	public void setMforpa(String mforpa) {
		this.mforpa = mforpa;
	}

	public void setMfpaga(String mfpaga) {
		this.mfpaga = mfpaga;
	}

	public void setMfper2(double mfper2) {
		this.mfper2 = mfper2;
	}

	public void setMfper3(double mfper3) {
		this.mfper3 = mfper3;
	}

	public void setMfperc(double mfperc) {
		this.mfperc = mfperc;
	}

	public void setMfpesl(double mfpesl) {
		this.mfpesl = mfpesl;
	}

	public void setMfpesn(double mfpesn) {
		this.mfpesn = mfpesn;
	}

	public void setMfport(String mfport) {
		this.mfport = mfport;
	}

	public void setMfprca(String mfprca) {
		this.mfprca = mfprca;
	}

	public void setMfrade(String mfrade) {
		this.mfrade = mfrade;
	}

	public void setMfrap2(String mfrap2) {
		this.mfrap2 = mfrap2;
	}

	public void setMfrap3(String mfrap3) {
		this.mfrap3 = mfrap3;
	}

	public void setMfrapp(String mfrapp) {
		this.mfrapp = mfrapp;
	}

	public void setMfrave(String mfrave) {
		this.mfrave = mfrave;
	}

	public void setMfrida(String mfrida) {
		this.mfrida = mfrida;
	}

	public void setMfrifa(String mfrifa) {
		this.mfrifa = mfrifa;
	}

	public void setMfsc1(String mfsc1) {
		this.mfsc1 = mfsc1;
	}

	public void setMfsc2(String mfsc2) {
		this.mfsc2 = mfsc2;
	}

	public void setMfscfa(double mfscfa) {
		this.mfscfa = mfscfa;
	}

	public void setMfscfu(double mfscfu) {
		this.mfscfu = mfscfu;
	}

	public void setMfsco1(double mfsco1) {
		this.mfsco1 = mfsco1;
	}

	public void setMfsco2(double mfsco2) {
		this.mfsco2 = mfsco2;
	}

	public void setMfsco3(double mfsco3) {
		this.mfsco3 = mfsco3;
	}

	public void setMfsptr(double mfsptr) {
		this.mfsptr = mfsptr;
	}

	public void setMfstag(String mfstag) {
		this.mfstag = mfstag;
	}

	public void setMfstat(String mfstat) {
		this.mfstat = mfstat;
	}

	public void setMftan1(String mftan1) {
		this.mftan1 = mftan1;
	}

	public void setMftan2(String mftan2) {
		this.mftan2 = mftan2;
	}

	public void setMftido(String mftido) {
		this.mftido = mftido;
	}

	public void setMftipft(String mftipft) {
		this.mftipft = mftipft;
	}

	public void setMfvett(String mfvett) {
		this.mfvett = mfvett;
	}

	public void setRighe(List<Tmovrig> righe) {
		this.righe = righe;
	}

}