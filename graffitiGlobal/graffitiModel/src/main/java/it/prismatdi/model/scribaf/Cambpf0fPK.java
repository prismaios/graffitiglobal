package it.prismatdi.model.scribaf;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the cambpf0f database table.
 *
 */
@Embeddable
public class Cambpf0fPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long	serialVersionUID	= 1L;

	private String					cmtipo;

	private short					cmseco;

	private short					cmmere;

	private short					cmgire;

	private String					cmdivi;

	public Cambpf0fPK() {
	}

	public Cambpf0fPK(String cmtipo, short cmseco, short cmmere, short cmgire, String cmdivi) {
		super();
		this.cmtipo = cmtipo;
		this.cmseco = cmseco;
		this.cmmere = cmmere;
		this.cmgire = cmgire;
		this.cmdivi = cmdivi;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Cambpf0fPK)) {
			return false;
		}
		Cambpf0fPK castOther = (Cambpf0fPK) other;
		return cmtipo.equals(castOther.cmtipo)
				&& cmseco == castOther.cmseco
				&& cmmere == castOther.cmmere
				&& cmgire == castOther.cmgire
				&& cmdivi.equals(castOther.cmdivi);
	}

	public String getCmdivi() {
		return cmdivi;
	}

	public short getCmgire() {
		return cmgire;
	}

	public short getCmmere() {
		return cmmere;
	}

	public short getCmseco() {
		return cmseco;
	}

	public String getCmtipo() {
		return cmtipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + cmtipo.hashCode();
		hash = hash * prime + cmseco;
		hash = hash * prime + cmmere;
		hash = hash * prime + cmgire;
		hash = hash * prime + cmdivi.hashCode();

		return hash;
	}

	public void setCmdivi(String cmdivi) {
		this.cmdivi = cmdivi;
	}

	public void setCmgire(short cmgire) {
		this.cmgire = cmgire;
	}

	public void setCmmere(short cmmere) {
		this.cmmere = cmmere;
	}

	public void setCmseco(short cmseco) {
		this.cmseco = cmseco;
	}

	public void setCmtipo(String cmtipo) {
		this.cmtipo = cmtipo;
	}

	@Override
	public String toString() {
		return "Cambpf0fPK [cmtipo=" + cmtipo + ", cmseco=" + cmseco + ", cmmere=" + cmmere + ", cmgire=" + cmgire + ", cmdivi="
				+ cmdivi + "]";
	}

}