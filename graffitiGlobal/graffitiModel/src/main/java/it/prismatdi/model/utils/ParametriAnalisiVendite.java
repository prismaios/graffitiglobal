package it.prismatdi.model.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.StringUtils;

import it.prismatdi.general.AliasPerCriteriaQuery;
import it.prismatdi.general.ItaliaEstero;
import it.prismatdi.general.ProdCamp;
import it.prismatdi.general.TipoTessuto;
import it.prismatdi.model.produzio.Matpri;
import it.prismatdi.model.produzio.Matpri_;
import it.prismatdi.model.produzio.Ordini;
import it.prismatdi.model.produzio.Ordini_;
import it.prismatdi.model.produzio.Tmovmag;
import it.prismatdi.model.produzio.Tmovmag_;
import it.prismatdi.model.produzio.Tmovrig;
import it.prismatdi.model.produzio.Tmovrig_;

public class ParametriAnalisiVendite implements ParametriConQuery {

	private TipoTessuto	tipoTessuto;
	private LocalDate		dallaData;
	private LocalDate		allaData;
	private List<String>	stagioni;
	private Boolean		annoPrecedente;
	private Busta			rappresentante;
	private Boolean		cliente;
	private List<String>	selCliente;
	private Boolean		perOrdine;

	private ProdCamp		prodCampion;

	private ItaliaEstero	paese;
	private Boolean		margineLordo;
	private Boolean		venditeOrdini;
	private Boolean		excludeCamp;
	private List<String>	lineaProd;
	private boolean		stagio	= false;

	public ParametriAnalisiVendite() {
		super();
		selCliente = new ArrayList<>();
		lineaProd = new ArrayList<>();
		stagioni = new ArrayList<>();
	}

	public void addCliente(String client) {
		selCliente.add(client);
	}

	public void addLinea(String linea) {
		lineaProd.add(linea);
	}

	public void addStagione(String stagio) {
		stagioni.add(stagio);
	}

	public Predicate faiPredicatoPerClienti(CriteriaBuilder cb, Root<Tmovrig> tmovRoot, Join<Tmovrig, Tmovmag> testata) {
		Predicate ana2 = null;
		if (!selCliente.isEmpty()) {
			String cli = selCliente.get(0);
			//			Predicate ana1 = cb.equal(testata.get(Tmovmag_.mfana1), cli);
			ana2 = cb.equal(testata.get(Tmovmag_.mfana2), cli);
			//			orPred = cb.or(ana1, ana2);
		}
		return ana2;
	}

	public Predicate faiPredicatoPerClientiNCT(CriteriaBuilder cb, Root<Tmovrig> tmovRoot, Join<Tmovrig, Tmovmag> testata) {
		Predicate ana2 = null;
		if (!selCliente.isEmpty()) {
			String cli = selCliente.get(0);
			ana2 = cb.equal(testata.get(Tmovmag_.mfana1), cli);
		}
		return ana2;
	}

	public List<Predicate> faiPredicatoPerDate(CriteriaBuilder cb, Root<Tmovrig> tmovRoot, Join<Tmovrig, Tmovmag> testata) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		List<Predicate> datePreds = new ArrayList<>();
		if (dallaData != null && allaData != null && !stagio) {
			String daData = dallaData.format(formatter);
			String aData = allaData.format(formatter);
			Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), daData);
			Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), aData);
			datePreds.add(daDataPred);
			datePreds.add(aDataPred);

		}
		//		if (dallaData != null && allaData != null && !stagio) {
		//			// DA DATA A final DATA
		//			final String daData = dallaData.format(formatter);
		//			final String aData = allaData.format(formatter);
		//			q.append(" AND r.testa.mfdado >= '");
		//			q.append(daData);
		//			q.append("' AND r.testa.mfdado <= '");
		//			q.append(aData);
		//			q.append("'");
		//		}
		return datePreds;
	}

	public Predicate faiPredicatoPerRappresentante(CriteriaBuilder cb, Root<Tmovrig> tmovRoot, Join<Tmovrig, Tmovmag> testata) {
		// RAPPRESENTANTE
		//			if (rappresentante != null && rappresentante.length() > 0) {
		//				q.append(" AND r.testa.mfrapp = '");
		//				q.append(rappresentante);
		//				q.append("'");
		//			}
		Predicate rappPred = null;
		if (rappresentante != null) {
			rappPred = cb.equal(testata.get(Tmovmag_.mfrapp), rappresentante.getCodice());
		}
		return rappPred;
	}

	public List<Predicate> faiPredicatoPerStagioni(CriteriaBuilder cb, Root<Tmovrig> tmovRoot, Join<Tmovrig, Ordini> ordine) {

		List<Predicate> stagioPreds = new ArrayList<>();
		if (!stagioni.isEmpty()) {
			for (String stagione : stagioni) {
				Predicate cond = cb.equal(ordine.get(Ordini_.orcoan), stagione);
				stagioPreds.add(cond);
			}
		}
		return stagioPreds;
	}

	public Predicate faiPredicatoPerTipoTessuto(CriteriaBuilder cb, Root<Tmovrig> tmovRoot, Join<Tmovrig, Matpri> matPri) {
		Predicate tipoTess = null;
		if (tipoTessuto != null) {
			String t = tipoTessuto.getString();
			tipoTess = cb.equal(matPri.get(Matpri_.mtfami), t);
		}
		return tipoTess;
	}

	public LocalDate getAllaData() {
		return allaData;
	}

	public Boolean getAnnoPrecedente() {
		return annoPrecedente;
	}

	public Boolean getCliente() {
		return cliente;
	}

	@Override
	public CriteriaQuery<?> getCriteriaQuery(CriteriaBuilder cb) {
		if (!perOrdine) {
			return getQueryMovimenti(cb);
		}
		else {
			return getQueryOrdini(cb);
		}
	}

	public LocalDate getDallaData() {
		return dallaData;
	}

	public Boolean getExcludeCamp() {
		return excludeCamp;
	}

	public List<String> getLineaProd() {
		return lineaProd;
	}

	public String getLineaProdStr() {
		String ret = "";
		if (lineaProd.size() > 0) {
			ret = StringUtils.join(lineaProd, ",");
		}
		return ret;
	}

	public Boolean getMargineLordo() {
		return margineLordo;
	}

	public ItaliaEstero getPaese() {
		return paese;
	}

	public Boolean getPerOrdine() {
		return perOrdine;
	}

	public ProdCamp getProdCampion() {
		return prodCampion;
	}

	@Override
	public String getQuery() {
		if (!perOrdine) {
			return getQueryMovimenti();
		}
		else {
			return getQueryOrdini();
		}
	}

	private String getQueryMovimenti() {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

		final StringBuffer q = new StringBuffer(
				"SELECT r FROM Tmovrig r WHERE (r.testa.mftido = 'VEN' OR r.testa.mftido = 'VEL' OR r.testa.mftido = 'NCR' "
						+ "OR r.testa.mftido='NCT' OR r.testa.mftido='NCL' OR r.testa.mftido='FAC') AND (r.mrarti NOT LIKE '*%') ");
		boolean stagio = false;
		if (stagioni != null && stagioni.size() > 0) {
			// STAGIONE
			if (stagioni.size() == 1) {
				q.append(" AND r.rigaOrdine.orcoan = '");
				q.append(stagioni.get(0));
				q.append("'");
				stagio = true;
			}
			else {
				q.append(" AND (");
				final int i = stagioni.size();
				for (int j = 0; j < stagioni.size(); j++) {
					q.append("r.rigaOrdine.orcoan = '");
					q.append(stagioni.get(j));
					q.append("'");
					if (j != i - 1) {
						q.append(" OR ");
					}
				}
				q.append(")");
				stagio = true;
			}
		}
		if (dallaData != null && allaData != null && !stagio) {
			// DA DATA A final DATA
			final String daData = dallaData.format(formatter);
			final String aData = allaData.format(formatter);
			q.append(" AND r.testa.mfdado >= '");
			q.append(daData);
			q.append("' AND r.testa.mfdado <= '");
			q.append(aData);
			q.append("'");
		}
		// TIPO TESSUTO
		if (tipoTessuto != null) {
			final String s = tipoTessuto.getString();
			q.append(" AND matpri.mtfami = '");
			q.append(s);
			q.append("'");
		}
		// ANNO PRECEDENTE fa due ricerche vedi SCRIBAFSERVICE
		// RAPPRESENTANTE
		if (rappresentante != null) {
			q.append(" AND r.testa.mfrapp = '");
			q.append(rappresentante);
			q.append("'");
		}
		// CLIENTI: in prisma sono tutti scemi
		if (!selCliente.isEmpty()) {
			final String cli = selCliente.get(0);
			q.append(" AND (");
			q.append("r.testa.mfana1 = '");
			q.append(cli);
			q.append("' OR r.testa.mfana2 = '");
			q.append(cli);
			q.append("')");
		}

		//		if (stagio) {
		//			q.append("ORDER BY r.rigaOrdine.orcoan");
		//		}
		final String s = q.toString();
		System.err.println(">>>>> ParametriAnalisiVendite-getQuery <<<<<< " + s);
		return s;
	}

	private CriteriaQuery<Tuple> getQueryMovimenti(CriteriaBuilder cb) {
		/*
		 * x controllare se italia /estero campo anesiv == "" e' italiano altro estero
		 */
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.LEFT);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		//		Root<Anagra> anagra = testata.join(Tmovmag_.mfana2, JoinType.INNER);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		if (cliente) {
			selezioni.add(testata.get(Tmovmag_.mfana2)
					.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
			//			selezioni.add(testata.get(Tmovmag_.mfana1)
			//					.alias(AliasPerCriteriaQuery.codCli1.toString())); //codcli
		}
		else {
			selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
					.alias(AliasPerCriteriaQuery.codArticolo.toString())); // codArticolo
		}
		selezioni.add(cb.sum(tmovRoot.get(Tmovrig_.mrqtan))
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni
				.add(cb
						.sum(cb.prod(tmovRoot.get(Tmovrig_.mrqtan),
								cb.sum(tmovRoot.get(Tmovrig_.mrprli),
										cb.prod(tmovRoot.get(Tmovrig_.mrprli), cb.quot(tmovRoot.get(Tmovrig_.mrmag1), 100)))))
						.alias(AliasPerCriteriaQuery.valore.toString())); //valore

		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		// tipo
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		// articolo con asterisco
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// divisa si sceglie solo EUR o blank
		List<Predicate> pDiv = new ArrayList<>();
		pDiv.add(cb.equal(testata.get(Tmovmag_.mfdivi), "EUR"));
		pDiv.add(cb.equal(testata.get(Tmovmag_.mfdivi), ""));
		Predicate ppDiv = cb.or(pDiv.toArray(new Predicate[pDiv.size()]));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// stagioni
		List<Predicate> pStagio = faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		if (!pStagio.isEmpty()) {
			if (pStagio.size() == 1) {
				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
			else {
				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
		}
		// date
		List<Predicate> pDates = faiPredicatoPerDate(cb, tmovRoot, testata);
		if (!pDates.isEmpty()) {
			Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
			addPreds.add(pDate);
		}
		// tipo tessuto
		Predicate tipoTessuto = faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		// rappresentante
		Predicate rappPred = faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// GROUP
		if (cliente) {
			cq.groupBy(testata.get(Tmovmag_.mfana2));
		}
		else {
			cq.groupBy(tmovRoot.get(Tmovrig_.mrarti));
		}
		// FINAL
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll, ppDiv);

		return cq;
	}

	public CriteriaQuery<Tuple> getQueryMovimentiInValuta(CriteriaBuilder cb, ParametriAnalisiVendite parametri) {

		CriteriaQuery<Tuple> cqq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cqq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		Join<Tmovrig, Ordini> ordine = null;
		if (margineLordo) {
			ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.INNER);
		}
		else {
			ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.LEFT);
		}
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		selezioni.add(testata.get(Tmovmag_.mfana2)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana1)
				.alias(AliasPerCriteriaQuery.codCli1.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString()));
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); // codArticolo
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString()));
		selezioni.add(ordine.get(Ordini_.orabpc)
				.alias(AliasPerCriteriaQuery.costo.toString()));
		selezioni.add(ordine.get(Ordini_.orabmt)
				.alias(AliasPerCriteriaQuery.costoProduzione.toString()));
		// DEFINIZIONE DEI RISULTATI
		cqq.multiselect(selezioni);
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		// tipo
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		// articolo con asterisco
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));

		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// divisa si sceglie solo EUR o blank
		List<Predicate> pDiv = new ArrayList<>();
		pDiv.add(cb.notEqual(testata.get(Tmovmag_.mfdivi), "EUR"));
		pDiv.add(cb.notEqual(testata.get(Tmovmag_.mfdivi), ""));
		addPreds.add(cb.and(pDiv.toArray(new Predicate[pDiv.size()])));
		// stagioni
		List<Predicate> pStagio = faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		if (!pStagio.isEmpty()) {
			if (pStagio.size() == 1) {
				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
			else {
				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
		}
		// date
		List<Predicate> pDates = faiPredicatoPerDate(cb, tmovRoot, testata);
		if (!pDates.isEmpty()) {
			Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
			addPreds.add(pDate);
		}
		// tipo tessuto
		Predicate tipoTessuto = faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		// rappresentante
		Predicate rappPred = faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}

		// FINAL
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cqq.where(p, pAnd, pAll);
		return cqq;
	}

	public CriteriaQuery<Tuple> getQueryMovimentiNoteCredito(CriteriaBuilder cb, ParametriAnalisiVendite parametri, String daData,
			String aData) {
		//		System.err.println(">>>>> ParametriAnalisiVendite-getQueryMovimentiNoteCredito <<<<<< ");
		CriteriaQuery<Tuple> cqq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cqq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.LEFT);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		selezioni.add(testata.get(Tmovmag_.mfana2)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString()));
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); // codArticolo
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString()));
		// DEFINIZIONE DEI RISULTATI
		cqq.multiselect(selezioni);
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		// tipo
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		// articolo con asterisco
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));

		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// divisa si sceglie solo EUR o blank
		//		List<Predicate> pDiv = new ArrayList<>();
		//		pDiv.add(cb.notEqual(testata.get(Tmovmag_.mfdivi), "EUR"));
		//		pDiv.add(cb.notEqual(testata.get(Tmovmag_.mfdivi), "   "));
		//		addPreds.add(cb.and(pDiv.toArray(new Predicate[pDiv.size()])));
		// stagioni PER LE NOTE CREDITO NON ESISTE STAGIONE
		List<Predicate> pStagio = faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		if (!pStagio.isEmpty()) {
			if (pStagio.size() == 1) {
				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
			else {
				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
		}
		// date
		//		if (pStagio.isEmpty()) {
		//DATE DERIVATE DA MIN E MAX
		List<Predicate> datePreds = new ArrayList<>();
		Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), daData);
		Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), aData);
		datePreds.add(daDataPred);
		datePreds.add(aDataPred);
		Predicate pDate = cb.and(datePreds.toArray(new Predicate[datePreds.size()]));
		addPreds.add(pDate);
		//		}
		// tipo tessuto
		Predicate tipoTessuto = faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		// rappresentante
		Predicate rappPred = faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}

		// FINAL
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cqq.where(p, pAnd, pAll);
		return cqq;
	}

	public CriteriaQuery<Tuple> getQueryMovimentiNoteNCT(CriteriaBuilder cb, ParametriAnalisiVendite parametri, String daData,
			String aData) {
		CriteriaQuery<Tuple> cqq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cqq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.LEFT);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		selezioni.add(testata.get(Tmovmag_.mfana1)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString()));
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); // codArticolo
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtal)
				.alias(AliasPerCriteriaQuery.qtaL.toString())); //qta
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString()));
		selezioni.add(testata.get(Tmovmag_.mfkey)
				.alias(AliasPerCriteriaQuery.numMov.toString()));
		// DEFINIZIONE DEI RISULTATI
		cqq.multiselect(selezioni);
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		// tipo

		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		// articolo con asterisco
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// divisa si sceglie solo EUR o blank
		//		List<Predicate> pDiv = new ArrayList<>();
		//		pDiv.add(cb.notEqual(testata.get(Tmovmag_.mfdivi), "EUR"));
		//		pDiv.add(cb.notEqual(testata.get(Tmovmag_.mfdivi), "   "));
		//		addPreds.add(cb.and(pDiv.toArray(new Predicate[pDiv.size()])));
		// stagioni PER LE NOTE CREDITO NON ESISTE STAGIONE
		//		List<Predicate> pStagio = faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		//		if (!pStagio.isEmpty()) {
		//			if (pStagio.size() == 1) {
		//				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
		//				addPreds.add(pStagios);
		//			}
		//			else {
		//				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
		//				addPreds.add(pStagios);
		//			}
		//		}
		// date
		//		if (pStagio.isEmpty()) {
		//DATE DERIVATE DA MIN E MAX
		List<Predicate> datePreds = new ArrayList<>();
		Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), daData);
		Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), aData);
		datePreds.add(daDataPred);
		datePreds.add(aDataPred);
		Predicate pDate = cb.and(datePreds.toArray(new Predicate[datePreds.size()]));
		addPreds.add(pDate);
		//		}
		// tipo tessuto
		Predicate tipoTessuto = faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		// rappresentante
		Predicate rappPred = faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = faiPredicatoPerClientiNCT(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}

		// FINAL
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cqq.where(p, pAnd, pAll);
		return cqq;
	}

	public CriteriaQuery<Tuple> getQueryMovimentiPerMargineLordo(CriteriaBuilder cb) {
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.INNER);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		//		Join<Ordini, Teordi> testOrdine = ordine.join(Ordini_.testata);
		//		Join<Teordi, Telist0f> testListino = ordine.join(Ordini_.testata)
		//				.join(Teordi_.testaListino);
		//		//		Root<Anagra> anagra = testata.join(Tmovmag_.mfana2, JoinType.INNER);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		if (cliente) {
			selezioni.add(testata.get(Tmovmag_.mfana2)
					.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		}
		else {
			selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
					.alias(AliasPerCriteriaQuery.codArticolo.toString())); // codArticolo
		}
		//		mrqtan
		//		mrqtft big dec
		//		orabpc costo
		//		orabpc costo produzione
		selezioni.add(cb.sum(tmovRoot.get(Tmovrig_.mrqtan))
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni
				.add(cb
						.sum(cb.prod(tmovRoot.get(Tmovrig_.mrqtan),
								cb.sum(tmovRoot.get(Tmovrig_.mrprli),
										cb.prod(tmovRoot.get(Tmovrig_.mrprli), cb.quot(tmovRoot.get(Tmovrig_.mrmag1), 100)))))
						.alias(AliasPerCriteriaQuery.valore.toString())); //valore
		selezioni.add(cb.sum(cb.prod(tmovRoot.get(Tmovrig_.mrqtan), ordine.get(Ordini_.orabpc))));
		if (tipoTessuto != null && tipoTessuto.equals(TipoTessuto.SCIARPE)) {
			selezioni.add(cb.prod(tmovRoot.get(Tmovrig_.mrqtan), cb.sum(ordine.get(Ordini_.orabmt))));
		}
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		// tipo
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		// articolo con asterisco
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// divisa si sceglie solo EUR o blank
		List<Predicate> pDiv = new ArrayList<>();
		pDiv.add(cb.equal(testata.get(Tmovmag_.mfdivi), "EUR"));
		pDiv.add(cb.equal(testata.get(Tmovmag_.mfdivi), ""));
		Predicate ppDiv = cb.or(pDiv.toArray(new Predicate[pDiv.size()]));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// stagioni
		List<Predicate> pStagio = faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		if (!pStagio.isEmpty()) {
			if (pStagio.size() == 1) {
				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
			else {
				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
		}
		// date
		List<Predicate> pDates = faiPredicatoPerDate(cb, tmovRoot, testata);
		if (!pDates.isEmpty()) {
			Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
			addPreds.add(pDate);
		}
		// tipo tessuto
		Predicate tipoTessuto = faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		// rappresentante
		Predicate rappPred = faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// GROUP
		if (cliente) {
			cq.groupBy(testata.get(Tmovmag_.mfana2));
		}
		else {
			cq.groupBy(tmovRoot.get(Tmovrig_.mrarti));
		}
		// FINAL
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll, ppDiv);

		return cq;
	}

	private String getQueryOrdini() {
		final StringBuffer q = new StringBuffer("SELECT o FROM Ordini o WHERE (o.ordprop <> 'P' OR o.procam <> 'O') ");
		return null;
	}

	private CriteriaQuery<?> getQueryOrdini(CriteriaBuilder cb) {
		// TODO Auto-generated method stub
		return null;
	}

	public Busta getRappresentante() {
		return rappresentante;
	}

	public List<String> getSelCliente() {
		return selCliente;
	}

	public String getSelClienteStr() {
		String ret = "";
		if (selCliente.size() > 0) {
			ret = StringUtils.join(selCliente, ",");
		}
		return ret;
	}

	public List<String> getStagioni() {
		return stagioni;
	}

	public String getStagioniStr() {
		String ret = "";
		if (stagioni.size() > 0) {
			ret = StringUtils.join(stagioni, ",");
		}
		return ret;
	}

	public TipoTessuto getTipoTessuto() {
		return tipoTessuto;
	}

	public Boolean getVenditeOrdini() {
		return venditeOrdini;
	}

	public void setAllaData(LocalDate allaData) {
		this.allaData = allaData;
	}

	public void setAnnoPrecedente(Boolean annoPrecedente) {
		this.annoPrecedente = annoPrecedente;
	}

	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}

	public void setDallaData(LocalDate dallaData) {
		this.dallaData = dallaData;
	}

	public void setExcludeCamp(Boolean excludeCamp) {
		this.excludeCamp = excludeCamp;
	}

	public void setLineaProd(List<String> lineaProd) {
		this.lineaProd = lineaProd;
	}

	public void setLineaProdStr(String s) {
		if (s.length() > 0) {
			lineaProd = new ArrayList<>();
			if (!s.contains(",")) {
				lineaProd.add(s);
			}
			else {
				final String[] ss = s.split(",");
				for (final String string : ss) {
					lineaProd.add(string);
				}
			}
		}
	}

	public void setMargineLordo(Boolean margineLordo) {
		this.margineLordo = margineLordo;
	}

	public void setPaese(ItaliaEstero paese) {
		this.paese = paese;
	}

	public void setPerOrdine(Boolean perOrdine) {
		this.perOrdine = perOrdine;
	}

	public void setProdCampion(ProdCamp prodCampion) {
		this.prodCampion = prodCampion;
	}

	public void setRappresentante(Busta rappresentante) {
		this.rappresentante = rappresentante;
	}

	public void setSelCliente(List<String> selCliente) {
		this.selCliente = selCliente;
	}

	public void setSelClientiStr(String clientiStr) {
		if (clientiStr.length() > 0) {
			if (!clientiStr.contains(",")) {
				selCliente.add(clientiStr);
			}
			else {
				final String[] clis = clientiStr.split(",");
				for (final String string : clis) {
					selCliente.add(string);
				}

			}
		}

	}

	public void setStagioneStr(String stagioneStr) {
		if (stagioneStr.length() > 0) {
			stagioni = new ArrayList<>();
			if (!stagioneStr.contains(",")) {
				stagioni.add(stagioneStr);
			}
			else {
				final String[] stagios = stagioneStr.split(",");
				for (final String string : stagios) {
					stagioni.add(string);
				}

			}
		}
	}

	public void setStagioni(List<String> stagioni) {
		this.stagioni = stagioni;
	}

	public void setTipoTessuto(TipoTessuto tipoTessuto) {
		this.tipoTessuto = tipoTessuto;
	}

	public void setVenditeOrdini(Boolean venditeOrdini) {
		this.venditeOrdini = venditeOrdini;
	}

}
