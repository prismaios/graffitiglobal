package it.prismatdi.model.scribaf;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the anagra database table.
 *
 */
@Entity
@Table(name = "anagra")
//@NamedQueries({ @NamedQuery(name = "Anagra.findAll", query = "SELECT a FROM Anagra a"),
//		@NamedQuery(name = "Anagra.findAllPerAgente", query = "SELECT a FROM Anagra a"),
//		@NamedQuery(name = "Anagra.findCliente", query = "SELECT DISTINCT w FROM Anagra AS w WHERE w.id = :paramPK"),
//		@NamedQuery(name = "Anagra.findAllClienti",
//				query = "SELECT DISTINCT w FROM Anagra AS w WHERE w.id.anditt = :paramDitta AND w.id.anmaco = :paramConto AND w.anticf = 'C' ORDER BY w.anraso") })
public class Anagra implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private AnagraPK				id;

	private String					anabic;

	private String					anaccv;

	private BigDecimal			anarac;

	private BigDecimal			anareg;

	private String					anbanc;

	private String					anblkl;

	private String					ancad;

	private String					ancadd;

	private String					ancap;

	private BigDecimal			ancapd;

	private String					ancban;

	private String					ancbes;

	private String					ancdas;

	private String					ancfbk;

	private String					ancide;

	private String					ancipt;

	private String					ancitt;

	private String					anclas;

	private BigDecimal			ancoa$;

	private String					ancoc2;

	private String					ancoc3;

	private String					ancoc4;

	private String					ancoco;

	private BigDecimal			ancod$;

	private String					ancofi;

	private String					ancona;

	private BigDecimal			ancpac;

	private BigDecimal			ancpag;

	private BigDecimal			ancpoc;

	private BigDecimal			ancpor;

	private BigDecimal			ancra3;

	private BigDecimal			ancrap;

	private BigDecimal			ancspc;

	private BigDecimal			ancspe;

	private BigDecimal			andaca;

	private BigDecimal			andana;

	private BigDecimal			andans;

	private BigDecimal			andarl;

	private BigDecimal			andatt;

	private String					andest;

	private BigDecimal			andima;

	private String					andisp;

	private BigDecimal			andivi;

	private String					andol;

	private BigDecimal			andreg;

	private String					anelim;

	private String					aneqclfo;

	private String					aneqclifo;

	private String					anesiv;

	private String					anfax;

	private BigDecimal			anfido;

	private String					anfil2;

	private String					anfilc;

	private String					anfill;

	private String					anfrap;

	private String					anfspe;

	private String					angrco;

	private String					angrum;

	private String					anibes;

	private String					aninca;

	private String					aninde;

	private String					anindi;

	private String					anines;

	private String					aninpt;

	private String					aninte;

	private BigDecimal			anling;

	private String					anlis1;

	private String					anlis2;

	private String					anlis3;

	private String					anmail;

	private BigDecimal			annatt;

	private String					annazi;

	private String					annbes;

	private String					annodi;

	private String					annot1;

	private String					annot2;

	private String					annot3;

	private BigDecimal			annreg;

	private String					anpadr;

	private String					anpasa;

	private String					anpate;

	private BigDecimal			anpep2;

	private BigDecimal			anpepr;

	private String					anpibk;

	private BigDecimal			anpiva;

	private BigDecimal			anprac;

	private String					anprde;

	private String					anprna;

	private BigDecimal			anpro3;

	private String					anprof;

	private String					anprov;

	private BigDecimal			anprs1;

	private BigDecimal			anprsc;

	private BigDecimal			anraco;

	private String					anraes;

	private String					anrapt;

	private BigDecimal			anras1;

	private BigDecimal			anrasc;

	private String					anraso;

	private String					anrepo;

	private String					anrepx;

	private String					anria1;

	private String					anria2;

	private BigDecimal			anscon;

	private String					ansele;

	private String					anspar;

	private BigDecimal			ansps1;

	private BigDecimal			anspsc;

	private BigDecimal			ansta1;

	private BigDecimal			ansta2;

	private BigDecimal			ansta3;

	private String					antele;

	private String					antelx;

	private String					anticf;

	private String					anticl;

	private String					antif1;

	private String					antifo;

	private String					anufco;

	private String					anzobk;

	private String					anzona;

	private String					datacrea;

	private String					datamod;

	private String					utecrea;

	private String					utemod;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Anagra other = (Anagra) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getAnabic() {
		return anabic;
	}

	public String getAnaccv() {
		return anaccv;
	}

	public BigDecimal getAnarac() {
		return anarac;
	}

	public BigDecimal getAnareg() {
		return anareg;
	}

	public String getAnbanc() {
		return anbanc;
	}

	public String getAnblkl() {
		return anblkl;
	}

	public String getAncad() {
		return ancad;
	}

	public String getAncadd() {
		return ancadd;
	}

	public String getAncap() {
		return ancap;
	}

	public BigDecimal getAncapd() {
		return ancapd;
	}

	public String getAncban() {
		return ancban;
	}

	public String getAncbes() {
		return ancbes;
	}

	public String getAncdas() {
		return ancdas;
	}

	public String getAncfbk() {
		return ancfbk;
	}

	public String getAncide() {
		return ancide;
	}

	public String getAncipt() {
		return ancipt;
	}

	public String getAncitt() {
		return ancitt;
	}

	public String getAnclas() {
		return anclas;
	}

	public BigDecimal getAncoa$() {
		return ancoa$;
	}

	public String getAncoc2() {
		return ancoc2;
	}

	public String getAncoc3() {
		return ancoc3;
	}

	public String getAncoc4() {
		return ancoc4;
	}

	public String getAncoco() {
		return ancoco;
	}

	public BigDecimal getAncod$() {
		return ancod$;
	}

	public String getAncofi() {
		return ancofi;
	}

	public String getAncona() {
		return ancona;
	}

	public BigDecimal getAncpac() {
		return ancpac;
	}

	public BigDecimal getAncpag() {
		return ancpag;
	}

	public BigDecimal getAncpoc() {
		return ancpoc;
	}

	public BigDecimal getAncpor() {
		return ancpor;
	}

	public BigDecimal getAncra3() {
		return ancra3;
	}

	public BigDecimal getAncrap() {
		return ancrap;
	}

	public BigDecimal getAncspc() {
		return ancspc;
	}

	public BigDecimal getAncspe() {
		return ancspe;
	}

	public BigDecimal getAndaca() {
		return andaca;
	}

	public BigDecimal getAndana() {
		return andana;
	}

	public BigDecimal getAndans() {
		return andans;
	}

	public BigDecimal getAndarl() {
		return andarl;
	}

	public BigDecimal getAndatt() {
		return andatt;
	}

	public String getAndest() {
		return andest;
	}

	public BigDecimal getAndima() {
		return andima;
	}

	public String getAndisp() {
		return andisp;
	}

	public BigDecimal getAndivi() {
		return andivi;
	}

	public String getAndol() {
		return andol;
	}

	public BigDecimal getAndreg() {
		return andreg;
	}

	public String getAnelim() {
		return anelim;
	}

	public String getAneqclfo() {
		return aneqclfo;
	}

	public String getAneqclifo() {
		return aneqclifo;
	}

	public String getAnesiv() {
		return anesiv;
	}

	public String getAnfax() {
		return anfax;
	}

	public BigDecimal getAnfido() {
		return anfido;
	}

	public String getAnfil2() {
		return anfil2;
	}

	public String getAnfilc() {
		return anfilc;
	}

	public String getAnfill() {
		return anfill;
	}

	public String getAnfrap() {
		return anfrap;
	}

	public String getAnfspe() {
		return anfspe;
	}

	public String getAngrco() {
		return angrco;
	}

	public String getAngrum() {
		return angrum;
	}

	public String getAnibes() {
		return anibes;
	}

	public String getAninca() {
		return aninca;
	}

	public String getAninde() {
		return aninde;
	}

	public String getAnindi() {
		return anindi;
	}

	public String getAnines() {
		return anines;
	}

	public String getAninpt() {
		return aninpt;
	}

	public String getAninte() {
		return aninte;
	}

	public BigDecimal getAnling() {
		return anling;
	}

	public String getAnlis1() {
		return anlis1;
	}

	public String getAnlis2() {
		return anlis2;
	}

	public String getAnlis3() {
		return anlis3;
	}

	public String getAnmail() {
		return anmail;
	}

	public BigDecimal getAnnatt() {
		return annatt;
	}

	public String getAnnazi() {
		return annazi;
	}

	public String getAnnbes() {
		return annbes;
	}

	public String getAnnodi() {
		return annodi;
	}

	public String getAnnot1() {
		return annot1;
	}

	public String getAnnot2() {
		return annot2;
	}

	public String getAnnot3() {
		return annot3;
	}

	public BigDecimal getAnnreg() {
		return annreg;
	}

	public String getAnpadr() {
		return anpadr;
	}

	public String getAnpasa() {
		return anpasa;
	}

	public String getAnpate() {
		return anpate;
	}

	public BigDecimal getAnpep2() {
		return anpep2;
	}

	public BigDecimal getAnpepr() {
		return anpepr;
	}

	public String getAnpibk() {
		return anpibk;
	}

	public BigDecimal getAnpiva() {
		return anpiva;
	}

	public BigDecimal getAnprac() {
		return anprac;
	}

	public String getAnprde() {
		return anprde;
	}

	public String getAnprna() {
		return anprna;
	}

	public BigDecimal getAnpro3() {
		return anpro3;
	}

	public String getAnprof() {
		return anprof;
	}

	public String getAnprov() {
		return anprov;
	}

	public BigDecimal getAnprs1() {
		return anprs1;
	}

	public BigDecimal getAnprsc() {
		return anprsc;
	}

	public BigDecimal getAnraco() {
		return anraco;
	}

	public String getAnraes() {
		return anraes;
	}

	public String getAnrapt() {
		return anrapt;
	}

	public BigDecimal getAnras1() {
		return anras1;
	}

	public BigDecimal getAnrasc() {
		return anrasc;
	}

	public String getAnraso() {
		return anraso;
	}

	public String getAnrepo() {
		return anrepo;
	}

	public String getAnrepx() {
		return anrepx;
	}

	public String getAnria1() {
		return anria1;
	}

	public String getAnria2() {
		return anria2;
	}

	public BigDecimal getAnscon() {
		return anscon;
	}

	public String getAnsele() {
		return ansele;
	}

	public String getAnspar() {
		return anspar;
	}

	public BigDecimal getAnsps1() {
		return ansps1;
	}

	public BigDecimal getAnspsc() {
		return anspsc;
	}

	public BigDecimal getAnsta1() {
		return ansta1;
	}

	public BigDecimal getAnsta2() {
		return ansta2;
	}

	public BigDecimal getAnsta3() {
		return ansta3;
	}

	public String getAntele() {
		return antele;
	}

	public String getAntelx() {
		return antelx;
	}

	public String getAnticf() {
		return anticf;
	}

	public String getAnticl() {
		return anticl;
	}

	public String getAntif1() {
		return antif1;
	}

	public String getAntifo() {
		return antifo;
	}

	public String getAnufco() {
		return anufco;
	}

	public String getAnzobk() {
		return anzobk;
	}

	public String getAnzona() {
		return anzona;
	}

	public String getDatacrea() {
		return datacrea;
	}

	public String getDatamod() {
		return datamod;
	}

	public AnagraPK getId() {
		return id;
	}

	public String getUtecrea() {
		return utecrea;
	}

	public String getUtemod() {
		return utemod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setAnabic(String anabic) {
		this.anabic = anabic;
	}

	public void setAnaccv(String anaccv) {
		this.anaccv = anaccv;
	}

	public void setAnarac(BigDecimal anarac) {
		this.anarac = anarac;
	}

	public void setAnareg(BigDecimal anareg) {
		this.anareg = anareg;
	}

	public void setAnbanc(String anbanc) {
		this.anbanc = anbanc;
	}

	public void setAnblkl(String anblkl) {
		this.anblkl = anblkl;
	}

	public void setAncad(String ancad) {
		this.ancad = ancad;
	}

	public void setAncadd(String ancadd) {
		this.ancadd = ancadd;
	}

	public void setAncap(String ancap) {
		this.ancap = ancap;
	}

	public void setAncapd(BigDecimal ancapd) {
		this.ancapd = ancapd;
	}

	public void setAncban(String ancban) {
		this.ancban = ancban;
	}

	public void setAncbes(String ancbes) {
		this.ancbes = ancbes;
	}

	public void setAncdas(String ancdas) {
		this.ancdas = ancdas;
	}

	public void setAncfbk(String ancfbk) {
		this.ancfbk = ancfbk;
	}

	public void setAncide(String ancide) {
		this.ancide = ancide;
	}

	public void setAncipt(String ancipt) {
		this.ancipt = ancipt;
	}

	public void setAncitt(String ancitt) {
		this.ancitt = ancitt;
	}

	public void setAnclas(String anclas) {
		this.anclas = anclas;
	}

	public void setAncoa$(BigDecimal ancoa$) {
		this.ancoa$ = ancoa$;
	}

	public void setAncoc2(String ancoc2) {
		this.ancoc2 = ancoc2;
	}

	public void setAncoc3(String ancoc3) {
		this.ancoc3 = ancoc3;
	}

	public void setAncoc4(String ancoc4) {
		this.ancoc4 = ancoc4;
	}

	public void setAncoco(String ancoco) {
		this.ancoco = ancoco;
	}

	public void setAncod$(BigDecimal ancod$) {
		this.ancod$ = ancod$;
	}

	public void setAncofi(String ancofi) {
		this.ancofi = ancofi;
	}

	public void setAncona(String ancona) {
		this.ancona = ancona;
	}

	public void setAncpac(BigDecimal ancpac) {
		this.ancpac = ancpac;
	}

	public void setAncpag(BigDecimal ancpag) {
		this.ancpag = ancpag;
	}

	public void setAncpoc(BigDecimal ancpoc) {
		this.ancpoc = ancpoc;
	}

	public void setAncpor(BigDecimal ancpor) {
		this.ancpor = ancpor;
	}

	public void setAncra3(BigDecimal ancra3) {
		this.ancra3 = ancra3;
	}

	public void setAncrap(BigDecimal ancrap) {
		this.ancrap = ancrap;
	}

	public void setAncspc(BigDecimal ancspc) {
		this.ancspc = ancspc;
	}

	public void setAncspe(BigDecimal ancspe) {
		this.ancspe = ancspe;
	}

	public void setAndaca(BigDecimal andaca) {
		this.andaca = andaca;
	}

	public void setAndana(BigDecimal andana) {
		this.andana = andana;
	}

	public void setAndans(BigDecimal andans) {
		this.andans = andans;
	}

	public void setAndarl(BigDecimal andarl) {
		this.andarl = andarl;
	}

	public void setAndatt(BigDecimal andatt) {
		this.andatt = andatt;
	}

	public void setAndest(String andest) {
		this.andest = andest;
	}

	public void setAndima(BigDecimal andima) {
		this.andima = andima;
	}

	public void setAndisp(String andisp) {
		this.andisp = andisp;
	}

	public void setAndivi(BigDecimal andivi) {
		this.andivi = andivi;
	}

	public void setAndol(String andol) {
		this.andol = andol;
	}

	public void setAndreg(BigDecimal andreg) {
		this.andreg = andreg;
	}

	public void setAnelim(String anelim) {
		this.anelim = anelim;
	}

	public void setAneqclfo(String aneqclfo) {
		this.aneqclfo = aneqclfo;
	}

	public void setAneqclifo(String aneqclifo) {
		this.aneqclifo = aneqclifo;
	}

	public void setAnesiv(String anesiv) {
		this.anesiv = anesiv;
	}

	public void setAnfax(String anfax) {
		this.anfax = anfax;
	}

	public void setAnfido(BigDecimal anfido) {
		this.anfido = anfido;
	}

	public void setAnfil2(String anfil2) {
		this.anfil2 = anfil2;
	}

	public void setAnfilc(String anfilc) {
		this.anfilc = anfilc;
	}

	public void setAnfill(String anfill) {
		this.anfill = anfill;
	}

	public void setAnfrap(String anfrap) {
		this.anfrap = anfrap;
	}

	public void setAnfspe(String anfspe) {
		this.anfspe = anfspe;
	}

	public void setAngrco(String angrco) {
		this.angrco = angrco;
	}

	public void setAngrum(String angrum) {
		this.angrum = angrum;
	}

	public void setAnibes(String anibes) {
		this.anibes = anibes;
	}

	public void setAninca(String aninca) {
		this.aninca = aninca;
	}

	public void setAninde(String aninde) {
		this.aninde = aninde;
	}

	public void setAnindi(String anindi) {
		this.anindi = anindi;
	}

	public void setAnines(String anines) {
		this.anines = anines;
	}

	public void setAninpt(String aninpt) {
		this.aninpt = aninpt;
	}

	public void setAninte(String aninte) {
		this.aninte = aninte;
	}

	public void setAnling(BigDecimal anling) {
		this.anling = anling;
	}

	public void setAnlis1(String anlis1) {
		this.anlis1 = anlis1;
	}

	public void setAnlis2(String anlis2) {
		this.anlis2 = anlis2;
	}

	public void setAnlis3(String anlis3) {
		this.anlis3 = anlis3;
	}

	public void setAnmail(String anmail) {
		this.anmail = anmail;
	}

	public void setAnnatt(BigDecimal annatt) {
		this.annatt = annatt;
	}

	public void setAnnazi(String annazi) {
		this.annazi = annazi;
	}

	public void setAnnbes(String annbes) {
		this.annbes = annbes;
	}

	public void setAnnodi(String annodi) {
		this.annodi = annodi;
	}

	public void setAnnot1(String annot1) {
		this.annot1 = annot1;
	}

	public void setAnnot2(String annot2) {
		this.annot2 = annot2;
	}

	public void setAnnot3(String annot3) {
		this.annot3 = annot3;
	}

	public void setAnnreg(BigDecimal annreg) {
		this.annreg = annreg;
	}

	public void setAnpadr(String anpadr) {
		this.anpadr = anpadr;
	}

	public void setAnpasa(String anpasa) {
		this.anpasa = anpasa;
	}

	public void setAnpate(String anpate) {
		this.anpate = anpate;
	}

	public void setAnpep2(BigDecimal anpep2) {
		this.anpep2 = anpep2;
	}

	public void setAnpepr(BigDecimal anpepr) {
		this.anpepr = anpepr;
	}

	public void setAnpibk(String anpibk) {
		this.anpibk = anpibk;
	}

	public void setAnpiva(BigDecimal anpiva) {
		this.anpiva = anpiva;
	}

	public void setAnprac(BigDecimal anprac) {
		this.anprac = anprac;
	}

	public void setAnprde(String anprde) {
		this.anprde = anprde;
	}

	public void setAnprna(String anprna) {
		this.anprna = anprna;
	}

	public void setAnpro3(BigDecimal anpro3) {
		this.anpro3 = anpro3;
	}

	public void setAnprof(String anprof) {
		this.anprof = anprof;
	}

	public void setAnprov(String anprov) {
		this.anprov = anprov;
	}

	public void setAnprs1(BigDecimal anprs1) {
		this.anprs1 = anprs1;
	}

	public void setAnprsc(BigDecimal anprsc) {
		this.anprsc = anprsc;
	}

	public void setAnraco(BigDecimal anraco) {
		this.anraco = anraco;
	}

	public void setAnraes(String anraes) {
		this.anraes = anraes;
	}

	public void setAnrapt(String anrapt) {
		this.anrapt = anrapt;
	}

	public void setAnras1(BigDecimal anras1) {
		this.anras1 = anras1;
	}

	public void setAnrasc(BigDecimal anrasc) {
		this.anrasc = anrasc;
	}

	public void setAnraso(String anraso) {
		this.anraso = anraso;
	}

	public void setAnrepo(String anrepo) {
		this.anrepo = anrepo;
	}

	public void setAnrepx(String anrepx) {
		this.anrepx = anrepx;
	}

	public void setAnria1(String anria1) {
		this.anria1 = anria1;
	}

	public void setAnria2(String anria2) {
		this.anria2 = anria2;
	}

	public void setAnscon(BigDecimal anscon) {
		this.anscon = anscon;
	}

	public void setAnsele(String ansele) {
		this.ansele = ansele;
	}

	public void setAnspar(String anspar) {
		this.anspar = anspar;
	}

	public void setAnsps1(BigDecimal ansps1) {
		this.ansps1 = ansps1;
	}

	public void setAnspsc(BigDecimal anspsc) {
		this.anspsc = anspsc;
	}

	public void setAnsta1(BigDecimal ansta1) {
		this.ansta1 = ansta1;
	}

	public void setAnsta2(BigDecimal ansta2) {
		this.ansta2 = ansta2;
	}

	public void setAnsta3(BigDecimal ansta3) {
		this.ansta3 = ansta3;
	}

	public void setAntele(String antele) {
		this.antele = antele;
	}

	public void setAntelx(String antelx) {
		this.antelx = antelx;
	}

	public void setAnticf(String anticf) {
		this.anticf = anticf;
	}

	public void setAnticl(String anticl) {
		this.anticl = anticl;
	}

	public void setAntif1(String antif1) {
		this.antif1 = antif1;
	}

	public void setAntifo(String antifo) {
		this.antifo = antifo;
	}

	public void setAnufco(String anufco) {
		this.anufco = anufco;
	}

	public void setAnzobk(String anzobk) {
		this.anzobk = anzobk;
	}

	public void setAnzona(String anzona) {
		this.anzona = anzona;
	}

	public void setDatacrea(String datacrea) {
		this.datacrea = datacrea;
	}

	public void setDatamod(String datamod) {
		this.datamod = datamod;
	}

	public void setId(AnagraPK id) {
		this.id = id;
	}

	public void setUtecrea(String utecrea) {
		this.utecrea = utecrea;
	}

	public void setUtemod(String utemod) {
		this.utemod = utemod;
	}

}