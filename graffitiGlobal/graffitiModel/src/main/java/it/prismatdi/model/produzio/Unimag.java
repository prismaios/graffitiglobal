package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

/**
 * The persistent class for the unimag database table.
 *
 * Per il campionario: <br>
 * campo FASSUB = 'CM' <br>
 * campo FASEZ = 'FI' <br>
 * campo CAPRPZ = 'C' <br>
 * campo NORDIN = 0 <br>
 * campo NORDRI = 0 <br>
 * campo TIPOAR = 'S'/'T' <br>
 * campo PZLAAT = 0 <br>
 * campo QTA > 0
 *
 * Campi da fare vedere: <br>
 * ARTICO <br>
 * COLORE <br>
 * (DESC ARTICOLO) <br>
 * QTA<br>
 * BAGNO (numero pezza)<br>
 * NOTEPZ<br>
 *
 *
 *
 *
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Unimag.findAll", query = "SELECT u FROM Unimag u"), @NamedQuery(
		name = "Unimag.findCampioneConStagione",
		query = "SELECT new it.prismatdi.model.utils.UnimagData(u.artico, u.colore, u.matpri.mtdesc, u.qta, u.bagno) FROM Unimag AS u "
				+ "WHERE u.fassub = 'CM' AND u.fasez = 'FI' AND u.caprpz = 'C' AND u.nordin = 0 AND u.nordri = 0 AND u.pzlaat = 0 "
				+ "AND u.qta > 0 AND u.artico = :pCodice"),
		@NamedQuery(name = "Unimag.findCampioneConStagioneTipo",
				query = "SELECT new it.prismatdi.model.utils.UnimagData(u.artico, u.colore, u.matpri.mtdesc, u.qta, u.bagno) FROM Unimag AS u "
						+ "WHERE u.fassub = 'CM' AND u.fasez = 'FI' AND u.caprpz = 'C' AND u.nordin = 0 AND u.nordri = 0 AND u.pzlaat = 0 "
						+ "AND u.qta > 0 AND u.artico = :pCodice AND u.id.tipoar = :pTipo") })
public class Unimag implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private UnimagPK				id;

	private String					artico;

	private String					bagno;

	private String					caprpz;

	private BigDecimal			cliepr;

	private BigDecimal			colore;

	private BigDecimal			datddt;

	private String					fasez;

	private String					fassub;

	private String					flag1;

	private String					flag2;

	private String					flagpz;

	private BigDecimal			gruuni;

	private BigDecimal			nordin;

	private BigDecimal			nordri;

	private String					notepz;

	private String					notpz2;

	private BigDecimal			npezzi;

	private BigDecimal			numcol;

	private BigDecimal			numddt;

	private BigDecimal			peso;

	private String					pzcert;

	private String					pzconf;

	private String					pzdida;

	private String					pzdisp;

	private String					pzfaslav;

	private BigDecimal			pzlaat;

	private String					pzlape;

	private BigDecimal			pzmtgr;

	private String					pzpart;

	private BigDecimal			pzpesg;

	private int						pzprog;

	private BigDecimal			pzpzgr;

	private BigDecimal			pzpzor;

	private String					pzstato;

	private String					pzubic;

	private BigDecimal			qta;

	private BigDecimal			qtaabb;

	private BigDecimal			qtacar;

	private BigDecimal			qtasca;

	private String					stocpz;

	private String					telaio;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumnsOrFormulas(value = {
			@JoinColumnOrFormula(column = @JoinColumn(name = "artico", referencedColumnName = "mtarti",
					insertable = false, updatable = false, nullable = false)),
			@JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "mttipo", value = "'T'")),
			@JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "mtcolo", value = "0")) })
	private Matpri					matpri;

	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "dlarti", referencedColumnName = "artico")
	private List<Delist0f>		listini;

	public Unimag() {
	}

	public String getArtico() {
		return artico;
	}

	public String getBagno() {
		return bagno;
	}

	public String getCaprpz() {
		return caprpz;
	}

	public BigDecimal getCliepr() {
		return cliepr;
	}

	public BigDecimal getColore() {
		return colore;
	}

	public BigDecimal getDatddt() {
		return datddt;
	}

	public String getFasez() {
		return fasez;
	}

	public String getFassub() {
		return fassub;
	}

	public String getFlag1() {
		return flag1;
	}

	public String getFlag2() {
		return flag2;
	}

	public String getFlagpz() {
		return flagpz;
	}

	public BigDecimal getGruuni() {
		return gruuni;
	}

	public UnimagPK getId() {
		return id;
	}

	public List<Delist0f> getListini() {
		return listini;
	}

	public Matpri getMatpri() {
		return matpri;
	}

	public BigDecimal getNordin() {
		return nordin;
	}

	public BigDecimal getNordri() {
		return nordri;
	}

	public String getNotepz() {
		return notepz;
	}

	public String getNotpz2() {
		return notpz2;
	}

	public BigDecimal getNpezzi() {
		return npezzi;
	}

	public BigDecimal getNumcol() {
		return numcol;
	}

	public BigDecimal getNumddt() {
		return numddt;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public String getPzcert() {
		return pzcert;
	}

	public String getPzconf() {
		return pzconf;
	}

	public String getPzdida() {
		return pzdida;
	}

	public String getPzdisp() {
		return pzdisp;
	}

	public String getPzfaslav() {
		return pzfaslav;
	}

	public BigDecimal getPzlaat() {
		return pzlaat;
	}

	public String getPzlape() {
		return pzlape;
	}

	public BigDecimal getPzmtgr() {
		return pzmtgr;
	}

	public String getPzpart() {
		return pzpart;
	}

	public BigDecimal getPzpesg() {
		return pzpesg;
	}

	public int getPzprog() {
		return pzprog;
	}

	public BigDecimal getPzpzgr() {
		return pzpzgr;
	}

	public BigDecimal getPzpzor() {
		return pzpzor;
	}

	public String getPzstato() {
		return pzstato;
	}

	public String getPzubic() {
		return pzubic;
	}

	public BigDecimal getQta() {
		return qta;
	}

	public BigDecimal getQtaabb() {
		return qtaabb;
	}

	public BigDecimal getQtacar() {
		return qtacar;
	}

	public BigDecimal getQtasca() {
		return qtasca;
	}

	public String getStocpz() {
		return stocpz;
	}

	public String getTelaio() {
		return telaio;
	}

	public void setArtico(String artico) {
		this.artico = artico;
	}

	public void setBagno(String bagno) {
		this.bagno = bagno;
	}

	public void setCaprpz(String caprpz) {
		this.caprpz = caprpz;
	}

	public void setCliepr(BigDecimal cliepr) {
		this.cliepr = cliepr;
	}

	public void setColore(BigDecimal colore) {
		this.colore = colore;
	}

	public void setDatddt(BigDecimal datddt) {
		this.datddt = datddt;
	}

	public void setFasez(String fasez) {
		this.fasez = fasez;
	}

	public void setFassub(String fassub) {
		this.fassub = fassub;
	}

	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}

	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}

	public void setFlagpz(String flagpz) {
		this.flagpz = flagpz;
	}

	public void setGruuni(BigDecimal gruuni) {
		this.gruuni = gruuni;
	}

	public void setId(UnimagPK id) {
		this.id = id;
	}

	public void setListini(List<Delist0f> listini) {
		this.listini = listini;
	}

	public void setMatpri(Matpri matpri) {
		this.matpri = matpri;
	}

	public void setNordin(BigDecimal nordin) {
		this.nordin = nordin;
	}

	public void setNordri(BigDecimal nordri) {
		this.nordri = nordri;
	}

	public void setNotepz(String notepz) {
		this.notepz = notepz;
	}

	public void setNotpz2(String notpz2) {
		this.notpz2 = notpz2;
	}

	public void setNpezzi(BigDecimal npezzi) {
		this.npezzi = npezzi;
	}

	public void setNumcol(BigDecimal numcol) {
		this.numcol = numcol;
	}

	public void setNumddt(BigDecimal numddt) {
		this.numddt = numddt;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public void setPzcert(String pzcert) {
		this.pzcert = pzcert;
	}

	public void setPzconf(String pzconf) {
		this.pzconf = pzconf;
	}

	public void setPzdida(String pzdida) {
		this.pzdida = pzdida;
	}

	public void setPzdisp(String pzdisp) {
		this.pzdisp = pzdisp;
	}

	public void setPzfaslav(String pzfaslav) {
		this.pzfaslav = pzfaslav;
	}

	public void setPzlaat(BigDecimal pzlaat) {
		this.pzlaat = pzlaat;
	}

	public void setPzlape(String pzlape) {
		this.pzlape = pzlape;
	}

	public void setPzmtgr(BigDecimal pzmtgr) {
		this.pzmtgr = pzmtgr;
	}

	public void setPzpart(String pzpart) {
		this.pzpart = pzpart;
	}

	public void setPzpesg(BigDecimal pzpesg) {
		this.pzpesg = pzpesg;
	}

	public void setPzprog(int pzprog) {
		this.pzprog = pzprog;
	}

	public void setPzpzgr(BigDecimal pzpzgr) {
		this.pzpzgr = pzpzgr;
	}

	public void setPzpzor(BigDecimal pzpzor) {
		this.pzpzor = pzpzor;
	}

	public void setPzstato(String pzstato) {
		this.pzstato = pzstato;
	}

	public void setPzubic(String pzubic) {
		this.pzubic = pzubic;
	}

	public void setQta(BigDecimal qta) {
		this.qta = qta;
	}

	public void setQtaabb(BigDecimal qtaabb) {
		this.qtaabb = qtaabb;
	}

	public void setQtacar(BigDecimal qtacar) {
		this.qtacar = qtacar;
	}

	public void setQtasca(BigDecimal qtasca) {
		this.qtasca = qtasca;
	}

	public void setStocpz(String stocpz) {
		this.stocpz = stocpz;
	}

	public void setTelaio(String telaio) {
		this.telaio = telaio;
	}

}