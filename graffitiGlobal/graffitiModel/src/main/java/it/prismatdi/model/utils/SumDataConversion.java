package it.prismatdi.model.utils;

import javax.persistence.Tuple;

import it.prismatdi.general.AliasPerCriteriaQuery;

public class SumDataConversion {
	private String	codice;
	private String	descrizione;
	private Double	qta;
	private Double	qtaAp			= 0D;
	private Double	valore;
	private Double	costo			= 0D;
	private Double	costoProd	= 0D;

	private Double	valoreAp		= 0D;

	public SumDataConversion() {
		// TODO Auto-generated constructor stub
	}

	public SumDataConversion(Tuple tuple, Boolean clienti) {
		if (clienti) {
			codice = tuple.get(AliasPerCriteriaQuery.codCli.toString(), String.class);
		}
		else {
			codice = tuple.get(AliasPerCriteriaQuery.codArticolo.toString(), String.class);
		}
		qta = DataConversion.arrotonda(tuple.get(AliasPerCriteriaQuery.qta.toString(), Double.class), 2);
		Double varArr = tuple.get(AliasPerCriteriaQuery.valore.toString(), Double.class);
		if (varArr != null) {
			valore = DataConversion.arrotonda(varArr, 2);
		}
		else {
			valore = 0D;
		}
		try {
			costo = DataConversion.arrotonda(tuple.get(3, Double.class), 2);
			costoProd = DataConversion.arrotonda(tuple.get(4, Double.class), 2);
		}
		catch (Exception e) {
			//			System.err.println(">>>>> SumDataConversion-SumDataConversion <<<<<< " + e.getClass()
			//					.getName());
		}
	}

	public void addCosto(Double costo) {
		this.costo += costo;
		this.costo = DataConversion.arrotonda(this.costo, 2);
	}

	public void addCostoProd(Double costoProd) {
		this.costoProd += costoProd;
		this.costoProd = DataConversion.arrotonda(this.costoProd, 2);
	}

	public void addQta(Double qta) {
		this.qta += qta;
		this.qta = DataConversion.arrotonda(this.qta, 2);
	}

	public void addValore(Double valore) {
		this.valore += valore;
		this.valore = DataConversion.arrotonda(this.valore, 2);
	}

	public String getCodice() {
		return codice;
	}

	public Double getCosto() {
		return costo;
	}

	public Double getCostoProd() {
		return costoProd;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public Double getQta() {
		return qta;
	}

	public Double getQtaAp() {
		return qtaAp;
	}

	public Double getValore() {
		return valore;
	}

	public Double getValoreAp() {
		return valoreAp;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public void setCostoProd(Double costoProd) {
		this.costoProd = costoProd;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setQta(Double qta) {
		this.qta = qta;
	}

	public void setQtaAp(Double qtaAp) {
		this.qtaAp = qtaAp;
	}

	public void setValore(Double valore) {
		this.valore = valore;

	}

	public void setValoreAp(Double valoreAp) {
		this.valoreAp = valoreAp;
	}

}
