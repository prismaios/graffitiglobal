package it.prismatdi.model.user;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the utente database table.
 * 
 */
@Embeddable
public class UtentePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String ditcod;

	private String utecod;

	public UtentePK() {
	}
	public String getDitcod() {
		return this.ditcod;
	}
	public void setDitcod(String ditcod) {
		this.ditcod = ditcod;
	}
	public String getUtecod() {
		return this.utecod;
	}
	public void setUtecod(String utecod) {
		this.utecod = utecod;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UtentePK)) {
			return false;
		}
		UtentePK castOther = (UtentePK)other;
		return 
			this.ditcod.equals(castOther.ditcod)
			&& this.utecod.equals(castOther.utecod);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.ditcod.hashCode();
		hash = hash * prime + this.utecod.hashCode();
		
		return hash;
	}
}