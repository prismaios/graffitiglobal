package it.prismatdi.model.scribaf;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the tabel database table.
 *
 */
@Entity
@NamedQuery(name = "Tabel.findAll", query = "SELECT t FROM Tabel t")
public class Tabel implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Id
	private String					tbkey;

	private String					tbdel1;

	private String					tbdel2;

	private String					tbdel3;

	private String					tbdel4;

	private String					tbdeles;

	private String					tbdes;

	private String					tbres1;

	private String					tbrest;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tabel other = (Tabel) obj;
		if (tbkey == null) {
			if (other.tbkey != null) {
				return false;
			}
		}
		else if (!tbkey.equals(other.tbkey)) {
			return false;
		}
		return true;
	}

	public String getTbdel1() {
		return tbdel1;
	}

	public String getTbdel2() {
		return tbdel2;
	}

	public String getTbdel3() {
		return tbdel3;
	}

	public String getTbdel4() {
		return tbdel4;
	}

	public String getTbdeles() {
		return tbdeles;
	}

	public String getTbdes() {
		return tbdes;
	}

	public String getTbkey() {
		return tbkey;
	}

	public String getTbres1() {
		return tbres1;
	}

	public String getTbrest() {
		return tbrest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (tbkey == null ? 0 : tbkey.hashCode());
		return result;
	}

	public void setTbdel1(String tbdel1) {
		this.tbdel1 = tbdel1;
	}

	public void setTbdel2(String tbdel2) {
		this.tbdel2 = tbdel2;
	}

	public void setTbdel3(String tbdel3) {
		this.tbdel3 = tbdel3;
	}

	public void setTbdel4(String tbdel4) {
		this.tbdel4 = tbdel4;
	}

	public void setTbdeles(String tbdeles) {
		this.tbdeles = tbdeles;
	}

	public void setTbdes(String tbdes) {
		this.tbdes = tbdes;
	}

	public void setTbkey(String tbkey) {
		this.tbkey = tbkey;
	}

	public void setTbres1(String tbres1) {
		this.tbres1 = tbres1;
	}

	public void setTbrest(String tbrest) {
		this.tbrest = tbrest;
	}

}