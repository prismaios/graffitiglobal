package it.prismatdi.model.as400;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the cardis database table.
 *
 */

@Embeddable
public class CardisPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long	serialVersionUID	= 1L;

	private String					cotipo;

	private String					coarti;

	private String					covari;

	private String					codise;

	public CardisPK(String cotipo, String coarti, String covari, String codise) {
		super();
		this.cotipo = cotipo;
		this.coarti = coarti;
		this.covari = covari;
		this.codise = codise;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CardisPK other = (CardisPK) obj;
		if (coarti == null) {
			if (other.coarti != null) {
				return false;
			}
		}
		else if (!coarti.equals(other.coarti)) {
			return false;
		}
		if (codise == null) {
			if (other.codise != null) {
				return false;
			}
		}
		else if (!codise.equals(other.codise)) {
			return false;
		}
		if (cotipo == null) {
			if (other.cotipo != null) {
				return false;
			}
		}
		else if (!cotipo.equals(other.cotipo)) {
			return false;
		}
		if (covari == null) {
			if (other.covari != null) {
				return false;
			}
		}
		else if (!covari.equals(other.covari)) {
			return false;
		}
		return true;
	}

	public String getCoarti() {
		return coarti;
	}

	public String getCodise() {
		return codise;
	}

	public String getCotipo() {
		return cotipo;
	}

	public String getCovari() {
		return covari;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (coarti == null ? 0 : coarti.hashCode());
		result = prime * result + (codise == null ? 0 : codise.hashCode());
		result = prime * result + (cotipo == null ? 0 : cotipo.hashCode());
		result = prime * result + (covari == null ? 0 : covari.hashCode());
		return result;
	}

	public void setCoarti(String coarti) {
		this.coarti = coarti;
	}

	public void setCodise(String codise) {
		this.codise = codise;
	}

	public void setCotipo(String cotipo) {
		this.cotipo = cotipo;
	}

	public void setCovari(String covari) {
		this.covari = covari;
	}

}