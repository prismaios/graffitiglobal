package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the teordi database table.
 *
 */
@Entity
@NamedQuery(name = "Teordi.findAll", query = "SELECT t FROM Teordi t")
public class Teordi implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Id
	private Long					noordi;

	private BigDecimal			ageor1;

	private BigDecimal			ageor2;

	private BigDecimal			ageor3;

	private String					baapor;

	private String					citdeo;

	private String					citdor;

	private BigDecimal			client;

	private BigDecimal			codbao;

	private BigDecimal			codpao;

	private String					confor;

	private BigDecimal			copor;

	private BigDecimal			dataor;

	private String					datdec;

	private BigDecimal			datreo;

	private BigDecimal			datres;

	private String					destor;

	private String					divior;

	private String					marche;

	private String					mtoyat;

	private String					noteg2;

	private String					noteg3;

	private String					noteg4;

	private String					notego;

	private String					prdoct;

	private String					procam;

	private String					propost;

	private BigDecimal			prov1;

	private BigDecimal			provv2;

	private BigDecimal			provv3;

	private String					reparto;

	private String					riford;

	private BigDecimal			sconor;

	private String					sigla;

	private String					spearr;

	private BigDecimal			spedor;

	private String					stagio;

	private BigDecimal			terepa;

	private String					tocoan;

	private BigDecimal			tocode;

	private String					tofil1;

	private String					tofil2;

	private String					tofil3;

	private String					tofil4;

	private BigDecimal			tosiip;

	private BigDecimal			tospin;

	private String					utente;

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "stagio", referencedColumnName = "tlstag", insertable = false, updatable = false),
			@JoinColumn(name = "tocoan", referencedColumnName = "tlzona", insertable = false, updatable = false) })
	private Telist0f				testaListino;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Teordi other = (Teordi) obj;
		if (noordi == null) {
			if (other.noordi != null) {
				return false;
			}
		}
		else if (!noordi.equals(other.noordi)) {
			return false;
		}
		return true;
	}

	public BigDecimal getAgeor1() {
		return ageor1;
	}

	public BigDecimal getAgeor2() {
		return ageor2;
	}

	public BigDecimal getAgeor3() {
		return ageor3;
	}

	public String getBaapor() {
		return baapor;
	}

	public String getCitdeo() {
		return citdeo;
	}

	public String getCitdor() {
		return citdor;
	}

	public BigDecimal getClient() {
		return client;
	}

	public BigDecimal getCodbao() {
		return codbao;
	}

	public BigDecimal getCodpao() {
		return codpao;
	}

	public String getConfor() {
		return confor;
	}

	public BigDecimal getCopor() {
		return copor;
	}

	public BigDecimal getDataor() {
		return dataor;
	}

	public String getDatdec() {
		return datdec;
	}

	public BigDecimal getDatreo() {
		return datreo;
	}

	public BigDecimal getDatres() {
		return datres;
	}

	public String getDestor() {
		return destor;
	}

	public String getDivior() {
		return divior;
	}

	public String getMarche() {
		return marche;
	}

	public String getMtoyat() {
		return mtoyat;
	}

	public Long getNoordi() {
		return noordi;
	}

	public String getNoteg2() {
		return noteg2;
	}

	public String getNoteg3() {
		return noteg3;
	}

	public String getNoteg4() {
		return noteg4;
	}

	public String getNotego() {
		return notego;
	}

	public String getPrdoct() {
		return prdoct;
	}

	public String getProcam() {
		return procam;
	}

	public String getPropost() {
		return propost;
	}

	public BigDecimal getProv1() {
		return prov1;
	}

	public BigDecimal getProvv2() {
		return provv2;
	}

	public BigDecimal getProvv3() {
		return provv3;
	}

	public String getReparto() {
		return reparto;
	}

	public String getRiford() {
		return riford;
	}

	public BigDecimal getSconor() {
		return sconor;
	}

	public String getSigla() {
		return sigla;
	}

	public String getSpearr() {
		return spearr;
	}

	public BigDecimal getSpedor() {
		return spedor;
	}

	public String getStagio() {
		return stagio;
	}

	public BigDecimal getTerepa() {
		return terepa;
	}

	public Telist0f getTestaListino() {
		return testaListino;
	}

	public String getTocoan() {
		return tocoan;
	}

	public BigDecimal getTocode() {
		return tocode;
	}

	public String getTofil1() {
		return tofil1;
	}

	public String getTofil2() {
		return tofil2;
	}

	public String getTofil3() {
		return tofil3;
	}

	public String getTofil4() {
		return tofil4;
	}

	public BigDecimal getTosiip() {
		return tosiip;
	}

	public BigDecimal getTospin() {
		return tospin;
	}

	public String getUtente() {
		return utente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (noordi == null ? 0 : noordi.hashCode());
		return result;
	}

	public void setAgeor1(BigDecimal ageor1) {
		this.ageor1 = ageor1;
	}

	public void setAgeor2(BigDecimal ageor2) {
		this.ageor2 = ageor2;
	}

	public void setAgeor3(BigDecimal ageor3) {
		this.ageor3 = ageor3;
	}

	public void setBaapor(String baapor) {
		this.baapor = baapor;
	}

	public void setCitdeo(String citdeo) {
		this.citdeo = citdeo;
	}

	public void setCitdor(String citdor) {
		this.citdor = citdor;
	}

	public void setClient(BigDecimal client) {
		this.client = client;
	}

	public void setCodbao(BigDecimal codbao) {
		this.codbao = codbao;
	}

	public void setCodpao(BigDecimal codpao) {
		this.codpao = codpao;
	}

	public void setConfor(String confor) {
		this.confor = confor;
	}

	public void setCopor(BigDecimal copor) {
		this.copor = copor;
	}

	public void setDataor(BigDecimal dataor) {
		this.dataor = dataor;
	}

	public void setDatdec(String datdec) {
		this.datdec = datdec;
	}

	public void setDatreo(BigDecimal datreo) {
		this.datreo = datreo;
	}

	public void setDatres(BigDecimal datres) {
		this.datres = datres;
	}

	public void setDestor(String destor) {
		this.destor = destor;
	}

	public void setDivior(String divior) {
		this.divior = divior;
	}

	public void setMarche(String marche) {
		this.marche = marche;
	}

	public void setMtoyat(String mtoyat) {
		this.mtoyat = mtoyat;
	}

	public void setNoordi(Long noordi) {
		this.noordi = noordi;
	}

	public void setNoteg2(String noteg2) {
		this.noteg2 = noteg2;
	}

	public void setNoteg3(String noteg3) {
		this.noteg3 = noteg3;
	}

	public void setNoteg4(String noteg4) {
		this.noteg4 = noteg4;
	}

	public void setNotego(String notego) {
		this.notego = notego;
	}

	public void setPrdoct(String prdoct) {
		this.prdoct = prdoct;
	}

	public void setProcam(String procam) {
		this.procam = procam;
	}

	public void setPropost(String propost) {
		this.propost = propost;
	}

	public void setProv1(BigDecimal prov1) {
		this.prov1 = prov1;
	}

	public void setProvv2(BigDecimal provv2) {
		this.provv2 = provv2;
	}

	public void setProvv3(BigDecimal provv3) {
		this.provv3 = provv3;
	}

	public void setReparto(String reparto) {
		this.reparto = reparto;
	}

	public void setRiford(String riford) {
		this.riford = riford;
	}

	public void setSconor(BigDecimal sconor) {
		this.sconor = sconor;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public void setSpearr(String spearr) {
		this.spearr = spearr;
	}

	public void setSpedor(BigDecimal spedor) {
		this.spedor = spedor;
	}

	public void setStagio(String stagio) {
		this.stagio = stagio;
	}

	public void setTerepa(BigDecimal terepa) {
		this.terepa = terepa;
	}

	public void setTestaListino(Telist0f testaListino) {
		this.testaListino = testaListino;
	}

	public void setTocoan(String tocoan) {
		this.tocoan = tocoan;
	}

	public void setTocode(BigDecimal tocode) {
		this.tocode = tocode;
	}

	public void setTofil1(String tofil1) {
		this.tofil1 = tofil1;
	}

	public void setTofil2(String tofil2) {
		this.tofil2 = tofil2;
	}

	public void setTofil3(String tofil3) {
		this.tofil3 = tofil3;
	}

	public void setTofil4(String tofil4) {
		this.tofil4 = tofil4;
	}

	public void setTosiip(BigDecimal tosiip) {
		this.tosiip = tosiip;
	}

	public void setTospin(BigDecimal tospin) {
		this.tospin = tospin;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

}