package it.prismatdi.model.produzio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the delist0f database table.
 *
 */
@Entity
@NamedQuery(name = "Delist0f.findAll", query = "SELECT d FROM Delist0f d")
public class Delist0f implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private Delist0fPK			id;

	@Column(name = "DL_PERC")
	private double					dlPerc;

	private short					dlalt1;

	private short					dlaltc;

	private short					dlalte;

	private String					dlarcl;

	private double					dlarea;

	private double					dlarep;

	private String					dlartbase;

	private String					dlarti;

	private short					dlatc1;

	private String					dlcocl;

	private String					dlcolo;

	private String					dlcomc;

	private String					dlcomp;

	private double					dlcos1;

	private double					dlcos2;

	private double					dlcos3;

	private double					dlcos4;

	private double					dlcos5;

	private double					dlcost;

	private String					dldatf;

	private String					dldati;

	private String					dldcl1;

	private String					dldecl;

	private String					dldes1;

	private String					dldesc;

	private double					dldiv1;

	private double					dldiv2;

	private double					dldiv3;

	private String					dllavo;

	private String					dlnote;

	private short					dlpec1;

	private short					dlpes1;

	private short					dlpesc;

	private short					dlpeso;

	private double					dlprez;

	private double					dlqtas;

	private double					dlroya;

	private double					dlscon;

	private double					dltras;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dlcodi", nullable = false, insertable = false, updatable = false)
	private Telist0f				testata;

	public Delist0f() {
	}

	public short getDlalt1() {
		return dlalt1;
	}

	public short getDlaltc() {
		return dlaltc;
	}

	public short getDlalte() {
		return dlalte;
	}

	public String getDlarcl() {
		return dlarcl;
	}

	public double getDlarea() {
		return dlarea;
	}

	public double getDlarep() {
		return dlarep;
	}

	public String getDlartbase() {
		return dlartbase;
	}

	public String getDlarti() {
		return dlarti;
	}

	public short getDlatc1() {
		return dlatc1;
	}

	public String getDlcocl() {
		return dlcocl;
	}

	public String getDlcolo() {
		return dlcolo;
	}

	public String getDlcomc() {
		return dlcomc;
	}

	public String getDlcomp() {
		return dlcomp;
	}

	public double getDlcos1() {
		return dlcos1;
	}

	public double getDlcos2() {
		return dlcos2;
	}

	public double getDlcos3() {
		return dlcos3;
	}

	public double getDlcos4() {
		return dlcos4;
	}

	public double getDlcos5() {
		return dlcos5;
	}

	public double getDlcost() {
		return dlcost;
	}

	public String getDldatf() {
		return dldatf;
	}

	public String getDldati() {
		return dldati;
	}

	public String getDldcl1() {
		return dldcl1;
	}

	public String getDldecl() {
		return dldecl;
	}

	public String getDldes1() {
		return dldes1;
	}

	public String getDldesc() {
		return dldesc;
	}

	public double getDldiv1() {
		return dldiv1;
	}

	public double getDldiv2() {
		return dldiv2;
	}

	public double getDldiv3() {
		return dldiv3;
	}

	public String getDllavo() {
		return dllavo;
	}

	public String getDlnote() {
		return dlnote;
	}

	public short getDlpec1() {
		return dlpec1;
	}

	public double getDlPerc() {
		return dlPerc;
	}

	public short getDlpes1() {
		return dlpes1;
	}

	public short getDlpesc() {
		return dlpesc;
	}

	public short getDlpeso() {
		return dlpeso;
	}

	public double getDlprez() {
		return dlprez;
	}

	public double getDlqtas() {
		return dlqtas;
	}

	public double getDlroya() {
		return dlroya;
	}

	public double getDlscon() {
		return dlscon;
	}

	public double getDltras() {
		return dltras;
	}

	public Delist0fPK getId() {
		return id;
	}

	public Telist0f getTestata() {
		return testata;
	}

	public void setDlalt1(short dlalt1) {
		this.dlalt1 = dlalt1;
	}

	public void setDlaltc(short dlaltc) {
		this.dlaltc = dlaltc;
	}

	public void setDlalte(short dlalte) {
		this.dlalte = dlalte;
	}

	public void setDlarcl(String dlarcl) {
		this.dlarcl = dlarcl;
	}

	public void setDlarea(double dlarea) {
		this.dlarea = dlarea;
	}

	public void setDlarep(double dlarep) {
		this.dlarep = dlarep;
	}

	public void setDlartbase(String dlartbase) {
		this.dlartbase = dlartbase;
	}

	public void setDlarti(String dlarti) {
		this.dlarti = dlarti;
	}

	public void setDlatc1(short dlatc1) {
		this.dlatc1 = dlatc1;
	}

	public void setDlcocl(String dlcocl) {
		this.dlcocl = dlcocl;
	}

	public void setDlcolo(String dlcolo) {
		this.dlcolo = dlcolo;
	}

	public void setDlcomc(String dlcomc) {
		this.dlcomc = dlcomc;
	}

	public void setDlcomp(String dlcomp) {
		this.dlcomp = dlcomp;
	}

	public void setDlcos1(double dlcos1) {
		this.dlcos1 = dlcos1;
	}

	public void setDlcos2(double dlcos2) {
		this.dlcos2 = dlcos2;
	}

	public void setDlcos3(double dlcos3) {
		this.dlcos3 = dlcos3;
	}

	public void setDlcos4(double dlcos4) {
		this.dlcos4 = dlcos4;
	}

	public void setDlcos5(double dlcos5) {
		this.dlcos5 = dlcos5;
	}

	public void setDlcost(double dlcost) {
		this.dlcost = dlcost;
	}

	public void setDldatf(String dldatf) {
		this.dldatf = dldatf;
	}

	public void setDldati(String dldati) {
		this.dldati = dldati;
	}

	public void setDldcl1(String dldcl1) {
		this.dldcl1 = dldcl1;
	}

	public void setDldecl(String dldecl) {
		this.dldecl = dldecl;
	}

	public void setDldes1(String dldes1) {
		this.dldes1 = dldes1;
	}

	public void setDldesc(String dldesc) {
		this.dldesc = dldesc;
	}

	public void setDldiv1(double dldiv1) {
		this.dldiv1 = dldiv1;
	}

	public void setDldiv2(double dldiv2) {
		this.dldiv2 = dldiv2;
	}

	public void setDldiv3(double dldiv3) {
		this.dldiv3 = dldiv3;
	}

	public void setDllavo(String dllavo) {
		this.dllavo = dllavo;
	}

	public void setDlnote(String dlnote) {
		this.dlnote = dlnote;
	}

	public void setDlpec1(short dlpec1) {
		this.dlpec1 = dlpec1;
	}

	public void setDlPerc(double dlPerc) {
		this.dlPerc = dlPerc;
	}

	public void setDlpes1(short dlpes1) {
		this.dlpes1 = dlpes1;
	}

	public void setDlpesc(short dlpesc) {
		this.dlpesc = dlpesc;
	}

	public void setDlpeso(short dlpeso) {
		this.dlpeso = dlpeso;
	}

	public void setDlprez(double dlprez) {
		this.dlprez = dlprez;
	}

	public void setDlqtas(double dlqtas) {
		this.dlqtas = dlqtas;
	}

	public void setDlroya(double dlroya) {
		this.dlroya = dlroya;
	}

	public void setDlscon(double dlscon) {
		this.dlscon = dlscon;
	}

	public void setDltras(double dltras) {
		this.dltras = dltras;
	}

	public void setId(Delist0fPK id) {
		this.id = id;
	}

	public void setTestata(Telist0f testata) {
		this.testata = testata;
	}

}