package it.prismatdi.model.produzio;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the ordini database table.
 *
 */
@Embeddable
public class OrdiniPK implements Serializable {
	private static final long	serialVersionUID	= 1L;

	private Long					noordi;

	private Long					nskro;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OrdiniPK other = (OrdiniPK) obj;
		if (noordi == null) {
			if (other.noordi != null) {
				return false;
			}
		}
		else if (!noordi.equals(other.noordi)) {
			return false;
		}
		if (nskro == null) {
			if (other.nskro != null) {
				return false;
			}
		}
		else if (!nskro.equals(other.nskro)) {
			return false;
		}
		return true;
	}

	public Long getNoordi() {
		return noordi;
	}

	public Long getNskro() {
		return nskro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (noordi == null ? 0 : noordi.hashCode());
		result = prime * result + (nskro == null ? 0 : nskro.hashCode());
		return result;
	}

	public void setNoordi(Long noordi) {
		this.noordi = noordi;
	}

	public void setNskro(Long nskro) {
		this.nskro = nskro;
	}

}