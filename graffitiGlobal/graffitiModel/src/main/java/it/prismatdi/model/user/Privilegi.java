package it.prismatdi.model.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import it.prismatdi.general.Ruoli;

@Entity
@Table(name = "privilegi")
@NamedQueries({ @NamedQuery(name = "Privilegi.findAll", query = "SELECT w FROM Privilegi AS w") })
public class Privilegi {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, updatable = false)
	private Integer				id;
	@Column(name = "DESCRIZIONE", length = 255)
	private String					descrizione;

	@Column(name = "CODICE")
	@Enumerated
	private Ruoli					ruolo;

	@Column(name = "MODULO", length = 255)
	private String					modulo;

	@ManyToMany(mappedBy = "privilegi")
	private List<RuoliWebUser>	ruoli;

	public Privilegi() {
		super();
		ruoli = new ArrayList<RuoliWebUser>();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Privilegi other = (Privilegi) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public Integer getId() {
		return id;
	}

	public String getModulo() {
		return modulo;
	}

	public List<RuoliWebUser> getRuoli() {
		return ruoli;
	}

	public Ruoli getRuolo() {
		return ruolo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public void setRuoli(List<RuoliWebUser> ruoli) {
		this.ruoli = ruoli;
	}

	public void setRuolo(Ruoli ruolo) {
		this.ruolo = ruolo;
	}

	@Override
	public String toString() {
		return "Privilegi [id=" + id + ", descrizione=" + descrizione + ", modulo=" + modulo + "]";
	}

}
