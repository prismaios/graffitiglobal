package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 * The persistent class for the telist0f database table.
 *
 */
@Entity
@NamedQuery(name = "Telist0f.findAll", query = "SELECT t FROM Telist0f t")
public class Telist0f implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@Id
	private int						tlcodi;

	@Column(name = "TL_AZI")
	private String					tlAzi;

	@Column(name = "TL_MOD")
	private int						tlMod;

	@Column(name = "TL_NOTE")
	private String					tlNote;

	@Column(name = "TL_OFF")
	private String					tlOff;

	@Column(name = "TL_PERC")
	private double					tlPerc;

	@Column(name = "TL_USER")
	private String					tlUser;

	private double					tlcamb;

	private String					tlclie;

	private String					tldatf;

	private String					tldati;

	private String					tldesc;

	private String					tldivi;

	private double					tldivs;

	private String					tlflag;

	private short					tlncen;

	private double					tlndec;

	private double					tlpro2;

	private double					tlpro3;

	private double					tlprov;

	private String					tlrap2;

	private String					tlrapp;

	private double					tlscon;

	private String					tlstag;

	private String					tltiar;

	private String					tltili;

	private String					tltipo;

	private String					tlzona;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "testata")
	private List<Delist0f>		listini;

	public Telist0f() {
	}

	public List<Delist0f> getListini() {
		return listini;
	}

	public String getTlAzi() {
		return tlAzi;
	}

	public double getTlcamb() {
		return tlcamb;
	}

	public String getTlclie() {
		return tlclie;
	}

	public int getTlcodi() {
		return tlcodi;
	}

	public String getTldatf() {
		return tldatf;
	}

	public String getTldati() {
		return tldati;
	}

	public String getTldesc() {
		return tldesc;
	}

	public String getTldivi() {
		return tldivi;
	}

	public double getTldivs() {
		return tldivs;
	}

	public String getTlflag() {
		return tlflag;
	}

	public int getTlMod() {
		return tlMod;
	}

	public short getTlncen() {
		return tlncen;
	}

	public double getTlndec() {
		return tlndec;
	}

	public String getTlNote() {
		return tlNote;
	}

	public String getTlOff() {
		return tlOff;
	}

	public double getTlPerc() {
		return tlPerc;
	}

	public double getTlpro2() {
		return tlpro2;
	}

	public double getTlpro3() {
		return tlpro3;
	}

	public double getTlprov() {
		return tlprov;
	}

	public String getTlrap2() {
		return tlrap2;
	}

	public String getTlrapp() {
		return tlrapp;
	}

	public double getTlscon() {
		return tlscon;
	}

	public String getTlstag() {
		return tlstag;
	}

	public String getTltiar() {
		return tltiar;
	}

	public String getTltili() {
		return tltili;
	}

	public String getTltipo() {
		return tltipo;
	}

	public String getTlUser() {
		return tlUser;
	}

	public String getTlzona() {
		return tlzona;
	}

	public void setListini(List<Delist0f> listini) {
		this.listini = listini;
	}

	public void setTlAzi(String tlAzi) {
		this.tlAzi = tlAzi;
	}

	public void setTlcamb(double tlcamb) {
		this.tlcamb = tlcamb;
	}

	public void setTlclie(String tlclie) {
		this.tlclie = tlclie;
	}

	public void setTlcodi(int tlcodi) {
		this.tlcodi = tlcodi;
	}

	public void setTldatf(String tldatf) {
		this.tldatf = tldatf;
	}

	public void setTldati(String tldati) {
		this.tldati = tldati;
	}

	public void setTldesc(String tldesc) {
		this.tldesc = tldesc;
	}

	public void setTldivi(String tldivi) {
		this.tldivi = tldivi;
	}

	public void setTldivs(double tldivs) {
		this.tldivs = tldivs;
	}

	public void setTlflag(String tlflag) {
		this.tlflag = tlflag;
	}

	public void setTlMod(int tlMod) {
		this.tlMod = tlMod;
	}

	public void setTlncen(short tlncen) {
		this.tlncen = tlncen;
	}

	public void setTlndec(double tlndec) {
		this.tlndec = tlndec;
	}

	public void setTlNote(String tlNote) {
		this.tlNote = tlNote;
	}

	public void setTlOff(String tlOff) {
		this.tlOff = tlOff;
	}

	public void setTlPerc(double tlPerc) {
		this.tlPerc = tlPerc;
	}

	public void setTlpro2(double tlpro2) {
		this.tlpro2 = tlpro2;
	}

	public void setTlpro3(double tlpro3) {
		this.tlpro3 = tlpro3;
	}

	public void setTlprov(double tlprov) {
		this.tlprov = tlprov;
	}

	public void setTlrap2(String tlrap2) {
		this.tlrap2 = tlrap2;
	}

	public void setTlrapp(String tlrapp) {
		this.tlrapp = tlrapp;
	}

	public void setTlscon(double tlscon) {
		this.tlscon = tlscon;
	}

	public void setTlstag(String tlstag) {
		this.tlstag = tlstag;
	}

	public void setTltiar(String tltiar) {
		this.tltiar = tltiar;
	}

	public void setTltili(String tltili) {
		this.tltili = tltili;
	}

	public void setTltipo(String tltipo) {
		this.tltipo = tltipo;
	}

	public void setTlUser(String tlUser) {
		this.tlUser = tlUser;
	}

	public void setTlzona(String tlzona) {
		this.tlzona = tlzona;
	}

}