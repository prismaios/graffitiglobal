package it.prismatdi.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import it.prismatdi.general.Ruoli;

/**
 * Entity implementation class for Entity: WebUser
 *
 */
@Entity
@Table(name = "user_graffiti_web")
@NamedQueries({ @NamedQuery(name = "UserGraffitiWeb.findAll", query = "SELECT w FROM UserGraffitiWeb AS w") })
public class UserGraffitiWeb implements Serializable, UserDetails {

	private static final long	serialVersionUID	= 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, updatable = false)
	private Integer				id;
	@Column(name = "LOGIN_NAME", length = 30, unique = true, nullable = false)
	private String					loginName;
	@Column(name = "PASSWORD", length = 100, nullable = false)
	private String					password;
	@Column(name = "NOME", length = 50)
	private String					nome;
	@Column(name = "COGNOME", length = 50)
	private String					cognome;
	@Column(name = "N_LOGIN")
	private Integer				nLogin				= 0;
	@Column(name = "LOCKED")
	private Boolean				locked				= false;
	@Column(name = "RICHIEDI_PASSD")
	private Boolean				richiediPasswd		= false;
	@Column(name = "COD_GRAFFITI", length = 20)
	private String					codGraffiti;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
	private List<RuoliWebUser>	ruoli;

	public UserGraffitiWeb() {
		super();
		ruoli = new ArrayList<>();
	}

	public UserGraffitiWeb(String loginName, String password) {
		this();
		ruoli = new ArrayList<>();
		this.loginName = loginName;
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserGraffitiWeb other = (UserGraffitiWeb) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	public Set<GrantedAuthority> getAuths() {
		Set<GrantedAuthority> granted = new HashSet<>();

		Ruoli role = null;
		for (RuoliWebUser ruolo : ruoli) {
			for (Privilegi privilegio : ruolo.getPrivilegi()) {
				if (role == null) {
					role = privilegio.getRuolo();
				}
				else {
					if (role.getVal() < privilegio.getRuolo()
							.getVal()) {
						role = privilegio.getRuolo();
					}
				}
			}
		}
		//		System.err.println(">>>>> UserGraffitiWeb-getAuths <<<<<< " + role.toString());
		List<Ruoli> roles = Ruoli.getRuoliCompresi(role);
		for (Ruoli ruolo : roles) {
			SimpleGrantedAuthority sga = new SimpleGrantedAuthority(ruolo.toString());
			granted.add(sga);
		}
		return granted;
	}

	public String getCodGraffiti() {
		return codGraffiti;
	}

	public String getCognome() {
		return cognome;
	}

	public Integer getId() {
		return id;
	}

	public Boolean getLocked() {
		if (locked == null) {
			return false;
		}
		return locked;
	}

	public String getLoginName() {
		return loginName;
	}

	public Integer getnLogin() {
		if (nLogin == null) {
			nLogin = 0;
		}
		return nLogin;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public Boolean getRichiediPasswd() {
		return richiediPasswd;
	}

	public List<RuoliWebUser> getRuoli() {
		return ruoli;
	}

	@Override
	public String getUsername() {
		return loginName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean isAccountNonExpired() {

		return true;
	}

	@Override
	public boolean isAccountNonLocked() {

		return locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {

		return true;
	}

	@Override
	public boolean isEnabled() {

		return true;
	}

	public void setCodGraffiti(String codGraffiti) {
		this.codGraffiti = codGraffiti;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public void setnLogin(Integer nLogin) {
		this.nLogin = nLogin;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRichiediPasswd(Boolean richiediPasswd) {
		this.richiediPasswd = richiediPasswd;
	}

	public void setRuoli(List<RuoliWebUser> ruoli) {
		this.ruoli = ruoli;
	}

	@Override
	public String toString() {
		return nome + " " + cognome;
	}

	public void update(UserGraffitiWeb changed) {
		loginName = changed.getLoginName();
		password = changed.getPassword();
		nome = changed.getNome();
		cognome = changed.getCognome();
		nLogin = changed.getnLogin();
		locked = changed.getLocked();
		richiediPasswd = changed.getRichiediPasswd();
		codGraffiti = changed.getCodGraffiti();
	}

}
