package it.prismatdi.model.produzio;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the tmovrig database table.
 *
 */
@Embeddable
public class TmovrigPK implements Serializable {
	private static final long	serialVersionUID	= 1L;

	private Integer				mrkeye;

	private Integer				mrriga;

	private Integer				mrrigt;

	public Integer getMrkeye() {
		return mrkeye;
	}

	public Integer getMrriga() {
		return mrriga;
	}

	public Integer getMrrigt() {
		return mrrigt;
	}

	public void setMrkeye(Integer mrkeye) {
		this.mrkeye = mrkeye;
	}

	public void setMrriga(Integer mrriga) {
		this.mrriga = mrriga;
	}

	public void setMrrigt(Integer mrrigt) {
		this.mrrigt = mrrigt;
	}

}