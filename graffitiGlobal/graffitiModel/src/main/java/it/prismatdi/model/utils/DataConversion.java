
package it.prismatdi.model.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Tuple;

import it.prismatdi.general.AliasPerCriteriaQuery;
import it.prismatdi.model.produzio.Matpri;
import it.prismatdi.model.produzio.Ordini;
import it.prismatdi.model.produzio.Tmovrig;

@SuppressWarnings("serial")
public class DataConversion implements Serializable {
	public static Double arrotonda(double d, int cifre) {
		try {
			if (cifre < 0) {
				throw new IllegalArgumentException();
			}
			BigDecimal bd = new BigDecimal(d);
			bd = bd.setScale(cifre, RoundingMode.HALF_UP);
			return bd.doubleValue();
		}
		catch (final Exception e) {
			return 0D;
		}
	}

	private String		stagione;

	private String		causale;

	private String		codice;			// dopo

	private String		descrizione;	// dopo

	private String		codRappr;

	private String		codColore;

	private String		desColore;		// dopo

	private String		prodCamp;

	private Double		qta;

	private Double		valore;

	private String		unitaM;

	private Double		maggiorazioni;

	private Double		prezzo;

	private String		valuta;

	private Double		segno	= 1D;

	private Tmovrig	rigaMovimento;

	private Ordini		rigaOrdine;

	private Matpri		matpri;

	public void converti(Tmovrig riga) {
		//		System.err.println(">>>>> DataConversion-converti <<<<<< " + riga.getDesColore());
		rigaMovimento = riga;
		stagione = riga.getRigaOrdine()
				.getOrcoan();
		rigaOrdine = riga.getRigaOrdine();
		matpri = riga.getMatpri();
		codice = riga.getCodice();
		descrizione = riga.getDescrizione();
		causale = riga.getMrcau();
		codColore = riga.getMrcolo();
		desColore = riga.getDesColore();
		maggiorazioni = riga.getMrmag1();
		prezzo = riga.getMrprli();
		codRappr = riga.getMrrapp();
		unitaM = riga.getMrumis();
		prodCamp = riga.getMrcapr();
		qta = riga.getMrqtan();
		valore = DataConversion.arrotonda(riga.getMrqtan() * riga.getMrprli(), 2);
	}

	public void converti(Tuple tupla) {
		try {
			stagione = tupla.get(AliasPerCriteriaQuery.stagione.toString(), String.class);
		}
		catch (Exception e) {
			//			System.err.println(">>>>> DataConversion-converti <<<<<< ERRR NO STAGIONE");
			stagione = "N.A.";
		}
		if (stagione == null) {
			stagione = "M.MAN.";
		}
		causale = tupla.get(AliasPerCriteriaQuery.causale.toString(), String.class);
		codColore = tupla.get(AliasPerCriteriaQuery.codColore.toString(), String.class);
		maggiorazioni = tupla.get(AliasPerCriteriaQuery.maggiorazioni.toString(), Double.class);
		prezzo = tupla.get(AliasPerCriteriaQuery.prezzo.toString(), Double.class);
		codRappr = tupla.get(AliasPerCriteriaQuery.codRappr.toString(), String.class);
		unitaM = tupla.get(AliasPerCriteriaQuery.unitaM.toString(), String.class);
		prodCamp = tupla.get(AliasPerCriteriaQuery.prodCamp.toString(), String.class);
		qta = tupla.get(AliasPerCriteriaQuery.qta.toString(), Double.class);
		valore = DataConversion.arrotonda(qta * prezzo * segno, 2);
		valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);

	}

	public String getCausale() {
		return causale;
	}

	public String getCodColore() {
		return codColore;
	}

	public String getCodice() {
		return codice;
	}

	public String getCodRappr() {
		return codRappr;
	}

	public String getDesColore() {
		return desColore;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public Double getMaggiorazioni() {
		return maggiorazioni;
	}

	public Matpri getMatpri() {
		return matpri;
	}

	public Double getPrezzo() {
		return prezzo;
	}

	public String getProdCamp() {
		return prodCamp;
	}

	public Double getQta() {
		return qta * segno;
	}

	public Tmovrig getRigaMovimento() {
		return rigaMovimento;
	}

	public Ordini getRigaOrdine() {
		return rigaOrdine;
	}

	public Double getSegno() {
		return segno;
	}

	public String getStagione() {
		return stagione;
	}

	public String getUnitaM() {
		return unitaM;
	}

	public Double getValore() {
		return valore;
	}

	public String getValuta() {
		return valuta;
	}

	public void setCausale(String causale) {
		this.causale = causale;
	}

	public void setCodColore(String codColore) {
		this.codColore = codColore;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public void setCodRappr(String codRappr) {
		this.codRappr = codRappr;
	}

	public void setDesColore(String desColore) {
		this.desColore = desColore;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setMaggiorazioni(Double maggiorazioni) {
		this.maggiorazioni = maggiorazioni;
	}

	public void setMatpri(Matpri matpri) {
		this.matpri = matpri;
	}

	public void setPrezzo(Double prezzo) {
		this.prezzo = prezzo;
	}

	public void setProdCamp(String prodCamp) {
		this.prodCamp = prodCamp;
	}

	public void setQta(Double qta) {
		this.qta = qta;
	}

	public void setRigaMovimento(Tmovrig rigaMovimento) {
		this.rigaMovimento = rigaMovimento;
	}

	public void setRigaOrdine(Ordini rigaOrdine) {
		this.rigaOrdine = rigaOrdine;
	}

	public void setSegno(Double segno) {
		this.segno = segno;
	}

	public void setStagione(String stagione) {
		this.stagione = stagione;
	}

	public void setUnitaM(String unitaM) {
		this.unitaM = unitaM;
	}

	public void setValore(Double valore) {
		this.valore = valore;
	}

	public void setValuta(String valuta) {
		this.valuta = valuta;
	}

}
