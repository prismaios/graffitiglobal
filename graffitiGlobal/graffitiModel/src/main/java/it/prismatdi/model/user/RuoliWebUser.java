package it.prismatdi.model.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import it.prismatdi.user.TipoRuolo;

@Entity
@Table(name = "ruoli_web_user")
@NamedQueries({ @NamedQuery(name = "RuoliWebUser.findAll", query = "SELECT w FROM RuoliWebUser AS w") })
public class RuoliWebUser {
	public enum Lingue {
		ITALIANO("it"),
		ENGLISH("en"),
		DEUTSCH("de"),
		FRANçAIS("fr");
		private String string;

		private Lingue(String string) {
			this.string = string;
		}

		public String getString() {
			return string;
		}
	}
	public enum TipoArticolo {
		TESSUTI, SCIARPE, TUTTI;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID_RUOLO", unique = true, nullable = false, updatable = false)
	private Integer			idRuolo;
	@Column(name = "TIPO")
	@Enumerated
	private TipoRuolo			tipo;
	@Column(name = "SOTTOCONTO", length = 5)
	private String				sottoconto;

	@Column(name = "RAG_SOC", length = 150, nullable = false)
	private String				ragioneSociale;

	@Column(name = "P_IVA", length = 11)
	private String				pIva;
	@Column(name = "E_MAIL", length = 100, unique = true, nullable = false)
	private String				eMail;
	@Column(name = "LANGUAGE", length = 4)
	private String				language;
	@Column(name = "COUNTRY", length = 4)
	private String				country;
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "TIPO_ARTICOLO", columnDefinition = "int(11) default 2")
	private TipoArticolo		tipoArticolo;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USER", nullable = false)
	private UserGraffitiWeb	user;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "ruolo_privilegi_cross", joinColumns = @JoinColumn(name = "ID_RUOLO"),
			inverseJoinColumns = @JoinColumn(name = "ID"))
	private List<Privilegi>	privilegi;

	@Transient
	private Locale				locale;

	public RuoliWebUser() {
		super();
		privilegi = new ArrayList<Privilegi>();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RuoliWebUser other = (RuoliWebUser) obj;
		if (idRuolo == null) {
			if (other.idRuolo != null) {
				return false;
			}
		}
		else if (!idRuolo.equals(other.idRuolo)) {
			return false;
		}
		return true;
	}

	public String getCountry() {
		return country;
	}

	public String geteMail() {
		return eMail;
	}

	public Integer getId() {
		return idRuolo;
	}

	public String getLanguage() {
		return language;
	}

	public Locale getLocale() {
		if (language == null || language.length() == 0) {
			return null;
		}
		if (locale == null) {
			locale = new Locale(language, country);
		}
		return locale;
	}

	public String getpIva() {
		return pIva;
	}

	public List<Privilegi> getPrivilegi() {
		return privilegi;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public String getSottoconto() {
		return sottoconto;
	}

	public TipoRuolo getTipo() {
		return tipo;
	}

	public TipoArticolo getTipoArticolo() {
		return tipoArticolo;
	}

	public UserGraffitiWeb getUser() {
		return user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (idRuolo == null ? 0 : idRuolo.hashCode());
		return result;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public void setId(Integer idRuolo) {
		this.idRuolo = idRuolo;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
		country = locale.getCountry();
		language = locale.getLanguage();
	}

	public void setpIva(String pIva) {
		this.pIva = pIva;
	}

	public void setPrivilegi(List<Privilegi> privilegi) {
		this.privilegi = privilegi;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public void setSottoconto(String sottoconto) {
		this.sottoconto = sottoconto;
	}

	public void setTipo(TipoRuolo tipo) {
		this.tipo = tipo;
	}

	public void setTipoArticolo(TipoArticolo tipoArticolo) {
		this.tipoArticolo = tipoArticolo;
	}

	public void setUser(UserGraffitiWeb user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Ruoli_web_user [id=" + idRuolo + ", sottoconto=" + sottoconto + ", ragioneSociale="
				+ ragioneSociale + ", pIva=" + pIva + ", eMail=" + eMail + ", language=" + language + ", country=" + country
				+ ", tipoArticolo=" + tipoArticolo + ", locale=" + locale + "]";
	}

}
