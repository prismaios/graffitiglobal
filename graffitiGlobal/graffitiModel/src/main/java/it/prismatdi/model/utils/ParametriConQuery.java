package it.prismatdi.model.utils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public interface ParametriConQuery {
	CriteriaQuery<?> getCriteriaQuery(CriteriaBuilder cb);

	String getQuery();
}
