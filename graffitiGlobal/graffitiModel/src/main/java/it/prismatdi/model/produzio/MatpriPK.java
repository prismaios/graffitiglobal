package it.prismatdi.model.produzio;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the matpri database table.
 *
 */
@SuppressWarnings("serial")
@Embeddable
public class MatpriPK implements Serializable {

	private String	mttipo;

	private String	mtarti;

	private Long	mtcolo;

	public MatpriPK() {
		super();
	}

	public MatpriPK(String tipo, String codArti, long mtcolo) {
		mttipo = tipo;
		mtarti = codArti;
		this.mtcolo = mtcolo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MatpriPK other = (MatpriPK) obj;
		if (mtarti == null) {
			if (other.mtarti != null) {
				return false;
			}
		}
		else if (!mtarti.equals(other.mtarti)) {
			return false;
		}
		if (mtcolo == null) {
			if (other.mtcolo != null) {
				return false;
			}
		}
		else if (!mtcolo.equals(other.mtcolo)) {
			return false;
		}
		if (mttipo == null) {
			if (other.mttipo != null) {
				return false;
			}
		}
		else if (!mttipo.equals(other.mttipo)) {
			return false;
		}
		return true;
	}

	public String getMtarti() {
		return mtarti;
	}

	public Long getMtcolo() {
		return mtcolo;
	}

	public String getMttipo() {
		return mttipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mtarti == null ? 0 : mtarti.hashCode());
		result = prime * result + (mtcolo == null ? 0 : mtcolo.hashCode());
		result = prime * result + (mttipo == null ? 0 : mttipo.hashCode());
		return result;
	}

	public void setMtarti(String mtarti) {
		this.mtarti = mtarti;
	}

	public void setMtcolo(Long mtcolo) {
		this.mtcolo = mtcolo;
	}

	public void setMttipo(String mttipo) {
		this.mttipo = mttipo;
	}

}