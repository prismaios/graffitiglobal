package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

/**
 * The persistent class for the ordini database table.
 *
 */
@Entity
@NamedQuery(name = "Ordini.findAll", query = "SELECT o FROM Ordini o")
public class Ordini implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private OrdiniPK				id;

	private String					adispo;

	private BigDecimal			altpeo;

	private String					artifo;

	private String					artior;

	private String					artppr;

	private String					artpro;

	private String					assfis;

	private BigDecimal			cambor;

	private BigDecimal			client;

	private BigDecimal			cligre;

	private BigDecimal			colord;

	private BigDecimal			colpro;

	private BigDecimal			colrov;

	private String					confez;

	private BigDecimal			conrir;

	private String					divrio;

	private String					docabb;

	private String					fiera;

	private String					fille1;

	private String					fillek;

	private String					flabar;

	private String					flabdef;

	private String					flag1;

	private String					flag2;

	private String					flagr1;

	private String					flagr2;

	private String					flagr3;

	private String					fornit;

	private String					grptot;

	private String					lavora;

	private String					lavoraz;

	private String					letgr1;

	private String					letgr2;

	private String					letgr3;

	private BigDecimal			mtanno;

	private String					mtoya;

	private BigDecimal			noogre;

	private String					notrio;

	private BigDecimal			npzlao;

	private BigDecimal			nskgre;

	private String					ocdear;

	private BigDecimal			ocoric;

	private BigDecimal			ocsedo;

	private BigDecimal			ocsesc;

	private BigDecimal			ofper2;

	private Double					orabmt;						//costo della produzione x sciarpe

	private Double					orabpc;						//costo prodotto

	private BigDecimal			orappa;

	private BigDecimal			orappg;

	private BigDecimal			orappm;

	private BigDecimal			orapps;

	private String					orcoan;

	private String					ordprop;

	private BigDecimal			ordtaa;

	private BigDecimal			ordtgg;

	private BigDecimal			ordtmm;

	private BigDecimal			orperr;

	private BigDecimal			orpeur;

	private String					orprio;

	private BigDecimal			orprls;

	private BigDecimal			orrap2;

	private BigDecimal			orrapp;

	private BigDecimal			orrepa;

	private String					orsiap;

	private String					orsico;

	private String					orsosp;

	private BigDecimal			preord;

	private BigDecimal			prgre2;

	private BigDecimal			prgre3;

	private BigDecimal			prmtan;

	private String					procam;

	private String					protdo;

	private BigDecimal			prpzan;

	private BigDecimal			pzann;

	private BigDecimal			pzasse;

	private BigDecimal			pzeord;

	private BigDecimal			pzspe;

	private BigDecimal			qbolla;

	private BigDecimal			qta1or;

	private BigDecimal			qta2or;

	private BigDecimal			qtaord;

	private BigDecimal			qtaspo;

	private BigDecimal			qtass;

	private String					qtatag;

	private BigDecimal			qtorme;

	private String					reparto;

	private String					restok;

	private BigDecimal			rifordor;

	private String					riforf;

	private String					riforr;

	private BigDecimal			rigaorig;

	private String					saldor;

	private String					salprs;

	private BigDecimal			scador;

	private BigDecimal			scapro;

	private String					sigla;

	private String					tipcam;

	private String					tipcco;

	private BigDecimal			tipoor;

	private String					tiptir;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumnsOrFormulas(
			value = {
					@JoinColumnOrFormula(
							column = @JoinColumn(name = "artior", referencedColumnName = "mtarti", insertable = false,
									updatable = false,
									nullable = false)),
					@JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "mttipo", value = "'T'")),
					@JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "mtcolo", value = "0")) })
	private Matpri					articolo;

	@ManyToOne
	@JoinColumn(name = "noordi", insertable = false, updatable = false)
	private Teordi					testata;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ordini other = (Ordini) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getAdispo() {
		return adispo;
	}

	public BigDecimal getAltpeo() {
		return altpeo;
	}

	public Matpri getArticolo() {
		return articolo;
	}

	public String getArtifo() {
		return artifo;
	}

	public String getArtior() {
		return artior;
	}

	public String getArtppr() {
		return artppr;
	}

	public String getArtpro() {
		return artpro;
	}

	public String getAssfis() {
		return assfis;
	}

	public BigDecimal getCambor() {
		return cambor;
	}

	public BigDecimal getClient() {
		return client;
	}

	public BigDecimal getCligre() {
		return cligre;
	}

	public BigDecimal getColord() {
		return colord;
	}

	public BigDecimal getColpro() {
		return colpro;
	}

	public BigDecimal getColrov() {
		return colrov;
	}

	public String getConfez() {
		return confez;
	}

	public BigDecimal getConrir() {
		return conrir;
	}

	public String getDivrio() {
		return divrio;
	}

	public String getDocabb() {
		return docabb;
	}

	public String getFiera() {
		return fiera;
	}

	public String getFille1() {
		return fille1;
	}

	public String getFillek() {
		return fillek;
	}

	public String getFlabar() {
		return flabar;
	}

	public String getFlabdef() {
		return flabdef;
	}

	public String getFlag1() {
		return flag1;
	}

	public String getFlag2() {
		return flag2;
	}

	public String getFlagr1() {
		return flagr1;
	}

	public String getFlagr2() {
		return flagr2;
	}

	public String getFlagr3() {
		return flagr3;
	}

	public String getFornit() {
		return fornit;
	}

	public String getGrptot() {
		return grptot;
	}

	public OrdiniPK getId() {
		return id;
	}

	public String getLavora() {
		return lavora;
	}

	public String getLavoraz() {
		return lavoraz;
	}

	public String getLetgr1() {
		return letgr1;
	}

	public String getLetgr2() {
		return letgr2;
	}

	public String getLetgr3() {
		return letgr3;
	}

	public BigDecimal getMtanno() {
		return mtanno;
	}

	public String getMtoya() {
		return mtoya;
	}

	public BigDecimal getNoogre() {
		return noogre;
	}

	public String getNotrio() {
		return notrio;
	}

	public BigDecimal getNpzlao() {
		return npzlao;
	}

	public BigDecimal getNskgre() {
		return nskgre;
	}

	public String getOcdear() {
		return ocdear;
	}

	public BigDecimal getOcoric() {
		return ocoric;
	}

	public BigDecimal getOcsedo() {
		return ocsedo;
	}

	public BigDecimal getOcsesc() {
		return ocsesc;
	}

	public BigDecimal getOfper2() {
		return ofper2;
	}

	public Double getOrabmt() {
		return orabmt;
	}

	public Double getOrabpc() {
		return orabpc;
	}

	public BigDecimal getOrappa() {
		return orappa;
	}

	public BigDecimal getOrappg() {
		return orappg;
	}

	public BigDecimal getOrappm() {
		return orappm;
	}

	public BigDecimal getOrapps() {
		return orapps;
	}

	public String getOrcoan() {
		return orcoan;
	}

	public String getOrdprop() {
		return ordprop;
	}

	public BigDecimal getOrdtaa() {
		return ordtaa;
	}

	public BigDecimal getOrdtgg() {
		return ordtgg;
	}

	public BigDecimal getOrdtmm() {
		return ordtmm;
	}

	public BigDecimal getOrperr() {
		return orperr;
	}

	public BigDecimal getOrpeur() {
		return orpeur;
	}

	public String getOrprio() {
		return orprio;
	}

	public BigDecimal getOrprls() {
		return orprls;
	}

	public BigDecimal getOrrap2() {
		return orrap2;
	}

	public BigDecimal getOrrapp() {
		return orrapp;
	}

	public BigDecimal getOrrepa() {
		return orrepa;
	}

	public String getOrsiap() {
		return orsiap;
	}

	public String getOrsico() {
		return orsico;
	}

	public String getOrsosp() {
		return orsosp;
	}

	public BigDecimal getPreord() {
		return preord;
	}

	public BigDecimal getPrgre2() {
		return prgre2;
	}

	public BigDecimal getPrgre3() {
		return prgre3;
	}

	public BigDecimal getPrmtan() {
		return prmtan;
	}

	public String getProcam() {
		return procam;
	}

	public String getProtdo() {
		return protdo;
	}

	public BigDecimal getPrpzan() {
		return prpzan;
	}

	public BigDecimal getPzann() {
		return pzann;
	}

	public BigDecimal getPzasse() {
		return pzasse;
	}

	public BigDecimal getPzeord() {
		return pzeord;
	}

	public BigDecimal getPzspe() {
		return pzspe;
	}

	public BigDecimal getQbolla() {
		return qbolla;
	}

	public BigDecimal getQta1or() {
		return qta1or;
	}

	public BigDecimal getQta2or() {
		return qta2or;
	}

	public BigDecimal getQtaord() {
		return qtaord;
	}

	public BigDecimal getQtaspo() {
		return qtaspo;
	}

	public BigDecimal getQtass() {
		return qtass;
	}

	public String getQtatag() {
		return qtatag;
	}

	public BigDecimal getQtorme() {
		return qtorme;
	}

	public String getReparto() {
		return reparto;
	}

	public String getRestok() {
		return restok;
	}

	public BigDecimal getRifordor() {
		return rifordor;
	}

	public String getRiforf() {
		return riforf;
	}

	public String getRiforr() {
		return riforr;
	}

	public BigDecimal getRigaorig() {
		return rigaorig;
	}

	public String getSaldor() {
		return saldor;
	}

	public String getSalprs() {
		return salprs;
	}

	public BigDecimal getScador() {
		return scador;
	}

	public BigDecimal getScapro() {
		return scapro;
	}

	public String getSigla() {
		return sigla;
	}

	public Teordi getTestata() {
		return testata;
	}

	public String getTipcam() {
		return tipcam;
	}

	public String getTipcco() {
		return tipcco;
	}

	public BigDecimal getTipoor() {
		return tipoor;
	}

	public String getTiptir() {
		return tiptir;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setAdispo(String adispo) {
		this.adispo = adispo;
	}

	public void setAltpeo(BigDecimal altpeo) {
		this.altpeo = altpeo;
	}

	public void setArticolo(Matpri articolo) {
		this.articolo = articolo;
	}

	public void setArtifo(String artifo) {
		this.artifo = artifo;
	}

	public void setArtior(String artior) {
		this.artior = artior;
	}

	public void setArtppr(String artppr) {
		this.artppr = artppr;
	}

	public void setArtpro(String artpro) {
		this.artpro = artpro;
	}

	public void setAssfis(String assfis) {
		this.assfis = assfis;
	}

	public void setCambor(BigDecimal cambor) {
		this.cambor = cambor;
	}

	public void setClient(BigDecimal client) {
		this.client = client;
	}

	public void setCligre(BigDecimal cligre) {
		this.cligre = cligre;
	}

	public void setColord(BigDecimal colord) {
		this.colord = colord;
	}

	public void setColpro(BigDecimal colpro) {
		this.colpro = colpro;
	}

	public void setColrov(BigDecimal colrov) {
		this.colrov = colrov;
	}

	public void setConfez(String confez) {
		this.confez = confez;
	}

	public void setConrir(BigDecimal conrir) {
		this.conrir = conrir;
	}

	public void setDivrio(String divrio) {
		this.divrio = divrio;
	}

	public void setDocabb(String docabb) {
		this.docabb = docabb;
	}

	public void setFiera(String fiera) {
		this.fiera = fiera;
	}

	public void setFille1(String fille1) {
		this.fille1 = fille1;
	}

	public void setFillek(String fillek) {
		this.fillek = fillek;
	}

	public void setFlabar(String flabar) {
		this.flabar = flabar;
	}

	public void setFlabdef(String flabdef) {
		this.flabdef = flabdef;
	}

	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}

	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}

	public void setFlagr1(String flagr1) {
		this.flagr1 = flagr1;
	}

	public void setFlagr2(String flagr2) {
		this.flagr2 = flagr2;
	}

	public void setFlagr3(String flagr3) {
		this.flagr3 = flagr3;
	}

	public void setFornit(String fornit) {
		this.fornit = fornit;
	}

	public void setGrptot(String grptot) {
		this.grptot = grptot;
	}

	public void setId(OrdiniPK id) {
		this.id = id;
	}

	public void setLavora(String lavora) {
		this.lavora = lavora;
	}

	public void setLavoraz(String lavoraz) {
		this.lavoraz = lavoraz;
	}

	public void setLetgr1(String letgr1) {
		this.letgr1 = letgr1;
	}

	public void setLetgr2(String letgr2) {
		this.letgr2 = letgr2;
	}

	public void setLetgr3(String letgr3) {
		this.letgr3 = letgr3;
	}

	public void setMtanno(BigDecimal mtanno) {
		this.mtanno = mtanno;
	}

	public void setMtoya(String mtoya) {
		this.mtoya = mtoya;
	}

	public void setNoogre(BigDecimal noogre) {
		this.noogre = noogre;
	}

	public void setNotrio(String notrio) {
		this.notrio = notrio;
	}

	public void setNpzlao(BigDecimal npzlao) {
		this.npzlao = npzlao;
	}

	public void setNskgre(BigDecimal nskgre) {
		this.nskgre = nskgre;
	}

	public void setOcdear(String ocdear) {
		this.ocdear = ocdear;
	}

	public void setOcoric(BigDecimal ocoric) {
		this.ocoric = ocoric;
	}

	public void setOcsedo(BigDecimal ocsedo) {
		this.ocsedo = ocsedo;
	}

	public void setOcsesc(BigDecimal ocsesc) {
		this.ocsesc = ocsesc;
	}

	public void setOfper2(BigDecimal ofper2) {
		this.ofper2 = ofper2;
	}

	public void setOrabmt(Double orabmt) {
		this.orabmt = orabmt;
	}

	public void setOrabpc(Double orabpc) {
		this.orabpc = orabpc;
	}

	public void setOrappa(BigDecimal orappa) {
		this.orappa = orappa;
	}

	public void setOrappg(BigDecimal orappg) {
		this.orappg = orappg;
	}

	public void setOrappm(BigDecimal orappm) {
		this.orappm = orappm;
	}

	public void setOrapps(BigDecimal orapps) {
		this.orapps = orapps;
	}

	public void setOrcoan(String orcoan) {
		this.orcoan = orcoan;
	}

	public void setOrdprop(String ordprop) {
		this.ordprop = ordprop;
	}

	public void setOrdtaa(BigDecimal ordtaa) {
		this.ordtaa = ordtaa;
	}

	public void setOrdtgg(BigDecimal ordtgg) {
		this.ordtgg = ordtgg;
	}

	public void setOrdtmm(BigDecimal ordtmm) {
		this.ordtmm = ordtmm;
	}

	public void setOrperr(BigDecimal orperr) {
		this.orperr = orperr;
	}

	public void setOrpeur(BigDecimal orpeur) {
		this.orpeur = orpeur;
	}

	public void setOrprio(String orprio) {
		this.orprio = orprio;
	}

	public void setOrprls(BigDecimal orprls) {
		this.orprls = orprls;
	}

	public void setOrrap2(BigDecimal orrap2) {
		this.orrap2 = orrap2;
	}

	public void setOrrapp(BigDecimal orrapp) {
		this.orrapp = orrapp;
	}

	public void setOrrepa(BigDecimal orrepa) {
		this.orrepa = orrepa;
	}

	public void setOrsiap(String orsiap) {
		this.orsiap = orsiap;
	}

	public void setOrsico(String orsico) {
		this.orsico = orsico;
	}

	public void setOrsosp(String orsosp) {
		this.orsosp = orsosp;
	}

	public void setPreord(BigDecimal preord) {
		this.preord = preord;
	}

	public void setPrgre2(BigDecimal prgre2) {
		this.prgre2 = prgre2;
	}

	public void setPrgre3(BigDecimal prgre3) {
		this.prgre3 = prgre3;
	}

	public void setPrmtan(BigDecimal prmtan) {
		this.prmtan = prmtan;
	}

	public void setProcam(String procam) {
		this.procam = procam;
	}

	public void setProtdo(String protdo) {
		this.protdo = protdo;
	}

	public void setPrpzan(BigDecimal prpzan) {
		this.prpzan = prpzan;
	}

	public void setPzann(BigDecimal pzann) {
		this.pzann = pzann;
	}

	public void setPzasse(BigDecimal pzasse) {
		this.pzasse = pzasse;
	}

	public void setPzeord(BigDecimal pzeord) {
		this.pzeord = pzeord;
	}

	public void setPzspe(BigDecimal pzspe) {
		this.pzspe = pzspe;
	}

	public void setQbolla(BigDecimal qbolla) {
		this.qbolla = qbolla;
	}

	public void setQta1or(BigDecimal qta1or) {
		this.qta1or = qta1or;
	}

	public void setQta2or(BigDecimal qta2or) {
		this.qta2or = qta2or;
	}

	public void setQtaord(BigDecimal qtaord) {
		this.qtaord = qtaord;
	}

	public void setQtaspo(BigDecimal qtaspo) {
		this.qtaspo = qtaspo;
	}

	public void setQtass(BigDecimal qtass) {
		this.qtass = qtass;
	}

	public void setQtatag(String qtatag) {
		this.qtatag = qtatag;
	}

	public void setQtorme(BigDecimal qtorme) {
		this.qtorme = qtorme;
	}

	public void setReparto(String reparto) {
		this.reparto = reparto;
	}

	public void setRestok(String restok) {
		this.restok = restok;
	}

	public void setRifordor(BigDecimal rifordor) {
		this.rifordor = rifordor;
	}

	public void setRiforf(String riforf) {
		this.riforf = riforf;
	}

	public void setRiforr(String riforr) {
		this.riforr = riforr;
	}

	public void setRigaorig(BigDecimal rigaorig) {
		this.rigaorig = rigaorig;
	}

	public void setSaldor(String saldor) {
		this.saldor = saldor;
	}

	public void setSalprs(String salprs) {
		this.salprs = salprs;
	}

	public void setScador(BigDecimal scador) {
		this.scador = scador;
	}

	public void setScapro(BigDecimal scapro) {
		this.scapro = scapro;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public void setTestata(Teordi testata) {
		this.testata = testata;
	}

	public void setTipcam(String tipcam) {
		this.tipcam = tipcam;
	}

	public void setTipcco(String tipcco) {
		this.tipcco = tipcco;
	}

	public void setTipoor(BigDecimal tipoor) {
		this.tipoor = tipoor;
	}

	public void setTiptir(String tiptir) {
		this.tiptir = tiptir;
	}

}