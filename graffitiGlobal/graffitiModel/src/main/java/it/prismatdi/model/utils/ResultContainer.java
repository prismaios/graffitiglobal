package it.prismatdi.model.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class ResultContainer implements Serializable {
	private List<SumDataConversion>	dati;
	private Double							totQta;

	private Double							totValore;

	private List<SumDataConversion>	datiAp;

	private Double							totQtaAp;
	private Double							totValoreAp;
	private Double							totCosto;
	private ParametriAnalisiVendite	parametri;

	public ResultContainer() {
		dati = new ArrayList<>();
		totQta = 0D;
		totValore = 0D;
		totCosto = 0D;
	}

	public void addDati(SumDataConversion dato) {
		dati.add(dato);
	}

	public List<SumDataConversion> getDati() {
		return dati;
	}

	public List<SumDataConversion> getDatiAp() {
		return datiAp;
	}

	public ParametriAnalisiVendite getParametri() {
		return parametri;
	}

	public Double getTotCosto() {
		return totCosto;
	}

	public Double getTotQta() {
		return totQta;
	}

	public Double getTotQtaAp() {
		return totQtaAp;
	}

	public Double getTotValore() {
		return totValore;
	}

	public Double getTotValoreAp() {
		return totValoreAp;
	}

	public void setDati(List<SumDataConversion> dati) {
		this.dati = dati;
	}

	public void setDatiAp(List<SumDataConversion> datiAp) {
		this.datiAp = datiAp;
	}

	public void setParametri(ParametriAnalisiVendite parametri) {
		this.parametri = parametri;
	}

	public void setTotCosto(Double totCosto) {
		this.totCosto = totCosto;
	}

	public void setTotQta(Double totQta) {
		this.totQta = totQta;
	}

	public void setTotQtaAp(Double totQtaAp) {
		this.totQtaAp = totQtaAp;
	}

	public void setTotValore(Double totValore) {
		this.totValore = totValore;
	}

	public void setTotValoreAp(Double totValoreAp) {
		this.totValoreAp = totValoreAp;
	}

	public void sumCosto(Double costo) {
		totCosto += costo;
	}

	public void sumQta(Double qta) {
		totQta += qta;
	}

	public void sumValore(Double val) {
		totValore += val;
	}

}
