package it.prismatdi.model.user;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the utente database table.
 * 
 */
@Entity
@NamedQuery(name="Utente.findAll", query="SELECT u FROM Utente u")
public class Utente implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UtentePK id;

	private String datcre;

	private String grucod;

	private String nonusa;

	private String noteg;

	private String noteg2;

	private String passwr;

	private String repcod;

	private String tipogu;

	private String utecog;

	private String utenom;

	public Utente() {
	}

	public UtentePK getId() {
		return this.id;
	}

	public void setId(UtentePK id) {
		this.id = id;
	}

	public String getDatcre() {
		return this.datcre;
	}

	public void setDatcre(String datcre) {
		this.datcre = datcre;
	}

	public String getGrucod() {
		return this.grucod;
	}

	public void setGrucod(String grucod) {
		this.grucod = grucod;
	}

	public String getNonusa() {
		return this.nonusa;
	}

	public void setNonusa(String nonusa) {
		this.nonusa = nonusa;
	}

	public String getNoteg() {
		return this.noteg;
	}

	public void setNoteg(String noteg) {
		this.noteg = noteg;
	}

	public String getNoteg2() {
		return this.noteg2;
	}

	public void setNoteg2(String noteg2) {
		this.noteg2 = noteg2;
	}

	public String getPasswr() {
		return this.passwr;
	}

	public void setPasswr(String passwr) {
		this.passwr = passwr;
	}

	public String getRepcod() {
		return this.repcod;
	}

	public void setRepcod(String repcod) {
		this.repcod = repcod;
	}

	public String getTipogu() {
		return this.tipogu;
	}

	public void setTipogu(String tipogu) {
		this.tipogu = tipogu;
	}

	public String getUtecog() {
		return this.utecog;
	}

	public void setUtecog(String utecog) {
		this.utecog = utecog;
	}

	public String getUtenom() {
		return this.utenom;
	}

	public void setUtenom(String utenom) {
		this.utenom = utenom;
	}

}