package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the matpri database table.
 *
 */
@Entity
@NamedQuery(name = "Matpri.findAll", query = "SELECT m FROM Matpri m")
public class Matpri implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private MatpriPK				id;

	private BigDecimal			mt1cos;

	private BigDecimal			mt1for;

	private BigDecimal			mt2cos;

	private BigDecimal			mt2for;

	private String					mtacpr;

	private BigDecimal			mtacq1;

	private BigDecimal			mtacq2;

	private String					mtafit;

	private BigDecimal			mtalp1;

	private BigDecimal			mtalp2;

	private BigDecimal			mtalt1;

	private BigDecimal			mtalte;

	private BigDecimal			mtbaco;

	private String					mtbase;

	private String					mtccol;

	private String					mtcdog;

	private String					mtciva;

	private String					mtclxc;

	private String					mtco1f;

	private String					mtco2f;

	private String					mtcola;

	private String					mtcomp;

	private String					mtcosk;

	private BigDecimal			mtcrea;

	private BigDecimal			mtcstd;

	private String					mtdes1;

	private String					mtdesc;

	private String					mtdess;

	private String					mtdett;

	private BigDecimal			mtdivi;

	private BigDecimal			mtelor;

	private BigDecimal			mteltr;

	private String					mtfami;

	private String					mtfill;

	private String					mtfiti;

	private BigDecimal			mtfixc;

	private BigDecimal			mtfixp;

	private BigDecimal			mtfixs;

	private String					mtgest;

	private String					mtgfis;

	private BigDecimal			mtgiri;

	private BigDecimal			mtgito;

	private String					mtgreg;

	private String					mtgrme;

	private BigDecimal			mtlupe;

	private String					mtmant;

	private BigDecimal			mtmini;

	private BigDecimal			mtncap;

	private String					mtnoli;

	private String					mtnote;

	private String					mtorig;

	private String					mtpafa;

	private BigDecimal			mtpcma;

	private BigDecimal			mtpeca;

	private BigDecimal			mtpepa;

	private BigDecimal			mtpes1;

	private BigDecimal			mtpeso;

	private BigDecimal			mtpoz1;

	private BigDecimal			mtpoz2;

	private BigDecimal			mtpre1;

	private BigDecimal			mtpre2;

	private BigDecimal			mtpre3;

	private BigDecimal			mtpre4;

	private BigDecimal			mtragf;

	private String					mtsfam;

	private String					mtsigl;

	private String					mtsxbo;

	private BigDecimal			mttidx;

	private BigDecimal			mttima;

	private BigDecimal			mttine;

	private BigDecimal			mttito;

	private String					mttpzf;

	private BigDecimal			mtulac;

	private BigDecimal			mtulap;

	private BigDecimal			mtulas;

	private String					mtunmi;

	private BigDecimal			mtvari;

	private String					mtzs;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Matpri other = (Matpri) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public MatpriPK getId() {
		return id;
	}

	public BigDecimal getMt1cos() {
		return mt1cos;
	}

	public BigDecimal getMt1for() {
		return mt1for;
	}

	public BigDecimal getMt2cos() {
		return mt2cos;
	}

	public BigDecimal getMt2for() {
		return mt2for;
	}

	public String getMtacpr() {
		return mtacpr;
	}

	public BigDecimal getMtacq1() {
		return mtacq1;
	}

	public BigDecimal getMtacq2() {
		return mtacq2;
	}

	public String getMtafit() {
		return mtafit;
	}

	public BigDecimal getMtalp1() {
		return mtalp1;
	}

	public BigDecimal getMtalp2() {
		return mtalp2;
	}

	public BigDecimal getMtalt1() {
		return mtalt1;
	}

	public BigDecimal getMtalte() {
		return mtalte;
	}

	public BigDecimal getMtbaco() {
		return mtbaco;
	}

	public String getMtbase() {
		return mtbase;
	}

	public String getMtccol() {
		return mtccol;
	}

	public String getMtcdog() {
		return mtcdog;
	}

	public String getMtciva() {
		return mtciva;
	}

	public String getMtclxc() {
		return mtclxc;
	}

	public String getMtco1f() {
		return mtco1f;
	}

	public String getMtco2f() {
		return mtco2f;
	}

	public String getMtcola() {
		return mtcola;
	}

	public String getMtcomp() {
		return mtcomp;
	}

	public String getMtcosk() {
		return mtcosk;
	}

	public BigDecimal getMtcrea() {
		return mtcrea;
	}

	public BigDecimal getMtcstd() {
		return mtcstd;
	}

	public String getMtdes1() {
		return mtdes1;
	}

	public String getMtdesc() {
		return mtdesc;
	}

	public String getMtdess() {
		return mtdess;
	}

	public String getMtdett() {
		return mtdett;
	}

	public BigDecimal getMtdivi() {
		return mtdivi;
	}

	public BigDecimal getMtelor() {
		return mtelor;
	}

	public BigDecimal getMteltr() {
		return mteltr;
	}

	public String getMtfami() {
		return mtfami;
	}

	public String getMtfill() {
		return mtfill;
	}

	public String getMtfiti() {
		return mtfiti;
	}

	public BigDecimal getMtfixc() {
		return mtfixc;
	}

	public BigDecimal getMtfixp() {
		return mtfixp;
	}

	public BigDecimal getMtfixs() {
		return mtfixs;
	}

	public String getMtgest() {
		return mtgest;
	}

	public String getMtgfis() {
		return mtgfis;
	}

	public BigDecimal getMtgiri() {
		return mtgiri;
	}

	public BigDecimal getMtgito() {
		return mtgito;
	}

	public String getMtgreg() {
		return mtgreg;
	}

	public String getMtgrme() {
		return mtgrme;
	}

	public BigDecimal getMtlupe() {
		return mtlupe;
	}

	public String getMtmant() {
		return mtmant;
	}

	public BigDecimal getMtmini() {
		return mtmini;
	}

	public BigDecimal getMtncap() {
		return mtncap;
	}

	public String getMtnoli() {
		return mtnoli;
	}

	public String getMtnote() {
		return mtnote;
	}

	public String getMtorig() {
		return mtorig;
	}

	public String getMtpafa() {
		return mtpafa;
	}

	public BigDecimal getMtpcma() {
		return mtpcma;
	}

	public BigDecimal getMtpeca() {
		return mtpeca;
	}

	public BigDecimal getMtpepa() {
		return mtpepa;
	}

	public BigDecimal getMtpes1() {
		return mtpes1;
	}

	public BigDecimal getMtpeso() {
		return mtpeso;
	}

	public BigDecimal getMtpoz1() {
		return mtpoz1;
	}

	public BigDecimal getMtpoz2() {
		return mtpoz2;
	}

	public BigDecimal getMtpre1() {
		return mtpre1;
	}

	public BigDecimal getMtpre2() {
		return mtpre2;
	}

	public BigDecimal getMtpre3() {
		return mtpre3;
	}

	public BigDecimal getMtpre4() {
		return mtpre4;
	}

	public BigDecimal getMtragf() {
		return mtragf;
	}

	public String getMtsfam() {
		return mtsfam;
	}

	public String getMtsigl() {
		return mtsigl;
	}

	public String getMtsxbo() {
		return mtsxbo;
	}

	public BigDecimal getMttidx() {
		return mttidx;
	}

	public BigDecimal getMttima() {
		return mttima;
	}

	public BigDecimal getMttine() {
		return mttine;
	}

	public BigDecimal getMttito() {
		return mttito;
	}

	public String getMttpzf() {
		return mttpzf;
	}

	public BigDecimal getMtulac() {
		return mtulac;
	}

	public BigDecimal getMtulap() {
		return mtulap;
	}

	public BigDecimal getMtulas() {
		return mtulas;
	}

	public String getMtunmi() {
		return mtunmi;
	}

	public BigDecimal getMtvari() {
		return mtvari;
	}

	public String getMtzs() {
		return mtzs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setId(MatpriPK id) {
		this.id = id;
	}

	public void setMt1cos(BigDecimal mt1cos) {
		this.mt1cos = mt1cos;
	}

	public void setMt1for(BigDecimal mt1for) {
		this.mt1for = mt1for;
	}

	public void setMt2cos(BigDecimal mt2cos) {
		this.mt2cos = mt2cos;
	}

	public void setMt2for(BigDecimal mt2for) {
		this.mt2for = mt2for;
	}

	public void setMtacpr(String mtacpr) {
		this.mtacpr = mtacpr;
	}

	public void setMtacq1(BigDecimal mtacq1) {
		this.mtacq1 = mtacq1;
	}

	public void setMtacq2(BigDecimal mtacq2) {
		this.mtacq2 = mtacq2;
	}

	public void setMtafit(String mtafit) {
		this.mtafit = mtafit;
	}

	public void setMtalp1(BigDecimal mtalp1) {
		this.mtalp1 = mtalp1;
	}

	public void setMtalp2(BigDecimal mtalp2) {
		this.mtalp2 = mtalp2;
	}

	public void setMtalt1(BigDecimal mtalt1) {
		this.mtalt1 = mtalt1;
	}

	public void setMtalte(BigDecimal mtalte) {
		this.mtalte = mtalte;
	}

	public void setMtbaco(BigDecimal mtbaco) {
		this.mtbaco = mtbaco;
	}

	public void setMtbase(String mtbase) {
		this.mtbase = mtbase;
	}

	public void setMtccol(String mtccol) {
		this.mtccol = mtccol;
	}

	public void setMtcdog(String mtcdog) {
		this.mtcdog = mtcdog;
	}

	public void setMtciva(String mtciva) {
		this.mtciva = mtciva;
	}

	public void setMtclxc(String mtclxc) {
		this.mtclxc = mtclxc;
	}

	public void setMtco1f(String mtco1f) {
		this.mtco1f = mtco1f;
	}

	public void setMtco2f(String mtco2f) {
		this.mtco2f = mtco2f;
	}

	public void setMtcola(String mtcola) {
		this.mtcola = mtcola;
	}

	public void setMtcomp(String mtcomp) {
		this.mtcomp = mtcomp;
	}

	public void setMtcosk(String mtcosk) {
		this.mtcosk = mtcosk;
	}

	public void setMtcrea(BigDecimal mtcrea) {
		this.mtcrea = mtcrea;
	}

	public void setMtcstd(BigDecimal mtcstd) {
		this.mtcstd = mtcstd;
	}

	public void setMtdes1(String mtdes1) {
		this.mtdes1 = mtdes1;
	}

	public void setMtdesc(String mtdesc) {
		this.mtdesc = mtdesc;
	}

	public void setMtdess(String mtdess) {
		this.mtdess = mtdess;
	}

	public void setMtdett(String mtdett) {
		this.mtdett = mtdett;
	}

	public void setMtdivi(BigDecimal mtdivi) {
		this.mtdivi = mtdivi;
	}

	public void setMtelor(BigDecimal mtelor) {
		this.mtelor = mtelor;
	}

	public void setMteltr(BigDecimal mteltr) {
		this.mteltr = mteltr;
	}

	public void setMtfami(String mtfami) {
		this.mtfami = mtfami;
	}

	public void setMtfill(String mtfill) {
		this.mtfill = mtfill;
	}

	public void setMtfiti(String mtfiti) {
		this.mtfiti = mtfiti;
	}

	public void setMtfixc(BigDecimal mtfixc) {
		this.mtfixc = mtfixc;
	}

	public void setMtfixp(BigDecimal mtfixp) {
		this.mtfixp = mtfixp;
	}

	public void setMtfixs(BigDecimal mtfixs) {
		this.mtfixs = mtfixs;
	}

	public void setMtgest(String mtgest) {
		this.mtgest = mtgest;
	}

	public void setMtgfis(String mtgfis) {
		this.mtgfis = mtgfis;
	}

	public void setMtgiri(BigDecimal mtgiri) {
		this.mtgiri = mtgiri;
	}

	public void setMtgito(BigDecimal mtgito) {
		this.mtgito = mtgito;
	}

	public void setMtgreg(String mtgreg) {
		this.mtgreg = mtgreg;
	}

	public void setMtgrme(String mtgrme) {
		this.mtgrme = mtgrme;
	}

	public void setMtlupe(BigDecimal mtlupe) {
		this.mtlupe = mtlupe;
	}

	public void setMtmant(String mtmant) {
		this.mtmant = mtmant;
	}

	public void setMtmini(BigDecimal mtmini) {
		this.mtmini = mtmini;
	}

	public void setMtncap(BigDecimal mtncap) {
		this.mtncap = mtncap;
	}

	public void setMtnoli(String mtnoli) {
		this.mtnoli = mtnoli;
	}

	public void setMtnote(String mtnote) {
		this.mtnote = mtnote;
	}

	public void setMtorig(String mtorig) {
		this.mtorig = mtorig;
	}

	public void setMtpafa(String mtpafa) {
		this.mtpafa = mtpafa;
	}

	public void setMtpcma(BigDecimal mtpcma) {
		this.mtpcma = mtpcma;
	}

	public void setMtpeca(BigDecimal mtpeca) {
		this.mtpeca = mtpeca;
	}

	public void setMtpepa(BigDecimal mtpepa) {
		this.mtpepa = mtpepa;
	}

	public void setMtpes1(BigDecimal mtpes1) {
		this.mtpes1 = mtpes1;
	}

	public void setMtpeso(BigDecimal mtpeso) {
		this.mtpeso = mtpeso;
	}

	public void setMtpoz1(BigDecimal mtpoz1) {
		this.mtpoz1 = mtpoz1;
	}

	public void setMtpoz2(BigDecimal mtpoz2) {
		this.mtpoz2 = mtpoz2;
	}

	public void setMtpre1(BigDecimal mtpre1) {
		this.mtpre1 = mtpre1;
	}

	public void setMtpre2(BigDecimal mtpre2) {
		this.mtpre2 = mtpre2;
	}

	public void setMtpre3(BigDecimal mtpre3) {
		this.mtpre3 = mtpre3;
	}

	public void setMtpre4(BigDecimal mtpre4) {
		this.mtpre4 = mtpre4;
	}

	public void setMtragf(BigDecimal mtragf) {
		this.mtragf = mtragf;
	}

	public void setMtsfam(String mtsfam) {
		this.mtsfam = mtsfam;
	}

	public void setMtsigl(String mtsigl) {
		this.mtsigl = mtsigl;
	}

	public void setMtsxbo(String mtsxbo) {
		this.mtsxbo = mtsxbo;
	}

	public void setMttidx(BigDecimal mttidx) {
		this.mttidx = mttidx;
	}

	public void setMttima(BigDecimal mttima) {
		this.mttima = mttima;
	}

	public void setMttine(BigDecimal mttine) {
		this.mttine = mttine;
	}

	public void setMttito(BigDecimal mttito) {
		this.mttito = mttito;
	}

	public void setMttpzf(String mttpzf) {
		this.mttpzf = mttpzf;
	}

	public void setMtulac(BigDecimal mtulac) {
		this.mtulac = mtulac;
	}

	public void setMtulap(BigDecimal mtulap) {
		this.mtulap = mtulap;
	}

	public void setMtulas(BigDecimal mtulas) {
		this.mtulas = mtulas;
	}

	public void setMtunmi(String mtunmi) {
		this.mtunmi = mtunmi;
	}

	public void setMtvari(BigDecimal mtvari) {
		this.mtvari = mtvari;
	}

	public void setMtzs(String mtzs) {
		this.mtzs = mtzs;
	}

}