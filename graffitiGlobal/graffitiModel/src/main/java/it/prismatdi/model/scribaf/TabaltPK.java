package it.prismatdi.model.scribaf;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the tabalt database table.
 *
 */
@Embeddable
public class TabaltPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long	serialVersionUID	= 1L;

	private String					tltipo;

	private String					tlsigl;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TabaltPK other = (TabaltPK) obj;
		if (tlsigl == null) {
			if (other.tlsigl != null) {
				return false;
			}
		}
		else if (!tlsigl.equals(other.tlsigl)) {
			return false;
		}
		if (tltipo == null) {
			if (other.tltipo != null) {
				return false;
			}
		}
		else if (!tltipo.equals(other.tltipo)) {
			return false;
		}
		return true;
	}

	public String getTlsigl() {
		return tlsigl;
	}

	public String getTltipo() {
		return tltipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (tlsigl == null ? 0 : tlsigl.hashCode());
		result = prime * result + (tltipo == null ? 0 : tltipo.hashCode());
		return result;
	}

	public void setTlsigl(String tlsigl) {
		this.tlsigl = tlsigl;
	}

	public void setTltipo(String tltipo) {
		this.tltipo = tltipo;
	}

}