package it.prismatdi.model.utils;

import java.math.BigDecimal;

import javax.persistence.Tuple;

public class UnimagData {
	private String		articolo;
	private Integer	colore;
	private String		descArticolo;
	private Double		qta;
	private String		bagno;
	private String		note;

	public UnimagData() {
		// TODO Auto-generated constructor stub
	}

	public UnimagData(String articolo, BigDecimal colore, String descArticolo, BigDecimal qta, String bagno) {
		super();
		this.articolo = articolo;
		this.colore = colore.intValue();
		this.descArticolo = descArticolo;
		this.qta = qta.doubleValue();
		this.bagno = bagno;
	}

	public UnimagData(Tuple tupla) {
		articolo = tupla.get("ARTICO", String.class);
		colore = tupla.get("COLORE", BigDecimal.class)
				.intValue();
		descArticolo = tupla.get("DESC_ARTI", String.class);
		qta = tupla.get("QUANTITA", BigDecimal.class)
				.doubleValue();
		bagno = tupla.get("BAGNO", String.class);
	}

	public String getArticolo() {
		return articolo;
	}

	public String getBagno() {
		return bagno;
	}

	public Integer getColore() {
		return colore;
	}

	public String getDescArticolo() {
		return descArticolo;
	}

	public String getNote() {
		return note;
	}

	public Double getQta() {
		return qta;
	}

	public void setArticolo(String articolo) {
		this.articolo = articolo;
	}

	public void setBagno(String bagno) {
		this.bagno = bagno;
	}

	public void setColore(Integer colore) {
		this.colore = colore;
	}

	public void setDescArticolo(String descArticolo) {
		this.descArticolo = descArticolo;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setQta(Double qta) {
		this.qta = qta;
	}

}
