package it.prismatdi.model.utils;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Busta implements Serializable {
	private String		codice;
	private String		descrizione;
	private Boolean	toStringCosa	= false;	// con false to string stampa desc con true stampa codice

	public Busta(String codice, String descrizione) {
		super();
		if (codice.startsWith("00G")) {
			this.codice = codice.substring(3);
		}
		else {
			this.codice = codice;
		}
		this.descrizione = descrizione;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Busta other = (Busta) obj;
		if (codice == null) {
			if (other.codice != null) {
				return false;
			}
		}
		else if (!codice.equals(other.codice)) {
			return false;
		}
		return true;
	}

	public String getCodice() {
		return codice;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public Boolean getToStringCosa() {
		return toStringCosa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (codice == null ? 0 : codice.hashCode());
		return result;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setToStringCosa(Boolean toStringCosa) {
		this.toStringCosa = toStringCosa;
	}

	@Override
	public String toString() {
		if (!toStringCosa) {
			return descrizione;
		}
		else {
			return codice;
		}
	}

}
