package it.prismatdi.model.scribaf;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the tabalt database table.
 *
 */
@Entity
@NamedQuery(name = "Tabalt.findAll", query = "SELECT t FROM Tabalt t")
public class Tabalt implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private TabaltPK				id;

	private String					tlblak;

	private String					tlcee;

	private String					tlcivs;

	private String					tldesc;

	private String					tlpaau;

	private String					tlsige;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tabalt other = (Tabalt) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public TabaltPK getId() {
		return id;
	}

	public String getTlblak() {
		return tlblak;
	}

	public String getTlcee() {
		return tlcee;
	}

	public String getTlcivs() {
		return tlcivs;
	}

	public String getTldesc() {
		return tldesc;
	}

	public String getTlpaau() {
		return tlpaau;
	}

	public String getTlsige() {
		return tlsige;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setId(TabaltPK id) {
		this.id = id;
	}

	public void setTlblak(String tlblak) {
		this.tlblak = tlblak;
	}

	public void setTlcee(String tlcee) {
		this.tlcee = tlcee;
	}

	public void setTlcivs(String tlcivs) {
		this.tlcivs = tlcivs;
	}

	public void setTldesc(String tldesc) {
		this.tldesc = tldesc;
	}

	public void setTlpaau(String tlpaau) {
		this.tlpaau = tlpaau;
	}

	public void setTlsige(String tlsige) {
		this.tlsige = tlsige;
	}

}