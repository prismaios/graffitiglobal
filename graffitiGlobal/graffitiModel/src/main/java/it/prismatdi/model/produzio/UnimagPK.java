package it.prismatdi.model.produzio;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the unimag database table.
 * 
 */
@Embeddable
public class UnimagPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String ditta;

	private int numuni;

	private String tipoar;

	public UnimagPK() {
	}
	public String getDitta() {
		return this.ditta;
	}
	public void setDitta(String ditta) {
		this.ditta = ditta;
	}
	public int getNumuni() {
		return this.numuni;
	}
	public void setNumuni(int numuni) {
		this.numuni = numuni;
	}
	public String getTipoar() {
		return this.tipoar;
	}
	public void setTipoar(String tipoar) {
		this.tipoar = tipoar;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UnimagPK)) {
			return false;
		}
		UnimagPK castOther = (UnimagPK)other;
		return 
			this.ditta.equals(castOther.ditta)
			&& (this.numuni == castOther.numuni)
			&& this.tipoar.equals(castOther.tipoar);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.ditta.hashCode();
		hash = hash * prime + this.numuni;
		hash = hash * prime + this.tipoar.hashCode();
		
		return hash;
	}
}