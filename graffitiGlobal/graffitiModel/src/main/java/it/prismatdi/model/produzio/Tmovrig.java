package it.prismatdi.model.produzio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

/**
 * The persistent class for the tmovrig database table.
 *
 */
@Entity
@NamedQuery(name = "Tmovrig.findAll", query = "SELECT t FROM Tmovrig t")
public class Tmovrig implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private TmovrigPK				id;

	private String					mraliv;

	private Double					mralqt;

	private String					mralum;

	private String					mrarde;

	private String					mrarti;

	private String					mrbagn;

	private String					mrcapr;

	private String					mrcasc;

	private String					mrcau;

	private String					mrcert;

	private String					mrclip;

	private String					mrcode;

	private String					mrcolo;

	private String					mrconf;

	private String					mrcontft;

	private String					mrdafa;

	private String					mrdida;

	private String					mrdisp;

	private String					mrdivi;

	private String					mrdoti;

	private String					mrfase;

	private String					mrfaslav;

	private String					mrfass;

	private String					mrfil;

	private String					mrfla2;

	private String					mrflag;

	private Integer				mrgrun;

	private String					mrlape;

	private Double					mrmag1;

	private Double					mrmag2;

	private Double					mrmag3;

	private String					mrmaga;

	private Double					mrmtgr;

	private String					mrnfat;

	private String					mrnot2;

	private String					mrnote;

	private BigDecimal			mrnpezzi;

	private Integer				mrnuco;

	private Integer				mrordi;

	private Integer				mrorri;

	private String					mrpaga;

	private Double					mrper2;

	private Double					mrper3;

	private Double					mrperc;

	private Double					mrpeso;

	private Double					mrprdi;

	private Double					mrprli;

	private Integer				mrprog;

	private Double					mrprsc;

	private String					mrpta;

	private Integer				mrpzgr;

	private Integer				mrpzor;

	private Double					mrqtaa;

	private Double					mrqtal;

	private Double					mrqtan;

	private BigDecimal			mrqtft;

	private String					mrrap2;

	private String					mrrap3;

	private String					mrrapp;

	private Integer				mrrima;

	private Double					mrsco1;

	private Double					mrsco2;

	private Double					mrsco3;

	private Double					mrscon;

	private String					mrstat;

	private String					mrstato;

	private String					mrstoc;

	private String					mrtaco;

	private String					mrtari;

	private String					mrtela;

	private String					mrtiar;

	private String					mrtior;

	private String					mrubic;

	private String					mrumis;

	@ManyToOne
	@JoinColumn(name = "mrkeye", insertable = false, updatable = false)
	private Tmovmag				testa;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "mrordi", referencedColumnName = "noordi", insertable = false, updatable = false),
			@JoinColumn(name = "mrorri", referencedColumnName = "nskro", insertable = false, updatable = false) })
	private Ordini					rigaOrdine;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumnsOrFormulas(value = {
			@JoinColumnOrFormula(column = @JoinColumn(name = "mrarti", referencedColumnName = "mtarti", insertable = false,
					updatable = false,
					nullable = false)),
			@JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "mttipo", value = "'T'")),
			@JoinColumnOrFormula(formula = @JoinFormula(referencedColumnName = "mtcolo", value = "0")) })
	private Matpri					matpri;

	@Transient
	private String					codice;

	@Transient
	private String					descrizione;

	@Transient
	private String					desColore;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tmovrig other = (Tmovrig) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public String getCodice() {
		return codice;
	}

	public String getDesColore() {
		return desColore;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public TmovrigPK getId() {
		return id;
	}

	public Matpri getMatpri() {
		return matpri;
	}

	public String getMraliv() {
		return mraliv;
	}

	public Double getMralqt() {
		return mralqt;
	}

	public String getMralum() {
		return mralum;
	}

	public String getMrarde() {
		return mrarde;
	}

	public String getMrarti() {
		return mrarti;
	}

	public String getMrbagn() {
		return mrbagn;
	}

	public String getMrcapr() {
		return mrcapr;
	}

	public String getMrcasc() {
		return mrcasc;
	}

	public String getMrcau() {
		return mrcau;
	}

	public String getMrcert() {
		return mrcert;
	}

	public String getMrclip() {
		return mrclip;
	}

	public String getMrcode() {
		return mrcode;
	}

	public String getMrcolo() {
		return mrcolo;
	}

	public String getMrconf() {
		return mrconf;
	}

	public String getMrcontft() {
		return mrcontft;
	}

	public String getMrdafa() {
		return mrdafa;
	}

	public String getMrdida() {
		return mrdida;
	}

	public String getMrdisp() {
		return mrdisp;
	}

	public String getMrdivi() {
		return mrdivi;
	}

	public String getMrdoti() {
		return mrdoti;
	}

	public String getMrfase() {
		return mrfase;
	}

	public String getMrfaslav() {
		return mrfaslav;
	}

	public String getMrfass() {
		return mrfass;
	}

	public String getMrfil() {
		return mrfil;
	}

	public String getMrfla2() {
		return mrfla2;
	}

	public String getMrflag() {
		return mrflag;
	}

	public Integer getMrgrun() {
		return mrgrun;
	}

	public String getMrlape() {
		return mrlape;
	}

	public Double getMrmag1() {
		return mrmag1;
	}

	public Double getMrmag2() {
		return mrmag2;
	}

	public Double getMrmag3() {
		return mrmag3;
	}

	public String getMrmaga() {
		return mrmaga;
	}

	public Double getMrmtgr() {
		return mrmtgr;
	}

	public String getMrnfat() {
		return mrnfat;
	}

	public String getMrnot2() {
		return mrnot2;
	}

	public String getMrnote() {
		return mrnote;
	}

	public BigDecimal getMrnpezzi() {
		return mrnpezzi;
	}

	public Integer getMrnuco() {
		return mrnuco;
	}

	public Integer getMrordi() {
		return mrordi;
	}

	public Integer getMrorri() {
		return mrorri;
	}

	public String getMrpaga() {
		return mrpaga;
	}

	public Double getMrper2() {
		return mrper2;
	}

	public Double getMrper3() {
		return mrper3;
	}

	public Double getMrperc() {
		return mrperc;
	}

	public Double getMrpeso() {
		return mrpeso;
	}

	public Double getMrprdi() {
		return mrprdi;
	}

	public Double getMrprli() {
		return mrprli;
	}

	public Integer getMrprog() {
		return mrprog;
	}

	public Double getMrprsc() {
		return mrprsc;
	}

	public String getMrpta() {
		return mrpta;
	}

	public Integer getMrpzgr() {
		return mrpzgr;
	}

	public Integer getMrpzor() {
		return mrpzor;
	}

	public Double getMrqtaa() {
		return mrqtaa;
	}

	public Double getMrqtal() {
		return mrqtal;
	}

	public Double getMrqtan() {
		return mrqtan;
	}

	public BigDecimal getMrqtft() {
		return mrqtft;
	}

	public String getMrrap2() {
		return mrrap2;
	}

	public String getMrrap3() {
		return mrrap3;
	}

	public String getMrrapp() {
		return mrrapp;
	}

	public Integer getMrrima() {
		return mrrima;
	}

	public Double getMrsco1() {
		return mrsco1;
	}

	public Double getMrsco2() {
		return mrsco2;
	}

	public Double getMrsco3() {
		return mrsco3;
	}

	public Double getMrscon() {
		return mrscon;
	}

	public String getMrstat() {
		return mrstat;
	}

	public String getMrstato() {
		return mrstato;
	}

	public String getMrstoc() {
		return mrstoc;
	}

	public String getMrtaco() {
		return mrtaco;
	}

	public String getMrtari() {
		return mrtari;
	}

	public String getMrtela() {
		return mrtela;
	}

	public String getMrtiar() {
		return mrtiar;
	}

	public String getMrtior() {
		return mrtior;
	}

	public String getMrubic() {
		return mrubic;
	}

	public String getMrumis() {
		return mrumis;
	}

	public Ordini getRigaOrdine() {
		return rigaOrdine;
	}

	public Tmovmag getTesta() {
		return testa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public void setDesColore(String desColore) {
		this.desColore = desColore;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public void setId(TmovrigPK id) {
		this.id = id;
	}

	public void setMatpri(Matpri matpri) {
		this.matpri = matpri;
	}

	public void setMraliv(String mraliv) {
		this.mraliv = mraliv;
	}

	public void setMralqt(Double mralqt) {
		this.mralqt = mralqt;
	}

	public void setMralum(String mralum) {
		this.mralum = mralum;
	}

	public void setMrarde(String mrarde) {
		this.mrarde = mrarde;
	}

	public void setMrarti(String mrarti) {
		this.mrarti = mrarti;
	}

	public void setMrbagn(String mrbagn) {
		this.mrbagn = mrbagn;
	}

	public void setMrcapr(String mrcapr) {
		this.mrcapr = mrcapr;
	}

	public void setMrcasc(String mrcasc) {
		this.mrcasc = mrcasc;
	}

	public void setMrcau(String mrcau) {
		this.mrcau = mrcau;
	}

	public void setMrcert(String mrcert) {
		this.mrcert = mrcert;
	}

	public void setMrclip(String mrclip) {
		this.mrclip = mrclip;
	}

	public void setMrcode(String mrcode) {
		this.mrcode = mrcode;
	}

	public void setMrcolo(String mrcolo) {
		this.mrcolo = mrcolo;
	}

	public void setMrconf(String mrconf) {
		this.mrconf = mrconf;
	}

	public void setMrcontft(String mrcontft) {
		this.mrcontft = mrcontft;
	}

	public void setMrdafa(String mrdafa) {
		this.mrdafa = mrdafa;
	}

	public void setMrdida(String mrdida) {
		this.mrdida = mrdida;
	}

	public void setMrdisp(String mrdisp) {
		this.mrdisp = mrdisp;
	}

	public void setMrdivi(String mrdivi) {
		this.mrdivi = mrdivi;
	}

	public void setMrdoti(String mrdoti) {
		this.mrdoti = mrdoti;
	}

	public void setMrfase(String mrfase) {
		this.mrfase = mrfase;
	}

	public void setMrfaslav(String mrfaslav) {
		this.mrfaslav = mrfaslav;
	}

	public void setMrfass(String mrfass) {
		this.mrfass = mrfass;
	}

	public void setMrfil(String mrfil) {
		this.mrfil = mrfil;
	}

	public void setMrfla2(String mrfla2) {
		this.mrfla2 = mrfla2;
	}

	public void setMrflag(String mrflag) {
		this.mrflag = mrflag;
	}

	public void setMrgrun(Integer mrgrun) {
		this.mrgrun = mrgrun;
	}

	public void setMrlape(String mrlape) {
		this.mrlape = mrlape;
	}

	public void setMrmag1(Double mrmag1) {
		this.mrmag1 = mrmag1;
	}

	public void setMrmag2(Double mrmag2) {
		this.mrmag2 = mrmag2;
	}

	public void setMrmag3(Double mrmag3) {
		this.mrmag3 = mrmag3;
	}

	public void setMrmaga(String mrmaga) {
		this.mrmaga = mrmaga;
	}

	public void setMrmtgr(Double mrmtgr) {
		this.mrmtgr = mrmtgr;
	}

	public void setMrnfat(String mrnfat) {
		this.mrnfat = mrnfat;
	}

	public void setMrnot2(String mrnot2) {
		this.mrnot2 = mrnot2;
	}

	public void setMrnote(String mrnote) {
		this.mrnote = mrnote;
	}

	public void setMrnpezzi(BigDecimal mrnpezzi) {
		this.mrnpezzi = mrnpezzi;
	}

	public void setMrnuco(Integer mrnuco) {
		this.mrnuco = mrnuco;
	}

	public void setMrordi(Integer mrordi) {
		this.mrordi = mrordi;
	}

	public void setMrorri(Integer mrorri) {
		this.mrorri = mrorri;
	}

	public void setMrpaga(String mrpaga) {
		this.mrpaga = mrpaga;
	}

	public void setMrper2(Double mrper2) {
		this.mrper2 = mrper2;
	}

	public void setMrper3(Double mrper3) {
		this.mrper3 = mrper3;
	}

	public void setMrperc(Double mrperc) {
		this.mrperc = mrperc;
	}

	public void setMrpeso(Double mrpeso) {
		this.mrpeso = mrpeso;
	}

	public void setMrprdi(Double mrprdi) {
		this.mrprdi = mrprdi;
	}

	public void setMrprli(Double mrprli) {
		this.mrprli = mrprli;
	}

	public void setMrprog(Integer mrprog) {
		this.mrprog = mrprog;
	}

	public void setMrprsc(Double mrprsc) {
		this.mrprsc = mrprsc;
	}

	public void setMrpta(String mrpta) {
		this.mrpta = mrpta;
	}

	public void setMrpzgr(Integer mrpzgr) {
		this.mrpzgr = mrpzgr;
	}

	public void setMrpzor(Integer mrpzor) {
		this.mrpzor = mrpzor;
	}

	public void setMrqtaa(Double mrqtaa) {
		this.mrqtaa = mrqtaa;
	}

	public void setMrqtal(Double mrqtal) {
		this.mrqtal = mrqtal;
	}

	public void setMrqtan(Double mrqtan) {
		this.mrqtan = mrqtan;
	}

	public void setMrqtft(BigDecimal mrqtft) {
		this.mrqtft = mrqtft;
	}

	public void setMrrap2(String mrrap2) {
		this.mrrap2 = mrrap2;
	}

	public void setMrrap3(String mrrap3) {
		this.mrrap3 = mrrap3;
	}

	public void setMrrapp(String mrrapp) {
		this.mrrapp = mrrapp;
	}

	public void setMrrima(Integer mrrima) {
		this.mrrima = mrrima;
	}

	public void setMrsco1(Double mrsco1) {
		this.mrsco1 = mrsco1;
	}

	public void setMrsco2(Double mrsco2) {
		this.mrsco2 = mrsco2;
	}

	public void setMrsco3(Double mrsco3) {
		this.mrsco3 = mrsco3;
	}

	public void setMrscon(Double mrscon) {
		this.mrscon = mrscon;
	}

	public void setMrstat(String mrstat) {
		this.mrstat = mrstat;
	}

	public void setMrstato(String mrstato) {
		this.mrstato = mrstato;
	}

	public void setMrstoc(String mrstoc) {
		this.mrstoc = mrstoc;
	}

	public void setMrtaco(String mrtaco) {
		this.mrtaco = mrtaco;
	}

	public void setMrtari(String mrtari) {
		this.mrtari = mrtari;
	}

	public void setMrtela(String mrtela) {
		this.mrtela = mrtela;
	}

	public void setMrtiar(String mrtiar) {
		this.mrtiar = mrtiar;
	}

	public void setMrtior(String mrtior) {
		this.mrtior = mrtior;
	}

	public void setMrubic(String mrubic) {
		this.mrubic = mrubic;
	}

	public void setMrumis(String mrumis) {
		this.mrumis = mrumis;
	}

	public void setRigaOrdine(Ordini rigaOrdine) {
		this.rigaOrdine = rigaOrdine;
	}

	public void setTesta(Tmovmag testa) {
		this.testa = testa;
	}

}