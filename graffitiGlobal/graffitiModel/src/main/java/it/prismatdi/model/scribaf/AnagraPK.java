package it.prismatdi.model.scribaf;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * The primary key class for the anagra database table.
 *
 */
@Embeddable
public class AnagraPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long	serialVersionUID	= 1L;

	private String					anditt;

	private String					anmaco;

	private String					ansott;

	private long					antire;

	public AnagraPK() {
	}

	public AnagraPK(String anditt, String anmaco, String ansott, long antire) {
		super();
		this.anditt = anditt;
		this.anmaco = anmaco;
		this.ansott = ansott;
		this.antire = antire;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AnagraPK)) {
			return false;
		}
		final AnagraPK castOther = (AnagraPK) other;
		return anditt.equals(castOther.anditt)
				&& anmaco.equals(castOther.anmaco)
				&& ansott.equals(castOther.ansott)
				&& antire == castOther.antire;
	}

	public String getAnditt() {
		return anditt;
	}

	public String getAnmaco() {
		return anmaco;
	}

	public String getAnsott() {
		return ansott;
	}

	public long getAntire() {
		return antire;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + anditt.hashCode();
		hash = hash * prime + anmaco.hashCode();
		hash = hash * prime + ansott.hashCode();
		hash = hash * prime + (int) (antire ^ antire >>> 32);

		return hash;
	}

	public void setAnditt(String anditt) {
		this.anditt = anditt;
	}

	public void setAnmaco(String anmaco) {
		this.anmaco = anmaco;
	}

	public void setAnsott(String ansott) {
		this.ansott = ansott;
	}

	public void setAntire(long antire) {
		this.antire = antire;
	}
}