package it.prismatdi.model.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import it.prismatdi.model.produzio.Unimag;
import it.prismatdi.model.produzio.UnimagPK_;
import it.prismatdi.model.produzio.Unimag_;

public class UnimagCampionariSpec {
	static Specification<Unimag> getPredicateCampionario(String ts) {

		return (root, query, criteriaBuilder) -> {
			String tipo = "";
			switch (ts) {
				case "TESSUTO":
					tipo = "T";
					break;
				case "SCIARPE":
					tipo = "S";
					break;
				default:
					tipo = "A";
					break;
			}

			List<Predicate> predicates = new ArrayList<>();
			if (!tipo.equals("A")) {
				Predicate tipoPred = criteriaBuilder.equal(root.get(Unimag_.id)
						.get(UnimagPK_.tipoar), tipo);
				predicates.add(tipoPred);
			}
			Predicate fassub = criteriaBuilder.equal(root.get(Unimag_.fassub), "CM");
			predicates.add(fassub);
			Predicate fasez = criteriaBuilder.equal(root.get(Unimag_.fasez), "FI");
			predicates.add(fasez);
			Predicate caprpz = criteriaBuilder.equal(root.get(Unimag_.caprpz), "C");
			predicates.add(caprpz);
			Predicate nordin = criteriaBuilder.equal(root.get(Unimag_.nordin), 0);
			predicates.add(nordin);
			Predicate nordri = criteriaBuilder.equal(root.get(Unimag_.nordri), 0);
			predicates.add(nordri);
			Predicate pzlaat = criteriaBuilder.equal(root.get(Unimag_.pzlaat), 0);
			predicates.add(pzlaat);
			Predicate qtaPred = criteriaBuilder.greaterThan(root.get(Unimag_.qta), BigDecimal.ZERO);
			predicates.add(qtaPred);
			// parte finale

			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		};

	}

}
