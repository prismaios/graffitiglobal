package it.prismatdi.model.produzio;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the delist0f database table.
 * 
 */
@Embeddable
public class Delist0fPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int dlcodi;

	private int dlkey;

	public Delist0fPK() {
	}
	public int getDlcodi() {
		return this.dlcodi;
	}
	public void setDlcodi(int dlcodi) {
		this.dlcodi = dlcodi;
	}
	public int getDlkey() {
		return this.dlkey;
	}
	public void setDlkey(int dlkey) {
		this.dlkey = dlkey;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Delist0fPK)) {
			return false;
		}
		Delist0fPK castOther = (Delist0fPK)other;
		return 
			(this.dlcodi == castOther.dlcodi)
			&& (this.dlkey == castOther.dlkey);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.dlcodi;
		hash = hash * prime + this.dlkey;
		
		return hash;
	}
}