package it.prismatdi.model.scribaf;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the cambpf0f database table.
 *
 */
@Entity
@NamedQuery(name = "Cambpf0f.findAll", query = "SELECT c FROM Cambpf0f c")
public class Cambpf0f implements Serializable {
	private static final long	serialVersionUID	= 1L;

	@EmbeddedId
	private Cambpf0fPK			id;

	private double					cmcam2;

	private double					cmcamb;

	private String					cmdiri;

	private String					cmfill;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cambpf0f other = (Cambpf0f) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public double getCmcam2() {
		return cmcam2;
	}

	public double getCmcamb() {
		return cmcamb;
	}

	public String getCmdiri() {
		return cmdiri;
	}

	public String getCmfill() {
		return cmfill;
	}

	public Cambpf0fPK getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	public void setCmcam2(double cmcam2) {
		this.cmcam2 = cmcam2;
	}

	public void setCmcamb(double cmcamb) {
		this.cmcamb = cmcamb;
	}

	public void setCmdiri(String cmdiri) {
		this.cmdiri = cmdiri;
	}

	public void setCmfill(String cmfill) {
		this.cmfill = cmfill;
	}

	public void setId(Cambpf0fPK id) {
		this.id = id;
	}

}