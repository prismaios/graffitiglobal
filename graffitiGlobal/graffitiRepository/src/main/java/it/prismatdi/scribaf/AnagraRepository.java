package it.prismatdi.scribaf;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.scribaf.Anagra;
import it.prismatdi.model.scribaf.AnagraPK;
import it.prismatdi.model.utils.Busta;

@Repository
public interface AnagraRepository extends JpaRepository<Anagra, AnagraPK> {

	@Query("SELECT new it.prismatdi.model.utils.Busta(a.id.ansott, a.anraso) FROM Anagra AS a"
			+ " WHERE a.id.anditt='00' AND a.id.anmaco = :pConto "
			+ "AND a.antifo = 'R' AND a.id.ansott <> '00000' ORDER BY a.anraso")
	List<Busta> findRappresentanti(@Param("pConto") String conto);

	@Query("SELECT new it.prismatdi.model.utils.Busta(a.id.ansott, a.anraso) FROM Anagra AS a"
			+ " WHERE a.id.anditt='00' AND a.id.anmaco = :pConto "
			+ "AND a.id.ansott <> '00000' ORDER BY a.anraso")
	List<Busta> findSetAnagra(@Param("pConto") String conto);

}
