package it.prismatdi.scribaf;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.scribaf.Tabel;
import it.prismatdi.model.utils.Busta;

@Repository
public interface TabelRepository extends JpaRepository<Tabel, String> {

	@Query("SELECT t FROM Tabel AS t WHERE t.tbkey LIKE '00I%'")
	List<Tabel> getAllCodiciIva();

	@Query("SELECT new it.prismatdi.model.utils.Busta(t.tbkey, t.tbdes) FROM Tabel AS t WHERE t.tbkey LIKE '00G%'")
	List<Busta> getAllStagioni();
}
