package it.prismatdi.as400;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.as400.Cardis;
import it.prismatdi.model.as400.CardisPK;

@Repository
public interface CardisRepository extends JpaRepository<Cardis, CardisPK> {

	@Query("SELECT c FROM Cardis AS c WHERE c.cocaco = '999999'")
	public List<Cardis> findAll999();

}
