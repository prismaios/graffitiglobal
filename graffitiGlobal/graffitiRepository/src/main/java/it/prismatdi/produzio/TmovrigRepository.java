package it.prismatdi.produzio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.produzio.Tmovrig;
import it.prismatdi.model.produzio.TmovrigPK;

@Repository
public interface TmovrigRepository extends JpaRepository<Tmovrig, TmovrigPK>, RepositoryCustomForParametri {

	@Override
	@Query("SELECT t FROM Tmovrig AS t")
	public List<Tmovrig> findAll();

}
