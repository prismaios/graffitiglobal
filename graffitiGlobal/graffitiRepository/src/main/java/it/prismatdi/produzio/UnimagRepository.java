package it.prismatdi.produzio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.produzio.Unimag;
import it.prismatdi.model.produzio.UnimagPK;

@Repository
public interface UnimagRepository extends JpaRepository<Unimag, UnimagPK>, UnimagCustomRepository {

	@Override
	@Query("SELECT u FROM Unimag AS u")
	List<Unimag> findAll();

}
