package it.prismatdi.produzio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.produzio.Matpri;
import it.prismatdi.model.produzio.MatpriPK;

@Repository
public interface MatpriRepository extends JpaRepository<Matpri, MatpriPK> {

	@Query("SELECT m FROM Matpri AS m WHERE m.id.mtarti = '00001'")
	public List<Matpri> findAll111();
}
