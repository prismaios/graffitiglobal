package it.prismatdi.produzio;

import java.util.List;

import it.prismatdi.general.ScelteAgenti;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.model.utils.UnimagData;

public interface UnimagCustomRepository {

	List<UnimagData> findCampionario(ScelteAgenti tessSciar, Busta stagione);

}
