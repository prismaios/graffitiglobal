package it.prismatdi.produzio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import it.prismatdi.general.ScelteAgenti;
import it.prismatdi.model.produzio.Delist0f;
import it.prismatdi.model.produzio.Delist0f_;
import it.prismatdi.model.produzio.Matpri;
import it.prismatdi.model.produzio.Matpri_;
import it.prismatdi.model.produzio.Telist0f;
import it.prismatdi.model.produzio.Telist0f_;
import it.prismatdi.model.produzio.Unimag;
import it.prismatdi.model.produzio.UnimagPK_;
import it.prismatdi.model.produzio.Unimag_;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.model.utils.UnimagData;

public class UnimagCustomRepositoryImpl implements UnimagCustomRepository {
	@Autowired
	protected Environment	env;

	@PersistenceContext(unitName = "produzioUnit")
	private EntityManager	produzioEntityManager;

	public UnimagCustomRepositoryImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<UnimagData> findCampionario(ScelteAgenti tessSciar, Busta stagione) {
		String tipo = "";
		switch (tessSciar) {
			case TESSUTO:
				tipo = "T";
				break;
			case SCIARPE:
				tipo = "S";
				break;
			default:
				tipo = "A";
				break;
		}
		if (stagione != null) {
			return findCampionarioConStagione(stagione, tipo);
		}
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Unimag> uniRoot = cq.from(Unimag.class);
		Join<Unimag, Matpri> matPri = uniRoot.join(Unimag_.matpri);
		//		Join<Unimag, Delist0f> listini = uniRoot.join(Unimag_.listini);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		Selection<String> artico = uniRoot.get(Unimag_.artico)
				.alias("ARTICO");
		Selection<BigDecimal> colore = uniRoot.get(Unimag_.colore)
				.alias("COLORE");
		Selection<String> descArt = matPri.get(Matpri_.mtdesc)
				.alias("DESC_ARTI");
		Selection<BigDecimal> qta = uniRoot.get(Unimag_.qta)
				.alias("QUANTITA");
		Selection<String> bagno = uniRoot.get(Unimag_.bagno)
				.alias("BAGNO");
		// collection selezioni
		selezioni.add(artico);
		selezioni.add(colore);
		selezioni.add(descArt);
		selezioni.add(qta);
		selezioni.add(bagno);
		// qualifico
		cq.multiselect(selezioni);
		// parte where
		List<Predicate> predicates = new ArrayList<>();
		if (!tipo.equals("A")) {
			Predicate tipoPred = cb.equal(uniRoot.get(Unimag_.id)
					.get(UnimagPK_.tipoar), tipo);
			predicates.add(tipoPred);
		}
		Predicate fassub = cb.equal(uniRoot.get(Unimag_.fassub), "CM");
		predicates.add(fassub);
		Predicate fasez = cb.equal(uniRoot.get(Unimag_.fasez), "FI");
		predicates.add(fasez);
		Predicate caprpz = cb.equal(uniRoot.get(Unimag_.caprpz), "C");
		predicates.add(caprpz);
		Predicate nordin = cb.equal(uniRoot.get(Unimag_.nordin), 0);
		predicates.add(nordin);
		Predicate nordri = cb.equal(uniRoot.get(Unimag_.nordri), 0);
		predicates.add(nordri);
		Predicate pzlaat = cb.equal(uniRoot.get(Unimag_.pzlaat), 0);
		predicates.add(pzlaat);
		Predicate qtaPred = cb.greaterThan(uniRoot.get(Unimag_.qta), BigDecimal.ZERO);
		predicates.add(qtaPred);
		// qualifico
		cq.where(predicates.toArray(new Predicate[0]))
				.orderBy(cb.asc(uniRoot.get(Unimag_.artico)));
		// parte finale
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		List<Tuple> lista = qq.getResultList();
		List<UnimagData> result = new ArrayList<>();
		for (Tuple tupla : lista) {
			UnimagData ud = new UnimagData(tupla);
			result.add(ud);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private List<UnimagData> findCampionarioConStagione(Busta stagione, String tipo) {
		// Parte LISTINI
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<String> cq = cb.createQuery(String.class);
		Root<Delist0f> testaRoot = cq.from(Delist0f.class);
		Join<Delist0f, Telist0f> testata = testaRoot.join(Delist0f_.testata);
		Selection<String> articoli = testaRoot.get(Delist0f_.dlarti)
				.alias("ARTICOLO");
		cq.select(articoli);
		cq.where(cb.equal(testata.get(Telist0f_.tlstag), stagione.getCodice()))
				.groupBy(testaRoot.get(Delist0f_.dlarti))
				.orderBy(cb.asc(testaRoot.get(Delist0f_.dlarti)));
		List<String> res = produzioEntityManager.createQuery(cq)
				.getResultList();
		System.err.println(">>>>> UnimagCustomRepositoryImpl-findCampionarioConStagione <<<<<< " + res.size());
		// Parte UNIMAG
		//		CriteriaQuery<Tuple> cqUni = cb.createTupleQuery();
		//		Root<Unimag> uniRoot = cqUni.from(Unimag.class);
		//		Join<Unimag, Matpri> matPri = uniRoot.join(Unimag_.matpri);
		//		List<Selection<?>> selezioni = new ArrayList<>();
		//		Selection<String> artico = uniRoot.get(Unimag_.artico)
		//				.alias("ARTICO");
		//		Selection<BigDecimal> colore = uniRoot.get(Unimag_.colore)
		//				.alias("COLORE");
		//		Selection<String> descArt = matPri.get(Matpri_.mtdesc)
		//				.alias("DESC_ARTI");
		//		Selection<BigDecimal> qta = uniRoot.get(Unimag_.qta)
		//				.alias("QUANTITA");
		//		Selection<String> bagno = uniRoot.get(Unimag_.bagno)
		//				.alias("BAGNO");
		//		// collection selezioni
		//		selezioni.add(artico);
		//		selezioni.add(colore);
		//		selezioni.add(descArt);
		//		selezioni.add(qta);
		//		selezioni.add(bagno);
		//		// qualifico
		//		cqUni.multiselect(selezioni);
		//		// parte where
		//		List<Predicate> predicates = new ArrayList<>();
		//		if (!tipo.equals("A")) {
		//			Predicate tipoPred = cb.equal(uniRoot.get(Unimag_.id)
		//					.get(UnimagPK_.tipoar), tipo);
		//			predicates.add(tipoPred);
		//		}
		//		Predicate fassub = cb.equal(uniRoot.get(Unimag_.fassub), "CM");
		//		predicates.add(fassub);
		//		Predicate fasez = cb.equal(uniRoot.get(Unimag_.fasez), "FI");
		//		predicates.add(fasez);
		//		Predicate caprpz = cb.equal(uniRoot.get(Unimag_.caprpz), "C");
		//		predicates.add(caprpz);
		//		Predicate nordin = cb.equal(uniRoot.get(Unimag_.nordin), 0);
		//		predicates.add(nordin);
		//		Predicate nordri = cb.equal(uniRoot.get(Unimag_.nordri), 0);
		//		predicates.add(nordri);
		//		Predicate pzlaat = cb.equal(uniRoot.get(Unimag_.pzlaat), 0);
		//		predicates.add(pzlaat);
		//		Predicate qtaPred = cb.greaterThan(uniRoot.get(Unimag_.qta), BigDecimal.ZERO);
		//		predicates.add(qtaPred);
		List<UnimagData> finalResult = new ArrayList<>();
		Query q;
		if (!tipo.equals("A")) {
			q = produzioEntityManager.createNamedQuery("Unimag.findCampioneConStagioneTipo", UnimagData.class);
		}
		else {
			q = produzioEntityManager.createNamedQuery("Unimag.findCampioneConStagione", UnimagData.class);
		}
		for (String codArti : res) {
			List<UnimagData> data;
			if (!tipo.equals("A")) {
				data = q.setParameter("pCodice", codArti)
						.setParameter("pTipo", tipo)
						.getResultList();
			}
			else {
				data = q.setParameter("pCodice", codArti)
						.getResultList();
			}
			for (UnimagData unimagData : data) {
				finalResult.add(unimagData);
			}
			//			Predicate codPred = cb.equal(uniRoot.get(Unimag_.artico), codArti);
			//			predicates.add(codPred);
			//			cqUni.where(predicates.toArray(new Predicate[0]));
			//			// parte finale
			//			TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cqUni);
			//			List<Tuple> lista = qq.getResultList();
			//			for (Tuple tupla : lista) {
			//				UnimagData data = new UnimagData(tupla);
			//				finalResult.add(data);
			//			}
			//			predicates.remove(codPred);
		}
		return finalResult;
	}

}
