package it.prismatdi.produzio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.prismatdi.model.produzio.Tmovmag;

public interface TmovmagRepository extends JpaRepository<Tmovmag, Integer> {

	@Override
	@Query("SELECT t FROM Tmovmag AS t")
	public List<Tmovmag> findAll();

	@Query("SELECT t FROM Tmovmag AS t WHERE SUBSTRING(t.mfdado,1,4) >= :pDaAnno AND SUBSTRING(t.mfdado,1,4) <= :pAdAnno")
	public List<Tmovmag> findDaAnnoAdAnno(@Param("pDaAnno") String pDaAnno, @Param("pAdAnno") String pAdAnno);
}
