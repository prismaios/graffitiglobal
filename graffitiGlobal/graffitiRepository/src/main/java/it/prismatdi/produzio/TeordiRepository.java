package it.prismatdi.produzio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.produzio.Teordi;

@Repository
public interface TeordiRepository extends JpaRepository<Teordi, Long> {

	@Override
	@Query("SELECT t FROM Teordi AS t")
	public List<Teordi> findAll();

}
