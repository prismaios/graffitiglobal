package it.prismatdi.produzio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.produzio.Ordini;
import it.prismatdi.model.produzio.OrdiniPK;

@Repository
public interface OrdiniRepository extends JpaRepository<Ordini, OrdiniPK> {

	@Override
	@Query("SELECT o FROM Ordini AS o")
	public List<Ordini> findAll();

}
