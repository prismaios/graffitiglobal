package it.prismatdi.produzio;

import java.util.List;

import it.prismatdi.model.utils.DataConversion;
import it.prismatdi.model.utils.ParametriAnalisiVendite;
import it.prismatdi.model.utils.ResultContainer;

public interface RepositoryCustomForParametri {
	ResultContainer findConParametri(ParametriAnalisiVendite parametri);

	List<DataConversion> findMovimentiDiArticolo(ParametriAnalisiVendite parametri, String codArticolo);

	List<DataConversion> findMovimentiDiArticoloConTuple(ParametriAnalisiVendite parametri, String codArticolo);

	List<DataConversion> findMovimentiDiCliente(ParametriAnalisiVendite parametri, String codCli);

	List<DataConversion> findMovimentiDiClienteConTuple(ParametriAnalisiVendite parametri, String codCli);

}
