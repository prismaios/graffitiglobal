package it.prismatdi.produzio;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import it.prismatdi.general.AliasPerCriteriaQuery;
import it.prismatdi.model.as400.Cardis;
import it.prismatdi.model.as400.CardisPK;
import it.prismatdi.model.produzio.Matpri;
import it.prismatdi.model.produzio.MatpriPK;
import it.prismatdi.model.produzio.Ordini;
import it.prismatdi.model.produzio.Ordini_;
import it.prismatdi.model.produzio.Tmovmag;
import it.prismatdi.model.produzio.Tmovmag_;
import it.prismatdi.model.produzio.Tmovrig;
import it.prismatdi.model.produzio.Tmovrig_;
import it.prismatdi.model.scribaf.Anagra;
import it.prismatdi.model.scribaf.AnagraPK;
import it.prismatdi.model.scribaf.Cambpf0f;
import it.prismatdi.model.scribaf.Cambpf0fPK;
import it.prismatdi.model.utils.DataConversion;
import it.prismatdi.model.utils.ParametriAnalisiVendite;
import it.prismatdi.model.utils.ResultContainer;
import it.prismatdi.model.utils.SumDataConversion;

public class RepositoryCustomForParametriImpl implements RepositoryCustomForParametri {
	@Autowired
	protected Environment	env;

	@PersistenceContext(unitName = "produzioUnit")
	private EntityManager	produzioEntityManager;

	@PersistenceContext(unitName = "as400Unit")
	private EntityManager	as400EntityManager;

	@PersistenceContext(unitName = "scribafUnit")
	private EntityManager	scribafEntityManager;

	private Matpri cercaArticolo(String codArti) {
		MatpriPK key = new MatpriPK("T", codArti, 0L);
		Matpri articolo = null;
		try {
			articolo = produzioEntityManager.find(Matpri.class, key);
		}
		catch (Exception e) {
		}
		return articolo;
	}

	private Double cercaCambio(String valuta, String data) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate dataValuta = LocalDate.parse(data, dtf);
		int anno = dataValuta.getYear();
		int mese = dataValuta.getMonthValue();
		int giorno = dataValuta.getDayOfMonth();

		Cambpf0fPK key = new Cambpf0fPK("C", (short) anno, (short) mese, (short) giorno, valuta);
		Cambpf0f cambio = scribafEntityManager.find(Cambpf0f.class, key);
		if (cambio != null) {
			return cambio.getCmcamb();
		}
		else {
			return 0D;
		}

	}

	private Anagra cercaCliente(String codCli) {
		final String contoClienti = env.getProperty("CONTO_CLIENTI");
		final AnagraPK key = new AnagraPK("00", contoClienti, codCli, 0);
		Anagra ana = null;
		try {
			ana = scribafEntityManager.find(Anagra.class, key);
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaCliente <<<<<< " + codCli);
		}
		return ana;

	}

	private Cardis cercaColore(String codArti, String codColo) {
		final CardisPK key = new CardisPK("T", codArti, codColo, "999999");
		//		System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaColore <<<<<< " + key.toString());
		try {
			final Cardis color = as400EntityManager.find(Cardis.class, key);
			return color;
		}
		catch (final Exception e) {
			return null;
		}

	}

	private void cercaMovimentiInValuta(Map<String, SumDataConversion> hashSum, ParametriAnalisiVendite parametri,
			CriteriaBuilder cb, ResultContainer conten) {
		TypedQuery<?> tq = produzioEntityManager.createQuery(parametri.getQueryMovimentiInValuta(cb, parametri));
		//		System.err.println(
		//				">>>>> QUERY <<<<<< " + tq.unwrap(org.hibernate.Query.class)
		//						.getQueryString());

		@SuppressWarnings("unchecked")
		List<Tuple> losta = (List<Tuple>) tq.getResultList();
		System.err.println(">>>>> RepositoryCustomForParametriImpl-VALUTA <<<<<< " + losta.size());

		if (!losta.isEmpty()) {
			for (Tuple tuple : losta) {
				String causale = tuple.get(AliasPerCriteriaQuery.causale.toString(), String.class);
				double segno = 1D;
				if (causale.startsWith("N")) {
					segno = -1D;
				}
				String codice = null;
				String descrizione = null;
				if (parametri.getCliente()) {
					codice = (String) tuple.get(AliasPerCriteriaQuery.codCli.toString()); // prima ana2
					Anagra ana = cercaCliente(codice);
					try {
						descrizione = ana.getAnraso();
					}
					catch (Exception e) {
						descrizione = "****************";
					}
				}
				else {
					codice = (String) tuple.get(AliasPerCriteriaQuery.codArticolo.toString());
					Matpri matpri = cercaArticolo(codice);
					try {
						descrizione = matpri.getMtdesc();
					}
					catch (Exception e) {
						descrizione = "****************";
					}
				}
				if (!codice.isEmpty() && hashSum.containsKey(codice)) {
					SumDataConversion dat = hashSum.get(codice);
					Double qta = (Double) tuple.get(AliasPerCriteriaQuery.qta.toString());
					Double prz = (Double) tuple.get(AliasPerCriteriaQuery.prezzo.toString());
					String valuta = (String) tuple.get(AliasPerCriteriaQuery.valuta.toString());
					String data = (String) tuple.get(AliasPerCriteriaQuery.dataValuta.toString());
					Double cambio = cercaCambio(valuta, data);
					// maggioro il prezzo
					if (cambio != 0) {
						prz = prz / cambio;
						Double magg = (Double) tuple.get(AliasPerCriteriaQuery.maggiorazioni.toString());
						prz += prz * (magg / 100);
						Double valore = prz * qta;
						dat.addQta(qta * segno);
						dat.addValore(valore * segno);
						conten.sumQta(qta * segno);
						conten.sumValore(valore * segno);

						if (parametri.getMargineLordo()) {
							Double cc = tuple.get(AliasPerCriteriaQuery.costo.toString(), Double.class);
							dat.addCosto(cc * qta);
							conten.sumCosto(dat.getCosto());
							try {
								dat.addCostoProd(tuple.get(AliasPerCriteriaQuery.costoProduzione.toString(), Double.class) * qta);
							}
							catch (Exception e) {
							}
							if (codice.equals("TICINO")) {
								System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaMovimentiInValuta TICINO <<<<<< "
										+ dat.getCosto() + " " + dat.getCostoProd());
							}
						}
					}
					else {
						System.err.println(">>>>> CAMBIO A ZERO PER <<<<<< " + codice);
					}

				}
				else {
					SumDataConversion sdc = new SumDataConversion();
					sdc.setCodice(codice);
					sdc.setDescrizione(descrizione);
					sdc.setQta(tuple.get(AliasPerCriteriaQuery.qta.toString(), Double.class) * segno);
					Double prz = (Double) tuple.get(AliasPerCriteriaQuery.prezzo.toString());
					String valuta = (String) tuple.get(AliasPerCriteriaQuery.valuta.toString());
					String data = (String) tuple.get(AliasPerCriteriaQuery.dataValuta.toString());
					Double cambio = cercaCambio(valuta, data);
					// maggioro il prezzo
					if (cambio != 0) {
						prz = prz / cambio;
						Double magg = (Double) tuple.get(AliasPerCriteriaQuery.maggiorazioni.toString());
						prz += prz * (magg / 100);
						Double valore = prz * Math.abs(sdc.getQta());
						sdc.setValore(valore * segno);
						conten.sumQta(sdc.getQta());
						conten.sumValore(sdc.getValore());
					}
					else {
						System.err.println(">>>>> CAMBIO A ZERO PER <<<<<< " + codice);
					}
					if (parametri.getMargineLordo()) {
						Double cc = tuple.get(AliasPerCriteriaQuery.costo.toString(), Double.class);
						sdc.addCosto(cc * sdc.getQta());
						conten.sumCosto(sdc.getCosto());
						try {
							sdc.addCostoProd(tuple.get(AliasPerCriteriaQuery.costoProduzione.toString(), Double.class));
						}
						catch (Exception e) {
						}
						if (codice.equals("TICINO")) {
							System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaMovimentiInValuta TICINO <<<<<< "
									+ sdc.getCosto() + " " + sdc.getCostoProd());
						}
					}
					hashSum.put(sdc.getCodice(), sdc);
				}
			}
		}

	}

	private void cercaMovimentiNoteCreditoResoClienti(Map<String, SumDataConversion> hashSum, ParametriAnalisiVendite parametri,
			CriteriaBuilder cb, ResultContainer conten, String minData, String maxData) {
		TypedQuery<?> rqq = produzioEntityManager
				.createQuery(parametri.getQueryMovimentiNoteNCT(cb, parametri, minData, maxData));
		//		System.err.println(
		//				">>>>> QUERY <<<<<< " + rqq.unwrap(org.hibernate.Query.class)
		//						.getQueryString());
		@SuppressWarnings("unchecked")
		List<Tuple> noteNCT = (List<Tuple>) rqq.getResultList();
		System.err.println(">>>>> RepositoryCustomForParametriImpl-NCT <<<<<< " + noteNCT.size());
		//		double tz = 0D;
		//		for (Tuple tuple2 : noteNCT) {
		//			String articolo = tuple2.get(AliasPerCriteriaQuery.codArticolo.toString(), String.class);
		//			Double qta = tuple2.get(AliasPerCriteriaQuery.qta.toString(), Double.class);
		//			tz += qta;
		//			String causale = tuple2.get(AliasPerCriteriaQuery.causale.toString(), String.class);
		//			Integer numMov = tuple2.get(AliasPerCriteriaQuery.numMov.toString(), Integer.class);
		//			System.err.println(">>>>> RIGHE NCT <<<<<< " + articolo + " " + qta + " " + causale + " " + numMov);
		//
		//		}
		//		System.err.println(">>>>> TOTALE NCT <<<<<< " + tz);
		if (!noteNCT.isEmpty()) {
			for (Tuple tuple : noteNCT) {
				double segno = -1D; // SEMPRE PER NCT
				String codice = null;
				if (parametri.getCliente()) {
					codice = (String) tuple.get(AliasPerCriteriaQuery.codCli.toString());
				}
				else {
					codice = (String) tuple.get(AliasPerCriteriaQuery.codArticolo.toString());
				}
				if (hashSum.containsKey(codice)) {
					SumDataConversion dat = hashSum.get(codice);
					Double qta = (Double) tuple.get(AliasPerCriteriaQuery.qta.toString());
					if (qta == 0) {
						qta = tuple.get(AliasPerCriteriaQuery.qtaL.toString(), Double.class);
					}
					Double prz = (Double) tuple.get(AliasPerCriteriaQuery.prezzo.toString());
					String valuta = (String) tuple.get(AliasPerCriteriaQuery.valuta.toString());
					if (!valuta.isEmpty() && !valuta.equals("EUR")) {
						// NOTA CRED IN VALUTA
						String data = (String) tuple.get(AliasPerCriteriaQuery.dataValuta.toString());
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
						LocalDate dataValuta = LocalDate.parse(data, dtf);
						int anno = dataValuta.getYear();
						int mese = dataValuta.getMonthValue();
						int giorno = dataValuta.getDayOfMonth();

						Cambpf0fPK key = new Cambpf0fPK("C", (short) anno, (short) mese, (short) giorno, valuta);
						Cambpf0f cambio = scribafEntityManager.find(Cambpf0f.class, key);
						// maggioro il prezzo
						if (cambio != null) {
							if (cambio.getCmcamb() != 0) {
								prz = prz / cambio.getCmcamb();
							}
							Double magg = (Double) tuple.get(AliasPerCriteriaQuery.maggiorazioni.toString());
							prz += prz * (magg / 100);
							Double valore = prz * qta;
							dat.addQta(qta * segno);
							dat.addValore(valore * segno);
							conten.sumQta(qta * segno);
							conten.sumValore(valore * segno);
						}
						else {
							System.err
									.println(">>>>> RepositoryCustomForParametriImpl-findMovimentiConParametri <<<<<< " + key.toString());
						}
					}
					else {
						// NORMALE IN EURO
						Double magg = (Double) tuple.get(AliasPerCriteriaQuery.maggiorazioni.toString());
						prz += prz * (magg / 100);
						Double valore = prz * qta;
						dat.addQta(qta * segno);
						dat.addValore(valore * segno);
						conten.sumQta(qta * segno);
						conten.sumValore(valore * segno);
					}
				}
				else {
					//					System.err.println(">>>>> NCT <<<<<< NON ESISTE NELLA MAP >" + codice + "<");
					SumDataConversion dat = new SumDataConversion();
					dat.setQta(DataConversion.arrotonda(tuple.get(AliasPerCriteriaQuery.qta.toString(), Double.class), 2));
					Double d = tuple.get(AliasPerCriteriaQuery.prezzo.toString(), Double.class);
					dat.setValore(dat.getQta() * d);
					if (parametri.getCliente()) {
						dat.setCodice(tuple.get(AliasPerCriteriaQuery.codCli.toString(), String.class));
						Anagra ana = cercaCliente(dat.getCodice());
						if (ana != null) {
							dat.setDescrizione(ana.getAnraso());
						}
						else {
							dat.setDescrizione("ANAGRAFICA INESISTENTE");
						}
					}
					else {
						dat.setCodice(tuple.get(AliasPerCriteriaQuery.codArticolo.toString(), String.class));
						Matpri matpri = cercaArticolo(dat.getCodice());
						if (matpri != null) {
							if (matpri.getMtdesc() == null) {
								matpri.setMtdesc("*************");
							}
							dat.setDescrizione(matpri.getMtdesc());
						}
						else {
							dat.setDescrizione("ARTICOLO INESISTENTE");
						}
					}
					conten.sumQta(dat.getQta());
					conten.sumValore(dat.getValore());
					conten.addDati(dat);
					hashSum.put(dat.getCodice(), dat);
				}
			}

		}

	}

	private void cercaMovimentiNoteDiCredito(Map<String, SumDataConversion> hashSum, ParametriAnalisiVendite parametri,
			CriteriaBuilder cb, ResultContainer conten, String minData, String maxData) {
		TypedQuery<?> tqq = produzioEntityManager
				.createQuery(parametri.getQueryMovimentiNoteCredito(cb, parametri, minData, maxData));
		//		System.err.println(
		//				">>>>> QUERY <<<<<< " + tqq.unwrap(org.hibernate.Query.class)
		//						.getQueryString());

		@SuppressWarnings("unchecked")
		List<Tuple> noteCred = (List<Tuple>) tqq.getResultList();
		System.err.println(">>>>> RepositoryCustomForParametriImpl-NOTE CREDITO <<<<<< " + noteCred.size());
		//		double tz = 0D;
		//				for (Tuple tuple2 : noteNCT) {
		//					String articolo = tuple2.get(AliasPerCriteriaQuery.codArticolo.toString(), String.class);
		//					Double qta = tuple2.get(AliasPerCriteriaQuery.qta.toString(), Double.class);
		//					tz += qta;
		//					String causale = tuple2.get(AliasPerCriteriaQuery.causale.toString(), String.class);
		//					Integer numMov = tuple2.get(AliasPerCriteriaQuery.numMov.toString(), Integer.class);
		//					System.err.println(">>>>> RIGHE NCT <<<<<< " + articolo + " " + qta + " " + causale + " " + numMov);
		//
		//				}
		//				System.err.println(">>>>> TOTALE NCT <<<<<< " + tz);
		if (!noteCred.isEmpty()) {
			for (Tuple tuple : noteCred) {
				double segno = -1D; // SEMPRE PER NOTE DI CREDITO
				String codice = null;
				if (parametri.getCliente()) {
					codice = (String) tuple.get(AliasPerCriteriaQuery.codCli.toString());
				}
				else {
					codice = (String) tuple.get(AliasPerCriteriaQuery.codArticolo.toString());
				}
				if (hashSum.containsKey(codice)) {
					SumDataConversion dat = hashSum.get(codice);
					Double qta = (Double) tuple.get(AliasPerCriteriaQuery.qta.toString());
					Double prz = (Double) tuple.get(AliasPerCriteriaQuery.prezzo.toString());
					String valuta = (String) tuple.get(AliasPerCriteriaQuery.valuta.toString());
					if (!valuta.isEmpty() && !valuta.equals("EUR")) {
						// NOTA CRED IN VALUTA
						String data = (String) tuple.get(AliasPerCriteriaQuery.dataValuta.toString());
						DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
						LocalDate dataValuta = LocalDate.parse(data, dtf);
						int anno = dataValuta.getYear();
						int mese = dataValuta.getMonthValue();
						int giorno = dataValuta.getDayOfMonth();

						Cambpf0fPK key = new Cambpf0fPK("C", (short) anno, (short) mese, (short) giorno, valuta);
						Cambpf0f cambio = scribafEntityManager.find(Cambpf0f.class, key);
						// maggioro il prezzo
						if (cambio != null) {
							if (cambio.getCmcamb() != 0) {
								prz = prz / cambio.getCmcamb();
							}
							Double magg = (Double) tuple.get(AliasPerCriteriaQuery.maggiorazioni.toString());
							prz += prz * (magg / 100);
							Double valore = prz * qta;
							dat.addQta(qta * segno);
							dat.addValore(valore * segno);
							conten.sumQta(qta * segno);
							conten.sumValore(valore * segno);
						}
						else {
							System.err
									.println(">>>>> RepositoryCustomForParametriImpl-findMovimentiConParametri <<<<<< " + key.toString());
						}
					}
					else {
						// NORMALE IN EURO
						Double magg = (Double) tuple.get(AliasPerCriteriaQuery.maggiorazioni.toString());
						prz += prz * (magg / 100);
						Double valore = prz * qta;
						dat.addQta(qta * segno);
						dat.addValore(valore * segno);
						conten.sumQta(qta * segno);
						conten.sumValore(valore * segno);
					}
				}
				else {
					System.err.println(">>>>> NOTE DI CREDITO <<<<<< NON ESISTE NELLA MAP >" + codice + "<");
				}
			}
		}

	}

	private void cercaNoteCreditoConArticolo(List<DataConversion> result, String minData, String maxData,
			ParametriAnalisiVendite parametri,
			String codArticolo) {
		System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaNoteCreditoConArticolo <<<<<< " + minData + " " + maxData);
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		//		selezioni.add(ordine.get(Ordini_.orcoan)
		//				.alias(AliasPerCriteriaQuery.stagione.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana2)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); //codArticolo
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString())); //causale
		selezioni.add(tmovRoot.get(Tmovrig_.mrcolo)
				.alias(AliasPerCriteriaQuery.codColore.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString())); //prezzo
		selezioni.add(tmovRoot.get(Tmovrig_.mrrapp)
				.alias(AliasPerCriteriaQuery.codRappr.toString())); //codRappr
		selezioni.add(tmovRoot.get(Tmovrig_.mrumis)
				.alias(AliasPerCriteriaQuery.unitaM.toString())); //unitaM
		selezioni.add(tmovRoot.get(Tmovrig_.mrcapr)
				.alias(AliasPerCriteriaQuery.prodCamp.toString())); //prodCamp
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdado)
				.alias(AliasPerCriteriaQuery.dataMov.toString()));
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// ABBIAMO SOLO DATE
		List<Predicate> datePreds = new ArrayList<>();
		Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), minData);
		Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), maxData);
		datePreds.add(daDataPred);
		datePreds.add(aDataPred);
		Predicate pDate = cb.and(datePreds.toArray(new Predicate[datePreds.size()]));
		addPreds.add(pDate);
		// tipo tessuto
		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// rappresentante
		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = parametri.faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// solo scarico
		// alle note di credito togliere controllo su carico scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);

		// FINAL
		addPreds.add(cb.equal(tmovRoot.get(Tmovrig_.mrarti), codArticolo));
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll);
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		//		System.err
		//				.println(">>>>> QUERY<<<<<< " + qq.unwrap(QueryImpl.class)
		//						.getQueryString());
		List<Tuple> lista = qq.getResultList();
		//		System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaNoteCreditoConArticolo <<<<<< " + lista.size());
		lista.sort((t1, t2) -> t1.get(AliasPerCriteriaQuery.qta.toString(), Double.class)
				.compareTo(t2.get(AliasPerCriteriaQuery.qta.toString(), Double.class)));
		Double tt = 0D;
		for (Tuple tupla : lista) {
			DataConversion dataConversion = new DataConversion();
			dataConversion.setSegno(-1D);
			dataConversion.setCodice((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()));
			Anagra ana = cercaCliente((String) tupla.get(AliasPerCriteriaQuery.codCli.toString()));
			if (ana != null) {
				dataConversion.setCodice(ana.getId()
						.getAnsott());
				dataConversion.setDescrizione(ana.getAnraso());
			}
			else {
				dataConversion.setCodice("????");
				dataConversion.setDescrizione("NON TROVATO");
			}
			Cardis colore = cercaColore((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()),
					(String) tupla.get(AliasPerCriteriaQuery.codColore.toString()));
			if (colore == null) {
				dataConversion.setDesColore("NON TROVATO");
			}
			else {
				dataConversion.setDesColore(colore.getCodesc());
			}
			dataConversion.converti(tupla);
			String valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);
			if (!valuta.equals("EUR") && !valuta.isEmpty()) {
				String data = (String) tupla.get(AliasPerCriteriaQuery.dataValuta.toString());
				faiCambio(dataConversion, valuta, data);
			}
			else {
				faiMaggiorazione(dataConversion);
			}
			tt += dataConversion.getQta();
			result.add(dataConversion);
		}

	}

	private void cercaNoteCreditoConCliente(List<DataConversion> result, String minData, String maxData,
			ParametriAnalisiVendite parametri, String codCli) {
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		//		selezioni.add(ordine.get(Ordini_.orcoan)
		//				.alias(AliasPerCriteriaQuery.stagione.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana2)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); //codArticolo
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString())); //causale
		selezioni.add(tmovRoot.get(Tmovrig_.mrcolo)
				.alias(AliasPerCriteriaQuery.codColore.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString())); //prezzo
		selezioni.add(tmovRoot.get(Tmovrig_.mrrapp)
				.alias(AliasPerCriteriaQuery.codRappr.toString())); //codRappr
		selezioni.add(tmovRoot.get(Tmovrig_.mrumis)
				.alias(AliasPerCriteriaQuery.unitaM.toString())); //unitaM
		selezioni.add(tmovRoot.get(Tmovrig_.mrcapr)
				.alias(AliasPerCriteriaQuery.prodCamp.toString())); //prodCamp
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdado)
				.alias(AliasPerCriteriaQuery.dataMov.toString()));
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// ABBIAMO SOLO DATE
		List<Predicate> datePreds = new ArrayList<>();
		Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), minData);
		Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), maxData);
		datePreds.add(daDataPred);
		datePreds.add(aDataPred);
		Predicate pDate = cb.and(datePreds.toArray(new Predicate[datePreds.size()]));
		addPreds.add(pDate);
		// tipo tessuto
		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// rappresentante
		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = parametri.faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);

		// FINAL
		addPreds.add(cb.equal(testata.get(Tmovmag_.mfana2), codCli));
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll);
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		//		System.err
		//				.println(">>>>> QUERY<<<<<< " + qq.unwrap(QueryImpl.class)
		//						.getQueryString());
		List<Tuple> lista = qq.getResultList();

		lista.sort((t1, t2) -> t1.get(AliasPerCriteriaQuery.qta.toString(), Double.class)
				.compareTo(t2.get(AliasPerCriteriaQuery.qta.toString(), Double.class)));
		Double tt = 0D;
		for (Tuple tupla : lista) {
			DataConversion dataConversion = new DataConversion();
			dataConversion.setSegno(-1D);
			dataConversion.setCodice((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()));
			Matpri matpri = cercaArticolo(tmovRoot.get(Tmovrig_.mrarti)
					.toString());
			if (matpri != null) {
				dataConversion.setCodice(matpri.getId()
						.getMtarti());
				dataConversion.setDescrizione(matpri.getMtdesc());
			}
			else {
				dataConversion.setCodice("?????");
				dataConversion.setDescrizione("NON TROVATO");
			}
			Cardis colore = cercaColore((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()),
					(String) tupla.get(AliasPerCriteriaQuery.codColore.toString()));
			if (colore == null) {
				dataConversion.setDesColore("NON TROVATO");
			}
			else {
				dataConversion.setDesColore(colore.getCodesc());
			}
			dataConversion.converti(tupla);
			String valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);
			if (!valuta.equals("EUR") && !valuta.isEmpty()) {
				String data = (String) tupla.get(AliasPerCriteriaQuery.dataValuta.toString());
				faiCambio(dataConversion, valuta, data);
			}
			else {
				faiMaggiorazione(dataConversion);
			}
			tt += dataConversion.getQta();
			result.add(dataConversion);
		}

	}

	private void cercaNoteCreditoNCTConArticolo(List<DataConversion> result, String minData, String maxData,
			ParametriAnalisiVendite parametri,
			String codArticolo) {
		System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaNoteCreditoConArticolo <<<<<< " + minData + " " + maxData);
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		//		selezioni.add(ordine.get(Ordini_.orcoan)
		//				.alias(AliasPerCriteriaQuery.stagione.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana1)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); //codArticolo
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString())); //causale
		selezioni.add(tmovRoot.get(Tmovrig_.mrcolo)
				.alias(AliasPerCriteriaQuery.codColore.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString())); //prezzo
		selezioni.add(tmovRoot.get(Tmovrig_.mrrapp)
				.alias(AliasPerCriteriaQuery.codRappr.toString())); //codRappr
		selezioni.add(tmovRoot.get(Tmovrig_.mrumis)
				.alias(AliasPerCriteriaQuery.unitaM.toString())); //unitaM
		selezioni.add(tmovRoot.get(Tmovrig_.mrcapr)
				.alias(AliasPerCriteriaQuery.prodCamp.toString())); //prodCamp
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdado)
				.alias(AliasPerCriteriaQuery.dataMov.toString()));
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// ABBIAMO SOLO DATE
		List<Predicate> datePreds = new ArrayList<>();
		Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), minData);
		Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), maxData);
		datePreds.add(daDataPred);
		datePreds.add(aDataPred);
		Predicate pDate = cb.and(datePreds.toArray(new Predicate[datePreds.size()]));
		addPreds.add(pDate);
		// tipo tessuto
		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// rappresentante
		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = parametri.faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// solo scarico
		// alle note di credito togliere controllo su carico scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);

		// FINAL
		addPreds.add(cb.equal(tmovRoot.get(Tmovrig_.mrarti), codArticolo));
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll);
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		//		System.err
		//				.println(">>>>> QUERY<<<<<< " + qq.unwrap(QueryImpl.class)
		//						.getQueryString());
		List<Tuple> lista = qq.getResultList();
		//		System.err.println(">>>>> RepositoryCustomForParametriImpl-cercaNoteCreditoConArticolo <<<<<< " + lista.size());
		lista.sort((t1, t2) -> t1.get(AliasPerCriteriaQuery.qta.toString(), Double.class)
				.compareTo(t2.get(AliasPerCriteriaQuery.qta.toString(), Double.class)));
		Double tt = 0D;
		for (Tuple tupla : lista) {
			DataConversion dataConversion = new DataConversion();
			dataConversion.setSegno(-1D);
			dataConversion.setCodice((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()));
			Anagra ana = cercaCliente((String) tupla.get(AliasPerCriteriaQuery.codCli.toString()));
			if (ana != null) {
				dataConversion.setCodice(ana.getId()
						.getAnsott());
				dataConversion.setDescrizione(ana.getAnraso());
			}
			else {
				dataConversion.setCodice("????");
				dataConversion.setDescrizione("NON TROVATO");
			}
			Cardis colore = cercaColore((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()),
					(String) tupla.get(AliasPerCriteriaQuery.codColore.toString()));
			if (colore == null) {
				dataConversion.setDesColore("NON TROVATO");
			}
			else {
				dataConversion.setDesColore(colore.getCodesc());
			}
			dataConversion.converti(tupla);
			String valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);
			if (!valuta.equals("EUR") && !valuta.isEmpty()) {
				String data = (String) tupla.get(AliasPerCriteriaQuery.dataValuta.toString());
				faiCambio(dataConversion, valuta, data);
			}
			else {
				faiMaggiorazione(dataConversion);
			}
			tt += dataConversion.getQta();
			result.add(dataConversion);
		}

	}

	private void cercaNoteCreditoNCTConCliente(List<DataConversion> result, String minData, String maxData,
			ParametriAnalisiVendite parametri, String codCli) {
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		//		selezioni.add(ordine.get(Ordini_.orcoan)
		//				.alias(AliasPerCriteriaQuery.stagione.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana1)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); //codArticolo
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString())); //causale
		selezioni.add(tmovRoot.get(Tmovrig_.mrcolo)
				.alias(AliasPerCriteriaQuery.codColore.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString())); //prezzo
		selezioni.add(tmovRoot.get(Tmovrig_.mrrapp)
				.alias(AliasPerCriteriaQuery.codRappr.toString())); //codRappr
		selezioni.add(tmovRoot.get(Tmovrig_.mrumis)
				.alias(AliasPerCriteriaQuery.unitaM.toString())); //unitaM
		selezioni.add(tmovRoot.get(Tmovrig_.mrcapr)
				.alias(AliasPerCriteriaQuery.prodCamp.toString())); //prodCamp
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdado)
				.alias(AliasPerCriteriaQuery.dataMov.toString()));
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// ABBIAMO SOLO DATE
		List<Predicate> datePreds = new ArrayList<>();
		Predicate daDataPred = cb.greaterThanOrEqualTo(testata.get(Tmovmag_.mfdado), minData);
		Predicate aDataPred = cb.lessThanOrEqualTo(testata.get(Tmovmag_.mfdado), maxData);
		datePreds.add(daDataPred);
		datePreds.add(aDataPred);
		Predicate pDate = cb.and(datePreds.toArray(new Predicate[datePreds.size()]));
		addPreds.add(pDate);
		// tipo tessuto
		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// rappresentante
		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = parametri.faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);

		// FINAL
		addPreds.add(cb.equal(testata.get(Tmovmag_.mfana1), codCli));
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll);
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		//		System.err
		//				.println(">>>>> QUERY<<<<<< " + qq.unwrap(QueryImpl.class)
		//						.getQueryString());
		List<Tuple> lista = qq.getResultList();

		lista.sort((t1, t2) -> t1.get(AliasPerCriteriaQuery.qta.toString(), Double.class)
				.compareTo(t2.get(AliasPerCriteriaQuery.qta.toString(), Double.class)));
		Double tt = 0D;
		for (Tuple tupla : lista) {
			DataConversion dataConversion = new DataConversion();
			dataConversion.setSegno(-1D);
			dataConversion.setCodice((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()));
			Matpri matpri = cercaArticolo(tmovRoot.get(Tmovrig_.mrarti)
					.toString());
			if (matpri != null) {
				dataConversion.setCodice(matpri.getId()
						.getMtarti());
				dataConversion.setDescrizione(matpri.getMtdesc());
			}
			else {
				dataConversion.setCodice("?????");
				dataConversion.setDescrizione("NON TROVATO");
			}
			Cardis colore = cercaColore((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()),
					(String) tupla.get(AliasPerCriteriaQuery.codColore.toString()));
			if (colore == null) {
				dataConversion.setDesColore("NON TROVATO");
			}
			else {
				dataConversion.setDesColore(colore.getCodesc());
			}
			dataConversion.converti(tupla);
			String valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);
			if (!valuta.equals("EUR") && !valuta.isEmpty()) {
				String data = (String) tupla.get(AliasPerCriteriaQuery.dataValuta.toString());
				faiCambio(dataConversion, valuta, data);
			}
			else {
				faiMaggiorazione(dataConversion);
			}
			tt += dataConversion.getQta();
			result.add(dataConversion);
		}

	}

	private void faiCambio(DataConversion dataConversion, String valuta, String data) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate dataValuta = LocalDate.parse(data, dtf);
		int anno = dataValuta.getYear();
		int mese = dataValuta.getMonthValue();
		int giorno = dataValuta.getDayOfMonth();
		double prz = dataConversion.getPrezzo();
		double magg = dataConversion.getMaggiorazioni();
		Cambpf0fPK key = new Cambpf0fPK("C", (short) anno, (short) mese, (short) giorno, valuta);

		Cambpf0f cambio = scribafEntityManager.find(Cambpf0f.class, key);
		// maggioro il prezzo
		if (cambio != null) {

			prz += prz * (magg / 100);
			if (cambio.getCmcamb() != 0) {
				prz = prz / cambio.getCmcamb();
			}
			//			System.err.println(">>>>> CAMBIO <<<<<< " + key.toString() + " PERC." + cambio.getCmcamb());
			Double valore = prz * dataConversion.getQta();
			dataConversion.setValore(valore);
			dataConversion.setPrezzo(prz);
		}

	}

	private void faiMaggiorazione(DataConversion dataConversion) {
		if (dataConversion.getMaggiorazioni() != 0) {
			double prz = dataConversion.getPrezzo();
			prz += prz * (dataConversion.getMaggiorazioni() / 100);
			dataConversion.setValore(prz * dataConversion.getQta());
		}

	}

	@Override
	public ResultContainer findConParametri(ParametriAnalisiVendite parametri) {
		if (!parametri.getPerOrdine()) {
			if (!parametri.getMargineLordo()) {
				return findMovimentiConParametri(parametri);
			}
			else {
				return findMovimentiConParametriMargineLordo(parametri);
			}
		}
		else {
			return findOrdiniConParametri(parametri);
		}

	}

	@SuppressWarnings("unchecked")
	private ResultContainer findMovimentiConParametri(ParametriAnalisiVendite parametri) {

		ResultContainer conten = new ResultContainer();
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		TypedQuery<?> q = produzioEntityManager.createQuery(parametri.getCriteriaQuery(cb));
		//		System.err.println(
		//				">>>>> QUERY <<<<<< " + q.unwrap(org.hibernate.Query.class)
		//						.getQueryString());

		List<Tuple> lista = null;
		lista = (List<Tuple>) q.getResultList();
		System.err.println(">>>>> RepositoryCustomForParametriImpl-NORMALI<<<<<< " + lista.size());
		//		System.err.println(">>>>> RepositoryCustomForParametriImpl-findMovimentiConParametri <<<<<< " + lista.size());

		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String minData = parametri.getDallaData()
				.format(formatter);
		String maxData = parametri.getAllaData()
				.format(formatter);
		if (!lista.isEmpty()) {
			for (Tuple tuple : lista) {
				SumDataConversion dat = new SumDataConversion(tuple, parametri.getCliente());
				if (parametri.getCliente()) {
					Anagra ana = cercaCliente(dat.getCodice());
					if (ana != null) {
						//						System.err.println(">>>>> RAG SOC NULL <<<<<< " + dat.getCodice() + " >" + ana.getAnraso() + "<");
						if (ana.getAnraso() == null) {
							ana.setAnraso("*************");
						}
						dat.setDescrizione(ana.getAnraso());
					}
					else {
						dat.setDescrizione("ANAGRAFICA INESISTENTE");
					}
				}
				else {
					Matpri matpri = cercaArticolo(dat.getCodice());
					if (matpri != null) {
						if (matpri.getMtdesc() == null) {
							matpri.setMtdesc("*************");
						}
						dat.setDescrizione(matpri.getMtdesc());
					}
					else {
						dat.setDescrizione("ARTICOLO INESISTENTE");
					}
				}
				conten.sumQta(dat.getQta());
				conten.sumValore(dat.getValore());
				conten.addDati(dat);
			}
		}
		// RICERCA MOVIMENTI IN VALUTA
		Map<String, SumDataConversion> hashSum = conten.getDati()
				.stream()
				.collect(
						Collectors.toMap(SumDataConversion::getCodice, item -> item));
		cercaMovimentiInValuta(hashSum, parametri, cb, conten);
		// NOTE DI CREDITO
		cercaMovimentiNoteDiCredito(hashSum, parametri, cb, conten, minData, maxData);
		// NOTA CREDITO RESO CLIENTI
		cercaMovimentiNoteCreditoResoClienti(hashSum, parametri, cb, conten, minData, maxData);
		if (conten.getDati()
				.isEmpty()) {
			return conten;
		}
		List<SumDataConversion> resNuovo = new ArrayList<>(hashSum.values());
		resNuovo.sort(Comparator.comparingDouble(SumDataConversion::getValore)
				.reversed());
		conten.setDati(resNuovo);
		conten.setParametri(parametri);
		return conten;
	}

	@SuppressWarnings("unchecked")
	private ResultContainer findMovimentiConParametriMargineLordo(ParametriAnalisiVendite parametri) {

		ResultContainer conten = new ResultContainer();
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		TypedQuery<?> q = produzioEntityManager.createQuery(parametri.getQueryMovimentiPerMargineLordo(cb));
		//		System.err.println(
		//				">>>>> QUERY <<<<<< " + q.unwrap(org.hibernate.Query.class)
		//						.getQueryString());

		List<Tuple> lista = null;
		lista = (List<Tuple>) q.getResultList();
		//		System.err.println(">>>>> RepositoryCustomForParametriImpl-NORMALI<<<<<< " + lista.size());
		//		System.err.println(">>>>> RepositoryCustomForParametriImpl-findMovimentiConParametri <<<<<< " + lista.size());

		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String minData = parametri.getDallaData()
				.format(formatter);
		String maxData = parametri.getAllaData()
				.format(formatter);
		if (!lista.isEmpty()) {
			for (Tuple tuple : lista) {
				SumDataConversion dat = new SumDataConversion(tuple, parametri.getCliente());
				if (parametri.getCliente()) {
					Anagra ana = cercaCliente(dat.getCodice());
					if (ana != null) {
						//						System.err.println(">>>>> RAG SOC NULL <<<<<< " + dat.getCodice() + " >" + ana.getAnraso() + "<");
						if (ana.getAnraso() == null) {
							ana.setAnraso("*************");
						}
						dat.setDescrizione(ana.getAnraso());
					}
					else {
						dat.setDescrizione("ANAGRAFICA INESISTENTE");
					}
				}
				else {
					Matpri matpri = cercaArticolo(dat.getCodice());
					if (matpri != null) {
						if (matpri.getMtdesc() == null) {
							matpri.setMtdesc("*************");
						}
						dat.setDescrizione(matpri.getMtdesc());
					}
					else {
						dat.setDescrizione("ARTICOLO INESISTENTE");
					}
				}
				conten.sumQta(dat.getQta());
				conten.sumValore(dat.getValore());
				conten.sumCosto(dat.getCosto() == null ? 0D : dat.getCosto());
				conten.addDati(dat);
			}
		}

		Map<String, SumDataConversion> hashSum = conten.getDati()
				.stream()
				.collect(
						Collectors.toMap(SumDataConversion::getCodice, item -> item));

		// NOTE DI CREDITO
		cercaMovimentiNoteDiCredito(hashSum, parametri, cb, conten, minData, maxData);
		// NOTA CREDITO RESO CLIENTI
		cercaMovimentiNoteCreditoResoClienti(hashSum, parametri, cb, conten, minData, maxData);
		// RICERCA MOVIMENTI IN VALUTA
		cercaMovimentiInValuta(hashSum, parametri, cb, conten);
		if (conten.getDati()
				.isEmpty()) {
			return conten;
		}
		List<SumDataConversion> resNuovo = new ArrayList<>(hashSum.values());
		resNuovo.sort(Comparator.comparingDouble(SumDataConversion::getValore)
				.reversed());
		conten.setDati(resNuovo);
		conten.setParametri(parametri);
		return conten;
	}

	@Override
	public List<DataConversion> findMovimentiDiArticolo(ParametriAnalisiVendite parametri, String codArticolo) {
		//		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		//		CriteriaQuery<Tmovrig> cq = cb.createQuery(Tmovrig.class);
		//		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		//		cq.select(tmovRoot);
		//		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
		//		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		//		// PARTE FISSA
		//		List<Predicate> orPredicate = new ArrayList<>();
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		//		Predicate p = cb.disjunction();
		//		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		//		// articolo con asterisco
		//		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		//		// PARAMETRI VARIABILI
		//		List<Predicate> addPreds = new ArrayList<>();
		//		// stagioni
		//		List<Predicate> pStagio = parametri.faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		//		if (!pStagio.isEmpty()) {
		//			if (pStagio.size() == 1) {
		//				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
		//				addPreds.add(pStagios);
		//			}
		//			else {
		//				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
		//				addPreds.add(pStagios);
		//			}
		//		}
		//		// date
		//		if (pStagio.isEmpty()) {
		//			List<Predicate> pDates = parametri.faiPredicatoPerDate(cb, tmovRoot, testata);
		//			if (!pDates.isEmpty()) {
		//				Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
		//				addPreds.add(pDate);
		//			}
		//		}
		//		// tipo tessuto
		//		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		//		if (tipoTessuto != null) {
		//			addPreds.add(tipoTessuto);
		//		}
		//		// rappresentante
		//		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		//		if (rappPred != null) {
		//			addPreds.add(rappPred);
		//		}
		//		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		//		// FINAL
		//		addPreds.add(cb.equal(tmovRoot.get(Tmovrig_.mrarti), codArticolo));
		//		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		//		cq.where(p, pAnd, pAll);
		//
		//		List<Tmovrig> lista = produzioEntityManager.createQuery(cq)
		//				.getResultList();
		//
		//		List<DataConversion> result = new ArrayList<>();
		//		for (Tmovrig tmovrig : lista) {
		//			DataConversion dataConversion = new DataConversion();
		//			Anagra ana = cercaCliente(tmovrig.getTesta()
		//					.getMfana2());
		//			if (ana != null) {
		//				tmovrig.setCodice(ana.getId()
		//						.getAnsott());
		//				tmovrig.setDescrizione(ana.getAnraso());
		//			}
		//			else {
		//				tmovrig.setCodice("????");
		//				tmovrig.setDescrizione("NON TROVATO");
		//			}
		//			Cardis colore = cercaColore(tmovrig.getMrarti(), tmovrig.getMrcolo());
		//			if (colore == null) {
		//				tmovrig.setDesColore("NON TROVATO");
		//			}
		//			else {
		//				tmovrig.setDesColore(colore.getCodesc());
		//			}
		//			dataConversion.converti(tmovrig);
		//			result.add(dataConversion);
		//		}
		//
		return null;
	}

	@Override
	public List<DataConversion> findMovimentiDiArticoloConTuple(ParametriAnalisiVendite parametri, String codArticolo) {
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.LEFT);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		selezioni.add(ordine.get(Ordini_.orcoan)
				.alias(AliasPerCriteriaQuery.stagione.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana2)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); //codArticolo
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString())); //causale
		selezioni.add(tmovRoot.get(Tmovrig_.mrcolo)
				.alias(AliasPerCriteriaQuery.codColore.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString())); //prezzo
		selezioni.add(tmovRoot.get(Tmovrig_.mrrapp)
				.alias(AliasPerCriteriaQuery.codRappr.toString())); //codRappr
		selezioni.add(tmovRoot.get(Tmovrig_.mrumis)
				.alias(AliasPerCriteriaQuery.unitaM.toString())); //unitaM
		selezioni.add(tmovRoot.get(Tmovrig_.mrcapr)
				.alias(AliasPerCriteriaQuery.prodCamp.toString())); //prodCamp
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdado)
				.alias(AliasPerCriteriaQuery.dataMov.toString()));
		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// stagioni

		List<Predicate> pStagio = parametri.faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		if (!pStagio.isEmpty()) {
			if (pStagio.size() == 1) {
				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
			else {
				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
		}
		// date
		List<Predicate> pDates = parametri.faiPredicatoPerDate(cb, tmovRoot, testata);
		if (!pDates.isEmpty()) {
			Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
			addPreds.add(pDate);
		}
		// tipo tessuto
		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// rappresentante
		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = parametri.faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// solo scarico
		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		addPreds.add(scarico);

		// FINAL
		addPreds.add(cb.equal(tmovRoot.get(Tmovrig_.mrarti), codArticolo));
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll);
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		//		System.err
		//				.println(">>>>> QUERY<<<<<< " + qq.unwrap(QueryImpl.class)
		//						.getQueryString());
		List<Tuple> lista = qq.getResultList();

		lista.sort((t1, t2) -> t1.get(AliasPerCriteriaQuery.qta.toString(), Double.class)
				.compareTo(t2.get(AliasPerCriteriaQuery.qta.toString(), Double.class)));
		List<DataConversion> result = new ArrayList<>();
		Double tt = 0D;
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String minData = parametri.getDallaData()
				.format(formatter);
		String maxData = parametri.getAllaData()
				.format(formatter);
		for (Tuple tupla : lista) {
			DataConversion dataConversion = new DataConversion();
			dataConversion.setCodice((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()));
			Anagra ana = cercaCliente((String) tupla.get(AliasPerCriteriaQuery.codCli.toString()));
			if (ana != null) {
				dataConversion.setCodice(ana.getId()
						.getAnsott());
				dataConversion.setDescrizione(ana.getAnraso());
			}
			else {
				dataConversion.setCodice("????");
				dataConversion.setDescrizione("NON TROVATO");
			}
			Cardis colore = cercaColore((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()),
					(String) tupla.get(AliasPerCriteriaQuery.codColore.toString()));
			if (colore == null) {
				dataConversion.setDesColore("NON TROVATO");
			}
			else {
				dataConversion.setDesColore(colore.getCodesc());
			}
			// STAGIONE
			//			Integer nOrdi = tupla.get(AliasPerCriteriaQuery.nOrdine.toString(), Integer.class);
			//			if (nOrdi != 0) {
			//				Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
			//				Expression<String> stagioPath = ordine.get(Ordini_.orcoan)
			//						.as(String.class);
			//				String stagio = stagioPath.asString();
			//			}
			//			else {
			//
			//			}
			dataConversion.converti(tupla);
			String valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);
			if (!valuta.equals("EUR") && !valuta.isEmpty()) {
				String data = (String) tupla.get(AliasPerCriteriaQuery.dataValuta.toString());
				faiCambio(dataConversion, valuta, data);
			}
			else {
				faiMaggiorazione(dataConversion);
			}
			tt += dataConversion.getQta();
			result.add(dataConversion);
		}
		cercaNoteCreditoConArticolo(result, minData, maxData, parametri, codArticolo);
		cercaNoteCreditoNCTConArticolo(result, minData, maxData, parametri, codArticolo);
		return result;
	}

	@Override
	public List<DataConversion> findMovimentiDiCliente(ParametriAnalisiVendite parametri, String codCli) {
		//		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		//		CriteriaQuery<Tmovrig> cq = cb.createQuery(Tmovrig.class);
		//		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		//		cq.select(tmovRoot);
		//		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		//		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine);
		//		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		//		// PARTE FISSA
		//		List<Predicate> orPredicate = new ArrayList<>();
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		//		Predicate p = cb.disjunction();
		//		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		//		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		//		// PARAMETRI VARIABILI
		//		List<Predicate> addPreds = new ArrayList<>();
		//		// stagioni
		//		List<Predicate> pStagio = parametri.faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		//		if (!pStagio.isEmpty()) {
		//			if (pStagio.size() == 1) {
		//				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
		//				addPreds.add(pStagios);
		//			}
		//			else {
		//				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
		//				addPreds.add(pStagios);
		//			}
		//		}
		//		// date
		//		if (pStagio.isEmpty()) {
		//			List<Predicate> pDates = parametri.faiPredicatoPerDate(cb, tmovRoot, testata);
		//			if (!pDates.isEmpty()) {
		//				Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
		//				addPreds.add(pDate);
		//			}
		//		}
		//		// tipo tessuto
		//		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		//		if (tipoTessuto != null) {
		//			addPreds.add(tipoTessuto);
		//		}
		//		// rappresentante
		//		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		//		if (rappPred != null) {
		//			addPreds.add(rappPred);
		//		}
		//		// solo scarico
		//		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		//		addPreds.add(scarico);
		//		// FINAL
		//		addPreds.add(cb.equal(testata.get(Tmovmag_.mfana2), codCli));
		//		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		//		cq.where(p, pAnd, pAll);
		//		List<Tmovrig> lista = produzioEntityManager.createQuery(cq)
		//				.getResultList();
		//		List<DataConversion> result = new ArrayList<>();
		//		for (Tmovrig tmovrig : lista) {
		//			DataConversion dataConversion = new DataConversion();
		//			Matpri matpri = cercaArticolo(tmovrig.getMrarti());
		//			tmovrig.setCodice(tmovrig.getMrarti());
		//			tmovrig.setDescrizione(matpri.getMtdesc());
		//
		//			Cardis colore = cercaColore(tmovrig.getMrarti(), tmovrig.getMrcolo());
		//			if (colore == null) {
		//				tmovrig.setDesColore("NON TROVATO");
		//			}
		//			else {
		//				tmovrig.setDesColore(colore.getCodesc());
		//			}
		//			dataConversion.converti(tmovrig);
		//			result.add(dataConversion);
		//		}
		return null;
	}

	@Override
	public List<DataConversion> findMovimentiDiClienteConTuple(ParametriAnalisiVendite parametri, String codCli) {
		CriteriaBuilder cb = produzioEntityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<Tmovrig> tmovRoot = cq.from(Tmovrig.class);
		Join<Tmovrig, Tmovmag> testata = tmovRoot.join(Tmovrig_.testa);
		Join<Tmovrig, Ordini> ordine = tmovRoot.join(Tmovrig_.rigaOrdine, JoinType.LEFT);
		Join<Tmovrig, Matpri> matPri = tmovRoot.join(Tmovrig_.matpri);
		// selezioni campi
		List<Selection<?>> selezioni = new ArrayList<>();
		selezioni.add(ordine.get(Ordini_.orcoan)
				.alias(AliasPerCriteriaQuery.stagione.toString())); //codcli
		selezioni.add(testata.get(Tmovmag_.mfana2)
				.alias(AliasPerCriteriaQuery.codCli.toString())); //codcli
		selezioni.add(tmovRoot.get(Tmovrig_.mrarti)
				.alias(AliasPerCriteriaQuery.codArticolo.toString())); //codArticolo
		selezioni.add(testata.get(Tmovmag_.mftido)
				.alias(AliasPerCriteriaQuery.causale.toString())); //causale
		selezioni.add(tmovRoot.get(Tmovrig_.mrcolo)
				.alias(AliasPerCriteriaQuery.codColore.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrmag1)
				.alias(AliasPerCriteriaQuery.maggiorazioni.toString())); //cod colore
		selezioni.add(tmovRoot.get(Tmovrig_.mrprli)
				.alias(AliasPerCriteriaQuery.prezzo.toString())); //prezzo
		selezioni.add(tmovRoot.get(Tmovrig_.mrrapp)
				.alias(AliasPerCriteriaQuery.codRappr.toString())); //codRappr
		selezioni.add(tmovRoot.get(Tmovrig_.mrumis)
				.alias(AliasPerCriteriaQuery.unitaM.toString())); //unitaM
		selezioni.add(tmovRoot.get(Tmovrig_.mrcapr)
				.alias(AliasPerCriteriaQuery.prodCamp.toString())); //prodCamp
		selezioni.add(tmovRoot.get(Tmovrig_.mrqtan)
				.alias(AliasPerCriteriaQuery.qta.toString())); //qta
		selezioni.add(testata.get(Tmovmag_.mfdivi)
				.alias(AliasPerCriteriaQuery.valuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfrida)
				.alias(AliasPerCriteriaQuery.dataValuta.toString()));
		selezioni.add(testata.get(Tmovmag_.mfdado)
				.alias(AliasPerCriteriaQuery.dataMov.toString()));

		// PARTE FISSA
		List<Predicate> orPredicate = new ArrayList<>();
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEN"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "VEL"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCR"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCT"));
		//		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "NCL"));
		orPredicate.add(cb.equal(testata.get(Tmovmag_.mftido), "FAC"));
		Predicate p = cb.disjunction();
		p = cb.or(orPredicate.toArray(new Predicate[orPredicate.size()]));
		Predicate pAnd = cb.and(cb.notLike(tmovRoot.get(Tmovrig_.mrarti), "*%"));
		// DEFINIZIONE DEI RISULTATI
		cq.multiselect(selezioni);
		// PARAMETRI VARIABILI
		List<Predicate> addPreds = new ArrayList<>();
		// stagioni
		List<Predicate> pStagio = parametri.faiPredicatoPerStagioni(cb, tmovRoot, ordine);
		if (!pStagio.isEmpty()) {
			if (pStagio.size() == 1) {
				Predicate pStagios = cb.and(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
			else {
				Predicate pStagios = cb.or(pStagio.toArray(new Predicate[pStagio.size()]));
				addPreds.add(pStagios);
			}
		}
		// date
		List<Predicate> pDates = parametri.faiPredicatoPerDate(cb, tmovRoot, testata);
		if (!pDates.isEmpty()) {
			Predicate pDate = cb.and(pDates.toArray(new Predicate[pDates.size()]));
			addPreds.add(pDate);
		}
		// tipo tessuto
		Predicate tipoTessuto = parametri.faiPredicatoPerTipoTessuto(cb, tmovRoot, matPri);
		if (tipoTessuto != null) {
			addPreds.add(tipoTessuto);
		}
		// rappresentante
		Predicate rappPred = parametri.faiPredicatoPerRappresentante(cb, tmovRoot, testata);
		if (rappPred != null) {
			addPreds.add(rappPred);
		}
		// clienti
		Predicate pClis = parametri.faiPredicatoPerClienti(cb, tmovRoot, testata);
		if (pClis != null) {
			addPreds.add(pClis);
		}
		// solo scarico
		Predicate scarico = cb.equal(tmovRoot.get(Tmovrig_.mrcasc), "S");
		addPreds.add(scarico);
		// FINAL
		addPreds.add(cb.equal(testata.get(Tmovmag_.mfana2), codCli));
		Predicate pAll = cb.and(addPreds.toArray(new Predicate[addPreds.size()]));
		cq.where(p, pAnd, pAll);
		TypedQuery<Tuple> qq = produzioEntityManager.createQuery(cq);
		//		System.err
		//				.println(">>>>> RepositoryCustomForParametriImpl-findMovimentiDiClienteConTuple <<<<<< " + qq.unwrap(QueryImpl.class)
		//						.getQueryString());
		List<Tuple> lista = qq.getResultList();

		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String minData = parametri.getDallaData()
				.format(formatter);
		String maxData = parametri.getAllaData()
				.format(formatter);
		List<DataConversion> result = new ArrayList<>();
		for (Tuple tupla : lista) {
			DataConversion dataConversion = new DataConversion();
			Matpri matpri = cercaArticolo(tmovRoot.get(Tmovrig_.mrarti)
					.toString());
			if (matpri != null) {
				dataConversion.setCodice(matpri.getId()
						.getMtarti());
				dataConversion.setDescrizione(matpri.getMtdesc());
			}
			else {
				dataConversion.setCodice("?????");
				dataConversion.setDescrizione("NON TROVATO");
			}

			dataConversion.setCodice((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()));
			//			Anagra ana = cercaCliente((String) tupla.get(AliasPerCriteriaQuery.codCli.toString()));
			//			if (ana != null) {
			//				dataConversion.setCodice(ana.getId()
			//						.getAnsott());
			//				dataConversion.setDescrizione(ana.getAnraso());
			//			}
			//			else {
			//				dataConversion.setCodice("????");
			//				dataConversion.setDescrizione("NON TROVATO");
			//			}
			Cardis colore = cercaColore((String) tupla.get(AliasPerCriteriaQuery.codArticolo.toString()),
					(String) tupla.get(AliasPerCriteriaQuery.codColore.toString()));
			if (colore == null) {
				dataConversion.setDesColore("NON TROVATO");
			}
			else {
				dataConversion.setDesColore(colore.getCodesc());
			}
			dataConversion.converti(tupla);
			String valuta = tupla.get(AliasPerCriteriaQuery.valuta.toString(), String.class);
			if (!valuta.equals("EUR") && !valuta.isEmpty()) {
				String data = (String) tupla.get(AliasPerCriteriaQuery.dataValuta.toString());
				faiCambio(dataConversion, valuta, data);
			}
			else {
				faiMaggiorazione(dataConversion);
			}
			result.add(dataConversion);
		}
		cercaNoteCreditoConCliente(result, minData, maxData, parametri, codCli);
		cercaNoteCreditoNCTConCliente(result, minData, maxData, parametri, codCli);
		return result;
	}

	private ResultContainer findOrdiniConParametri(ParametriAnalisiVendite parametri) {
		// TODO Auto-generated method stub
		return null;
	}

}
