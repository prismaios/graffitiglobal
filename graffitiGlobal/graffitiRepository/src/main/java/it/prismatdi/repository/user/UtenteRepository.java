package it.prismatdi.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;

import it.prismatdi.model.user.Utente;
import it.prismatdi.model.user.UtentePK;

public interface UtenteRepository extends JpaRepository<Utente, UtentePK>, RepoUtente {

}
