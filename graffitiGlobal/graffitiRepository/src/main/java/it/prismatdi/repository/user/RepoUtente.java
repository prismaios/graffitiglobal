package it.prismatdi.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.prismatdi.model.user.Utente;

public interface RepoUtente {
	@Query("SELECT u FROM Utente AS u WHERE u.id.ditcod = :pCodDitta")
	List<Utente> getAllUtenti(@Param("pCodDitta") String codDitta);

}
