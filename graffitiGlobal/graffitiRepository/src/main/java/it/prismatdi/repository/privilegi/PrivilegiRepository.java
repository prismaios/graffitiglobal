package it.prismatdi.repository.privilegi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.user.Privilegi;

@Repository
public interface PrivilegiRepository extends JpaRepository<Privilegi, Integer> {

	@Query("SELECT w FROM Privilegi AS w ORDER BY w.descrizione")
	List<Privilegi> getAllPrivilegi();
}
