package it.prismatdi.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.user.UserGraffitiWeb;

@Repository
public interface UserRepository extends JpaRepository<UserGraffitiWeb, Integer> {

	@Query("select u from UserGraffitiWeb u order by u.cognome")
	List<UserGraffitiWeb> getAllUsers();

}
