package it.prismatdi.repository.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.prismatdi.model.user.UserGraffitiWeb;

@Repository
public interface SecUserRepository extends JpaRepository<UserGraffitiWeb, Integer> {

	@Query("SELECT u FROM UserGraffitiWeb AS u WHERE u.loginName = :pLoginName")
	public UserGraffitiWeb findUserByLoginName(@Param("pLoginName") String loginName);

}
