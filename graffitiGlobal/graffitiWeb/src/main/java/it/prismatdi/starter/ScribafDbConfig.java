package it.prismatdi.starter;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
//@PropertySource({ "classpath:multiple-db.properties" })
@EnableJpaRepositories(
		entityManagerFactoryRef = "scribafEntityManagerFactory",
		basePackages = "it.prismatdi.scribaf",
		transactionManagerRef = "scribafTransactionManager")
public class ScribafDbConfig {

	@Autowired
	private Environment env;

	@Bean(name = "dataSourceScribaf")
	@ConfigurationProperties(prefix = "scribaf.datasource")
	public DataSource dataSourceScribaf() {
		System.err.println(">>>>> ScribafDbConfig-dataSource <<<<<< ");
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("scribaf.datasource.jdbc.driverClassName"));
		dataSource.setUrl(env.getProperty("scribaf.datasource.jdbc.jdbc-url"));
		dataSource.setUsername(env.getProperty("scribaf.datasource.jdbc.username"));
		dataSource.setPassword(env.getProperty("scribaf.datasource.jdbc.password"));
		System.err.println(">>>>> ScribafDbConfig-dataSource <<<<<< " + env.getProperty("scribaf.datasource.jdbc.jdbc-url"));
		return dataSource;
	}

	@Bean(name = "scribafEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryScribaf() {
		//		public LocalContainerEntityManagerFactoryBean entityManagerFactoryScribaf(EntityManagerFactoryBuilder builder,
		//				@Qualifier("dataSourceScribaf") DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSourceScribaf());
		em.setPackagesToScan(new String[] { "it.prismatdi.model.scribaf" });
		em.setPersistenceUnitName("scribafUnit");
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		//		properties.put("hibernate.hbm2ddl.auto",
		//				env.getProperty("hibernate.hbm2ddl.auto"));
		properties.put("hibernate.dialect",
				env.getProperty("hibernate.dialect"));
		em.setJpaPropertyMap(properties);
		return em;
		//		HikariDataSource ds = (HikariDataSource) dataSource;
		//		System.err.println(">>>>> ScribafDbConfig-entityManagerFactory <<<<<< " + ds.getDataSourceProperties());
		//		return builder
		//				.dataSource(dataSource)
		//				.packages("it.prismatdi.model.scribaf")
		//				.persistenceUnit("scribaf")
		//				.build();
	}

	@Bean(name = "scribafTransactionManager")
	public PlatformTransactionManager transactionManagerScribaf() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactoryScribaf().getObject());
		return transactionManager;
	}

}
