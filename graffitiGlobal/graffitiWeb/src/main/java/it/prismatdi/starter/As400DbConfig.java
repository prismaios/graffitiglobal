package it.prismatdi.starter;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
//@PropertySource({ "classpath:multiple-db.properties" })
@EnableJpaRepositories(
		entityManagerFactoryRef = "as400EntityManagerFactory",
		basePackages = "it.prismatdi.as400",
		transactionManagerRef = "as400TransactionManager")
public class As400DbConfig {
	@Autowired
	private Environment env;

	@Bean(name = "dataSourceAs400")
	@ConfigurationProperties(prefix = "as400.datasource")
	public DataSource dataSourceAs400() {
		System.err.println(">>>>> ScribafDbConfig-dataSource <<<<<< ");
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("scribaf.datasource.jdbc.driverClassName"));
		dataSource.setUrl(env.getProperty("as400.jdbc-url"));
		dataSource.setUsername(env.getProperty("scribaf.datasource.jdbc.username"));
		dataSource.setPassword(env.getProperty("scribaf.datasource.jdbc.password"));
		System.err.println(">>>>> As400DbConfig-dataSourceAs400 <<<<<< " + env.getProperty("as400.jdbc-url"));
		return dataSource;
	}

	@Bean(name = "as400EntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryAs400() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSourceAs400());
		em.setPackagesToScan(new String[] { "it.prismatdi.model.as400" });
		em.setPersistenceUnitName("as400Unit");
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		properties.put("hibernate.dialect",
				env.getProperty("hibernate.dialect"));
		em.setJpaPropertyMap(properties);
		return em;
	}

	@Bean(name = "as400TransactionManager")
	public PlatformTransactionManager transactionManagerAs400() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactoryAs400().getObject());
		return transactionManager;
	}
}
