package it.prismatdi.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	//	public static void main(String[] args) {
	//		System.err.println(new BCryptPasswordEncoder().encode("leo123"));
	//	}
	//	@Override
	//	@Bean
	//	public UserDetailsService userDetailsService() {
	//		return super.userDetailsService();
	//	}

	@Autowired
	private UserDetailsService detailsService;
	//	@Override
	//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
	//		//admin123
	//		auth.inMemoryAuthentication().withUser("admin").password("$2a$10$7yd5GkY/j6JpoXwO5LczSuFQZeWv9MeuoTzz06Ph3DmkltQuLRNaq")
	//				.roles("ADMIN");
	//		//4206vitt
	//		auth.inMemoryAuthentication().withUser("vitto").password("$2a$10$3ETXwZ74G36pO.VTRi7ZFemx8sWip8LGQqLeVFp6c5zQGtp15cIoC")
	//				.roles("ADMIN");
	//		auth.inMemoryAuthentication().withUser("leo").password("$2a$10$Z79dH5mW5Wy83Z7st/u.F.rMx2.07sIpkdSo7yY2EkDGERUz7Z/JK")
	//				.roles("ADMIN");
	//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf()
				.disable()
				.exceptionHandling()
				.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"))
				.and()
				.authorizeRequests()
				.antMatchers("/VAADIN/**", "/HEARTBEAT/**", "/PUSH/**", "/UIDL/**", "/login", "/signup", "/login/**", "/logout",
						"/vaadinServlet/**")
				.permitAll()
				.antMatchers("/ui", "/ui/**")
				.fullyAuthenticated();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
		final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(detailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		builder.userDetailsService(detailsService)
				.and()
				.authenticationProvider(authenticationProvider);
	}

	@Bean
	public DaoAuthenticationProvider createDaoAuthenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(detailsService);
		provider.setPasswordEncoder(passwordEncoder());
		return provider;
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
