package it.prismatdi.starter;

import org.atmosphere.cpr.SessionSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.annotation.EnableEventBus;
import org.vaadin.spring.events.support.ApplicationContextEventBroker;

@Configuration
@EnableAutoConfiguration
@EnableWebSecurity
@ComponentScan({ "it.prismatdi" })
@PropertySource({ "classpath:multiple-db.properties" })
@EnableJpaRepositories({ "it.prismatdi.repository" })
@EntityScan({ "it.prismatdi.model" })
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableEventBus

public class GraffitiWebBoot extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(GraffitiWebBoot.class, args);
	}

	@Autowired
	EventBus.ApplicationEventBus applicationEventBus;

	@Bean
	ApplicationContextEventBroker applicationContextEventBroker() {
		return new ApplicationContextEventBroker(applicationEventBus);
	}

	@Bean
	public SessionSupport atmosphereSessionSupport() {
		return new SessionSupport();
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GraffitiWebBoot.class);
	}

}
