package it.prismatdi.starter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.annotation.EventBusProxy;
import org.vaadin.spring.events.internal.ScopedEventBus;
import org.vaadin.spring.events.support.VaadinEventBusAwareProcessor;

import com.vaadin.spring.internal.UIScopeImpl;
import com.vaadin.spring.internal.VaadinSessionScope;
import com.vaadin.spring.internal.ViewScopeImpl;

@Configuration
public class EventBusConfiguration {

	@Bean
	EventBus.ApplicationEventBus applicationEventBus() {
		return new ScopedEventBus.DefaultApplicationEventBus();
	}

	@Bean
	@Scope(value = VaadinSessionScope.VAADIN_SESSION_SCOPE_NAME, proxyMode = ScopedProxyMode.INTERFACES)
	@EventBusProxy
	EventBus.SessionEventBus proxiedSessionEventBus() {
		return sessionEventBus();
	}

	@Bean
	@Scope(value = UIScopeImpl.VAADIN_UI_SCOPE_NAME, proxyMode = ScopedProxyMode.INTERFACES)
	@EventBusProxy
	EventBus.UIEventBus proxiedUiEventBus() {
		return uiEventBus();
	}

	@Bean
	@Scope(value = ViewScopeImpl.VAADIN_VIEW_SCOPE_NAME, proxyMode = ScopedProxyMode.INTERFACES)
	@EventBusProxy
	EventBus.ViewEventBus proxiedViewEventBus() {
		return viewEventBus();
	}

	@Bean
	@Scope(value = VaadinSessionScope.VAADIN_SESSION_SCOPE_NAME, proxyMode = ScopedProxyMode.NO)
	@Primary
	EventBus.SessionEventBus sessionEventBus() {
		return new ScopedEventBus.DefaultSessionEventBus(applicationEventBus());
	}

	@Bean
	@Scope(value = UIScopeImpl.VAADIN_UI_SCOPE_NAME, proxyMode = ScopedProxyMode.NO)
	@Primary
	EventBus.UIEventBus uiEventBus() {
		return new ScopedEventBus.DefaultUIEventBus(sessionEventBus());
	}

	@Bean
	VaadinEventBusAwareProcessor vaadinEventBusProcessor() {
		return new VaadinEventBusAwareProcessor();
	}

	@Bean
	@Scope(value = ViewScopeImpl.VAADIN_VIEW_SCOPE_NAME, proxyMode = ScopedProxyMode.NO)
	@Primary
	EventBus.ViewEventBus viewEventBus() {
		return new ScopedEventBus.DefualtViewEventBus(uiEventBus());
	}

}
