package it.prismatdi.starter;

import java.util.HashMap;

import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
//@PropertySource({ "classpath:multiple-db.properties" })
@EnableJpaRepositories(
		entityManagerFactoryRef = "produzioEntityManagerFactory",
		basePackages = "it.prismatdi.produzio",
		transactionManagerRef = "produzioTransactionManager")
@PersistenceContext(unitName = "produzioUnit")
public class ProduzioDbConfig {
	@Autowired
	private Environment env;
	//	LocalContainerEntityManagerFactoryBean	lcem;

	@Bean(name = "dataSourceProduzio")
	@ConfigurationProperties(prefix = "produzio.datasource")
	public DataSource dataSourceProduzio() {
		System.err.println(">>>>> ProduzioDbConfig-dataSource <<<<<< ");
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("scribaf.datasource.jdbc.driverClassName"));
		dataSource.setUrl(env.getProperty("produzio.jdbc-url"));
		dataSource.setUsername(env.getProperty("scribaf.datasource.jdbc.username"));
		dataSource.setPassword(env.getProperty("scribaf.datasource.jdbc.password"));
		System.err.println(
				">>>>> ProduzioDbConfig-dataSourceProduzio <<<<<< " + env.getProperty("produzio.jdbc-url") + dataSource.toString());
		return dataSource;
	}

	@Bean(name = "produzioEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryProduzio() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSourceProduzio());
		em.setPersistenceUnitName("produzioUnit");
		em.setPackagesToScan(new String[] { "it.prismatdi.model.produzio" });
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", env.getProperty("hibernate.format_sql"));

		em.setJpaPropertyMap(properties);

		return em;
	}

	@Bean(name = "produzioTransactionManager")
	public PlatformTransactionManager transactionManagerProduzio() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactoryProduzio().getObject());
		transactionManager.afterPropertiesSet();
		return transactionManager;
	}

}
