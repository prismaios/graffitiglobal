package it.prismatdi.ui.sintesi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.vaadin.addons.searchbox.SearchBox;
import org.vaadin.addons.searchbox.SearchBox.ButtonPosition;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.errors.BinderError;
import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.general.ChiamateDa;
import it.prismatdi.general.ItaliaEstero;
import it.prismatdi.general.ProdCamp;
import it.prismatdi.general.TipoTessuto;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.model.utils.ParametriAnalisiVendite;
import it.prismatdi.model.utils.ResultContainer;
import it.prismatdi.services.produzio.TmovrigService;
import it.prismatdi.ui.commons.GraffitiWebMainUI;
import it.prismatdi.ui.commons.WindowElenchiConBusta;

@SuppressWarnings("serial")
@SpringComponent
@Scope("prototype")
public class InputParametriRicerche extends GridLayout implements EventBusListener<Object>, Window.CloseListener {
	@Autowired
	private TmovrigService							tmovrigService;

	@Autowired
	private EventBus.ApplicationEventBus		eventBus;

	@Autowired
	private WindowElenchiConBusta					windowSelezioni;
	//
	private ParametriAnalisiVendite				parametri;

	//
	private ComboBox<TipoTessuto>					tipoTessuto;
	private DateField									dallaData;
	private DateField									allaData;
	private SearchBox									stagioni;
	private CheckBox									annoP;
	private SearchBox									rappresentante;
	private SearchBox									clienti;
	private CheckBox									perOrdine;
	private ComboBox<ProdCamp>						prodCampion;
	private ComboBox<ItaliaEstero>				paese;
	private CheckBox									margineLordo;
	private CheckBox									venditeOrdini;
	private CheckBox									excludeCamp;
	private SearchBox									lineaProd;
	private CheckBox									visCliente;
	//
	private Busta										rappScelto;
	private Button										cerca;
	private Binder<ParametriAnalisiVendite>	binder;

	public InputParametriRicerche() {
	}

	public InputParametriRicerche(int columns, int rows) {
		super(columns, rows);
		// TODO Auto-generated constructor stub
	}

	public InputParametriRicerche(int columns, int rows, Component... children) {
		super(columns, rows, children);
		// TODO Auto-generated constructor stub
	}

	public void bind() {
		//			System.err.println(">>>>> ParametriVenditeViewFactory.ParametriVenditeView-bind <<<<<< ");
		binder.forField(tipoTessuto)
				.bind(ParametriAnalisiVendite::getTipoTessuto, ParametriAnalisiVendite::setTipoTessuto);
		binder.forField(stagioni.getSearchField())
				.bind(ParametriAnalisiVendite::getStagioniStr, ParametriAnalisiVendite::setStagioneStr);
		binder.forField(dallaData)
				.bind(ParametriAnalisiVendite::getDallaData, ParametriAnalisiVendite::setDallaData);
		binder.forField(allaData)
				.bind(ParametriAnalisiVendite::getAllaData, ParametriAnalisiVendite::setAllaData);
		binder.forField(annoP)
				.bind(ParametriAnalisiVendite::getAnnoPrecedente, ParametriAnalisiVendite::setAnnoPrecedente);
		binder.forField(rappresentante.getSearchField())
				.bind(parametri -> parametri.getRappresentante() != null ? parametri.getRappresentante()
						.getDescrizione() : "", (parametri, busta) -> parametri.setRappresentante(rappScelto));
		binder.forField(clienti.getSearchField())
				.bind(ParametriAnalisiVendite::getSelClienteStr, ParametriAnalisiVendite::setSelClientiStr);
		binder.forField(perOrdine)
				.bind(ParametriAnalisiVendite::getPerOrdine, ParametriAnalisiVendite::setPerOrdine);
		binder.forField(prodCampion)
				.bind(ParametriAnalisiVendite::getProdCampion, ParametriAnalisiVendite::setProdCampion);
		binder.forField(paese)
				.bind(ParametriAnalisiVendite::getPaese, ParametriAnalisiVendite::setPaese);
		binder.forField(margineLordo)
				.bind(ParametriAnalisiVendite::getMargineLordo, ParametriAnalisiVendite::setMargineLordo);
		binder.forField(venditeOrdini)
				.bind(ParametriAnalisiVendite::getVenditeOrdini, ParametriAnalisiVendite::setVenditeOrdini);
		binder.forField(excludeCamp)
				.bind(ParametriAnalisiVendite::getExcludeCamp, ParametriAnalisiVendite::setExcludeCamp);
		binder.forField(lineaProd.getSearchField())
				.bind(ParametriAnalisiVendite::getLineaProdStr, ParametriAnalisiVendite::setLineaProdStr);
		binder.forField(visCliente)
				.bind(ParametriAnalisiVendite::getCliente, ParametriAnalisiVendite::setCliente);
	}

	public void cercaDati() {
		final boolean beanOk = binder.writeBeanIfValid(parametri);
		if (beanOk) {
			if (parametri.getDallaData() == null || parametri.getAllaData() == null) {
				Notification.show(BinderError.TITOLO.getString(), BinderError.ERR_ANNO_PRECEDENTE.getString(), Type.ERROR_MESSAGE);
				return;
			}
			if (rappresentante.getSearchField()
					.getValue()
					.isEmpty()) {
				parametri.setRappresentante(null);
				rappScelto = null;
			}
			final ResultContainer righe = tmovrigService.findConFiltri(parametri);
			if (righe == null) {
				return;
			}
			if (righe.getDati()
					.size() > 0) {
				eventBus.publish(TypeForEventBus.ARRIVATE_RIGHE_PER_ANALISI_VENDITE.toString(), this, righe);
			}
			//				for (final DataConversion dataConversion : righe.getDati()) {
			//					System.err.println(
			//							">>>>> ParametriVenditeViewFactory.ParametriVenditeView-cercaDati <<<<<< " + dataConversion.toString());
			//				}
		}
	}

	@PostConstruct
	public void configura() {
		eventBus.subscribe(this);
		init();
		layout();
		bind();
	}

	public void init() {
		//			System.err.println(">>>>> ParametriVenditeViewFactory.ParametriVenditeView-init <<<<<< ");
		setColumns(2);
		setRows(9);
		//			setColumnExpandRatio(0, ratio);
		if (parametri == null) {
			parametri = new ParametriAnalisiVendite();
		}
		binder = new Binder<>();
		tipoTessuto = new ComboBox<>("Sciarpe/Tessuto", Arrays.asList(TipoTessuto.values()));
		dallaData = new DateField("Dalla data");
		allaData = new DateField("Alla data");
		stagioni = new SearchBox(VaadinIcons.SEARCH, ButtonPosition.LEFT);
		stagioni.setPlaceholder("Stagioni...");
		stagioni.setWidth("190px");
		stagioni.getSearchButton()
				.addClickListener(e -> {
					final List<Busta> stagios = GraffitiWebMainUI.getCurrent()
							.getStagioni();
					eventBus.publish(TypeForEventBus.SHOW_STAGIONI.toString(), this, stagios);
					showWindowSelezione(ChiamateDa.STAGIONI);
				});
		stagioni.getSearchField()
				.addValueChangeListener(e -> {
					if (stagioni.getSearchField()
							.getValue()
							.isEmpty()) {
						parametri.setStagioni(new ArrayList<>());
					}
				});
		annoP = new CheckBox("Anno precedente");
		rappresentante = new SearchBox(VaadinIcons.SEARCH, ButtonPosition.LEFT);
		rappresentante.setPlaceholder("Rappresentante");
		rappresentante.setWidth("190px");
		rappresentante.getSearchButton()
				.addClickListener(e -> {
					// Cerca rappresentanti
					final List<Busta> rapps = GraffitiWebMainUI.getCurrent()
							.getRappresentanti();
					eventBus.publish(TypeForEventBus.SHOW_RAPPRESENTANTI.toString(), this, rapps);
					showWindowSelezione(ChiamateDa.RAPPRESENTANTI);
				});
		clienti = new SearchBox(VaadinIcons.SEARCH, ButtonPosition.LEFT);
		clienti.setPlaceholder("Scegli clienti...");
		clienti.setWidth("190px");
		clienti.getSearchButton()
				.addClickListener(e -> {
					// Cerca clienti
					final List<Busta> clis = GraffitiWebMainUI.getCurrent()
							.getClienti();
					eventBus.publish(TypeForEventBus.SHOW_CLIENTI.toString(), this, clis);
					showWindowSelezione(ChiamateDa.CLIENTI);
				});
		perOrdine = new CheckBox("Per ordine");
		prodCampion = new ComboBox<>("Produzione/Campionario", Arrays.asList(ProdCamp.values()));
		paese = new ComboBox<>("Italia/Estero", Arrays.asList(ItaliaEstero.values()));
		margineLordo = new CheckBox("Margine lordo");
		venditeOrdini = new CheckBox("Vendite + ordini");
		excludeCamp = new CheckBox("Escludi camp.");
		lineaProd = new SearchBox(VaadinIcons.SEARCH, ButtonPosition.LEFT);
		lineaProd.setPlaceholder("Linee prodotti...");
		lineaProd.setWidth("190px");
		lineaProd.getSearchButton()
				.addClickListener(e -> {
					// cerca linee
					eventBus.publish(TypeForEventBus.SHOW_LINEA_PROD.toString(), this);
					showWindowSelezione(ChiamateDa.LINEE);
				});
		visCliente = new CheckBox("Vis.clienti");
		//
		cerca = new Button("Cerca");
		cerca.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		cerca.addClickListener(e -> {
			// Cliccato cerca
			cercaDati();
		});
	}

	public void layout() {
		//			System.err.println(">>>>> ParametriVenditeViewFactory.ParametriVenditeView-layout <<<<<< ");
		setMargin(true);
		setSpacing(true);
		addComponent(tipoTessuto, 0, 0);
		addComponent(stagioni, 1, 0);
		addComponent(dallaData, 0, 1);
		addComponent(allaData, 1, 1);
		addComponent(annoP, 0, 2);
		addComponent(rappresentante, 1, 2);
		addComponent(clienti, 0, 3);
		addComponent(perOrdine, 1, 3);
		addComponent(prodCampion, 0, 4);
		addComponent(paese, 1, 4);
		addComponent(margineLordo, 0, 5);
		addComponent(venditeOrdini, 1, 5);
		addComponent(excludeCamp, 0, 6);
		addComponent(lineaProd, 1, 6);
		addComponent(visCliente, 0, 7);
		//
		addComponent(cerca, 1, 8);

	}

	@Override
	public void onEvent(org.vaadin.spring.events.Event<Object> event) {
		// TODO Auto-generated method stub

	}

	public void showWindowSelezione(ChiamateDa daDove) {
		windowSelezioni.addCloseListener(this);
		windowSelezioni.setDaDove(daDove);
		UI.getCurrent()
				.addWindow(windowSelezioni);

	}

	@Override
	public void windowClose(CloseEvent e) {
		//		System.err.println(">>>>> ParametriVenditeViewFactory.ParametriVenditeView-windowClose <<<<<< ");
		final ChiamateDa daDove = windowSelezioni.getDaDove();
		final Set<Busta> result = windowSelezioni.getSelezioni();
		final StringBuilder sb = new StringBuilder();
		for (final Busta busta : result) {
			switch (daDove) {
				case STAGIONI:
					sb.append(busta.getCodice()
							.substring(3));
					sb.append(",");
					stagioni.getSearchField()
							.setValue(sb.toString());
					break;
				case CLIENTI:
					clienti.getSearchField()
							.setValue(busta.getCodice());
					break;
				case RAPPRESENTANTI:
					// l'ultimo
					rappScelto = busta;
					parametri.setRappresentante(busta);
					rappresentante.getSearchField()
							.setValue(busta.getDescrizione());
					break;
				case LINEE:
					//						parametri.addLinea(busta.getCodice());
					break;

				default:
					break;
			}
		}

		//		String idWin = windowSelezioni.getId();
		//		System.err.println(">>>>> ParametriVenditeViewFactory.ParametriVenditeView-windowClose <<<<<< " + idWin);
		UI.getCurrent()
				.removeWindow(windowSelezioni);
		//			windowSelezioni.setEnabled(false);
		//			binder.readBean(parametri);
		//			windowSelezioni.close();
	}

}
