package it.prismatdi.ui.agenti;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.general.ScelteAgenti;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.model.utils.UnimagData;
import it.prismatdi.services.produzio.UnimagService;
import it.prismatdi.ui.commons.GraffitiWebMainUI;

@SuppressWarnings("serial")
@SpringView(name = CampionarioView.VIEW_NAME, ui = GraffitiWebMainUI.class)
@UIScope
public class CampionarioView extends VerticalLayout implements View, EventBusListener<Object> {
	public static final String					VIEW_NAME	= "campionarioAgente";

	@Autowired
	protected UnimagService						unimagService;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	private List<UnimagData>					dati;
	private Grid<UnimagData>					grid;
	private ComboBox<ScelteAgenti>			scelta;
	private ComboBox<Busta>						stagione;
	private Button									cerca;
	private TextField								searchText;
	private final DecimalFormat				df				= (DecimalFormat) NumberFormat.getInstance();
	private List<Busta>							stagioni;

	public CampionarioView() {
		// TODO Auto-generated constructor stub
	}

	public CampionarioView(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

	private void bind() {
		grid.removeAllColumns();
		grid.setItems(dati);
		grid.addColumn(UnimagData::getArticolo)
				.setId("articolo")
				.setCaption("ARTICOLO");
		grid.addColumn(UnimagData::getColore)
				.setId("colore")
				.setCaption("COLORE");
		grid.addColumn(UnimagData::getDescArticolo)
				.setId("desc")
				.setCaption("DESCRIZIONE");
		grid.addColumn(UnimagData::getQta)
				.setId("qta")
				.setCaption("QUANTITA'")
				.setRenderer(new NumberRenderer(df));
		grid.addColumn(UnimagData::getBagno)
				.setId("bagno")
				.setCaption("N.PEZZA");

	}

	@PostConstruct
	private void configura() {
		eventBus.subscribe(this);
		removeAllComponents();
		init();
		layout();
	}

	private void init() {
		df.applyPattern("##0.00");
		scelta = new ComboBox<>("Scegliere");
		scelta.setItems(ScelteAgenti.values());

		stagioni = GraffitiWebMainUI.getCurrent()
				.getStagioni();
		for (Busta busta : stagioni) {
			busta.setToStringCosa(true);
		}
		stagione = new ComboBox<>("Stagione");
		stagione.setItems(stagioni);

		cerca = new Button("Cerca");
		cerca.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		cerca.addClickListener(e -> {
			// Cliccato cerca
			loadData();
		});

		searchText = new TextField("Cerca descrizione...");
		searchText.addValueChangeListener(this::onNameChanged);

		grid = new Grid<>();
		grid.setSizeFull();

	}

	private void layout() {
		HorizontalLayout hl = new HorizontalLayout(scelta, stagione, cerca, searchText);
		hl.setComponentAlignment(cerca, Alignment.BOTTOM_CENTER);
		addComponents(hl, grid);
		scelta.setValue(ScelteAgenti.TUTTI);
	}

	private void loadData() {
		ScelteAgenti tipo = scelta.getValue();
		Busta stagio = stagione.getValue();
		dati = unimagService.findCampionario(tipo, stagio);
		bind();
	}

	@Override
	public void onEvent(org.vaadin.spring.events.Event<Object> event) {

	}

	private void onNameChanged(HasValue.ValueChangeEvent<String> event) {
		@SuppressWarnings("unchecked")
		ListDataProvider<UnimagData> dataProvider = (ListDataProvider<UnimagData>) grid.getDataProvider();
		dataProvider.setFilter(UnimagData::getDescArticolo, s -> startWith(s, event.getValue()));
	}

	private Boolean startWith(String s, String value) {
		if (s == null) {
			return false;
		}
		else {
			return s.toLowerCase()
					.startsWith(value.toLowerCase());
		}
	}

}
