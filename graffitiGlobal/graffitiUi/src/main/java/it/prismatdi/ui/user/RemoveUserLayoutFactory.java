package it.prismatdi.ui.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.GridSelectionModel;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.message.DataBaseMessages;
import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.services.ricerche.user.RicercheUsers;
import it.prismatdi.services.user.RemoveUserServiceImpl;
import it.prismatdi.ui.commons.GraffitiWebMainUI;

@SuppressWarnings("serial")
@SpringView(name = RemoveUserLayoutFactory.VIEW_NAME, ui = GraffitiWebMainUI.class)
public class RemoveUserLayoutFactory extends VerticalLayout implements View, ClickListener {
	public static final String		VIEW_NAME	= "Rimuovi user";
	private Grid<UserGraffitiWeb>	removeTable;
	private Button						removeButton;
	private List<UserGraffitiWeb>	users;
	@Autowired
	private RicercheUsers			userService;
	@Autowired
	private RemoveUserServiceImpl	removeService;

	private void addLayout() {
		setMargin(true);
		removeButton = new Button("Delete");
		removeButton.addClickListener(this);
		removeButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);

		removeTable = new Grid<>();
		removeTable.setItems(users);
		removeTable.addColumn(UserGraffitiWeb::getId).setCaption("ID");
		removeTable.addColumn(UserGraffitiWeb::getLoginName).setCaption("LOGIN");
		removeTable.addColumn(UserGraffitiWeb::getNome).setCaption("NOME");
		removeTable.addColumn(UserGraffitiWeb::getCognome).setCaption("COGNOME");
		removeTable.setSelectionMode(SelectionMode.MULTI);

		addComponent(removeTable);
		addComponent(removeButton);

	}

	@Override
	public void buttonClick(ClickEvent event) {
		GridSelectionModel<UserGraffitiWeb> model = removeTable.getSelectionModel();
		for (UserGraffitiWeb user : model.getSelectedItems()) {
			users.remove(user);
			removeService.removeUser(user);
		}
		Notification.show(DataBaseMessages.TITOLO_MESSAGGIO.getString(), DataBaseMessages.CANCELLAZIONE_OK.getString(),
				Type.WARNING_MESSAGE);
		removeTable.setItems(users);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (removeTable != null)
			return;
		loadUsers();
		addLayout();
	}

	private void loadUsers() {
		users = userService.loadAllUsers();

	}

}
