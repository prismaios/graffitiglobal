package it.prismatdi.ui.sintesi;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.FooterCell;
import com.vaadin.ui.components.grid.FooterRow;
import com.vaadin.ui.renderers.NumberRenderer;

import it.prismatdi.model.utils.DataConversion;

@SuppressWarnings("serial")
@SpringComponent
public class WindowShowMovimenti extends Window {

	private List<DataConversion>	listaRighe;
	private Grid<DataConversion>	gridElenco;
	private String						caption;
	private TextField					campo;
	private final DecimalFormat	df	= (DecimalFormat) NumberFormat.getInstance();

	public WindowShowMovimenti() {
		df.applyPattern("##0.00;-##0.00");
		center();
		setModal(true);
		setSizeUndefined();
		setWidth("1050px");
		setHeight("500px");
		VerticalLayout vl = new VerticalLayout();
		campo = new TextField();
		campo.setCaption("Movimenti realitivi a:");
		vl.addComponent(campo);
		gridElenco = new Grid<>();
		gridElenco.setSizeFull();
		vl.addComponent(gridElenco);
		setContent(vl);
	}

	public WindowShowMovimenti(String caption, List<DataConversion> listaRighe) {
		this();
		this.caption = caption;
		this.listaRighe = listaRighe;
		campo.setValue(caption);
		showRighe();
	}

	@Override
	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setListaRighe(List<DataConversion> listaRighe) {
		this.listaRighe = listaRighe;

	}

	private void showRighe() {
		gridElenco.removeAllColumns();
		gridElenco.setItems(listaRighe);
		gridElenco.addColumn(DataConversion::getStagione)
				.setId("stagione")
				.setCaption("STAGIONE");
		gridElenco.addColumn(DataConversion::getCausale)
				.setId("causale")
				.setCaption("CAUSALE");
		gridElenco.addColumn(DataConversion::getCodice)
				.setId("rif")
				.setCaption("RIF.");
		gridElenco.addColumn(DataConversion::getDescrizione)
				.setId("desc")
				.setCaption("DESCRIZIONE");
		gridElenco.addColumn(DataConversion::getDesColore)
				.setId("colore")
				.setCaption("DES.COLORE");
		gridElenco.addColumn(DataConversion::getUnitaM)
				.setId("unita")
				.setCaption("U.M.");
		gridElenco.addColumn(DataConversion::getQta)
				.setId("quantita")
				.setCaption("Q.TA")
				.setRenderer(new NumberRenderer(df));
		gridElenco.addColumn(DataConversion::getValuta)
				.setId("valuta")
				.setCaption("VAL.");
		gridElenco.addColumn(DataConversion::getPrezzo)
				.setId("prezzo")
				.setCaption("PREZZO €")
				.setRenderer(new NumberRenderer(df));
		gridElenco.addColumn(DataConversion::getValore)
				.setId("valore")
				.setCaption("VALORE €")
				.setRenderer(new NumberRenderer(df));
		gridElenco.addColumn(DataConversion::getMaggiorazioni)
				.setId("magg")
				.setCaption("MAGG.");
		// Add a footer row
		FooterRow footer = gridElenco.appendFooterRow();
		Supplier<Stream<DataConversion>> stream = () -> listaRighe.stream();
		long nr = stream.get()
				.count();
		double totQta = stream.get()
				.mapToDouble(DataConversion::getQta)
				.sum();
		double totMedio = stream.get()
				.mapToDouble(DataConversion::getPrezzo)
				.sum();

		double totVal = stream.get()
				.mapToDouble(DataConversion::getValore)
				.sum();
		totMedio /= nr;
		//		int nr = 0;
		//		for (DataConversion dc : listaRighe) {
		//			totQta += dc.getQta();
		//			totMedio += dc.getPrezzo();
		//			totVal += dc.getValore();
		//			++nr;
		//		}

		FooterCell footerCellQta = footer.getCell("quantita");
		footerCellQta.setHtml("<b>" + df.format(totQta) + "</b>");
		FooterCell footerCellMedio = footer.getCell("prezzo");
		footerCellMedio.setHtml("<b>" + df.format(totMedio) + "</b>");
		FooterCell footerCellVal = footer.getCell("valore");
		footerCellVal.setHtml("<b>" + df.format(totVal) + "</b>");
		FooterCell footerCellDes = footer.getCell("desc");
		footerCellDes.setHtml("TOTALI (n.rec: <b>" + nr + " </b>)");

	}

}
