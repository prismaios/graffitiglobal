package it.prismatdi.ui.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.model.user.UserGraffitiWeb;

@SpringComponent
public class AddUserRuoloLayoutFactory {
	@SuppressWarnings("serial")
	private class AddUserRuoloLayout extends VerticalLayout {
		@Autowired
		private UserGraffitiWeb	user;
		private Label				label;

		public AddUserRuoloLayout bind() {

			return this;
		}

		public AddUserRuoloLayout init() {
			if (user != null) {
				label = new Label(user.getNome());
			}
			else {
				label = new Label("Tentativo di autowire lo user");
			}
			return this;
		}

		public AddUserRuoloLayout layout() {
			addComponent(label);
			return this;
		}

	}

	public Component createComponent() {
		return new AddUserRuoloLayout().init().bind().layout();
	}

}
