package it.prismatdi.ui.privilegi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;

import com.vaadin.data.Binder;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.errors.BinderError;
import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.general.Ruoli;
import it.prismatdi.menu.MenuUtils;
import it.prismatdi.message.DataBaseMessages;
import it.prismatdi.model.user.Privilegi;
import it.prismatdi.privilegi.PrivilegiStrings;
import it.prismatdi.privilegi.PrivilegioInputValidator;
import it.prismatdi.services.privilegi.PrivilegiService;
import it.prismatdi.user.UserStringUtil;

@SuppressWarnings("serial")
@SpringComponent
public class AddPrivilegiLayout extends VerticalLayout implements ClickListener {

	@Autowired
	private PrivilegiService					services;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	private Binder<Privilegi>					binder;
	private TextField								descField;
	private ComboBox<String>					moduloField;
	private ComboBox<Ruoli>						comboRuoli;
	private Button									saveButton;
	private Button									clearButton;
	private Privilegi								privilegio;

	public AddPrivilegiLayout() {
		// TODO Auto-generated constructor stub
	}

	private void bind() {
		binder.forField(descField)
				.withValidator(new StringLengthValidator(PrivilegioInputValidator.DESCRIZIONE.getString(), 1, 256))
				.bind(Privilegi::getDescrizione, Privilegi::setDescrizione);
		binder.forField(moduloField)
				.withValidator(new StringLengthValidator(PrivilegioInputValidator.MODULO.getString(), 1, 256))
				.bind(Privilegi::getModulo, Privilegi::setModulo);
		binder.forField(comboRuoli)
				.bind(Privilegi::getRuolo, Privilegi::setRuolo);
		binder.readBean(privilegio);

	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (event
				.getSource()
				.equals(saveButton)) {
			savePrivilegio();
		}
		if (event
				.getSource()
				.equals(clearButton)) {
			clearPrivilegio();
		}

	}

	private void clearPrivilegio() {
		privilegio = new Privilegi();
		descField.setValue("");

	}

	@PostConstruct
	public void configura() {
		init();
		bind();
		layout();

	}

	private void init() {
		List<String> views = new ArrayList<>();
		MenuUtils[] menus = MenuUtils.values();
		for (MenuUtils menuUtils : menus) {
			views.add(menuUtils.getViewName());
		}

		binder = new Binder<>();
		privilegio = new Privilegi();
		descField = new TextField(PrivilegiStrings.DESCRIZIONE
				.getString());
		moduloField = new ComboBox<>(PrivilegiStrings.MODULO.getString(), views);
		comboRuoli = new ComboBox<>(PrivilegiStrings.COD_RUOLO
				.getString(),
				Arrays
						.asList(Ruoli
								.values()));
		comboRuoli
				.setValue(Ruoli.ALL);
		saveButton = new Button(UserStringUtil.SAVE_BUTTON
				.getString());
		saveButton
				.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		saveButton
				.addClickListener(this);
		clearButton = new Button(UserStringUtil.CLEAR_BUTTON
				.getString());
		clearButton
				.setStyleName(ValoTheme.BUTTON_PRIMARY);
		clearButton
				.addClickListener(this);

	}

	private void layout() {
		setWidth("100%");
		setMargin(true);
		GridLayout gridLayout = new GridLayout(2, 4);
		gridLayout
				.setSizeUndefined();
		gridLayout
				.setSpacing(true);
		gridLayout
				.addComponent(descField, 0, 0, 1, 0);
		gridLayout
				.addComponent(moduloField, 0, 1, 1, 1);
		gridLayout
				.addComponent(comboRuoli, 0, 2);
		gridLayout
				.addComponent(new HorizontalLayout(saveButton, clearButton), 0, 3);
		addComponent(gridLayout);

	}

	private void savePrivilegio() {
		boolean beanOk = binder.writeBeanIfValid(privilegio);
		if (beanOk) {
			services.savePrivilegio(privilegio);
			eventBus.publish(TypeForEventBus.REFRESH_PRIVILEGI_TABLE.toString(), AddPrivilegiLayout.this, privilegio);
		}
		else {
			Notification.show(BinderError.TITOLO.getString(), BinderError.DESCRIZIONE.getString(), Type.ERROR_MESSAGE);
			return;
		}
		clearPrivilegio();
		Notification.show(DataBaseMessages.TITOLO_MESSAGGIO.getString(), DataBaseMessages.DESCRIZIONE_MESSAGGIO.getString(),
				Type.WARNING_MESSAGE);

	}

	public void setPrivilegio(Privilegi privilegio) {
		this.privilegio = privilegio;
		binder.readBean(privilegio);
		saveButton.setCaption("Salva");
	}

}
