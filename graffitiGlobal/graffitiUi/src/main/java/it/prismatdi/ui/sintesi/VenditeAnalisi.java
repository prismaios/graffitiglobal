package it.prismatdi.ui.sintesi;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.FooterRow;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.model.utils.DataConversion;
import it.prismatdi.model.utils.ResultContainer;
import it.prismatdi.model.utils.SumDataConversion;
import it.prismatdi.services.produzio.TeordiService;
import it.prismatdi.services.produzio.TmovrigService;
import it.prismatdi.ui.commons.WindowParametriRicerca;

@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class VenditeAnalisi extends VerticalLayout
		implements EventBusListener<Object>, SelectionListener<SumDataConversion> {
	@Autowired
	private WindowParametriRicerca			parametriRicerca;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	@Autowired
	protected TeordiService						teordiService;

	@Autowired
	private TmovrigService						tmovrigService;

	private Grid<SumDataConversion>			venditeGrid;
	private final Boolean						caricaDati	= true;
	private Button									showFiltri;
	private TextField								searchText;
	private ResultContainer						listaRighe;
	private final DecimalFormat				df				= (DecimalFormat) NumberFormat.getInstance();
	private final DecimalFormat				dfR			= (DecimalFormat) NumberFormat.getInstance();

	public VenditeAnalisi() {
		// TODO Auto-generated constructor stub
	}

	private void bind() {
		// TODO Auto-generated method stub

	}

	private void cercaMovimenti(String codice, String descrizione) {
		List<DataConversion> lista = null;
		if (listaRighe.getParametri()
				.getCliente()) {
			lista = tmovrigService.findMovimentiDiCliente(listaRighe.getParametri(), codice);
			//				System.err.println(">>>>> AnalisiVenditeFactory.AnalisiVendite-cercaMovimenti CLIENTE <<<<<< " + lista.size());

		}
		else {
			lista = tmovrigService.findMovimentiDiArticolo(listaRighe.getParametri(), codice);
			//				System.err.println(">>>>> AnalisiVenditeFactory.AnalisiVendite-cercaMovimenti ARTICOLO <<<<<< " + lista.size());
		}
		if (lista != null) {
			WindowShowMovimenti wsm = new WindowShowMovimenti(descrizione, lista);
			UI.getCurrent()
					.addWindow(wsm);
		}
	}

	@PostConstruct
	public void configura() {
		eventBus.subscribe(this);
		init();
		layout();
		loadData();
		bind();

	}

	private void faiAnnoCorrente(Boolean clie) {
		venditeGrid.removeAllColumns();
		if (venditeGrid.getFooterRowCount() > 0) {
			venditeGrid.removeFooterRow(0);
		}
		venditeGrid.setItems(listaRighe.getDati());
		venditeGrid.addColumn(SumDataConversion::getCodice)
				.setId("codice")
				.setCaption(clie ? "COD.CLI" : "COD.ARTI");
		venditeGrid.addColumn(SumDataConversion::getDescrizione)
				.setId("desc")
				.setCaption(clie ? "RAG.SOCIALE" : "DESC.ARTICOLO");
		venditeGrid.addColumn(data -> data.getValore())
				.setId("valore")
				.setCaption("VALORE");

		venditeGrid.addColumn(SumDataConversion::getQta)
				.setId("qta")
				.setCaption("QUANTITA\'");
		venditeGrid.addColumn(data -> DataConversion.arrotonda(data.getValore() / data.getQta(), 2))
				.setId("media")
				.setCaption("P.MEDIO")
				.setRenderer(new NumberRenderer(df));
		venditeGrid
				.addColumn(
						data -> DataConversion.arrotonda(data.getValore() * 100 / listaRighe.getTotValore(), 2))
				.setId("percSuTot")
				.setCaption("% su TOT")
				.setRenderer(new NumberRenderer(df));
		FooterRow footer = venditeGrid.appendFooterRow();
		faiTotali(footer);

		venditeGrid.getDataProvider()
				.refreshAll();
		venditeGrid.getDataProvider()
				.addDataProviderListener(e -> {
					faiTotali(footer);
				});

	}

	private void faiAnnoPrecedente(Boolean clie) {
		venditeGrid.removeAllColumns();
		if (venditeGrid.getFooterRowCount() > 0) {
			venditeGrid.removeFooterRow(0);
		}
		venditeGrid.setItems(listaRighe.getDati());
		venditeGrid.addColumn(SumDataConversion::getCodice)
				.setId("codice")
				.setCaption(clie ? "COD.CLI" : "COD.ARTI");
		venditeGrid.addColumn(SumDataConversion::getDescrizione)
				.setId("desc")
				.setCaption(clie ? "RAG.SOCIALE" : "DESC.ARTICOLO");
		venditeGrid.addColumn(data -> df.format(data.getValore()) + "<br>" + df.format(data.getValoreAp()))
				.setId("valore")
				.setCaption("VALORE A.C./A.P.")
				.setRenderer(new HtmlRenderer());
		venditeGrid.addColumn(data -> formatRapporto(data))
				.setId("diff")
				.setCaption("DIFF.")
				.setRenderer(new HtmlRenderer());
		venditeGrid.addColumn(data -> df.format(data.getQta()) + "<br>" + df.format(data.getQtaAp()))
				.setId("qta")
				.setCaption("QUANTITA\'")
				.setRenderer(new HtmlRenderer());
		venditeGrid
				.addColumn(data -> df.format(DataConversion.arrotonda(data.getValore() / data.getQta(), 4))
						+ "<br>" + df.format(DataConversion.arrotonda(data.getValoreAp() / data.getQtaAp(), 2)))
				.setId("media")
				.setCaption("P.MEDIO")
				.setRenderer(new HtmlRenderer());
		venditeGrid
				.addColumn(data -> df
						.format(DataConversion.arrotonda(data.getValore() * 100 / listaRighe.getTotValore(), 2))
						+ "<br>"
						+ df.format(
								DataConversion.arrotonda(data.getValoreAp() * 100 / listaRighe.getTotValoreAp(), 2)))
				.setId("percSuTot")
				.setCaption("% su TOT")
				.setRenderer(new HtmlRenderer());
		FooterRow footer = venditeGrid.appendFooterRow();
		faiTotaliDueAnni(footer);

		venditeGrid.getDataProvider()
				.refreshAll();
		venditeGrid.getDataProvider()
				.addDataProviderListener(e -> {
					faiTotaliDueAnni(footer);
				});
		venditeGrid.setBodyRowHeight(75);
		venditeGrid.setHeaderRowHeight(40);
		venditeGrid.setFooterRowHeight(75);
	}

	private void faiMargineLordo(Boolean clie) {
		venditeGrid.removeAllColumns();
		if (venditeGrid.getFooterRowCount() > 0) {
			venditeGrid.removeFooterRow(0);
		}
		venditeGrid.setItems(listaRighe.getDati());
		venditeGrid.addColumn(SumDataConversion::getCodice)
				.setId("codice")
				.setCaption(clie ? "COD.CLI" : "COD.ARTI");
		venditeGrid.addColumn(SumDataConversion::getDescrizione)
				.setId("desc")
				.setCaption(clie ? "RAG.SOCIALE" : "DESC.ARTICOLO");
		venditeGrid.addColumn(
				data -> DataConversion.arrotonda(data.getValore() - data.getCosto() - data.getCostoProd(), 2))
				.setId("costo")
				.setCaption("MARG.LORDO");
		venditeGrid.addColumn(data -> data.getValore())
				.setId("valore")
				.setCaption("VALORE");

		venditeGrid.addColumn(SumDataConversion::getQta)
				.setId("qta")
				.setCaption("QUANTITA\'");
		venditeGrid
				.addColumn(data -> DataConversion.arrotonda(
						(data.getValore() - data.getCosto() - data.getCostoProd()) * 100 / data.getValore(), 2))
				.setId("media")
				.setCaption("% su MARGINE")
				.setRenderer(new NumberRenderer(df));
		venditeGrid
				.addColumn(
						data -> DataConversion.arrotonda(data.getValore() * 100 / listaRighe.getTotValore(), 2))
				.setId("percSuTot")
				.setCaption("% su TOT")
				.setRenderer(new NumberRenderer(df));
		FooterRow footer = venditeGrid.appendFooterRow();
		faiTotali(footer);

		venditeGrid.getDataProvider()
				.refreshAll();
		venditeGrid.getDataProvider()
				.addDataProviderListener(e -> {
					faiTotali(footer);
				});

	}

	private void faiTotali(FooterRow footer) {
		Supplier<Stream<SumDataConversion>> supplier = () -> venditeGrid.getDataProvider()
				.fetch(new Query<>());
		// CON STREAM
		long nRec = supplier.get()
				.count();
		double totVal = supplier.get()
				.mapToDouble(SumDataConversion::getValore)
				.sum();
		double totQta = supplier.get()
				.mapToDouble(SumDataConversion::getQta)
				.sum();
		double totMedio = supplier.get()
				.mapToDouble(s -> DataConversion.arrotonda(s.getValore() / s.getQta(), 4))
				.sum();
		double totPerc = supplier.get()
				.mapToDouble(s -> DataConversion.arrotonda(s.getValore() * 100 / listaRighe.getTotValore(), 8))
				.sum();
		totMedio /= nRec;

		footer.getCell("desc")
				.setHtml("REC.TOTALI: <b>" + nRec + "</b>");
		footer.getCell("valore")
				.setHtml("<b>" + df.format(totVal) + "</b>");
		footer.getCell("qta")
				.setHtml("<b>" + df.format(totQta) + "</b>");
		footer.getCell("media")
				.setHtml("<b>" + df.format(totMedio) + "</b>");
		footer.getCell("percSuTot")
				.setHtml("<b>" + df.format(totPerc) + "</b>");

	}

	private void faiTotaliDueAnni(FooterRow footer) {
		Supplier<Stream<SumDataConversion>> supplier = () -> venditeGrid.getDataProvider()
				.fetch(new Query<>());
		// CON STREAM
		long nRec = supplier.get()
				.count();
		double totVal = supplier.get()
				.mapToDouble(SumDataConversion::getValore)
				.sum();
		double totValAp = supplier.get()
				.mapToDouble(SumDataConversion::getValoreAp)
				.sum();
		double totQta = supplier.get()
				.mapToDouble(SumDataConversion::getQta)
				.sum();
		double totQtaAp = supplier.get()
				.mapToDouble(SumDataConversion::getQtaAp)
				.sum();
		double totMedio = supplier.get()
				.mapToDouble(s -> DataConversion.arrotonda(s.getValore() / s.getQta(), 4))
				.sum();
		double totMedioAp = supplier.get()
				.mapToDouble(s -> DataConversion.arrotonda(s.getValoreAp() / s.getQtaAp(), 4))
				.sum();
		double totPerc = supplier.get()
				.mapToDouble(s -> DataConversion.arrotonda(s.getValore() * 100 / listaRighe.getTotValore(), 8))
				.sum();
		double totPercAp = supplier.get()
				.mapToDouble(
						s -> DataConversion.arrotonda(s.getValoreAp() * 100 / listaRighe.getTotValoreAp(), 8))
				.sum();
		totMedio /= nRec;
		totMedioAp /= nRec;
		double diff = (totVal - totValAp) / totValAp * 100;
		String s = dfR.format(diff);
		String h = "";
		if (diff < 0) {
			h = "<font color=\"red\"><b>" + s;
		}
		else {
			h = "<b>" + s;
		}
		footer.getCell("desc")
				.setHtml("REC.TOTALI: <b>" + nRec + "</b>");
		footer.getCell("valore")
				.setHtml("<b>" + df.format(totVal) + "<br>" + df.format(totValAp) + "</b>");
		footer.getCell("diff")
				.setHtml(h);
		footer.getCell("qta")
				.setHtml("<b>" + df.format(totQta) + "<br>" + df.format(totQtaAp) + "</b>");
		footer.getCell("media")
				.setHtml("<b>" + df.format(totMedio) + "<br>" + df.format(totMedioAp) + "</b>");
		footer.getCell("percSuTot")
				.setHtml("<b>" + df.format(totPerc) + "<br>" + df.format(totPercAp) + "</b>");

	}

	private String formatRapporto(SumDataConversion data) {
		Double d = (data.getValore() - data.getValoreAp()) / data.getValoreAp() * 100;
		String s = dfR.format(d);
		String h = "";
		if (d < 0) {
			h = "<font color=\"red\"><b>" + s;
		}
		else {
			h = "<b>" + s;
		}

		return h;
	}

	public void init() {
		df.applyPattern("##0.00");
		dfR.applyPattern("+#.00;-#.00");
		//			System.err.println(">>>>> AnalisiVenditeFactory.AnalisiVendite-init <<<<<< ");
		showFiltri = new Button("Filtri");
		showFiltri.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		showFiltri.addClickListener(e -> {
			UI.getCurrent()
					.addWindow(parametriRicerca);
		});
		searchText = new TextField("Cerca nella table ...");
		searchText.addValueChangeListener(this::onNameChanged);
		venditeGrid = new Grid<>();
		venditeGrid.setWidth("100%");
		venditeGrid.setSelectionMode(SelectionMode.SINGLE);
		venditeGrid.addSelectionListener(this);
	}

	public void layout() {
		final HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(showFiltri);
		hl.setComponentAlignment(showFiltri, Alignment.BOTTOM_CENTER);
		hl.addComponent(searchText);
		hl.setComponentAlignment(searchText, Alignment.MIDDLE_LEFT);
		addComponent(hl);
		// Grid
		addComponent(venditeGrid);
	}

	public void loadData() {
		if (caricaDati) {

		}
	}

	@Override
	public void onEvent(org.vaadin.spring.events.Event<Object> event) {
		System.err.println(">>>>> VenditeAnalisi-onEvent <<<<<< " + event.getTopic());
		if (event.getTopic()
				.equals(TypeForEventBus.ARRIVATE_RIGHE_PER_ANALISI_VENDITE.toString())) {
			listaRighe = (ResultContainer) event.getPayload();
			UI.getCurrent()
					.removeWindow(parametriRicerca);
			showRighe();
		}
		if (event.getTopic()
				.equals(TypeForEventBus.CHIUDI_WINDOW.toString())) {

		}

	}

	private void onNameChanged(HasValue.ValueChangeEvent<String> event) {
		@SuppressWarnings("unchecked")
		ListDataProvider<SumDataConversion> dataProvider = (ListDataProvider<SumDataConversion>) venditeGrid
				.getDataProvider();
		dataProvider.setFilter(SumDataConversion::getDescrizione, s -> startWith(s, event.getValue()));
	}

	@Override
	public void selectionChange(SelectionEvent<SumDataConversion> event) {
		Optional<SumDataConversion> dat = event.getFirstSelectedItem();
		if (dat.isPresent()) {
			SumDataConversion conversion = dat.get();
			cercaMovimenti(conversion.getCodice(), conversion.getDescrizione());
		}

	}

	private void showRighe() {
		Boolean clie = listaRighe.getParametri()
				.getCliente();
		Boolean ap = listaRighe.getParametri()
				.getAnnoPrecedente();
		Boolean margine = listaRighe.getParametri()
				.getMargineLordo();

		if (!ap) {
			if (!margine) {
				faiAnnoCorrente(clie);
			}
			else {
				faiMargineLordo(clie);
			}
		}
		else {
			faiAnnoPrecedente(clie);
		}

	}

	private Boolean startWith(String s, String value) {
		if (s == null) {
			return false;
		}
		else {
			return s.toLowerCase()
					.startsWith(value.toLowerCase());
		}
	}

}
