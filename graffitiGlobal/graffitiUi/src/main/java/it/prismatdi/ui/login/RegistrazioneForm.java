package it.prismatdi.ui.login;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.general.Nazioni;
import it.prismatdi.general.Ruoli;
import it.prismatdi.message.RegistrazioneFormErrors;
import it.prismatdi.model.user.Privilegi;
import it.prismatdi.model.user.RuoliWebUser;
import it.prismatdi.model.user.RuoliWebUser.Lingue;
import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.services.security.RegisterUserService;

@SuppressWarnings("serial")
@SpringComponent
@Scope("prototype")
public class RegistrazioneForm extends VerticalLayout {

	@Autowired
	private RegisterUserService	registerService;

	private Panel						panel;
	private TextField					loginNameField;
	private PasswordField			passwordField;
	private PasswordField			repeatpassword;
	private TextField					nomeField;
	private TextField					cognomeField;
	private TextField					emailField;
	private ComboBox<Lingue>		lingueCombo;
	private ComboBox<Nazioni>		nazioneCombo;
	private Button						save;

	public RegistrazioneForm() {

	}

	@PostConstruct
	public void configura() {
		init();
		layout();
	}

	private void init() {
		setMargin(true);
		setHeight("100%");
		panel = new Panel("REGISTRAZIONE");
		panel.setSizeUndefined();

		loginNameField = new TextField("Nome per login");
		passwordField = new PasswordField("Password");
		repeatpassword = new PasswordField("Ripeti password");
		nomeField = new TextField("Nome");
		nomeField.setWidth("300px");
		cognomeField = new TextField("Cognome");
		cognomeField.setWidth("300px");
		emailField = new TextField("Email");
		emailField.setWidth("300px");
		lingueCombo = new ComboBox<>("Lingua", Arrays.asList(Lingue.values()));
		lingueCombo.setValue(Lingue.ITALIANO);
		nazioneCombo = new ComboBox<>("Nazione", Arrays.asList(Nazioni.values()));
		nazioneCombo.setValue(Nazioni.ITALIA);
		save = new Button("Save");
		save.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		save.addClickListener(ev -> {
			if (!passwordField.getValue()
					.equals(repeatpassword.getValue())) {
				Notification.show(RegistrazioneFormErrors.TITOLO.getString(),
						RegistrazioneFormErrors.ERRORE_PASSWORD.getString(), Type.ERROR_MESSAGE);
				return;
			}
			if (emailField.getValue()
					.isEmpty()) {
				Notification.show(RegistrazioneFormErrors.TITOLO.getString(),
						RegistrazioneFormErrors.ERRORE_MAIL.getString(), Type.ERROR_MESSAGE);
				return;
			}
			if (loginNameField.getValue()
					.isEmpty()) {
				Notification.show(RegistrazioneFormErrors.TITOLO.getString(),
						RegistrazioneFormErrors.ERRORE_LOGIN_NAME.getString(), Type.ERROR_MESSAGE);
				return;
			}
			UserGraffitiWeb user = new UserGraffitiWeb(loginNameField.getValue(), passwordField.getValue());
			user.setNome(nomeField.getValue());
			user.setCognome(cognomeField.getValue());
			RuoliWebUser ruolo = new RuoliWebUser();
			user.getRuoli()
					.add(ruolo);
			ruolo.setUser(user);
			ruolo.seteMail(emailField.getValue());
			Privilegi privilegio = new Privilegi();
			privilegio.getRuoli()
					.add(ruolo);
			ruolo.getPrivilegi()
					.add(privilegio);
			privilegio.setRuolo(Ruoli.USER);
			privilegio.setDescrizione("Ruolo normale");

			try {
				registerService.save(user);
			}
			catch (Exception e) {
				e.printStackTrace();
				if (e instanceof DataIntegrityViolationException) {
					Notification.show(RegistrazioneFormErrors.TITOLO.getString(),
							RegistrazioneFormErrors.ERRORE_LOGIN_NAME_DUPLICATO.getString(), Type.ERROR_MESSAGE);
					return;
				}
				e.printStackTrace();
			}
			UI.getCurrent()
					.getPage()
					.setLocation("/graffitiWeb/login");
		});

	}

	private void layout() {
		addComponent(panel);
		setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
		FormLayout formLayout = new FormLayout();
		formLayout.addComponent(loginNameField);
		formLayout.addComponent(passwordField);
		formLayout.addComponent(repeatpassword);
		formLayout.addComponent(nomeField);
		formLayout.addComponent(cognomeField);
		formLayout.addComponent(emailField);
		formLayout.addComponent(lingueCombo);
		formLayout.addComponent(nazioneCombo);
		formLayout.addComponent(save);
		formLayout.setSizeUndefined();
		formLayout.setMargin(true);
		panel.setContent(formLayout);

	}

}
