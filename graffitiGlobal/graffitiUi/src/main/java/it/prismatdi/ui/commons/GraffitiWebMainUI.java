package it.prismatdi.ui.commons;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.model.utils.Busta;
import it.prismatdi.services.scribaf.ScribafService;
import it.prismatdi.services.security.CustomUserDetails;

@SpringUI(path = GraffitiWebMainUI.NAME)
@Scope("prototype")
@Title("Graffiti WEB")
@Theme("mytheme")
@SuppressWarnings("serial")
@VaadinServletConfiguration(heartbeatInterval = 1, productionMode = false, closeIdleSessions = true,
		ui = GraffitiWebMainUI.class)
public class GraffitiWebMainUI extends UI implements EventBusListener<Object> {
	public static final String NAME = "/ui";

	public static GraffitiWebMainUI getCurrent() {
		return (GraffitiWebMainUI) UI.getCurrent();
	}

	@Autowired
	private ViewDisplay							changePanel;
	@Autowired
	private GraffitiMenu							menu;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	@Autowired
	protected ScribafService					scribafService;

	@Autowired
	protected Environment						env;

	@Autowired
	protected Navigator							navigator;

	private List<Busta>							clienti;

	private List<Busta>							rappresentanti;

	private List<Busta>							stagioni;

	private CustomUserDetails					cud;

	@PostConstruct
	public void afterPropertiesSet() {
		eventBus.subscribe(this, false);
		// dati
		final String contoClienti = env.getProperty("CONTO_CLIENTI");
		final String contoRappresentanti = env.getProperty("CONTO_RAPPRESENTANTI");
		clienti = scribafService.findAllClienti(contoClienti);
		//		eventBus.publish(TypeForEventBus.SHOW_CLIENTI.toString(), this, clienti);
		rappresentanti = scribafService.findAllRappresentanti(contoRappresentanti);
		//		eventBus.publish(TypeForEventBus.SHOW_RAPPRESENTANTI.toString(), this, clienti);
		stagioni = scribafService.getAllStagioni();
		//		eventBus.publish(TypeForEventBus.SHOW_STAGIONI.toString(), this, stagioni);
		cud = (CustomUserDetails) SecurityContextHolder.getContext()
				.getAuthentication()
				.getPrincipal();
		System.err.println(">>>>> GraffitiWebMainUI-afterPropertiesSet <<<<<< " + stagioni.size());

	}

	@Override
	public void attach() {
		super.attach();

	}

	@PreDestroy
	void destroy() {
		System.err.println(">>>>> GraffitiWebMainUI-destroy <<<<<< ");
		eventBus.unsubscribe(this); // It's good manners to do this, even though we should be automatically unsubscribed when the UI is garbage collected
	}

	public List<Busta> getClienti() {
		return clienti;
	}

	public List<Busta> getRappresentanti() {
		return rappresentanti;
	}

	public List<Busta> getStagioni() {
		return stagioni;
	}

	@Override
	protected void init(VaadinRequest request) {
		changePanel.setHeight("100%");
		final VerticalLayout rootLayout = new VerticalLayout();
		rootLayout.setSizeFull();

		final Panel contentPanel = new Panel();
		contentPanel.setWidth("100%");
		contentPanel.setHeight("100%");

		final HorizontalLayout uiLayout = new HorizontalLayout();
		uiLayout.setSizeFull();

		uiLayout.addComponent(changePanel);
		uiLayout.setComponentAlignment(changePanel, Alignment.TOP_CENTER);

		uiLayout.setExpandRatio(changePanel, 5);
		contentPanel.setContent(uiLayout);
		//		System.err.println(">>>>> GraffitiWebMainUI-init <<<<<< PRIMA DI MENU");
		rootLayout.addComponent(menu);
		rootLayout.addComponent(contentPanel);
		rootLayout.setComponentAlignment(contentPanel, Alignment.MIDDLE_CENTER);
		rootLayout.setExpandRatio(contentPanel, 1);

		setContent(rootLayout);
		navigator.navigateTo(cud.getDefaultView());
	}

	@Override
	public void onEvent(org.vaadin.spring.events.Event<Object> event) {
		// TODO Auto-generated method stub

	}

	public void setClienti(List<Busta> clienti) {
		this.clienti = clienti;
	}

	public void setRappresentanti(List<Busta> rappresentanti) {
		this.rappresentanti = rappresentanti;
	}

}
