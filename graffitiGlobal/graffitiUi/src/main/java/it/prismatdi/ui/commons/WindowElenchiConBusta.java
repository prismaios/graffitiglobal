package it.prismatdi.ui.commons;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.general.ChiamateDa;
import it.prismatdi.model.utils.Busta;

@SuppressWarnings("serial")
@SpringComponent
@Scope("prototype")
public class WindowElenchiConBusta extends Window implements EventBusListener<Object> {
	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	private TextField								searchText;
	private List<Busta>							listaRighe;
	private final Grid<Busta>					gridElenco;
	private ChiamateDa							daDove;

	protected WindowElenchiConBusta() {

		center();
		setModal(true);
		setSizeUndefined();
		setWidth("400px");
		setHeight("450px");
		VerticalLayout vl = new VerticalLayout();
		searchText = new TextField("Cerca nella tabella....");

		vl.addComponent(searchText);

		gridElenco = new Grid<>();
		gridElenco.setSelectionMode(SelectionMode.MULTI);
		listaRighe = new ArrayList<>();
		gridElenco.setSizeFull();
		gridElenco.addSelectionListener(e -> {
			selezionato();
		});
		vl.addComponent(gridElenco);
		//		gridElenco.setStyleGenerator(Styl);
		setContent(vl);

	}

	@Override
	public void attach() {
		super.attach();
		searchText.addValueChangeListener(this::onNameChanged);
	}

	public ChiamateDa getDaDove() {
		return daDove;
	}

	public Set<Busta> getSelezioni() {
		return gridElenco.getSelectedItems();
	}

	@PostConstruct
	public void initBus() {
		System.err.println("INIT BUS");
		eventBus.subscribe(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(org.vaadin.spring.events.Event<Object> event) {
		//		System.err.println(">>>>> WindowElenchiConBusta-onEvent <<<<<< " + event.getTopic());
		listaRighe = new ArrayList<>();
		if (event.getTopic()
				.equals(TypeForEventBus.SHOW_CLIENTI.toString())) {
			listaRighe = (List<Busta>) event.getPayload();
			setCaption("CLIENTI");
		}
		if (event.getTopic()
				.equals(TypeForEventBus.SHOW_RAPPRESENTANTI.toString())) {
			listaRighe = (List<Busta>) event.getPayload();
			setCaption("RAPPRESENTANTI");
		}
		if (event.getTopic()
				.equals(TypeForEventBus.SHOW_STAGIONI.toString())) {
			listaRighe = (List<Busta>) event.getPayload();
			setCaption("STAGIONI");
		}

		gridElenco.removeAllColumns();
		if (listaRighe.size() > 0) {
			gridElenco.setItems(listaRighe);
			gridElenco.addColumn(Busta::getDescrizione)
					.setCaption("Descrizione");
			gridElenco.getDataProvider()
					.refreshAll();
		}

	}

	//	{
	//		final Set<Busta> selezioni = gridElenco.getSelectedItems();
	//		eventBus.publish(TypeForEventBus.SELEZIONATO.toString(), this, selezioni);
	//		return super.addCloseListener(listener);
	//	}

	private void onNameChanged(HasValue.ValueChangeEvent<String> event) {
		@SuppressWarnings("unchecked")
		ListDataProvider<Busta> dataProvider = (ListDataProvider<Busta>) gridElenco.getDataProvider();
		dataProvider.setFilter(Busta::getDescrizione, s -> startWith(s, event.getValue()));
	}

	private void selezionato() {
		// TODO Auto-generated method stub

	}

	public void setDaDove(ChiamateDa daDove) {
		this.daDove = daDove;
	}

	private Boolean startWith(String s, String value) {
		return s.toLowerCase()
				.startsWith(value.toLowerCase());
	}

}
