package it.prismatdi.ui.privilegi;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.privilegi.PrivilegiStrings;
import it.prismatdi.ui.commons.GraffitiWebMainUI;

@SuppressWarnings("serial")
@SpringView(name = PrivilegiLayout.VIEW_NAME, ui = GraffitiWebMainUI.class)
public class PrivilegiLayout extends VerticalLayout implements View {
	public static final String	VIEW_NAME	= "Aggiungi privilegio";

	@Autowired
	private AddPrivilegiLayout	addPrivilegiLayout;

	@Autowired
	private ShowAllPrivilegi	showPrivilegi;

	private TabSheet				sheet;

	private void addLayout() {
		setMargin(true);
		sheet = new TabSheet();
		Component addPrivilegioTab = addPrivilegiLayout;
		Component showAllPrivilegi = showPrivilegi;
		sheet.addTab(showAllPrivilegi, PrivilegiStrings.SHOW_PRIVILEGI.getString());
		sheet.addTab(addPrivilegioTab, PrivilegiStrings.ADD_PRIVILEGIO.getString());
		addComponent(sheet);

	}

	@Override
	public void enter(ViewChangeEvent event) {
		removeAllComponents();
		addLayout();
	}

}
