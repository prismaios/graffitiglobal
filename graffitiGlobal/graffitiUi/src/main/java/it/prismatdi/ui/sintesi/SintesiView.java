package it.prismatdi.ui.sintesi;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.board.Board;
import com.vaadin.board.Row;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.ui.commons.GraffitiWebMainUI;

@SuppressWarnings("serial")
@SpringView(name = SintesiView.VIEW_NAME, ui = GraffitiWebMainUI.class)
@UIScope
public class SintesiView extends VerticalLayout implements View {
	public static final String	VIEW_NAME	= "sintesi";

	@Autowired
	private VenditeAnalisi		analisiVendite;
	protected Board				board;

	public SintesiView() {
		super();
	}

	@PostConstruct
	private void configura() {
		removeAllComponents();
		init();
		layout();
	}

	private void init() {
		setResponsive(true);
		board = new Board();

	}

	private void layout() {
		Row row = board.addRow(analisiVendite);
		addComponent(board);
	}

}
