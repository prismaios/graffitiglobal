package it.prismatdi.ui.privilegi;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.model.user.Privilegi;
import it.prismatdi.services.ricerche.privilegi.RicerchePrivilegi;

@SuppressWarnings("serial")
@SpringComponent
public class ShowAllPrivilegi extends VerticalLayout implements EventBusListener<Object> {

	@Autowired
	private RicerchePrivilegi					ricerche;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	@Autowired
	private AddPrivilegiLayout					addPrivilegiLayout;

	//	private SliderPanel							sliderPanel;
	private Component								comp;
	private List<Privilegi>						privs;
	private Grid<Privilegi>						privsTable;

	public ShowAllPrivilegi() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void configura() {
		loadData();
		initEventBus();
		init();
		layout();
	}

	private void init() {
		privsTable = new Grid<>();
		privsTable.setItems(privs);
		privsTable.addColumn(Privilegi::getId)
				.setCaption("ID");
		privsTable.addColumn(Privilegi::getDescrizione)
				.setCaption("DESCRIZIONE");
		privsTable.addColumn(Privilegi::getModulo)
				.setCaption("MODULO");
		privsTable.addColumn(Privilegi::getRuolo)
				.setCaption("RUOLO");
		comp = addPrivilegiLayout;
		//		sliderPanel = new SliderPanelBuilder(comp)
		//				.expanded(false)
		//				.mode(SliderMode.TOP)
		//				.caption("Modifica")
		//				.tabSize(10)
		//				.tabPosition(SliderTabPosition.BEGINNING)
		//				.style("my-sliderpanel")
		//				.build();
		privsTable.addSelectionListener(e -> {
			Optional<Privilegi> privilegio = e.getFirstSelectedItem();

			if (privilegio.isPresent()) {
				((AddPrivilegiLayout) comp).setPrivilegio(privilegio.get());
				//				sliderPanel.expand();
				//				System.err.println(">>>>> ShowAllPrivilegiLayoutFactory.ShowAllPrivilegi-init <<<<<< " + privilegio.get());
			}
		});

	}

	private void initEventBus() {
		eventBus.subscribe(this);

	}

	private void layout() {
		//		addComponent(sliderPanel);
		addComponent(privsTable);

	}

	private void loadData() {
		privs = ricerche.loadAllPrivilegi();

	}

	@Override
	public void onEvent(org.vaadin.spring.events.Event<Object> event) {
		if (event.getTopic()
				.equals(TypeForEventBus.REFRESH_PRIVILEGI_TABLE.toString())) {
			privs = ricerche.loadAllPrivilegi();
			privsTable.setItems(privs);
			//			sliderPanel.collapse();
		}

	}

}
