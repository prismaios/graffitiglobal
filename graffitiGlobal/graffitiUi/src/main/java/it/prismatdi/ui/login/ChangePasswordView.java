package it.prismatdi.ui.login;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.ui.commons.GraffitiWebMainUI;

@SuppressWarnings("serial")
@SpringView(name = ChangePasswordView.VIEW_NAME, ui = GraffitiWebMainUI.class)
public class ChangePasswordView extends VerticalLayout implements View {
	public static final String		VIEW_NAME	= "changePasswordView";

	@Autowired
	private ChangePasswordFactory	changePasswordFactory;

	@Override
	public void enter(ViewChangeEvent event) {
		addComponent(changePasswordFactory.createComponent());// TODO Auto-generated method stub
	}

}
