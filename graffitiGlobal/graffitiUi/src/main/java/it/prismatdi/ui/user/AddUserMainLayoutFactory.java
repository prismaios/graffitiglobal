package it.prismatdi.ui.user;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.vaadin.spring.events.EventBus;

import com.vaadin.data.Binder;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.errors.BinderError;
import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.message.DataBaseMessages;
import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.model.user.Utente;
import it.prismatdi.services.ricerche.user.RicercheUsers;
import it.prismatdi.services.user.AddUserService;
import it.prismatdi.services.user.UtenteService;
import it.prismatdi.user.UserStringInputValidator;
import it.prismatdi.user.UserStringUtil;

@SpringComponent
public class AddUserMainLayoutFactory {

	@SuppressWarnings("serial")
	private class AddUserMainLayout extends VerticalLayout implements ClickListener {
		GridLayout									gridLayout		= new GridLayout(2, 1);
		VerticalLayout								verticalLayout	= new VerticalLayout();
		Grid<Utente>								gridUtenti		= new Grid<>();
		private Binder<UserGraffitiWeb>		binder;
		private TextField							userName;
		private TextField							password;
		private TextField							nome;
		private TextField							cognome;
		private TextField							codGraffiti;
		private Button								creaButton;
		private Button								clearButton;
		private Button								resetPasswd;
		private ComboBox<UserGraffitiWeb>	comboUsers;
		private List<UserGraffitiWeb>			listaUsers;
		private UserGraffitiWeb					userWeb			= new UserGraffitiWeb();

		public AddUserMainLayout() {
		}

		public AddUserMainLayout bind() {
			comboUsers.setItems(listaUsers);
			binder.forField(userName)
					.withValidator(new StringLengthValidator(UserStringInputValidator.NOME_LOGIN.getString(), 1, 30))
					.bind(UserGraffitiWeb::getLoginName, UserGraffitiWeb::setLoginName);
			binder.forField(password)
					.withValidator(new StringLengthValidator(UserStringInputValidator.PASSWORD_LOGIN.getString(), 1, 100))
					.bind(UserGraffitiWeb::getPassword, UserGraffitiWeb::setPassword);
			binder.forField(nome)
					.withValidator(new StringLengthValidator(UserStringInputValidator.NOME.getString(), 1, 50))
					.bind(UserGraffitiWeb::getNome, UserGraffitiWeb::setNome);
			binder.forField(cognome)
					.withValidator(new StringLengthValidator(UserStringInputValidator.COGNOME.getString(), 1, 50))
					.bind(UserGraffitiWeb::getCognome, UserGraffitiWeb::setCognome);
			binder.forField(codGraffiti)
					.withValidator(new StringLengthValidator(UserStringInputValidator.COD_GRAFFITI.getString(), 1, 20))
					.bind(UserGraffitiWeb::getCodGraffiti, UserGraffitiWeb::setCodGraffiti);
			binder.readBean(userWeb);
			return this;
		}

		@Override
		public void buttonClick(ClickEvent event) {
			if (event.getSource()
					.equals(creaButton)) {
				// salva
				save();
			}
			if (event.getSource()
					.equals(clearButton)) {
				// pulisci campi
				clear();
			}
			if (event.getSource()
					.equals(resetPasswd)) {
				// pulisci campi
				resetPasswd();
			}

		}

		private void clear() {
			userWeb = new UserGraffitiWeb();
			userName.setValue("");
			password.setValue("");
			nome.setValue("");
			cognome.setValue("");
			codGraffiti.setValue("");
		}

		public AddUserMainLayout init() {
			//			if (eventBus == null) {
			//				System.err.println(">>>>> AddUserMainLayoutFactory.AddUserMainLayout-init <<<<<< NULLLLLLO");
			//			}
			//			else {
			//				System.err.println(">>>>> AddUserMainLayoutFactory.AddUserMainLayout-init <<<<<< " + eventBus.toString());
			//			}
			listaUsers = ricercheUserService.loadAllUsers();
			comboUsers = new ComboBox<>(UserStringUtil.COMBO_CAPTION.getString(), listaUsers);
			comboUsers.setPlaceholder(UserStringUtil.COMBO_PROMT.getString());
			comboUsers.addSelectionListener(e -> {
				Optional<UserGraffitiWeb> usrOpt = e.getFirstSelectedItem();
				if (usrOpt.isPresent()) {
					userWeb = usrOpt.get();
					binder.readBean(userWeb);
				}
			});
			binder = new Binder<>();
			userWeb = new UserGraffitiWeb();
			userName = new TextField(UserStringUtil.LOGIN_NAME.getString());
			password = new TextField(UserStringUtil.PASSWORD.getString());
			password.setReadOnly(true);
			nome = new TextField(UserStringUtil.NOME.getString());
			cognome = new TextField(UserStringUtil.COGNOME.getString());
			codGraffiti = new TextField(UserStringUtil.COD_GRAFFITI.getString());
			creaButton = new Button(UserStringUtil.SAVE_BUTTON.getString());
			creaButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			creaButton.addClickListener(this);
			resetPasswd = new Button(UserStringUtil.RESET_PASSWD.getString());
			resetPasswd.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			resetPasswd.addClickListener(this);
			clearButton = new Button(UserStringUtil.CLEAR_BUTTON.getString());
			clearButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
			clearButton.addClickListener(this);
			// grid
			String codDitta = env.getProperty("DITTA");
			List<Utente> listaUtenti = utenteService.findAllPerDitta(codDitta);
			gridUtenti.setCaption("UTENTI GRAFFITI");
			gridUtenti.setItems(listaUtenti);
			gridUtenti.addColumn(utente -> utente.getId()
					.getUtecod())
					.setId("codGraffiti")
					.setCaption("CODICE");
			gridUtenti.addColumn(Utente::getUtenom)
					.setId("nome")
					.setCaption("NOME UTENTE");
			gridUtenti.addSelectionListener(e -> {
				Optional<Utente> dat = e.getFirstSelectedItem();
				if (dat.isPresent()) {
					Utente ute = dat.get();
					codGraffiti.setValue(ute.getId()
							.getUtecod());
				}
			});

			return this;
		}

		public Component layout() {
			//			System.err.println(">>>>> AddUserMainLayoutFactory.AddUserMainLayout-layout <<<<<< " + eventBus.toString());

			GridLayout gridLa = new GridLayout(2, 6);
			gridLa.setMargin(true);
			gridLa.setSizeUndefined();
			gridLa.setSpacing(true);
			gridLa.addComponent(comboUsers, 0, 0);
			gridLa.addComponent(userName, 0, 1);
			gridLa.addComponent(password, 0, 2);
			gridLa.addComponent(nome, 0, 3);
			gridLa.addComponent(cognome, 1, 3);
			gridLa.addComponent(codGraffiti, 0, 4);
			gridLa.addComponent(new HorizontalLayout(creaButton, clearButton), 0, 5);
			gridLa.addComponent(resetPasswd, 1, 5);
			verticalLayout.addComponent(gridLa);
			gridLayout.addComponent(verticalLayout, 0, 0);
			gridLayout.addComponent(gridUtenti, 1, 0);
			addComponent(gridLayout);
			return this;
		}

		//		public AddUserMainLayout initEventBus() {
		//			//			eventBus = context.getBean(EventBus.UIEventBus.class);
		//			System.err.println(">>>>> AddUserMainLayoutFactory-initEventBus <<<<<< " + eventBus.toString());
		//			return this;
		//		}

		private void resetPasswd() {
			String newPaswd = userWeb.getLoginName() + "1234";
			String s = encoder.encode(newPaswd);
			password.setValue(s);
			Notification.show("ATTENZIONE",
					"Comunicare a " + userWeb.getNome() + " " + userWeb.getCognome() + " la NUOVA PASSWD: " + newPaswd,
					Type.ERROR_MESSAGE);
			userWeb.setPassword(s);
			addUserService.updateUserPassword(s, userWeb.getId());

		}

		private void save() {
			boolean beanOk = binder.writeBeanIfValid(userWeb);
			if (beanOk) {
				addUserService.updateUser(userWeb);
				eventBus.publish(TypeForEventBus.REFRESH_USER_TABLE.toString(), AddUserMainLayout.this, userWeb);

			}
			else {
				Notification.show(BinderError.TITOLO.getString(), BinderError.DESCRIZIONE.getString(), Type.ERROR_MESSAGE);
				return;
			}
			clear();
			Notification.show(DataBaseMessages.TITOLO_MESSAGGIO.getString(), DataBaseMessages.DESCRIZIONE_MESSAGGIO.getString(),
					Type.WARNING_MESSAGE);
		}
	}

	@Autowired
	private AddUserService						addUserService;

	@Autowired
	private Environment							env;

	@Autowired
	private UtenteService						utenteService;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	@Autowired
	private BCryptPasswordEncoder				encoder;

	@Autowired
	private RicercheUsers						ricercheUserService;

	//	@Autowired
	//	private TmovmagService						tmovmagService;

	public Component createComponent() {
		return new AddUserMainLayout()
				.init()
				.bind()
				//				.initEventBus()
				.layout();
	}

}
