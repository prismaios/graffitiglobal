package it.prismatdi.ui.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventBusListener;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.eventBus.TypeForEventBus;
import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.services.ricerche.user.RicercheUsers;
import it.prismatdi.ui.commons.UIComponentBuilder;

@SpringComponent
public class ShowAllUserLayoutFactory implements UIComponentBuilder {

	@SuppressWarnings("serial")
	private class ShowAllUserLayout extends VerticalLayout implements EventBusListener<Object> {

		public ShowAllUserLayout init() {

			setMargin(true);
			usersTable = new Grid<>();
			usersTable.setItems(users);
			usersTable.addColumn(UserGraffitiWeb::getId)
					.setCaption("ID");
			usersTable.addColumn(UserGraffitiWeb::getLoginName)
					.setCaption("LOGIN");
			usersTable.addColumn(UserGraffitiWeb::getNome)
					.setCaption("NOME");
			usersTable.addColumn(UserGraffitiWeb::getCognome)
					.setCaption("COGNOME");

			return this;
		}

		public ShowAllUserLayout initEventBus() {
			eventBus.subscribe(this);
			return this;
		}

		public ShowAllUserLayout layout() {
			//			eventBus.subscribe(this);
			addComponent(usersTable);
			return this;
		}

		public ShowAllUserLayout loadData() {
			users = userService.loadAllUsers();
			//			System.err.println(">>>>> ShowAllUserLayoutFactory.ShowAllUserLayout-loadData <<<<<< " + users.size());
			return this;
		}

		@Override
		public void onEvent(org.vaadin.spring.events.Event<Object> event) {
			if (event.getTopic()
					.equals(TypeForEventBus.REFRESH_USER_TABLE.toString())) {
				System.err.println(
						">>>>> ShowAllUserLayoutFactory.ShowAllUserLayout-onEvent <<<<<< " + event.getPayload()
								.toString());
				users = userService.loadAllUsers();
				usersTable.setItems(users);
			}
		}

	}
	private List<UserGraffitiWeb>				users;
	private Grid<UserGraffitiWeb>				usersTable;
	//	private ListDataProvider<UserGraffitiWeb>	dataProvider;
	@Autowired
	private RicercheUsers						userService;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	@Override
	public Component createComponent() {
		//		System.err.println(">>>>> ShowAllUserLayoutFactory-createComponent <<<<<< " + eventBus.toString());
		return new ShowAllUserLayout().loadData()
				.init()
				.layout()
				.initEventBus();
	}

	public void refreshTable() {
		users = userService.loadAllUsers();
		usersTable.setItems(users);
		//		System.err.println(">>>>> ShowAllUserLayoutFactory-refreshTable <<<<<< " + users.size());
		//		dataProvider.refreshAll();
	}

}
