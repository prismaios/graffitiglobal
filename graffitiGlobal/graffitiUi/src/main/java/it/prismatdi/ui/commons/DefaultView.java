package it.prismatdi.ui.commons;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@SpringView(name = DefaultView.VIEW_NAME, ui = GraffitiWebMainUI.class)
@UIScope
public class DefaultView extends VerticalLayout implements View {
	public static final String VIEW_NAME = "";

	public DefaultView() {
		// TODO Auto-generated constructor stub
	}

	public DefaultView(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

}
