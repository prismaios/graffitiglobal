package it.prismatdi.ui.commons;

import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;

@SuppressWarnings("serial")
@SpringView(name = ErrorView.VIEW_NAME)
@Scope("prototype")
public class ErrorView extends Panel implements View {
	public static final String VIEW_NAME = "errorView";

	public ErrorView() {
		// TODO Auto-generated constructor stub
	}

	public ErrorView(Component content) {
		super(content);
		// TODO Auto-generated constructor stub
	}

	public ErrorView(String caption) {
		super(caption);
		// TODO Auto-generated constructor stub
	}

	public ErrorView(String caption, Component content) {
		super(caption, content);
		// TODO Auto-generated constructor stub
	}

}
