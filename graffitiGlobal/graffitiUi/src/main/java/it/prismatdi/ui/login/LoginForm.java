package it.prismatdi.ui.login;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.ui.commons.GraffitiLogoLayoutFactory;

@SuppressWarnings("serial")
@SpringComponent
@Scope("prototype")
public class LoginForm extends VerticalLayout {

	@Autowired
	private DaoAuthenticationProvider	daoAuthenticationProvider;

	@Autowired
	private GraffitiLogoLayoutFactory	logoFactory;

	private Panel								panel;
	private Component							logo;
	private TextField							nameField;
	private PasswordField					passField;
	private Button								loginButton;
	private Button								regisButton;

	public LoginForm() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void configura() {
		init();
		layout();
	}

	private void init() {
		setMargin(true);
		setHeight("100%");
		panel = new Panel("LOGIN");
		panel.setSizeUndefined();
		logo = logoFactory.createComponent();
		loginButton = new Button("Login");
		loginButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		loginButton.addClickListener(e -> {
			login();

		});
		regisButton = new Button("Registrati");
		regisButton.setStyleName(ValoTheme.BUTTON_LINK);
		regisButton.addClickListener(e -> {
			UI.getCurrent()
					.getPage()
					.setLocation("/graffitiWeb/signup");
		});
		nameField = new TextField("Nome login");

		passField = new PasswordField("Password");
		passField.addShortcutListener(new ShortcutListener("Login", ShortcutAction.KeyCode.ENTER, null) {

			@Override
			public void handleAction(Object sender, Object target) {
				loginButton.click();
			}
		});
	}

	private void layout() {
		addComponent(panel);
		setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
		GridLayout gridL = new GridLayout(1, 2);
		gridL.addComponent(logo, 0, 0);
		FormLayout formLayout = new FormLayout();
		formLayout.addComponent(nameField);
		formLayout.addComponent(passField);
		//			VerticalLayout v = new VerticalLayout();
		//			v.setMargin(false);
		//			v.setSizeUndefined();
		//			v.addComponents(loginButton, regisButton, changePassButton);
		//			formLayout.addComponent(v);
		formLayout.addComponent(loginButton);
		formLayout.addComponent(regisButton);
		formLayout.setSizeUndefined();
		formLayout.setMargin(true);
		gridL.addComponent(formLayout, 0, 1);
		panel.setContent(gridL);
		nameField.focus();

	}

	private void login() {
		try {
			Authentication auth = new UsernamePasswordAuthenticationToken(nameField.getValue(), passField.getValue());
			Authentication passed = daoAuthenticationProvider.authenticate(auth);
			//			CustomUserDetails cud = (CustomUserDetails) passed.getPrincipal();
			//			System.err.println(">>>>> LoginForm-login <<<<<< " + cud.getGraffitiWeb()
			//					.getCognome());
			SecurityContextHolder.getContext()
					.setAuthentication(passed);
			UI.getCurrent()
					.getPage()
					.setLocation("/graffitiWeb/ui");
			nameField.clear();
			passField.clear();
			UI.getCurrent()
					.close();
		}
		catch (AuthenticationException ex) {
			ex.printStackTrace();
			Notification.show("ERRORE", "Login fallito!", Type.ERROR_MESSAGE);

		}

	}

}
