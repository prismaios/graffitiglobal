package it.prismatdi.ui.commons;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;

import it.prismatdi.general.Ruoli;
import it.prismatdi.menu.MenuUtils;
import it.prismatdi.security.SecurityUtils;

@SuppressWarnings("serial")
@SpringComponent
@UIScope
public class GraffitiMenu extends MenuBar {

	@Autowired
	private Navigator			navigator;

	private MenuBar.Command	comando;

	public GraffitiMenu() {
	}

	@PostConstruct
	private void configura() {
		init();
		layout();
	}

	private void init() {
		setWidth("100%");
		setHeightUndefined();
		comando = selectedItem -> {
			String viewName = MenuUtils.findComand(selectedItem.getText());
			if (viewName.equals("Logout")) {
				SecurityContextHolder.clearContext();
				UI.getCurrent()
						.getSession()
						.getService()
						.closeSession(VaadinSession.getCurrent());
				UI.getCurrent()
						.getSession()
						.close();
				UI.getCurrent()
						.close();
				VaadinService.getCurrentRequest()
						.getWrappedSession()
						.invalidate();
				UI.getCurrent()
						.getPage()
						.setLocation("/graffitiWeb/login");

				return;
			}
			try {
				navigator.navigateTo(viewName);
			}
			catch (Exception e) {
				//				e.printStackTrace();
			}

		};

	}

	private void layout() {

		// DASH BOARD
		boolean agg = SecurityUtils.isCurrentUserInRole(Ruoli.ADMINISTRATOR.toString())
				&& SecurityUtils.isCurrentUserInRole(Ruoli.USER.toString());
		if (agg) {
			addItem(MenuUtils.SINTESI.getString()
					.toUpperCase(), null, comando);
		}
		// USER
		MenuItem userNode = null;
		if (SecurityUtils.isCurrentUserInRole(Ruoli.ADMINISTRATOR.toString())) {
			userNode = addItem(MenuUtils.MENU_USER.getString(), null, null);
		}
		if (userNode != null) {
			if (SecurityUtils.isCurrentUserInRole(MenuUtils.ADD_USER)) {
				userNode.addItem(MenuUtils.ADD_USER.getString(), null, comando);
			}
			if (SecurityUtils.isCurrentUserInRole(MenuUtils.MODIFICA_USER)) {
				userNode.addItem(MenuUtils.MODIFICA_USER.getString(), null, comando);
			}
			if (SecurityUtils.isCurrentUserInRole(MenuUtils.REMOVE_USER)) {
				userNode.addItem(MenuUtils.REMOVE_USER.getString(), null, comando);
			}
		}

		// PRIVILEGI
		MenuItem privNode = null;
		if (SecurityUtils.isCurrentUserInRole(Ruoli.ADMINISTRATOR.toString())) {
			privNode = addItem(MenuUtils.MENU_PRIVILEGI.getString(), null, null);
		}
		if (privNode != null) {
			if (SecurityUtils.isCurrentUserInRole(MenuUtils.ADD_PRIVILEGIO)) {
				privNode.addItem(MenuUtils.ADD_PRIVILEGIO.getString(), null, comando);
			}
			if (SecurityUtils.isCurrentUserInRole(MenuUtils.REMOVE_PRIVILEGIO)) {
				privNode.addItem(MenuUtils.REMOVE_PRIVILEGIO.getString(), null, comando);
			}
			if (SecurityUtils.isCurrentUserInRole(MenuUtils.AGGIUNGI_PRIVILEGIO_A_USER)) {
				privNode.addItem(MenuUtils.AGGIUNGI_PRIVILEGIO_A_USER.getString(), null, comando);
			}
		}

		// CAMPIONARIO
		MenuItem campNode = null;
		if (SecurityUtils.isCurrentUserInRole(Ruoli.AGENTE.toString())) {
			campNode = addItem(MenuUtils.MENU_AGENTI.getString(), null, null);
		}
		if (campNode != null) {
			campNode.addItem(MenuUtils.CAMPIONARIO.getString(), null, comando);
		}

		// LOGOUT
		MenuItem outNode = addItem(MenuUtils.MENU_LOGOUT.getString(), null, null);
		outNode.addItem(MenuUtils.LOGOUT.getString(), null, comando);
		outNode.addItem(MenuUtils.CAMBIA_PASSWD.getString(), null, comando);

	}

}
