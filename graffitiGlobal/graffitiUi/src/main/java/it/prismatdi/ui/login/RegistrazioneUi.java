package it.prismatdi.ui.login;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

import it.prismatdi.ui.commons.ErrorView;

@SuppressWarnings("serial")
@SpringUI(path = "/signup")
@Theme("mytheme")
public class RegistrazioneUi extends UI {
	@Autowired
	public ErrorView				errorView;

	@Autowired
	private RegistrazioneForm	regisForm;

	@Override
	protected void init(VaadinRequest request) {
		setContent(regisForm);
		getNavigator().setErrorView(errorView);

	}

}
