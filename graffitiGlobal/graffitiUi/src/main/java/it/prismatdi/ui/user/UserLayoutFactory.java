package it.prismatdi.ui.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

import it.prismatdi.ui.commons.GraffitiWebMainUI;
import it.prismatdi.user.UserStringUtil;

@SuppressWarnings("serial")
@SpringView(name = UserLayoutFactory.VIEW_NAME, ui = GraffitiWebMainUI.class)
public class UserLayoutFactory extends VerticalLayout implements View {
	public static final String				VIEW_NAME	= "Aggiungi user";

	private TabSheet							tabSheet;
	@Autowired
	private AddUserMainLayoutFactory		userMainLayoutFactory;
	@Autowired
	private AddUserRuoloLayoutFactory	ruoloLayoutFactory;
	@Autowired
	private ShowAllUserLayoutFactory		allUserLayoutFactory;

	private void addLayout() {
		System.err.println(">>>>> UserLayoutFactory-addLayout <<<<<< ");
		setMargin(true);
		tabSheet = new TabSheet();
		tabSheet.setWidth("100%");
		Component addUserTab = userMainLayoutFactory.createComponent();
		Component addRuoloTab = ruoloLayoutFactory.createComponent();
		Component showAll = allUserLayoutFactory.createComponent();
		tabSheet.addTab(addUserTab)
				.setCaption(UserStringUtil.USER_TAB.getString());
		tabSheet.addTab(addRuoloTab)
				.setCaption(UserStringUtil.RUOLO_TAB.getString());
		tabSheet.addTab(showAll)
				.setCaption(UserStringUtil.SHOW_ALL_TAB.getString());
		addComponent(tabSheet);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		removeAllComponents();
		addLayout();
	}

}
