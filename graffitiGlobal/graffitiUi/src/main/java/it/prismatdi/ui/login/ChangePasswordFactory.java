package it.prismatdi.ui.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.errors.BinderError;
import it.prismatdi.general.Bottoni;
import it.prismatdi.general.Titoli;
import it.prismatdi.message.Messaggi;
import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.services.ricerche.user.RicercheUsers;
import it.prismatdi.services.security.CustomUserDetails;
import it.prismatdi.services.security.RegisterUserService;
import it.prismatdi.ui.commons.UIComponentBuilder;

@SpringComponent
public class ChangePasswordFactory implements UIComponentBuilder {

	@SuppressWarnings("serial")
	private class ChangePasswordForm extends VerticalLayout {
		private Authentication	authentication;
		private Panel				panel;
		private PasswordField	oldPasswordField;
		private PasswordField	newPasswordField;
		private PasswordField	repeatPassword;
		private Button				buttonSave;

		protected ChangePasswordForm init() {
			authentication = SecurityContextHolder.getContext()
					.getAuthentication();

			panel = new Panel(Titoli.CAMBIO_PASSWD.getString());
			panel.setSizeUndefined();

			oldPasswordField = new PasswordField("Vecchia password");
			newPasswordField = new PasswordField("Nuova password");
			repeatPassword = new PasswordField("Ripeti nuova password");

			buttonSave = new Button(Bottoni.SAVE.getString());
			buttonSave.setStyleName(ValoTheme.BUTTON_PRIMARY);
			buttonSave.addClickListener(e -> {
				salvaPasswd();
			});

			return this;
		}

		protected ChangePasswordForm layout() {
			setMargin(true);
			setHeight("100%");
			addComponent(panel);
			setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
			FormLayout formLayout = new FormLayout();
			formLayout.addComponent(oldPasswordField);
			formLayout.addComponent(newPasswordField);
			formLayout.addComponent(repeatPassword);
			formLayout.addComponent(buttonSave);

			formLayout.setMargin(true);
			panel.setContent(formLayout);
			return this;
		}

		private void salvaPasswd() {
			Object cred = authentication.getCredentials();
			if (cred.equals(oldPasswordField.getValue())) {
				if (!newPasswordField.getValue()
						.equals(repeatPassword.getValue())) {
					Notification.show(BinderError.TITOLO.getString(), Messaggi.PASSWD_NON_UGUALI.getString(), Type.ERROR_MESSAGE);
					return;
				}
				CustomUserDetails details = (CustomUserDetails) authentication.getPrincipal();
				UserGraffitiWeb us = ricercaUsers.loadUserGraffiti(details.getIdUser())
						.get();
				us.setPassword(newPasswordField.getValue());
				try {
					securityUserService.save(us);
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				oldPasswordField.setValue("");
				newPasswordField.setValue("");
				repeatPassword.setValue("");
				Notification.show("COMUNICAZIONE:", "Passward salvata!", Type.HUMANIZED_MESSAGE);
			}
			else {
				Notification.show(BinderError.TITOLO.getString(), Messaggi.PASSWD_ERRATA.getString(), Type.ERROR_MESSAGE);
				return;
			}

		}
	}
	@Autowired
	private RegisterUserService	securityUserService;

	@Autowired
	private RicercheUsers			ricercaUsers;

	@Override
	public Component createComponent() {
		// TODO Auto-generated method stub
		return new ChangePasswordForm().init()
				.layout();
	}

}
