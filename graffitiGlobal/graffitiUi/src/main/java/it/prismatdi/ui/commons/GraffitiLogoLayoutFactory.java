package it.prismatdi.ui.commons;

import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.VerticalLayout;

@SpringComponent
public class GraffitiLogoLayoutFactory implements UIComponentBuilder {

	@SuppressWarnings("serial")
	private class LogoLayout extends VerticalLayout {
		private Embedded logo;

		public LogoLayout init() {
			logo = new Embedded();
			logo.setSource(new ThemeResource("../../images/Bisentino.png"));
			logo.setWidth("100%");
			logo.setHeight("20%");
			return this;
		}

		public LogoLayout layout() {
			addComponent(logo);
			setComponentAlignment(logo, Alignment.TOP_CENTER);
			return this;
		}
	}

	@Override
	public Component createComponent() {
		return new LogoLayout().init()
				.layout();
	}

}
