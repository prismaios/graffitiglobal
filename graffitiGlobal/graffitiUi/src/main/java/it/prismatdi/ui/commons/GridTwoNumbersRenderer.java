package it.prismatdi.ui.commons;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import com.vaadin.server.ClientConnector;
import com.vaadin.server.ClientMethodInvocation;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.Extension;
import com.vaadin.server.ServerRpcManager;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.shared.Registration;
import com.vaadin.shared.communication.SharedState;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.Renderer;

import elemental.json.JsonObject;
import elemental.json.JsonValue;

@SuppressWarnings("serial")
public class GridTwoNumbersRenderer extends VerticalLayout implements Renderer<Number> {
	private final TextField	acText;
	private final TextField	apText;

	public GridTwoNumbersRenderer() {
		acText = new TextField();
		apText = new TextField();
		acText.setWidth("150px");
		acText.setHeight("50px");
		apText.setWidth("150px");
		apText.setHeight("50px");
		addComponent(acText);
		addComponent(apText);

	}

	@Override
	public Registration addAttachListener(AttachListener listener) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Registration addDetachListener(DetachListener listener) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void attach() {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeClientResponse(boolean initial) {
		// TODO Auto-generated method stub

	}

	@Override
	public void detach() {
		// TODO Auto-generated method stub

	}

	@Override
	public JsonValue encode(Number value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JsonObject encodeState() {
		// TODO Auto-generated method stub
		return null;
	}

	public TextField getAcText() {
		return acText;
	}

	public TextField getApText() {
		return apText;
	}

	@Override
	public String getConnectorId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ErrorHandler getErrorHandler() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Extension> getExtensions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HasComponents getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<Number> getPresentationType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServerRpcManager<?> getRpcManager(String rpcInterfaceName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<? extends SharedState> getStateType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UI getUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAttached() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isConnectorEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void markAsDirty() {
		// TODO Auto-generated method stub

	}

	@Override
	public void markAsDirtyRecursive() {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeAttachListener(AttachListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeDetachListener(DetachListener listener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeExtension(Extension extension) {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestRepaint() {
		// TODO Auto-generated method stub

	}

	@Override
	public void requestRepaintAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ClientMethodInvocation> retrievePendingRpcCalls() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setErrorHandler(ErrorHandler errorHandler) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setParent(ClientConnector parent) {
		// TODO Auto-generated method stub

	}

}
