package it.prismatdi.ui.privilegi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.SingleSelectionModel;
import com.vaadin.ui.themes.ValoTheme;

import it.prismatdi.general.Ruoli;
import it.prismatdi.model.user.Privilegi;
import it.prismatdi.model.user.RuoliWebUser;
import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.services.privilegi.PrivilegiService;
import it.prismatdi.services.ricerche.privilegi.RicerchePrivilegi;
import it.prismatdi.services.ricerche.user.RicercheUsers;
import it.prismatdi.services.user.AddUserService;
import it.prismatdi.ui.commons.GraffitiWebMainUI;

@SuppressWarnings("serial")
@SpringView(name = AddPrivToUserView.VIEW_NAME, ui = GraffitiWebMainUI.class)
public class AddPrivToUserView extends VerticalLayout implements View {
	public static final String					VIEW_NAME	= "addPrivUser";

	@Autowired
	private RicercheUsers						userService;

	@Autowired
	private PrivilegiService					privilegiService;

	@Autowired
	private RicerchePrivilegi					ricerchePrivs;

	@Autowired
	private AddUserService						registerService;

	@Autowired
	private EventBus.ApplicationEventBus	eventBus;

	//
	private List<UserGraffitiWeb>				users;
	private List<Privilegi>						privs;
	private Grid<UserGraffitiWeb>				usersTable;
	private Label									nomeLabel;
	private Grid<Ruoli>							nomeRuoliGrid;
	private Grid<Privilegi>						privsGrid;
	private Button									addButton;
	private Button									minusButton;
	private Button									saveButton;
	private RuoliWebUser							ruoloSelected;
	private UserGraffitiWeb						userSelected;

	private void aggiungiRuolo() {
		if (ruoloSelected == null) {
			return;
		}
		Optional<Privilegi> selected = ((SingleSelectionModel<Privilegi>) privsGrid.getSelectionModel()).getSelectedItem();
		if (!selected.isPresent()) {
			return;
		}
		Privilegi priv = selected.get();
		Optional<Privilegi> savedPriv = privilegiService.loadPrivilegioById(priv.getId());
		if (savedPriv.isPresent()) {
			savedPriv.get()
					.getRuoli()
					.add(ruoloSelected);
		}
		ruoloSelected.getPrivilegi()
				.add(savedPriv.get());

	}

	@PostConstruct
	public void configura() {
		removeAllComponents();
		initEventBus();
		loadData();
		init();
		layout();

	}

	@Override
	public void enter(ViewChangeEvent event) {

	}

	private void init() {
		setMargin(true);
		usersTable = new Grid<>();
		usersTable.setCaption("UTENTI");
		usersTable.setStyleName("myrow");
		usersTable.setItems(users);
		usersTable.addColumn(UserGraffitiWeb::getId)
				.setCaption("ID");
		usersTable.addColumn(UserGraffitiWeb::getLoginName)
				.setCaption("LOGIN");
		usersTable.addColumn(UserGraffitiWeb::getNome)
				.setCaption("NOME");
		usersTable.addColumn(UserGraffitiWeb::getCognome)
				.setCaption("COGNOME");
		usersTable.addSelectionListener(e -> {
			Optional<UserGraffitiWeb> userOpt = e.getFirstSelectedItem();
			if (userOpt.isPresent()) {
				userSelected = userOpt.get();
				ruoloSelected = null;
				showName(userSelected);
			}
		});
		nomeLabel = new Label();
		nomeLabel.setContentMode(ContentMode.HTML);
		nomeLabel.setStyleName(ValoTheme.LABEL_COLORED);
		nomeRuoliGrid = new Grid<>();
		nomeRuoliGrid.setCaption("RUOLI DI: ");
		nomeRuoliGrid.addColumn(Ruoli::toString)
				.setCaption("RUOLI");
		privsGrid = new Grid<>();
		privsGrid.setCaption("PRIVILEGI DISPONIBILI");
		privsGrid.setItems(privs);
		privsGrid.addColumn(Privilegi::getId)
				.setCaption("ID");
		privsGrid.addColumn(Privilegi::getDescrizione)
				.setCaption("DESCRIZIONE");
		privsGrid.addColumn(Privilegi::getRuolo)
				.setCaption("RUOLO");
		addButton = new Button("+");
		addButton.addClickListener(e -> {
			aggiungiRuolo();
		});
		minusButton = new Button("-");
		minusButton.addClickListener(e -> {
			togliRuolo();
		});
		saveButton = new Button("SALVA");
		saveButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
		saveButton.addClickListener(e -> {
			saveUser();
		});

	}

	private void initEventBus() {
		eventBus.subscribe(this);

	}

	private void layout() {
		Panel panUsers = new Panel("USERS E PRIVILEGI");
		HorizontalLayout hluno = new HorizontalLayout();
		usersTable.setHeightByRows(users.size());
		hluno.addComponent(usersTable);

		VerticalLayout vvert = new VerticalLayout();
		vvert.setSizeUndefined();
		vvert.addComponent(nomeLabel);
		nomeLabel.setWidth("100%");
		nomeRuoliGrid.setSizeUndefined();
		nomeRuoliGrid.setHeight("130px");
		vvert.addComponent(nomeRuoliGrid);
		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponents(addButton, minusButton, saveButton);
		vvert.addComponent(hl);
		privsGrid.setHeightByRows(privs.size());
		privsGrid.setHeight("40%");
		vvert.addComponent(privsGrid);
		hluno.addComponent(vvert);
		panUsers.setContent(hluno);
		addComponent(panUsers);

	}

	private void loadData() {
		users = userService.loadAllUsers();
		privs = ricerchePrivs.loadAllPrivilegi();

	}

	private void saveUser() {
		try {
			registerService.saveUser(userSelected);
			showName(userSelected);
			nomeRuoliGrid.removeAllColumns();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void showName(UserGraffitiWeb us) {
		// $2a$10$XgnIeh541aqoej2DcXCaWO3rnInRMypnjAMaKjkKowr7Ko7SUSrbK

		userSelected = us;
		//		System.err.println(">>>>> AddPrivToUserFactory.AddPrivToUser-showName <<<<<< " + userSelected.getPassword());
		nomeLabel.setValue("<b><h3>" + userSelected.getCognome() + " "
				+ userSelected.getNome()
				+ "</b>");
		nomeRuoliGrid.setCaption("RUOLI DI: " + userSelected.getLoginName());
		List<Ruoli> rols = new ArrayList<>();
		if (userSelected.getRuoli()
				.size() == 1) {
			ruoloSelected = userSelected.getRuoli()
					.get(0);
			for (Privilegi priv : ruoloSelected.getPrivilegi()) {
				rols.add(priv.getRuolo());
			}
		}
		else {
			Notification.show("ATTENZIONE", "User con più ruoli!", Type.ERROR_MESSAGE);
			return;
		}
		nomeRuoliGrid.setItems(rols);

	}

	private void togliRuolo() {
		Set<Ruoli> nomeRoles = nomeRuoliGrid.getSelectedItems();
		if (!nomeRoles.isEmpty()) {
			Ruoli ruolo = nomeRoles.iterator()
					.next();
			List<Ruoli> rols = new ArrayList<>();
			Iterator<Privilegi> iteMissa = ruoloSelected.getPrivilegi()
					.iterator();
			while (iteMissa.hasNext()) {
				Privilegi priv = iteMissa.next();
				if (priv.getRuolo()
						.equals(ruolo)) {
					iteMissa.remove();
				}
				else {
					rols.add(priv.getRuolo());
				}
			}
			nomeRuoliGrid.setItems(rols);
		}

	}

}
