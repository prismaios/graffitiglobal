package it.prismatdi.ui.commons;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Window;

import it.prismatdi.ui.sintesi.InputParametriRicerche;

@SpringComponent
@SuppressWarnings("serial")
@Scope("prototype")
public class WindowParametriRicerca extends Window {
	@Autowired
	InputParametriRicerche parametriRicerca;

	public WindowParametriRicerca() {
		super("Parametri ricerche");
		center();
		setModal(true);
		setSizeUndefined();
		setWidth("500px");
		setHeight("600px");

	}

	@PostConstruct
	public void initLayout() {
		setContent(parametriRicerca);
	}

}
