package it.prismatdi.ui.login;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

import it.prismatdi.ui.commons.ErrorView;

@SuppressWarnings("serial")
@SpringUI(path = "/login")
@Scope("prototype")
@Theme("mytheme")
public class LoginUI extends UI {
	@Autowired
	public ErrorView	errorView;

	@Autowired
	private LoginForm	loginForm;

	@Override
	public void attach() {
		//		System.err.println(">>>>> LoginUI-attach <<<<<< ");
		super.attach();
		getUI();
		if (getUI().getConnectorTracker() != null) {
			//			System.err.println(">>>>> LoginUI-attach CONNECTOR <<<<<< ");
		}
		Boolean bb;
		try {
			getUI();
			bb = UI.getCurrent()
					.getConnectorTracker()
					.hasDirtyConnectors();
			//			System.err.println(">>>>> LoginUI-attach DIRTY <<<<<< " + bb);
		}
		catch (Exception e) {

		}

		try {
			getUI();
			if (!UI.getCurrent()
					.getWindows()
					.isEmpty()) {
				getUI();
				//				System.err.println(">>>>> LoginUI-attach <<<<<< " + UI.getCurrent()
				//						.getWindows()
				//						.size());
				getUI();
				getUI();
				for (Window w : UI.getCurrent()
						.getWindows()) {
					getUI();
					getUI();
					UI.getCurrent()
							.removeWindow(w);
				}
			}
		}
		catch (Exception e) {
		}
	}

	@PreDestroy
	void destroy() {
		//		System.err.println(">>>>> LoginUI-destroy <<<<<< ");
	}

	@Override
	protected void init(VaadinRequest request) {
		//		System.err.println(">>>>> LoginUI-init <<<<<< ");
		getNavigator().setErrorView(errorView);
		setContent(loginForm);

	}

}
