package it.prismatdi.ui.commons;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;

@SuppressWarnings("serial")
@SpringViewDisplay
public class ViewDisplay extends Panel implements com.vaadin.navigator.ViewDisplay {

	public ViewDisplay() {
		// TODO Auto-generated constructor stub
	}

	public ViewDisplay(Component content) {
		super(content);
		// TODO Auto-generated constructor stub
	}

	public ViewDisplay(String caption) {
		super(caption);
		// TODO Auto-generated constructor stub
	}

	public ViewDisplay(String caption, Component content) {
		super(caption, content);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void showView(View view) {
		setContent((Component) view);

	}

}
