package it.prismatdi.services.scribaf;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.scribaf.Tabel;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.scribaf.AnagraRepository;
import it.prismatdi.scribaf.TabelRepository;

@Service
@Transactional("scribafTransactionManager")
public class ScribafServiceImpl implements ScribafService {

	@Autowired
	private TabelRepository		tabelRepository;

	@Autowired
	private AnagraRepository	anagraRepository;

	@Override
	@Transactional
	public List<Busta> findAllClienti(String contoClienti) {
		//		System.err.println(">>>>> ScribafServiceImpl-findAllClienti <<<<<< " + contoClienti);
		return anagraRepository.findSetAnagra(contoClienti);
	}

	@Override
	@Transactional
	public List<Busta> findAllRappresentanti(String contoRappresentanti) {
		//		System.err.println(">>>>> ScribafServiceImpl-findAllRappresentanti <<<<<< " + contoRappresentanti);
		return anagraRepository.findRappresentanti(contoRappresentanti);
	}

	@Override
	@Transactional
	public List<Tabel> getAllCodiciIva() {
		return tabelRepository.getAllCodiciIva();
	}

	@Override
	@Transactional
	public List<Busta> getAllStagioni() {
		return tabelRepository.getAllStagioni();
	}

}
