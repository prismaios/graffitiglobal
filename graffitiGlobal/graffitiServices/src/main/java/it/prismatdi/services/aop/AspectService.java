package it.prismatdi.services.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectService {

	@Before("guardaServices() || guardaModel()")
	public void aopLoadAllUsers(JoinPoint joinPoint) {
		String method = joinPoint.getSignature().toShortString();
		System.err.println(">>>>> AspectService-aopLoadAllUsers <<<<<< " + method);
	}

	@Pointcut("execution(* it.prismatdi.model.*.*.*(..))")
	public void guardaModel() {
	}

	@Pointcut("execution(* it.prismatdi.services.ricerche.*.*.*(..))")
	public void guardaServices() {
	}
}
