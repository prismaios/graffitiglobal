package it.prismatdi.services.produzio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.produzio.Ordini;
import it.prismatdi.produzio.OrdiniRepository;

@Service
@Transactional("produzioTransactionManager")
public class OrdiniServiceImpl implements OrdiniService {

	@Autowired
	private OrdiniRepository ordiniRepository;

	@Override
	@Transactional
	public List<Ordini> findAll() {

		return ordiniRepository.findAll();
	}

}
