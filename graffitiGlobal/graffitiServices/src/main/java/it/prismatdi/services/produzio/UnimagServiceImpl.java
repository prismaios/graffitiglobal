package it.prismatdi.services.produzio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.general.ScelteAgenti;
import it.prismatdi.model.produzio.Unimag;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.model.utils.UnimagData;
import it.prismatdi.produzio.UnimagRepository;

@Service
@Transactional("produzioTransactionManager")
public class UnimagServiceImpl implements UnimagService {

	@Autowired
	protected UnimagRepository unimagRepository;

	public UnimagServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@Transactional
	public List<Unimag> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public List<UnimagData> findCampionario(ScelteAgenti tessSciar, Busta stagione) {
		return unimagRepository.findCampionario(tessSciar, stagione);
	}

}
