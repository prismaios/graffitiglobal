package it.prismatdi.services.ricerche.user;

import java.util.List;
import java.util.Optional;

import it.prismatdi.model.user.UserGraffitiWeb;

public interface RicercheUsers {
	public List<UserGraffitiWeb> loadAllUsers();

	public Optional<UserGraffitiWeb> loadUserGraffiti(Integer id);

}
