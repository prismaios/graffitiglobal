package it.prismatdi.services.user;

import it.prismatdi.model.user.UserGraffitiWeb;

public interface AddUserService {
	void saveUser(UserGraffitiWeb user);

	void updateUser(UserGraffitiWeb user);

	void updateUserPassword(String paswdEncripted, Integer idUser);

}
