package it.prismatdi.services.ricerche.user;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.repository.user.UserRepository;

@Service
@Transactional("transactionManager")
public class RicercheUsersImpl implements RicercheUsers {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<UserGraffitiWeb> loadAllUsers() {
		List<UserGraffitiWeb> users = userRepository.getAllUsers();
		return users;
	}

	@Override
	public Optional<UserGraffitiWeb> loadUserGraffiti(Integer id) {
		Optional<UserGraffitiWeb> userGraffitiOpt = userRepository.findById(id);
		return userGraffitiOpt;
	}

}
