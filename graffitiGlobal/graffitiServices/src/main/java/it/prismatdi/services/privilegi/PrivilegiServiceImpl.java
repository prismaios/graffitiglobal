package it.prismatdi.services.privilegi;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.user.Privilegi;
import it.prismatdi.repository.privilegi.PrivilegiRepository;

@Service
@Transactional("transactionManager")

public class PrivilegiServiceImpl implements PrivilegiService {
	@Autowired
	private PrivilegiRepository repository;

	@Override
	@Transactional
	public Optional<Privilegi> loadPrivilegioById(Integer id) {
		Optional<Privilegi> priv = repository.findById(id);
		if (priv.isPresent()) {
			priv.get()
					.getRuoli()
					.size();
		}
		return priv;
	}

	@Override
	@Transactional
	public void removePrivilegio(Privilegi privilegio) {
		repository.delete(privilegio);
	}

	@Override
	@Transactional
	public void savePrivilegio(Privilegi privilegio) {
		repository.save(privilegio);

	}

}
