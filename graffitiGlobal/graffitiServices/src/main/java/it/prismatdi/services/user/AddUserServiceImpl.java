package it.prismatdi.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.repository.user.UserRepository;

@Service
@Transactional("transactionManager")
public class AddUserServiceImpl implements AddUserService {
	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public void saveUser(UserGraffitiWeb user) {
		userRepository.save(user);

	}

	@Override
	@Transactional
	public void updateUser(UserGraffitiWeb user) {
		UserGraffitiWeb updating = userRepository.getOne(user.getId());
		updating.update(user);
		userRepository.flush();

	}

	@Override
	@Transactional
	public void updateUserPassword(String paswdEncripted, Integer idUser) {
		UserGraffitiWeb updating = userRepository.getOne(idUser);
		updating.setPassword(paswdEncripted);
		userRepository.flush();

	}

}
