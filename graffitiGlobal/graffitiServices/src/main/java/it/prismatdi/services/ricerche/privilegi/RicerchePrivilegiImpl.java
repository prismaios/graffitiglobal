package it.prismatdi.services.ricerche.privilegi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.user.Privilegi;
import it.prismatdi.repository.privilegi.PrivilegiRepository;

@Service
@Transactional("transactionManager")
public class RicerchePrivilegiImpl implements RicerchePrivilegi {

	@Autowired
	private PrivilegiRepository repoeitory;

	@Override
	public List<Privilegi> loadAllPrivilegi() {
		List<Privilegi> privs = repoeitory.findAll();
		return privs;
	}

}
