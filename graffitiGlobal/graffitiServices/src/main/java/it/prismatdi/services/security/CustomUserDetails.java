package it.prismatdi.services.security;

import org.springframework.security.core.userdetails.User;

import it.prismatdi.general.Ruoli;
import it.prismatdi.model.user.Privilegi;
import it.prismatdi.model.user.RuoliWebUser;
import it.prismatdi.model.user.UserGraffitiWeb;

@SuppressWarnings("serial")
public class CustomUserDetails extends User {

	private Integer	idUser;
	private String		defaultView;

	public CustomUserDetails(UserGraffitiWeb userGraffiti) {
		super(userGraffiti.getLoginName(), userGraffiti.getPassword(), true, true, true, !userGraffiti.getLocked(),
				userGraffiti.getAuths());
		Ruoli role = null;
		defaultView = "";
		for (RuoliWebUser ruolo : userGraffiti.getRuoli()) {
			for (Privilegi privilegio : ruolo.getPrivilegi()) {
				if (role == null) {
					role = privilegio.getRuolo();
					defaultView = privilegio.getModulo();
				}
				else {
					if (role.getVal() < privilegio.getRuolo()
							.getVal()) {
						role = privilegio.getRuolo();
						defaultView = privilegio.getModulo();
					}
				}
			}
		}
		if (defaultView.equals("no view")) {
			defaultView = "";
		}
		idUser = userGraffiti.getId();
	}

	public String getDefaultView() {
		return defaultView;
	}

	public Integer getIdUser() {
		return idUser;
	}

}
