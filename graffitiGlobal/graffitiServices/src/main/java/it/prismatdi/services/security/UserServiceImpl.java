package it.prismatdi.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.repository.security.SecUserRepository;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	private SecUserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserGraffitiWeb user = userRepository.findUserByLoginName(username);
		CustomUserDetails customUserDetails = new CustomUserDetails(user);
		return customUserDetails;
	}

}
