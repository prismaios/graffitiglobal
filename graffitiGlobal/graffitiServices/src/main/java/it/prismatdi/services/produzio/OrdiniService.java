package it.prismatdi.services.produzio;

import java.util.List;

import it.prismatdi.model.produzio.Ordini;

public interface OrdiniService {
	public List<Ordini> findAll();
}
