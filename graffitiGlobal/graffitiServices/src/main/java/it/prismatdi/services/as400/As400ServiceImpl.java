package it.prismatdi.services.as400;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.as400.CardisRepository;
import it.prismatdi.model.as400.Cardis;

@Service
@Transactional("as400fTransactionManager")
public class As400ServiceImpl implements As400Service {

	@Autowired
	private CardisRepository cardisRepository;

	@Override
	@Transactional
	public List<Cardis> findAll999() {
		return cardisRepository.findAll999();
	}

}
