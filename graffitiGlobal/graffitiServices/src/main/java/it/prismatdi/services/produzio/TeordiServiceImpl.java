package it.prismatdi.services.produzio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.produzio.Teordi;
import it.prismatdi.produzio.TeordiRepository;

@Service
@Transactional("produzioTransactionManager")
public class TeordiServiceImpl implements TeordiService {

	@Autowired
	private TeordiRepository teordiRepository;

	@Override
	@Transactional
	public List<Teordi> findAll() {

		return teordiRepository.findAll();
	}

}
