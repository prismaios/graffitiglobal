package it.prismatdi.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.repository.security.SecUserRepository;

@Service
public class RegisterUserServiceImpl implements RegisterUserService {

	@Autowired
	private SecUserRepository		repository;

	@Autowired
	private BCryptPasswordEncoder	encoder;

	@Override
	public void save(UserGraffitiWeb userGraffiti) throws Exception {
		String s = encoder.encode(userGraffiti.getPassword());
		userGraffiti.setPassword(s);
		repository.save(userGraffiti);
	}

}
