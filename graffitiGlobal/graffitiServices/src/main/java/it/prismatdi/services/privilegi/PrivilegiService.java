package it.prismatdi.services.privilegi;

import java.util.Optional;

import it.prismatdi.model.user.Privilegi;

public interface PrivilegiService {

	public Optional<Privilegi> loadPrivilegioById(Integer id);

	public void removePrivilegio(Privilegi privilegio);

	public void savePrivilegio(Privilegi privilegio);
}
