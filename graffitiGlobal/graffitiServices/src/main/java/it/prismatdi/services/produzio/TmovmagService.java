package it.prismatdi.services.produzio;

import java.util.List;

import it.prismatdi.model.produzio.Tmovmag;

public interface TmovmagService {

	public List<Tmovmag> findAll();

	public List<Tmovmag> findDaAnnoAdAnno(Integer daAnno, Integer adAnno);

}
