package it.prismatdi.services.security;

import it.prismatdi.model.user.UserGraffitiWeb;

public interface RegisterUserService {
	public void save(UserGraffitiWeb userGraffiti) throws Exception;
}
