package it.prismatdi.services.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.user.Utente;
import it.prismatdi.repository.user.UtenteRepository;

@Service
@Transactional("transactionManager")
public class UtenteServiceImpl implements UtenteService {

	@Autowired
	private UtenteRepository utenteRepository;

	public UtenteServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@Transactional
	public List<Utente> findAllPerDitta(String codGraffiti) {
		return utenteRepository.getAllUtenti(codGraffiti);
	}

}
