package it.prismatdi.services.produzio;

import java.util.List;

import it.prismatdi.general.ScelteAgenti;
import it.prismatdi.model.produzio.Unimag;
import it.prismatdi.model.utils.Busta;
import it.prismatdi.model.utils.UnimagData;

public interface UnimagService {

	List<Unimag> findAll();

	List<UnimagData> findCampionario(ScelteAgenti tessSciar, Busta stagione);

}
