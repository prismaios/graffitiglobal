package it.prismatdi.services.scribaf;

import java.util.List;

import it.prismatdi.model.scribaf.Tabel;
import it.prismatdi.model.utils.Busta;

public interface ScribafService {
	List<Busta> findAllClienti(String contoClienti);

	List<Busta> findAllRappresentanti(String contoRappresentanti);

	List<Tabel> getAllCodiciIva();

	List<Busta> getAllStagioni();
}
