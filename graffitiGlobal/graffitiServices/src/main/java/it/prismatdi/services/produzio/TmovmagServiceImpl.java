package it.prismatdi.services.produzio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.produzio.Tmovmag;
import it.prismatdi.produzio.TmovmagRepository;

@Service
@Transactional("produzioTransactionManager")
public class TmovmagServiceImpl implements TmovmagService {

	@Autowired
	private TmovmagRepository tmovmagRepository;

	@Override
	@Transactional
	public List<Tmovmag> findAll() {
		return tmovmagRepository.findAll();
	}

	@Override
	public List<Tmovmag> findDaAnnoAdAnno(Integer daAnno, Integer adAnno) {
		String daAnnoStr = "" + daAnno;
		String adAnnoStr = "" + adAnno;
		return tmovmagRepository.findDaAnnoAdAnno(daAnnoStr, adAnnoStr);
	}

}
