package it.prismatdi.services.produzio;

import java.util.List;

import it.prismatdi.model.produzio.Tmovrig;
import it.prismatdi.model.utils.DataConversion;
import it.prismatdi.model.utils.ParametriAnalisiVendite;
import it.prismatdi.model.utils.ResultContainer;

public interface TmovrigService {

	List<Tmovrig> findAll();

	ResultContainer findConFiltri(ParametriAnalisiVendite parametri);

	List<DataConversion> findMovimentiDiArticolo(ParametriAnalisiVendite parametri, String codArticolo);

	List<DataConversion> findMovimentiDiCliente(ParametriAnalisiVendite parametri, String codCli);

}
