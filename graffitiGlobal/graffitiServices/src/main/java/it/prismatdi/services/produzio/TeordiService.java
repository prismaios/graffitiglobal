package it.prismatdi.services.produzio;

import java.util.List;

import it.prismatdi.model.produzio.Teordi;

public interface TeordiService {
	public List<Teordi> findAll();
}
