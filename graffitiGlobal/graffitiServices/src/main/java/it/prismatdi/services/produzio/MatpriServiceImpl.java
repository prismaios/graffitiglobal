package it.prismatdi.services.produzio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.produzio.Matpri;
import it.prismatdi.produzio.MatpriRepository;

@Service
@Transactional("produzioTransactionManager")
public class MatpriServiceImpl implements MatpriService {

	@Autowired
	private MatpriRepository matpriRepository;

	@Override
	@Transactional
	public List<Matpri> findAll111() {

		return matpriRepository.findAll111();
	}

}
