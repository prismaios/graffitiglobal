package it.prismatdi.services.user;

import java.util.List;

import it.prismatdi.model.user.Utente;

public interface UtenteService {

	List<Utente> findAllPerDitta(String codGraffiti);

}
