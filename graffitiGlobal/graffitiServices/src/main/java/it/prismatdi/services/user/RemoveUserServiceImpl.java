package it.prismatdi.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.user.UserGraffitiWeb;
import it.prismatdi.repository.user.UserRepository;

@Service
@Transactional("transactionManager")
public class RemoveUserServiceImpl implements RemoveUserService {
	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public void removeUser(UserGraffitiWeb user) {
		userRepository.delete(user);

	}

}
