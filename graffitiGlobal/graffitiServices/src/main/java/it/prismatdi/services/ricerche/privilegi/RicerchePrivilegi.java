package it.prismatdi.services.ricerche.privilegi;

import java.util.List;

import it.prismatdi.model.user.Privilegi;

public interface RicerchePrivilegi {
	List<Privilegi> loadAllPrivilegi();

}
