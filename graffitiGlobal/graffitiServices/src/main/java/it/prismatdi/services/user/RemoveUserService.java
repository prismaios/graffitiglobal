package it.prismatdi.services.user;

import it.prismatdi.model.user.UserGraffitiWeb;

public interface RemoveUserService {
	public void removeUser(UserGraffitiWeb user);
}
