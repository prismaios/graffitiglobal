package it.prismatdi.services.produzio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.prismatdi.model.produzio.Tmovrig;
import it.prismatdi.model.utils.DataConversion;
import it.prismatdi.model.utils.ParametriAnalisiVendite;
import it.prismatdi.model.utils.ResultContainer;
import it.prismatdi.model.utils.SumDataConversion;
import it.prismatdi.produzio.TmovrigRepository;

@Service
@Transactional("produzioTransactionManager")
public class TmovrigServiceImpl implements TmovrigService {

	@Autowired
	private TmovrigRepository tmovrigRepository;

	@Override
	@Transactional
	public List<Tmovrig> findAll() {
		return tmovrigRepository.findAll();
	}

	@Override
	@Transactional
	public ResultContainer findConFiltri(ParametriAnalisiVendite parametri) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime ora = LocalDateTime.now();
		//		System.err.println(">>>>> TmovrigServiceImpl-findConFiltri <<<<<< " + ora.format(formatter));
		final ResultContainer result = tmovrigRepository.findConParametri(parametri);
		if (parametri.getAnnoPrecedente()) {
			Map<String, SumDataConversion> hashSum = result.getDati()
					.stream()
					.collect(Collectors.toMap(SumDataConversion::getCodice, item -> item));
			LocalDate daOrig = parametri.getDallaData();
			LocalDate aOrig = parametri.getAllaData();
			final LocalDate daAp = parametri.getDallaData()
					.minusYears(1);
			final LocalDate aAp = parametri.getAllaData()
					.minusYears(1);
			parametri.setDallaData(daAp);
			parametri.setAllaData(aAp);
			final ResultContainer resultAp = tmovrigRepository.findConParametri(parametri);
			result.setDatiAp(resultAp.getDatiAp());
			result.setTotQtaAp(resultAp.getTotQta());
			result.setTotValoreAp(resultAp.getTotValore());
			for (SumDataConversion elem : resultAp.getDati()) {
				if (hashSum.containsKey(elem.getCodice())) {
					SumDataConversion data = hashSum.get(elem.getCodice());
					data.setQtaAp(elem.getQta());// la sposto in aP
					data.setValoreAp(elem.getValore());// idem x valore
				}
				else {
					elem.setQtaAp(elem.getQta());
					elem.setQta(0D);
					elem.setValoreAp(elem.getValore());
					elem.setValore(0D);
					hashSum.put(elem.getCodice(), elem);
				}
			}
			List<SumDataConversion> resNuovo = new ArrayList<>(hashSum.values());
			resNuovo.sort(Comparator.comparingDouble(SumDataConversion::getValore)
					.reversed());
			result.setDati(resNuovo);
			parametri.setDallaData(daOrig);
			parametri.setAllaData(aOrig);
		}

		//		System.err.println(">>>>> TmovrigServiceImpl-findConFiltri <<<<<< " + ora.format(formatter));
		return result;
	}

	@Override
	public List<DataConversion> findMovimentiDiArticolo(ParametriAnalisiVendite parametri, String codArticolo) {
		List<DataConversion> righeArticolo = tmovrigRepository.findMovimentiDiArticoloConTuple(parametri, codArticolo);
		//		List<DataConversion> righeArticolo = tmovrigRepository.findMovimentiDiArticolo(parametri, codArticolo);
		return righeArticolo;
	}

	@Override
	public List<DataConversion> findMovimentiDiCliente(ParametriAnalisiVendite parametri, String codCli) {
		List<DataConversion> righeCliente = tmovrigRepository.findMovimentiDiClienteConTuple(parametri, codCli);
		//		List<DataConversion> righeCliente = tmovrigRepository.findMovimentiDiCliente(parametri, codCli);
		return righeCliente;
	}

}
